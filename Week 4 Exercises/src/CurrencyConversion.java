import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

//This program uses Object called Currency. Be warned.
//It basically creates a currency Object, and manipulates that object around.
public class CurrencyConversion {
	//public static ArrayList<Currency> currencies = new ArrayList<Currency>();
	
	/*public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the currency and the unit (With no spaces): ");
		String input = scanner.nextLine();
		String[] result = convert(input);
		System.out.println("Here are the converted currencies: ");
		for (int i = 0; i<result.length; i++) {
			System.out.println(result[i]);
		}
		
	
	}*/
	
	public static ArrayList<Currency> createCurrency() {
		ArrayList<Currency> currencies = new ArrayList<Currency>();
		Currency currency = new Currency("�", 1f, 1f);
		currencies.add(currency);
		currency = new Currency("$", 0.6189f, 1.6159f);
		currencies.add(currency);
		currency = new Currency("eur", 0.7988f, 1.2516f);
		currencies.add(currency);
		currency = new Currency("yen", 0.0079f, 126.0440f);
		currencies.add(currency);
		return currencies;
	}
	
	public static void convert(String currency, float value) throws IOException {
		//String[] data = input.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		Money currencyFrom = new Money(currency, value);
		File file = new File("Converted.txt");
		if(file.exists()) {
			System.out.println("File found! This will overwrite the file!");
		}
		BufferedWriter output = new BufferedWriter(new FileWriter(file));
		String[] currencies = currencyFrom.getConversions();
		for (int i = 0; i < currencies.length; i++) {
			output.write(currencies[i]);
			output.newLine();
		}
		output.close();
	}
	
	
}
