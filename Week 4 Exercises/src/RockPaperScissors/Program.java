package RockPaperScissors;

import java.awt.EventQueue;

public class Program {
	private static MainFrame frame = new MainFrame();
	
	public enum Hands {
		ROCK,
		PAPER,
		SCISSORS;
	}
	
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new MainFrame();
					frame.setVisible(true);
					Helper.setFrame(frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
