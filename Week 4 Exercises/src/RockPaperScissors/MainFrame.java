package RockPaperScissors;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import RockPaperScissors.Hand.Hands;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	public JTextField txtPlayer1Pick;
	public JTextField txtPlayer2Pick;
	public JLabel lblWinner;
	public JLabel lblRound;
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel panel_5 = new JPanel();
		contentPane.add(panel_5);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));
		
		JPanel panel_4 = new JPanel();
		panel_5.add(panel_4);
		
		lblWinner = new JLabel("Winner:");
		panel_4.add(lblWinner);
		lblWinner.setFont(new Font("Dotum", Font.PLAIN, 41));
		
		JPanel panel_2 = new JPanel();
		panel_5.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		lblRound = new JLabel("Round:");
		lblRound.setFont(new Font("Dotum", Font.PLAIN, 23));
		panel_2.add(lblRound);
		
		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3);
		
		JPanel panel = new JPanel();
		panel_3.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		txtPlayer1Pick = new JTextField();
		panel.add(txtPlayer1Pick);
		txtPlayer1Pick.setText("Player1 Pick");
		txtPlayer1Pick.setColumns(10);
		
		JButton btnRock = new JButton("Rock");
		btnRock.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				Helper.createHand(1, Hands.ROCK);
			}
		});
		panel.add(btnRock);
		
		JButton btnPaper = new JButton("Paper");
		btnPaper.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Helper.createHand(1, Hands.PAPER);
			}
		});
		panel.add(btnPaper);
		
		JButton btnScissors = new JButton("Scissors");
		btnScissors.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Helper.createHand(1, Hands.SCISSORS);
			}
		});
		panel.add(btnScissors);
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		txtPlayer2Pick = new JTextField();
		panel_1.add(txtPlayer2Pick);
		txtPlayer2Pick.setText("Player2 Pick");
		txtPlayer2Pick.setColumns(10);
		
		JButton btnRock_1 = new JButton("Rock");
		btnRock_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Helper.createHand(2, Hands.ROCK);
			}
		});
		panel_1.add(btnRock_1);
		
		JButton btnPaper_1 = new JButton("Paper");
		btnPaper_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Helper.createHand(2, Hands.PAPER);
			}
		});
		panel_1.add(btnPaper_1);
		
		JButton btnScissors_1 = new JButton("Scissors");
		btnScissors_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Helper.createHand(2, Hands.SCISSORS);
			}
		});
		panel_1.add(btnScissors_1);
	}
}
