package RockPaperScissors;

public class Hand {
	public enum Hands {
		ROCK,
		PAPER,
		SCISSORS;
	}
	
	private final Hands hand;
	
	public Hand(Hands type) {
		this.hand = type;
	}
	
	public Hands getHands() {
		return hand;
	}
}
