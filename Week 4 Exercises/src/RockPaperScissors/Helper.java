package RockPaperScissors;

import javax.swing.JOptionPane;

import RockPaperScissors.Hand.Hands;

public class Helper {
	private static Hand p1hand;
	private static Hand p2hand;
	private static int rounds;
	private static int turns;
	private static MainFrame frame;
	private static int score = 0;
	
	public static void setFrame(MainFrame Frame) {
		frame = Frame;
	}
	
	public static Hand checkWinner(Hand player1, Hand player2) {
		if (player1.getHands() == Hands.ROCK && player2.getHands() == Hands.SCISSORS) {
			return player1;
		}
		if (player1.getHands() == Hands.ROCK && player2.getHands() == Hands.PAPER) {
			return player2;
		}
		if (player1.getHands() == Hands.SCISSORS && player2.getHands() == Hands.PAPER) {
			return player1;
		}
		if (player1.getHands() == Hands.SCISSORS && player2.getHands() == Hands.ROCK) {
			return player2;
		}
		if (player1.getHands() == Hands.PAPER && player2.getHands() == Hands.SCISSORS) {
			return player2;
		}
		if (player1.getHands() == Hands.PAPER && player2.getHands() == Hands.ROCK) {
			return player1;
		}
		return null;
	}
	
	public static void createHand(int player, Hands type) {
		if (player == 1) {
			p1hand = new Hand(type);
			frame.txtPlayer1Pick.setText(type.toString());
		} else {
			p2hand = new Hand(type);
			frame.txtPlayer2Pick.setText(type.toString());
		}
		turns++;
		System.out.println(turns);
		checkTurn();
	}
	
	public static void checkTurn() {
		if(turns > 1) {
			Hand winner = checkWinner(p1hand, p2hand);
			if (winner == p1hand) {
				frame.lblWinner.setText("Winner: Player 1!");
				score++;
			} else if (winner == p2hand) {
				frame.lblWinner.setText("Winner: Player 2!");
				score--;
			} else {
				frame.lblWinner.setText("Draw!");
			}
			rounds++;
			frame.lblRound.setText("Round: " + rounds);
			turns = 0;
			checkRounds();
		}
	}
	
	public static void checkRounds() {
		if (rounds == 10) {
			if (score > 0) {
				JOptionPane.showConfirmDialog(frame, "Player 1 wins!");
			}
			if (score < 0) {
				JOptionPane.showConfirmDialog(frame, "Player 2 wins!");
			}
			if (score == 0) {
				JOptionPane.showConfirmDialog(frame, "Draw!");
			}
			rounds = 0;
			frame.lblRound.setText("Round: " + rounds);
		}
	}
}
