
public class Money {
	public String unit;
	public float value;
	
	public Money (String unit, float value) {
		this.unit = unit;
		this.value = value;
		System.out.println(this.unit + this.value);
	}
	
	public String[] getConversions() {
		String[] moneys = new String[4];
		float baseValue;
		float GBconversionFactor = 0f;
		switch (unit) {
		case "�":
			GBconversionFactor = 1f;
			break;
		case "$":
			GBconversionFactor = 0.6189f;
			break;
		case "eur":
			GBconversionFactor = 0.7988f;
			break;
		case "yen":
			GBconversionFactor = 0.0079f;
			break;
		}
		
		baseValue = value*GBconversionFactor;
		moneys[0] = "�" + baseValue;
		moneys[1] = baseValue*1.6159f + "$";
		moneys[2] = baseValue*1.2516f + "�";
		moneys[3] = baseValue*126.0440f + "�";
		
		return moneys;
	}
}
