
public class Currency {
	private float GBratio;
	private float ratio;
	private String type;
	
	public Currency(String type, float GBratio, float ratio) {
		this.setType(type);
		this.setGBratio(GBratio);
		this.setRatio(ratio);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setGBratio(float GBratio) {
		this.GBratio = GBratio;
	}
	
	public void setRatio(float ratio) {
		this.ratio = ratio;
	}
	
	@Override
	public String toString() {
		return type;
	}
	
}
