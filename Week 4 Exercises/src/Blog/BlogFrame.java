package Blog;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class BlogFrame extends JFrame {

	private JPanel contentPane;
	public JTextField txtBlogText;

	/**
	 * Create the frame.
	 */
	public BlogFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		txtBlogText = new JTextField();
		txtBlogText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					Blog.addEntry(txtBlogText.getText());
			    }
			}
		});
		txtBlogText.setMaximumSize(new Dimension(200,30));
		txtBlogText.setPreferredSize(new Dimension(200,30));
		Component hglue = Box.createHorizontalGlue(); 
		getContentPane().add(hglue);
		txtBlogText.setText("Type something into Blog");
		contentPane.add(txtBlogText);
		txtBlogText.setColumns(100);
		Component hglue2 = Box.createHorizontalGlue();
		getContentPane().add(hglue2);
	}

}
