package Blog;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Blog {
	private static BlogFrame frame = new BlogFrame();
	
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new BlogFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		try {
			fileManager();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void addEntry(String text) {
		System.out.println("ENTER key pressed");
		try {
			writeBlogPost(frame.txtBlogText.getText());
			frame.txtBlogText.setText("");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void fileManager() throws IOException {
		File file = new File("Blog.html");
		if (!file.exists()) {
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write("<html>");
			output.newLine();
			output.write("<head>");
			output.write("<h1>My Blog</h1>");
			output.write("</head>");
			output.newLine();
			output.write("<body>");
			output.newLine();
			output.write("</body>");
			output.newLine();
			output.write("</html>");
			output.close();
		}
	}
	
	public static void writeBlogPost(String text) throws IOException {
		ArrayList<String> fileContent = new ArrayList<String>();
		File file = new File("Blog.html");
		/*String stringSearch = "<body>";  										// Open the file c:\test.txt as a buffered reader
		BufferedReader bf = new BufferedReader(new FileReader("Blog.html")); 	// Start a line count and declare a string to hold our current line.
		int linecount = 0;
		String line;															// Let the user know what we are searching for
		System.out.println("Searching for " + stringSearch + " in file...");	// Loop through each line, stashing the line into our line variable.
		while (( line = bf.readLine()) != null){								// Increment the count and find the index of the word
			linecount++;
			int indexfound = line.indexOf(stringSearch);						// If greater than -1, means we found the word
			if (indexfound > -1) {
				System.out.println("Word was found at position " + indexfound + " on line " + linecount);
			}
		}	
        bf.close();														// Close the file after done searching*/
		fileContent = saveFile(file);
        BufferedWriter output = new BufferedWriter(new FileWriter(file));
        //writeHTMLhead(output);
        for (int i = 0; i < fileContent.size(); i++) {
        	output.write(fileContent.get(i).toString());
        	if (fileContent.get(i).contains("<body>")) {
        		//System.out.println("Word was found at position " + " on line " + i);
        		output.newLine();
        		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        		Date date = new Date();
        		output.write(text + " - " + dateFormat.format(date) + "<br> \n");
        	}
        	output.newLine();
        }
    	output.close();
	}
	
	public static ArrayList<String> saveFile(File file) throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader(file));
		ArrayList<String> fileStuff = new ArrayList<String>();
		String line;
		while ((line = bf.readLine()) != null) {
			fileStuff.add(line);
		}	
        bf.close();
        
        /*ArrayList<String> list = new ArrayList<String>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add(1, "G");
        System.out.println(list);*/
        
        return fileStuff;
	}
}
