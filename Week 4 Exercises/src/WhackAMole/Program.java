package WhackAMole;

import java.awt.EventQueue;
import java.util.ArrayList;

public class Program {
	public static MainFrame frame;
	public static int gridSize = 4;
	public static int score = 0;
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new MainFrame();
					frame.setVisible(true);
					ArrayList<Mole> moles = createGrid();
					GameListener gl = new GameListener(moles);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void addScore() {
		score += 1;
		frame.setTitle("Score: " + score);
	}
	
	private static ArrayList<Mole> createGrid() {
		ArrayList<Mole> moles = new ArrayList<Mole>();
		float width = frame.getWidth();
		float height = frame.getHeight()-30;
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				Mole mole = new Mole();
				float x = (float)(width*((float)i/gridSize)+20);
				float y = (float)(height*((float)j/gridSize));
				mole.setLocation((int)x, (int)y);
				frame.add(mole);
				moles.add(mole);
			}
		}
		return moles;
	}
}
