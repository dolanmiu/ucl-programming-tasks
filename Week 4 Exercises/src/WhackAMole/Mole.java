package WhackAMole;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Mole extends JLabel implements MouseListener {
	private boolean isUp;
	
	public Mole() {
		this.setText("");
		setBounds(0, 0, 60, 60);
		setMoleDown();
		addMouseListener(this);
	}
	
	public void setMoleUp() {
		setIcon(new ImageIcon(((new ImageIcon(MainFrame.class.getResource("/assets/mole_1.png"))).getImage()).getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH)));
		setUp(true);
	}
	
	public void setMoleDown() {
		setIcon(new ImageIcon(((new ImageIcon(MainFrame.class.getResource("/assets/hole.png"))).getImage()).getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH)));
		setUp(false);
	}

	public boolean isUp() {
		return isUp;
	}
	
	public void switchState() {
		if (isUp() == true) {
			setMoleDown();
		} else {
			setMoleUp();
		}
	}

	private void setUp(boolean isUp) {
		this.isUp = isUp;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {	
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (isUp()) {
			Program.addScore();
			setMoleDown();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}
