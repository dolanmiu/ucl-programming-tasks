package WhackAMole;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Timer;

public class GameListener implements ActionListener {
	private Timer timer;
	private int speed = 1000;
	private ArrayList<Mole> moles;
	
	public GameListener(ArrayList<Mole> moles) {
		this.moles = moles;
		timer = new Timer(speed, this);
		timer.start();
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public void changeMoles() {
		for (int i = 0; i < moles.size(); i++) {
			if (Math.random() > 0.5) {
				moles.get(i).switchState();
			}
		}

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		changeMoles();
		speed = speed - speed/50;
		timer.setDelay(speed);
	}
}
