//Made by Dolan Miu
package com.gc01.AnDraw;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;



/**
 * DrawView.java 
 * @author Dean Mohamedally 12/10/2011
 * A first drawing view example
 */
public class DrawView extends View implements OnTouchListener
{

	private Paint backgroundPaint = new Paint();
	private Paint drawPaint = new Paint();
	private Paint circlePaint = new Paint();
	private Paint textPaint = new Paint();
	
	private Paint houseBrickPaint = new Paint();		//This creates the Paint for the various house parts
	private Paint houseRoofPaint = new Paint();
	private Paint grassPaint = new Paint();
	private Paint houseDoorPaint = new Paint();
	private Paint sunPaint = new Paint();


	private float sx, sy;

	public DrawView(Context context)
	{
		super(context);


		setFocusable(true);
		setFocusableInTouchMode(true);
		//This sets all the colours and properties of the Paint to be used on the house, colour, anti-alias etc.
		backgroundPaint.setColor(Color.CYAN);
		backgroundPaint.setAntiAlias(true);
		backgroundPaint.setStyle(Style.FILL);

		drawPaint.setColor(Color.BLUE);
		drawPaint.setStyle(Style.FILL);

		circlePaint.setColor(Color.RED);
		circlePaint.setStyle(Style.FILL);

		textPaint.setColor(Color.GREEN);
		textPaint.setStyle(Style.FILL);
		textPaint.setAntiAlias(true);
		
		houseBrickPaint.setColor(Color.rgb(255, 224, 138));
		houseBrickPaint.setStyle(Style.FILL_AND_STROKE);
		
		houseRoofPaint.setColor(Color.rgb(105,105,105));
		houseRoofPaint.setStyle(Style.FILL);
		houseRoofPaint.setAntiAlias(true);
		
		grassPaint.setColor(Color.GREEN);
		grassPaint.setStyle(Style.FILL);
		
		sunPaint.setColor(Color.YELLOW);
		sunPaint.setAntiAlias(true);
		sunPaint.setStyle(Style.FILL);
		
		houseDoorPaint.setColor(Color.RED);
		houseDoorPaint.setStyle(Style.FILL);

		this.setOnTouchListener(this);
	}

	@Override
	public void onDraw(Canvas canvas)
	{
		// Draw blue background - the sky
		canvas.drawRect(this.getLeft(), this.getTop(), this.getRight(), this.getBottom(), backgroundPaint);
		
		//Draw the house 
		canvas.drawRect(93,263,206,338, houseBrickPaint);
		canvas.drawRect(0,338, 320, 480, grassPaint);
		canvas.drawRect(110, 296, 134, 338, houseDoorPaint);
		canvas.drawCircle(268, 49, 40, sunPaint);
		
		//drawing a roof. Roof is a bit different as there is not a canvas.drawTriangle method.
		final int[] verticesColors = {
				//Setting of the roof colours. I made a gradient to simulate light hitting the roof.
		    houseRoofPaint.getColor(), Color.LTGRAY, houseRoofPaint.getColor(), 0xFF000000, 0xFF000000, 0xFF000000
		};
		//The verticies for the triangle in a Point object.
		Point point1 = new Point(), point2 = new Point(), point3 = new Point();
		point1.x = 72; point1.y = 266;
		point2.x = 147; point2.y = 225;
		point3.x = 221; point3.y = 266;
		float[] verts = {
			    point1.x, point1.y,
			    point2.x, point2.y,
			    point3.x, point3.y
			};
		//Drawing the triangle.
		canvas.drawVertices(Canvas.VertexMode.TRIANGLES, verts.length, verts, 0, null,0,verticesColors,0,null, 0,0, houseRoofPaint);

		//draws my name
		canvas.drawText("Dolan Miu!", 110, 160, textPaint);

		//draw a circle with red paint with the touch coordinates
		canvas.drawCircle(sx-30,sy-30, 30, circlePaint);

	}

	public boolean onTouch(View v, MotionEvent event)
	{   
		//update the coordinates for the OnDraw method above, with wherever we touch
		sx = event.getX();
		sy = event.getY();
		//This if statement will switch between touching, and releasing. Releasing will set to Day time. Touching will set to Night time.
		if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
			setNightTime();
			Log.d("TouchTest", "Touch down");
		} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
			setDayTime();
			Log.d("TouchTest", "Touch up");
		}
		invalidate();
		return true;
	}
	
	private void setDayTime() {
		//The colour scheme for the day time
		backgroundPaint.setColor(Color.CYAN);
		houseBrickPaint.setColor(Color.rgb(255, 224, 138));			
		houseRoofPaint.setColor(Color.rgb(105,105,105));		
		grassPaint.setColor(Color.GREEN);			
		sunPaint.setColor(Color.YELLOW);	
		houseDoorPaint.setColor(Color.RED);
		textPaint.setColor(Color.BLACK);
	}
	
	private void setNightTime() {
		//The colour scheme for night time.
		backgroundPaint.setColor(Color.rgb(0,52,97));
		houseBrickPaint.setColor(Color.rgb(184, 166, 118));
		houseRoofPaint.setColor(Color.rgb(24, 24, 24));
		grassPaint.setColor(Color.rgb(52,102,56));
		houseDoorPaint.setColor(Color.rgb(110,57,57));
		sunPaint.setColor(Color.WHITE);
		textPaint.setColor(Color.WHITE);
	}

}


