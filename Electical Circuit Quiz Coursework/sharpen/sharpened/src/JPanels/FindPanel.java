package JPanels;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Core.Question;
import Core.Reader;
import JPanels.Misc.HintTextArea;


/**
 * The Find Panel Class
 * Allows for the search of questions. Used in Circuit Maker Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class FindPanel extends JPanel {
	
	/** The questions. */
	private ArrayList<Question> questions = Reader.questionList;
	
	/** The text field. */
	private HintTextArea textField;
	
	/** The question list model. */
	private DefaultListModel<Question> questionInfo = new DefaultListModel<Question>();
	
	/** The question list. */
	private JList<Question> questionList = new JList<Question>(questionInfo);
	
	/** The selected. */
	private Question selected;
	/**
	 * Create the panel.
	 */
	public FindPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		panel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 50));
		add(panel);
		
		textField = new HintTextArea("Search...");
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				search(textField.getText());
			}
		});
		panel.add(textField);
		textField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		JScrollPane scrollPane = new JScrollPane(questionList);
		questionList.setVisibleRowCount(12);
		panel_1.add(scrollPane);
	}
	
	/**
	 * Gets the question list.
	 *
	 * @return the question list
	 */
	public JList<Question> getQuestionList() {
		return questionList;
	}
	
	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public Question getSelected() {
		return selected;
	}

	/**
	 * Search.
	 *
	 * @param rawQuery the raw search query
	 */
	public void search(String rawQuery) {
		questionInfo.clear();
		String query = rawQuery.toLowerCase();
		//String[] multiQ = query.split("\\s+");

		for (int i = 0; i < questions.size(); i++) {
			//if (multiQuery(multiQ, questions.get(i))) {
			String text = questions.get(i).getText().toLowerCase();
			String diff = questions.get(i).diff.toLowerCase();
			if (text.contains(query) || diff.contains(query)) {
				questionInfo.addElement(questions.get(i));
			}
		}
		if (query.isEmpty()) {
			questionInfo.clear();
		}
	}
	
	/**
	 * Multi-query.
	 *
	 * @param multiQ the multi-query
	 * @param question the question
	 * @return true, if successful
	 */
	public boolean multiQuery(String[] multiQ, Question question) {
		for (int i = 0; i < multiQ.length; i++) {
			String diff = question.diff;
			String text = question.getText();
			if (!text.contains(multiQ[i]) || !diff.contains(multiQ[i])) {
				return false;
			}
		}
		return true;
	}
}
