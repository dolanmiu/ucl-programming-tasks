package JPanels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Core.Marker;
import Core.Question;
import Core.Reader;
import JPanels.JMenus.StudentQuizMenu;
import JPanels.Misc.CardPanel;
import JPanels.Misc.HintTextArea;
import JPanels.Misc.PaintPanel;

import com.smardec.mousegestures.MouseGestures;
import com.smardec.mousegestures.MouseGesturesListener;

/**
 * The Student Quiz Class
 * Quizzes the student
 *
 * @author DolanMiu
 * @version 1.0
 */
public class StudentQuiz extends JPanel {
	
	/** The questions. */
	protected ArrayList<Question> questions;
	
	/** The circuit panel. */
	protected CircuitDraw circuitPanel;
	
	/** The question label. */
	protected JLabel questionLabel;
	
	/** The question text. */
	protected JLabel questionText;
	
	/** The card panel. */
	protected CardPanel cardPanel;
	
	/** The gesture label. */
	protected JLabel gestureLabel;
	
	/** The question number panel. */
	protected JPanel questionNumberPanel;
	
	/** The question text panel. */
	protected JPanel questionTextPanel;
	
	/** The timer panel. */
	protected JPanel timerPanel;
	
	/** The painter. */
	protected PaintPanel painter;
	
	/** The dock panel. */
	protected JPanel dockPanel;
	
	/** The show hide painter button. */
	protected JButton btnShowHidePainter;
	
	/** The show note panel. */
	protected JPanel showNotePanel;
	
	/** The answer field. */
	protected HintTextArea answerField;
	
	/** The global gesture. */
	protected String globalGesture = "";
	
	/** The points label. */
	protected JLabel lblPoints;
	
	/** The panel. */
	protected JPanel panel;
	
	/** The panel_1. */
	private JPanel panel_1;
	
	/**
	 * Instantiates a new student quiz.
	 *
	 * @param cardPanel the card panel
	 * @param difficulty the difficulty
	 * @param shuffle the shuffle
	 */
	public StudentQuiz(CardPanel cardPanel, String difficulty, boolean shuffle) {
		Marker.resetScore();
		mouseGestures();
		
		panel = new JPanel();
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		panel.setPreferredSize(new Dimension(1000, 40));
		StudentQuizMenu menu = new StudentQuizMenu(cardPanel);
		panel.add(menu, BorderLayout.NORTH);
		
		panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		panel.setMaximumSize(new Dimension(Integer.MAX_VALUE,30));
		this.cardPanel = cardPanel;
		questions = getQuestions(difficulty);
		if (shuffle) {
			Collections.shuffle(questions);
		}
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		circuitPanel = new CircuitDraw(questions);
		System.out.println("(StudentQuiz) Total Resistance: " + circuitPanel.getTotalResistance());
		
		JPanel topPanel = new JPanel();
		topPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 100));
		add(topPanel);
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		
		questionNumberPanel = new JPanel();
		questionNumberPanel.setBorder(new EmptyBorder(5, 6, 5, 19));
		topPanel.add(questionNumberPanel);
		questionNumberPanel.setLayout(new BoxLayout(questionNumberPanel, BoxLayout.Y_AXIS));
		gestureLabel = new JLabel("Waiting for gesture");
		questionNumberPanel.add(gestureLabel);
		questionLabel = new JLabel("Question: " + (circuitPanel.getQuestionNumber()+1));
		questionNumberPanel.add(questionLabel);
		
		JButton btnNextQuestion = new JButton("Next Question!");
		btnNextQuestion.setIcon(new ImageIcon(StudentQuiz.class.getResource("/resources/icons/Arrows/right.png")));
		topPanel.add(btnNextQuestion);
		btnNextQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				nextQuestion(questionLabel);
			}
		});
		Component horizontalGlue = Box.createHorizontalGlue();
		topPanel.add(horizontalGlue);
		
		lblPoints = new JLabel("0 Points!");
		topPanel.add(lblPoints);
		timerPanel = new JPanel();
		timerPanel.setMaximumSize(new Dimension(100, 100));
		topPanel.add(timerPanel);
		
		questionTextPanel = new JPanel();
		questionTextPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		add(questionTextPanel);
		
		questionText = new JLabel();
		questionTextPanel.add(questionText);
		questionText.setText(questions.get(circuitPanel.getQuestionNumber()).getText());

		add(circuitPanel);
		
		dockPanel = new JPanel();
		add(dockPanel);
		dockPanel.setLayout(new BoxLayout(dockPanel, BoxLayout.Y_AXIS));
		dockPanel.setMaximumSize(new Dimension(1000,600));
		
		showNotePanel = new JPanel();
		dockPanel.add(showNotePanel);
		
		btnShowHidePainter = new JButton("Show/Hide Note Taker");
		btnShowHidePainter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (painter.isVisible()) {
					painter.setVisible(false);
				} else {
					painter.setVisible(true);
				}
				
				
			}
		});
		showNotePanel.add(btnShowHidePainter);
		answerField = new HintTextArea("Type Answer Here");
		showNotePanel.add(answerField);
		setAnswerText();
		
		painter = new PaintPanel();
		painter.setPreferredSize(new Dimension(2000,600));
		painter.setVisible(false);
		dockPanel.add(painter);
	}
	
	/**
	 * Next question.
	 *
	 * @param questionLabel the question label
	 */
	private void nextQuestion(JLabel questionLabel) {
		boolean questionWrong = false;
		float scoretoAdd = 0;
		Question currentQ = circuitPanel.getCurrentQuestion();
		if (currentQ.getType().equals("word")) {
			if (!answerField.getText().equals(currentQ.getAnswers().get(0).getTextAnswer())) {
				questionWrong = true;
			}
		} else if (currentQ.getType().equals("gesture")) {
			if (currentQ.getType().equals("gesture")) {				//Pressing 'next question' is like a skip for this question
				if(!globalGesture.equals(currentQ.getAnswers().get(0).getTextAnswer())) {
					questionWrong = true;
				}
			}
		} else {
			if (!circuitPanel.isCorrect() || circuitPanel.isCollided == false) {
				questionWrong = true;
			}
		}
		
		if (questionWrong) {
			JOptionPane.showMessageDialog(null,"Wrong Answer!");
		} else {
			scoretoAdd = calculateScore();
			Marker.addScore((int) (scoretoAdd));
			lblPoints.setText(Marker.getScore() + " Points!");
		}
		long timeTaken = getTimeTaken();
		Marker.addQuestionStat(circuitPanel.getQuestionNumber()+1, questionWrong, timeTaken, scoretoAdd);
		if (circuitPanel.isDrawable()) {
			questionText.setText(questions.get(circuitPanel.getQuestionNumber()).getText());
			circuitPanel.drawNextCircuit();
			setAnswerText();
			questionLabel.setText("Question: " + (circuitPanel.getQuestionNumber()+1));
			setTimeCheckPoint();
		} else {
			JOptionPane.showMessageDialog(null,"Finish! Here is your result.");
			QuizResult finish = new QuizResult();
			cardPanel.add(finish, "quizResult");
			cardPanel.c1.show(cardPanel, "quizResult");
		}
	}
	
	/**
	 * Sets the answer text.
	 */
	private void setAnswerText() {
		if (circuitPanel.getCurrentQuestion().getType().equals("word")) {
			answerField.setVisible(true);
		} else {
			answerField.setVisible(false);
		}
	}
	
	/**
	 * Calculate score.
	 *
	 * @return the score to be added
	 */
	public float calculateScore() {
		float scoretoAdd = (100/questions.size());
		return scoretoAdd;
	}
	
	/**
	 * Gets the time taken.
	 *
	 * @return the time taken to finish the question
	 */
	public long getTimeTaken() {
		long timeTaken = 0;
		return timeTaken;
	}
	
	/**
	 * Sets the time check point.
	 */
	public void setTimeCheckPoint() {}
	
	/**
	 * Gets the questions.
	 *
	 * @param difficulty the difficulty
	 * @return the questions
	 */
	private ArrayList<Question> getQuestions(String difficulty) {
		return Reader.getQuestions(difficulty);
	}
	
	/**
	 * Mouse gestures.
	 */
	private void mouseGestures() {
		MouseGestures mg = new MouseGestures();
		mg.setMouseButton(MouseEvent.BUTTON3_MASK);
		mg.addMouseGesturesListener(new MouseGesturesListener() { 
			public void gestureMovementRecognized(String currentGesture) {
				globalGesture = currentGesture;
				if("DR".equals(currentGesture)) {
					nextQuestion(questionLabel);
				}
				gestureLabel.setText(currentGesture);
				gestureQuestion(currentGesture);
				/*if("DRU".equals(currentGesture)){
					gestureLabel.setText("    "  + currentGesture + " - Wow, U have drawn 'U'");
				}
				else if("DRU".startsWith(currentGesture)){
					gestureLabel.setText("    "  + currentGesture + " - You need to make a DRU");
				} 
				else{
					gestureLabel.setText("    "  + currentGesture + " - Wrong gesture! release your mouse and try again");
				} */
			} 
			//This method is called when the user releases the mouse button finally
			//Just display the current message for a few milliseconds then
			//redisplay the original text
			public void processGesture(String gesture) {
				try { 
					Thread.sleep(10000);
				} catch (InterruptedException e) {}
				gestureLabel.setText("Waiting for gesture");
			} 
		}); 
		mg.start();
	}
	
	/**
	 * For the Gesture type question.
	 *
	 * @param currentGesture the current gesture
	 */
	private void gestureQuestion(String currentGesture) {
		Question currentQ = circuitPanel.getCurrentQuestion();
		if (currentQ.getType().equals("gesture")) {
			if(currentGesture.equals(currentQ.getAnswers().get(0).getTextAnswer())) {
				JOptionPane.showMessageDialog(null,"You did it!");
				nextQuestion(questionLabel);
			}
		}
	}
}
