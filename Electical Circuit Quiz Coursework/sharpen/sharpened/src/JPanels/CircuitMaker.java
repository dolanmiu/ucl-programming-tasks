package JPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import Core.Circuit;
import Core.Question;
import Core.Reader;
import Core.Writer;
import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Module;
import JPanels.JDialogs.CreateComponent;
import JPanels.JDialogs.CreateQuestion;
import JPanels.JMenus.CircuitMakerMenu;
import JPanels.Misc.HintTextArea;
import JPanels.Misc.XMLFileFilter;

/**
 * The Circuit Maker Class
 * The control panel to create circuits.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CircuitMaker extends JPanel {
	
	/** The modules. */
	private ArrayList<Module> modules = getModulesFromQuestion(1);
	
	/** The question combo box. */
	private JComboBox<Question> questionComboBox = new JComboBox<Question>();
	
	/** The component info. */
	private DefaultListModel<ElectricalComponent> compInfo = new DefaultListModel<ElectricalComponent>();
	
	/** The answer info. */
	private DefaultListModel<ElectricalComponent> answerInfo = new DefaultListModel<ElectricalComponent>();
	
	/** The circuit draw panel (sand box). */
	private CircuitDraw circuitSandbox = new CircuitDraw(Reader.questionList);
	
	/** The module combo box. */
	private JComboBox<Module> moduleComboBox = new JComboBox<Module>();
	
	/** The question text text-box. */
	private JTextArea textText = new JTextArea();
	
	/** The current question id. */
	private int currentQuestionID;
	
	/** The component list. */
	private JList<ElectricalComponent> componentList = new JList<ElectricalComponent>(compInfo);
	
	/** The answer list. */
	private JList<ElectricalComponent> answerList = new JList<ElectricalComponent>(answerInfo);
	
	/** The file chooser. */
	private JFileChooser fc = new JFileChooser(".");
	
	/** The current directory. */
	private String currentDir = "electricaloutput.xml";
	
	/** The text answer. */
	private HintTextArea txtTextAnswer = new HintTextArea("Text Answer Student Types in...");

	/**
	 * Instantiates a new circuit maker.
	 */
	public CircuitMaker() {
		fc.setFileFilter(new XMLFileFilter());
		textText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				Reader.questionList.get(currentQuestionID).setText(textText.getText());
			}
		});

		questionComboBox.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				circuitSandbox.drawCircuit(0);
			}
		});
		setLayout(new BorderLayout(0, 0));
		
		JPanel toolbars = new JPanel();
		add(toolbars, BorderLayout.NORTH);

		add(circuitSandbox, BorderLayout.CENTER);
		circuitSandbox.setLayout(null);

		JPanel side = new JPanel();
		side.setPreferredSize(new Dimension(300,Integer.MAX_VALUE));
		JPanel topSide = new JPanel();
		JPanel sideQuestionText = new JPanel();
		JPanel sideQuestion = new JPanel();
		JPanel sideContainer = new JPanel();
		JPanel sideComponents = new JPanel();
		
		add(side, BorderLayout.WEST);
		side.setLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
		side.setMinimumSize(new Dimension(200,600));
		
		side.add(topSide);
		topSide.setLayout(new BoxLayout(topSide, BoxLayout.Y_AXIS));
		
		topSide.add(sideQuestionText);
		sideQuestionText.setLayout(new BoxLayout(sideQuestionText, BoxLayout.X_AXIS));
		
		JPanel panel_4 = new JPanel();
		sideQuestion.add(panel_4);
		
		JLabel lblQuestionText = new JLabel("Question Text");
		panel_4.add(lblQuestionText);
		textText.setLineWrap(true);  
		sideQuestion.add(textText);
		textText.setColumns(1);
		topSide.add(sideQuestion);
		sideQuestion.setLayout(new BoxLayout(sideQuestion, BoxLayout.Y_AXIS));
		
		JLabel lblQuestion = new JLabel("Question");
		sideQuestionText.add(lblQuestion);
		populateComboBox(questionComboBox, moduleComboBox);
		JScrollPane componentScollPane = new JScrollPane(componentList);
		componentList.setVisibleRowCount(5);
		
		questionComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				setFieldData(questionComboBox, moduleComboBox, textText, circuitSandbox);
			}
		});
		sideQuestionText.add(questionComboBox);

		moduleComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				updateComponentList();
			}
		});
		moduleComboBox.setModel(new DefaultComboBoxModel<Module>());
		setFieldData(questionComboBox, moduleComboBox, textText, circuitSandbox);
		
		sideQuestion.add(txtTextAnswer);
		side.add(sideContainer);
		sideContainer.setLayout(new BoxLayout(sideContainer, BoxLayout.Y_AXIS));
		
		JPanel panel_2 = new JPanel();
		sideContainer.add(panel_2);
		JLabel label = new JLabel("Module");
		panel_2.add(label);
		sideContainer.add(sideComponents);
		sideComponents.setLayout(new BoxLayout(sideComponents, BoxLayout.Y_AXIS));
		sideComponents.add(moduleComboBox);
		sideComponents.add(Box.createVerticalStrut(5));
		sideComponents.add(componentScollPane);
		
		JPanel panel_3 = new JPanel();
		sideContainer.add(panel_3);
		JLabel label_1 = new JLabel("Answers");
		panel_3.add(label_1);
		JPanel sideText = new JPanel();
		sideContainer.add(sideText);
		JScrollPane answerScrollPane = new JScrollPane(answerList);
		answerList.setVisibleRowCount(5);
		sideText.add(answerScrollPane);
		sideText.setLayout(new BoxLayout(sideText, BoxLayout.Y_AXIS));
		toolbars.setLayout(new BoxLayout(toolbars, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		toolbars.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		CircuitMakerMenu menuBar = new CircuitMakerMenu(this);
		panel.add(menuBar);
		
		
		JPanel panel_1 = new JPanel();
		toolbars.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JToolBar toolBar = new JToolBar();
		panel_1.add(toolBar);
		
		JButton btnAddModule = new JButton("Module");
		btnAddModule.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/plus.png")));
		btnAddModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				Module module = new Module(moduleComboBox.getItemCount());
				Reader.questionList.get(currentQuestionID).circuit.addModule(module);
				moduleComboBox.addItem(module);
				moduleComboBox.setSelectedItem(module);
			}
		});
		
		JButton btnAddQuestion = new JButton("Question");
		btnAddQuestion.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/plus.png")));
		btnAddQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				CreateQuestion createModuleDialog = new CreateQuestion();
				Question question =  createModuleDialog.getQuestion();
				createModuleDialog.dispose();
				if (question != null) {
					addQuestion(question, moduleComboBox, circuitSandbox);
				}
			}
		});
		toolBar.add(btnAddQuestion);
		
		JButton btnRemoveQuestion = new JButton("Question");
		btnRemoveQuestion.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/minus.png")));
		toolBar.add(btnRemoveQuestion);
		toolBar.add(btnAddModule);
		
		JButton btnAddComponent = new JButton("Component");
		btnAddComponent.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/plus.png")));
		btnAddComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CreateComponent createCompDialog = new CreateComponent();
				ElectricalComponent comp =  createCompDialog.getComp();
				createCompDialog.dispose();
				if (comp != null) {
					addComponent(comp, moduleComboBox, compInfo, circuitSandbox);
				}
			}
		});
		
		JButton btnRemoveModule = new JButton("Module");
		btnRemoveModule.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/minus.png")));
		btnRemoveModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(modules.size() > 1) {
					modules.remove(moduleComboBox.getSelectedItem());
					updateAll();
					moduleComboBox.setSelectedItem(modules.get(0));
				}
			}
		});
		toolBar.add(btnRemoveModule);
		toolBar.add(btnAddComponent);
		
		JButton btnRemoveComponent = new JButton("Component");
		btnRemoveComponent.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/minus.png")));
		btnRemoveComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				removeComponent(componentList, moduleComboBox, compInfo, circuitSandbox);
			}
		});
		toolBar.add(btnRemoveComponent);
		
		JButton btnAddAnswer = new JButton("Answer");
		btnAddAnswer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				CreateComponent createCompDialog = new CreateComponent();
				ElectricalComponent comp =  createCompDialog.getComp();
				createCompDialog.dispose();
				if (comp != null) {
					addAnswer(comp);
				}
			}
		});
		btnAddAnswer.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/plus.png")));
		toolBar.add(btnAddAnswer);
		
		JButton btnRemoveAnswer = new JButton("Answer");
		btnRemoveAnswer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				removeAnswer();
			}
		});
		btnRemoveAnswer.setIcon(new ImageIcon(CircuitMaker.class.getResource("/resources/icons/Very_Basic/minus.png")));
		toolBar.add(btnRemoveAnswer);
	}
	
	/**
	 * Show file open dialog.
	 */
	public void showFileOpen() {
		int returnVal = fc.showOpenDialog(CircuitMaker.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			Reader.questionList.clear();
			currentDir = file.getAbsolutePath();
			try {
				Reader.scanFile(file);
				updateAll();
			} catch (FileNotFoundException e) {}
		}
	}
	
	/**
	 * Show file save dialog.
	 */
	public void showFileSave() {
		Writer write = new Writer();
		write.writeFile(currentDir);
	}
	
	/**
	 * Show file save as dialog.
	 */
	public void showFileSaveAs() {
		int returnVal = fc.showOpenDialog(CircuitMaker.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			Writer write = new Writer();
			String path = file.getAbsolutePath();
			if (!path.endsWith(".xml")) {
				path = file.getAbsolutePath() + ".xml";
			}
			write.writeFile(path);
		}
	}
	
	/**
	 * Sets the question combo box.
	 *
	 * @param question the new question combo box
	 */
	public void setQuestionComboBox(Question question) {
		int i = getQuestionID(question);
		questionComboBox.setSelectedIndex(i);
	}
	
	/**
	 * Gets the question id.
	 *
	 * @param question the question
	 * @return the question id
	 */
	private int getQuestionID(Question question) {
		for (int i = 0; i <questionComboBox.getItemCount(); i++) {
			if (questionComboBox.getItemAt(i).equals(question)) {
				return i;
			}
		}
		return 0;
	}
	
	/**
	 * Adds the question.
	 *
	 * @param question the question
	 * @param moduleComboBox the module combo box
	 * @param circuitSandbox the circuit sand box
	 */
	private void addQuestion(Question question, JComboBox<Module> moduleComboBox, CircuitDraw circuitSandbox) {
		Reader.questionList.add(question);
		questionComboBox.addItem(question);
	}
	
	/**
	 * Adds the component.
	 *
	 * @param comp the component
	 * @param moduleComboBox the module combo box
	 * @param info the info
	 * @param circuitSandbox the circuit sand box
	 */
	private void addComponent(ElectricalComponent comp, JComboBox<Module> moduleComboBox, DefaultListModel<ElectricalComponent> info, CircuitDraw circuitSandbox) {
		Circuit circuit = Reader.questionList.get(currentQuestionID).circuit;
		circuit.getModules().get(moduleComboBox.getSelectedIndex()).addComponant(comp);
		updateAll();
	}
	
	/**
	 * Removes the component.
	 *
	 * @param componentList the component list
	 * @param moduleComboBox the module combo box
	 * @param info the info
	 * @param circuitSandbox the circuit sand box
	 */
	private void removeComponent(JList<ElectricalComponent> componentList, JComboBox<Module> moduleComboBox, DefaultListModel<ElectricalComponent> info, CircuitDraw circuitSandbox) {
		Circuit circuit = Reader.questionList.get(currentQuestionID).circuit;
		Module module = circuit.getModules().get(moduleComboBox.getSelectedIndex());
		if (module.getComponentAmmount() > 1 && componentList.getSelectedIndex() >= 0) { 	//if its selected an item
			module.removeComponent(componentList.getSelectedIndex());						//also a module cannot contain
			updateAll();																	//less than 1 component would make no sense
		}
	}
	
	/**
	 * Adds the answer.
	 *
	 * @param answer the answer
	 */
	private void addAnswer(ElectricalComponent answer) {
		Question question = Reader.questionList.get(currentQuestionID);
		question.addAnswer(answer);
		updateAll();
	}
	
	/**
	 * Removes the answer.
	 */
	private void removeAnswer() {
		if(modules.size() > 1) {
			ArrayList<ElectricalComponent> answers = Reader.questionList.get(currentQuestionID).getAnswers();
			answers.remove(answerList.getSelectedIndex());
			updateAll();
		}
	}
	
	/**
	 * Update everything on screen.
	 */
	public void updateAll() {
		refreshWires();
		updateModuleComboBox(moduleComboBox);
		if (questionComboBox.getItemAt(currentQuestionID).getType().equals("word")) {
			txtTextAnswer.setVisible(true);
			try {
				txtTextAnswer.setText(questionComboBox.getItemAt(currentQuestionID).answers.get(0).getTextAnswer());
			} catch(IndexOutOfBoundsException e) {
				txtTextAnswer.forceHint();
			}
		} else {
			txtTextAnswer.setVisible(false);
		}
		updateComponentList();
		updateAnswerList();
		circuitSandbox.updateCircuit(currentQuestionID);
	}
	
	/**
	 * Sets the field data.
	 *
	 * @param questionComboBox the question combo box
	 * @param moduleComboBox the module combo box
	 * @param textText the question text text-box
	 * @param circuitSandbox the circuit sand box
	 */
	private void setFieldData(JComboBox<Question> questionComboBox, JComboBox<Module> moduleComboBox, JTextArea textText, CircuitDraw circuitSandbox) {
		modules = getModulesFromQuestion(questionComboBox.getSelectedIndex());
		currentQuestionID = questionComboBox.getSelectedIndex();
		updateAll();
		textText.setText(getQuestionText(questionComboBox.getSelectedIndex()));
		updateCircuit(circuitSandbox, questionComboBox.getSelectedIndex());
	}
	
	/**
	 * Refresh wires.
	 */
	private void refreshWires() {
		Circuit circuit = Reader.questionList.get(currentQuestionID).circuit;
		circuit.removeAllWires();
		Reader.createWires(circuit);
	}
	
	/**
	 * Update component list.
	 */
	private void updateComponentList() {
		compInfo.clear();
		ArrayList<ElectricalComponent> components = getComponentsFromModule(moduleComboBox.getSelectedIndex(),currentQuestionID);
		if (components != null) {
			for (int i = 0; i < components.size(); i++) {
				compInfo.addElement(components.get(i));
			}
		}
	}
	
	/**
	 * Update answer list.
	 */
	private void updateAnswerList() {
		answerInfo.clear();
		ArrayList<ElectricalComponent> answers = getAnswers(currentQuestionID);
		if (answers != null) {
			for (int i = 0; i < answers.size(); i++) {
				answerInfo.addElement(answers.get(i));
			}
		}
	}
	
	/**
	 * Update module combo box.
	 *
	 * @param moduleComboBox the module combo box
	 */
	private void updateModuleComboBox(JComboBox<Module> moduleComboBox) {
		moduleComboBox.removeAllItems();
		for (int i = 0; i < modules.size(); i++) {
			moduleComboBox.addItem(modules.get(i));
		}
	}
	
	/**
	 * Update circuit.
	 *
	 * @param circuitSandbox the circuit sand box
	 * @param question the question
	 */
	private void updateCircuit(CircuitDraw circuitSandbox, int question) {
		circuitSandbox.updateCircuit(question);
	}

	/**
	 * Gets the modules from question.
	 *
	 * @param n the n
	 * @return the modules from question
	 */
	private ArrayList<Module> getModulesFromQuestion(int n) {
		return Reader.questionList.get(n).circuit.getModules();
	}
	
	/**
	 * Gets the question text.
	 *
	 * @param n the n
	 * @return the question text
	 */
	private String getQuestionText(int n) {
		 return Reader.questionList.get(n).getText();
	}
	
	/**
	 * Populate combo box.
	 *
	 * @param questionCombobox the question combo box
	 * @param moduleComboBox the module combo box
	 */
	private void populateComboBox(JComboBox<Question> questionCombobox, JComboBox<Module> moduleComboBox) { //Bug Report
		questionCombobox.removeAllItems();
		moduleComboBox.removeAllItems();
		for (int i = 0; i < Reader.questionList.size(); i++) {
			questionCombobox.addItem(Reader.questionList.get(i));
		}
		for (int i = 0; i < Reader.questionList.get(0).circuit.getModules().size(); i++) {
			moduleComboBox.addItem(Reader.questionList.get(0).circuit.getModules().get(i));
		}
	}
	
	/**
	 * Gets the answers.
	 *
	 * @param currentQID the current question ID
	 * @return the answers
	 */
	private ArrayList<ElectricalComponent> getAnswers(int currentQID) {
			Question question = Reader.questionList.get(currentQID);
			ArrayList<ElectricalComponent> components = question.getAnswers();
			return components;
	}
	
	/**
	 * Gets the components from module.
	 *
	 * @param n the n
	 * @param currentQID the current question ID
	 * @return the components from module
	 */
	private ArrayList<ElectricalComponent> getComponentsFromModule(int n, int currentQID) {
		if (n >= 0) {
			Question question = Reader.questionList.get(currentQID);
			Module module = question.circuit.getModules().get(n);
			ArrayList<ElectricalComponent> components = module.returnComponents();
			return components;
		}
		return null;
	}
}