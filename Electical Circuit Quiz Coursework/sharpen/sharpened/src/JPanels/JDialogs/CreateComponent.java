package JPanels.JDialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import Core.InitComponent;
import ElectricalComponents.ElectricalComponent;
import JPanels.Misc.HintTextArea;

/**
 * The Create Component Dialog Class
 * Opens up a dialog to create components (answers are also components)
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CreateComponent extends JDialog {
	
	/** The component type combo box items. */
	private String[] componentTypeComboBoxItems = { "Bulb", "Resistor", "Cell", "Void"};
	
	/** The component type. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox componentType = new JComboBox(componentTypeComboBoxItems);
	
	/** The resistance. */
	private HintTextArea resistance;
	
	/** The voltage. */
	private HintTextArea voltage;
	
	/** The current. */
	private HintTextArea current;
	
	/** The created result. */
	ElectricalComponent baby = null;
	
	/** The text answer field. */
	private JTextField textAnswerField;
	/**
	 * Create the panel.
	 */
	public CreateComponent() {
		setMinimumSize(new Dimension(200,300));
		this.setLocation(400, 100);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().add(componentType);
		componentType.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
		Component rigidArea = Box.createRigidArea(new Dimension(0,30));
		getContentPane().add(rigidArea);
		resistance = new HintTextArea("Resistance...");
		final Border origBorder = resistance.getBorder();
		resistance.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				resistance.setBorder(origBorder);
			}
		});
		getContentPane().add(resistance);
		resistance.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		Component rigidArea2 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea2);
		
		voltage = new HintTextArea("Voltage (Only for Cells)...");
		voltage.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				resistance.setBorder(origBorder);
			}
		});
		getContentPane().add(voltage);
		voltage.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		Component rigidArea3 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea3);
		
		current = new HintTextArea("Current (Only for Cells)...");
		current.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				resistance.setBorder(origBorder);
			}
		});
		getContentPane().add(current);
		current.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		Component rigidArea4 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea4);
		
		textAnswerField = new HintTextArea("Answer (For Void + Word type Q)");
		getContentPane().add(textAnswerField);
		textAnswerField.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		Component rigidArea5 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea5);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JButton btnCreateComponent = new JButton("Create Component");
		panel.add(btnCreateComponent);
		btnCreateComponent.setIcon(new ImageIcon(CreateComponent.class.getResource("/resources/icons/Very_Basic/checkmark.png")));
		btnCreateComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if (inputValidation()) {
					baby = InitComponent.create(getComponentType(),getResistance(),getVoltage(),getCurrent(),100,200, "");
					setVisible(false);
				}
			}
		});
		setEnabled(true);
		setModal(true);
		setVisible(true);

	}
	
	/**
	 * Gets the component.
	 *
	 * @return the component
	 */
	public ElectricalComponent getComp() {
		return baby;
	}
	
	/**
	 * Input validation.
	 *
	 * @return true, if successful
	 */
	private boolean inputValidation() {
		String item = componentType.getSelectedItem().toString();
		if (item.equals("Bulb") || item.equals("Resistor")) {
			if (resistance.getText().equals("")) {
				resistance.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				return false;
			}
		}
		if (item.equals("Cell")) {
			if (voltage.getText().equals("") || current.getText().equals("")) {
				voltage.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				current.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				return false;
			}
		}
		if (item.equals("Void")) {
			if (resistance.getText().equals("") && voltage.getText().equals("") && current.getText().equals("")) {
				resistance.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				voltage.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				current.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Gets the component type.
	 *
	 * @return the component type
	 */
	public String getComponentType() {
		String s = componentType.getSelectedItem().toString();
		return s;
	}
	
	/**
	 * Gets the resistance.
	 *
	 * @return the resistance
	 */
	public float getResistance() {
		if (!resistance.getText().equals("")) {
			Float f = new Float(resistance.getText());
			return f;
		} 
		return 0;
	}
	
	/**
	 * Gets the voltage.
	 *
	 * @return the voltage
	 */
	public float getVoltage() {
		try {
			voltage.getText();
		} catch(NumberFormatException e) {
			System.out.println("caught number exception");
			return 0;
		}
		if (!voltage.getText().equals("")) {
			Float f = new Float(voltage.getText());
			return f;
		}
		return 0;
	}
	
	/**
	 * Gets the current.
	 *
	 * @return the current
	 */
	public float getCurrent() {
		if (!current.getText().equals("")) {
			Float f = new Float(current.getText());
			return f;
		}
		return 0;
	}
}
