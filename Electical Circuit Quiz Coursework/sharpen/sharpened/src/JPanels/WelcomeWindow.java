package JPanels;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import JPanels.JMenus.BaseMenu;
import JPanels.Misc.CardPanel;



/**
 * The Welcome Window Class.
 * Shows at the beginning of the application.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class WelcomeWindow extends JPanel {

	/**
	 * Instantiates a new welcome window.
	 *
	 * @param cardPanel the card panel
	 */
	public WelcomeWindow(final CardPanel cardPanel) {
		setBounds(100, 100, 898, 477);
        setLayout(new BorderLayout(0, 0));
        JPanel panel = new JPanel();
        add(panel, BorderLayout.CENTER);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        Component horizontalGlue = Box.createHorizontalGlue();
        panel.add(horizontalGlue);
        BaseMenu menu = new BaseMenu();
        add(menu, BorderLayout.NORTH);
        JPanel panel_2 = new JPanel();
        panel.add(panel_2);
        panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
        
        JPanel panel_4 = new JPanel();
        panel_2.add(panel_4);
        panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));
        JLabel lblWhoWantsTo = new JLabel("Who wants to be Electricuted?");
        panel_4.add(lblWhoWantsTo);
        lblWhoWantsTo.setForeground(Color.DARK_GRAY);
        lblWhoWantsTo.setFont(new Font("Dotum", Font.PLAIN, 50));
        Component rigidArea_1 = Box.createRigidArea(new Dimension(0,30));
        panel_2.add(rigidArea_1);
        
        JPanel panel_1 = new JPanel();
        panel_2.add(panel_1);
        panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
        Component horizontalGlue3 = Box.createHorizontalGlue();
        panel_1.add(horizontalGlue3);
        JPanel panel_3 = new JPanel();
        panel_1.add(panel_3);
        panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
        
        JButton btnTeacher = new JButton("Teacher");
        btnTeacher.setIcon(new ImageIcon(WelcomeWindow.class.getResource("/resources/icons/Security/key.png")));
        panel_3.add(btnTeacher);
        Component rigidArea_2 = Box.createRigidArea(new Dimension(0,30));
        panel_3.add(rigidArea_2);
        
        JButton btnStudent = new JButton("Student");
        btnStudent.setIcon(new ImageIcon(WelcomeWindow.class.getResource("/resources/icons/System/login.png")));
        panel_3.add(btnStudent);
        Component horizontalGlue4 = Box.createHorizontalGlue();
        panel_1.add(horizontalGlue4);
        btnStudent.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseReleased(MouseEvent arg0) {
        		//Writer write = new Writer();
        		cardPanel.c1.show(cardPanel, "diffSelect");
        		
        	}
        });
        btnTeacher.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseReleased(MouseEvent e) {
        		JPasswordField pwd = new JPasswordField(10);  
        		int action = JOptionPane.showConfirmDialog(null, pwd,"Enter Password",JOptionPane.OK_CANCEL_OPTION);  
        	    if(action < 0) {
        	    	JOptionPane.showMessageDialog(null,"Cancel, X or escape key selected");  
        	    }
        	    String password = new String(pwd.getPassword());
        	    if (password.equals("")) {
                    cardPanel.c1.show(cardPanel, "circuitMaker");
        	    } else {
        	    	JOptionPane.showMessageDialog(null,"Incorrect Password.");
        	    }
        	}
        });
        Component horizontalGlue2 = Box.createHorizontalGlue();
        panel.add(horizontalGlue2);
	}

}
