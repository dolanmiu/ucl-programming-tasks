package JPanels.JMenus;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import Core.Question;
import Core.Reader;
import JPanels.CircuitMaker;
import JPanels.JDialogs.SearchQuestion;

/**
 * The Circuit Maker Menu Bar Class
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CircuitMakerMenu extends BaseMenu {
	
	/** The circuit. */
	CircuitMaker circuit;
	
	/**
	 * Instantiates a new circuit maker menu.
	 *
	 * @param cm the CircuitMaker object
	 */
	public CircuitMakerMenu(CircuitMaker cm) { 
		this.circuit = cm;
		editMenu.setVisible(true);
		
		JMenuItem newAction =   new JMenuItem("New Quiz");
		newAction.setIcon(new ImageIcon(CircuitMakerMenu.class.getResource("/resources/icons/Adds/add_file.png")));
		newAction.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
			    int result = JOptionPane.showConfirmDialog(null, "Are you sure? Be sure you saved!" , "alert", JOptionPane.OK_CANCEL_OPTION);
			    if (result == 0) { //2 is cancel
			    	Reader.questionList.clear();
			    	circuit.updateAll();
			    }
			}
		});
		
		JMenuItem openAction =  new JMenuItem("Open Quiz");
		openAction.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				circuit.showFileOpen();
			}
		});
		JMenuItem searchQuestion =  new JMenuItem("Search Question");
		searchQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SearchQuestion search = new SearchQuestion();
				Question question = search.getSelected();
				circuit.setQuestionComboBox(question);
			}
		});
		editMenu.add(searchQuestion);
		fileMenu.addSeparator();
		fileMenu.add(newAction);
		fileMenu.add(openAction);
		
		JMenuItem mntmSaveQuiz = new JMenuItem("Save Quiz");
		mntmSaveQuiz.setIcon(new ImageIcon(CircuitMakerMenu.class.getResource("/resources/icons/System/save.png")));
		mntmSaveQuiz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				circuit.showFileSave();
			}
		});
		fileMenu.add(mntmSaveQuiz);
		
		JMenuItem mntmSaveQuizAs = new JMenuItem("Save Quiz As..");
		mntmSaveQuizAs.setIcon(new ImageIcon(CircuitMakerMenu.class.getResource("/resources/icons/System/save_as.png")));
		mntmSaveQuizAs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				circuit.showFileSaveAs();
			}
		});
		fileMenu.add(mntmSaveQuizAs);
		editMenu.addSeparator();
		
		helpMenu.addSeparator();
		
		JMenuItem mntmTutorial = new JMenuItem("Tutorial On Circuit Creation");
		mntmTutorial.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JOptionPane.showMessageDialog(null,"Basically, you use the buttons on the top to create modules \n(parrallel parts of a circuit), then you put components inside them. Simple! \nYou could alternatively just put 1 component per module... \nto create a 100% series circuit!");
			}
		});
		
		JMenuItem mntmTutorialOnQuestion = new JMenuItem("Tutorial on Question Creation");
		mntmTutorialOnQuestion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JOptionPane.showMessageDialog(null,"Click on the \"+Question\" button, select what type of question. \nDrag is simple drag and drop, Text allows the student \nto type an answer in, Gesture makes the student follow a path you \nset with his/her finger! Fill the question \nwith text and its own circuit.\n\n Remember, there are 4 Types of Questions: \n\t1) Drag and Drop \n\t2) Text THEN Drag and Drop (Similar)\n\t3) Text \n\t4) Gestures");
			}
		});
		helpMenu.add(mntmTutorialOnQuestion);
		
		JMenuItem mntmTutorialOnAnswer = new JMenuItem("Tutorial on Answer Creation");
		mntmTutorialOnAnswer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JOptionPane.showMessageDialog(null,"The answer creation system uses the DolanSMART checker (c)\n\n By creating your circuit with the Voids having the appropriate resistances, voltages etc..\n you in effect create the WORKING circuit. By dragging answers onto the Voids, the answers replace the Voids. The system checks to see if the initial Circuit state matches with the \nstate afterwards. If they match, the student got it correct! This way, it will \naccomodate ALL variations (if any) of answers if the circuit becomes complex.");
			}
		});
		helpMenu.add(mntmTutorialOnAnswer);
		helpMenu.add(mntmTutorial);
	}
}
