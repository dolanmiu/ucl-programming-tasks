package JPanels.JMenus;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * The Base Menu Bar Class
 *
 * @author DolanMiu
 * @version 1.0
 */
public class BaseMenu extends JMenuBar {
    
    /** The file menu. */
    protected JMenu fileMenu = new JMenu("File");
    
    /** The help menu. */
    protected JMenu helpMenu = new JMenu("Help");
    
    /** The edit menu. */
    protected JMenu editMenu = new JMenu("Edit");

	/**
	 * Instantiates a new base menu.
	 */
	public BaseMenu() {
        add(fileMenu);
        add(editMenu);
        add(helpMenu);
        editMenu.setVisible(false);
        
        JMenuItem mntmAboutUs = new JMenuItem("About Us");
        mntmAboutUs.setIcon(new ImageIcon(BaseMenu.class.getResource("/resources/icons/System/help.png")));
        mntmAboutUs.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseReleased(MouseEvent e) {
        		JOptionPane.showMessageDialog(null,"Who wants to be Electricuted? Made by Dolan Miu. (c) 2012");
        	}
        });
        helpMenu.add(mntmAboutUs);
        
        JMenuItem exitAction =  new JMenuItem("Exit");
        exitAction.setIcon(new ImageIcon(BaseMenu.class.getResource("/resources/icons/System/logout.png")));
        exitAction.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseReleased(MouseEvent e) {
        		int result = JOptionPane.showConfirmDialog(null, "Are you sure?" , "alert", JOptionPane.OK_CANCEL_OPTION);
			    if (result == 0) { //2 is cancel
			    	System.exit(0);
			    }
        	}
        });
        fileMenu.add(exitAction);
	}
}
