package JPanels.JMenus;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import JPanels.Misc.CardPanel;

/**
 * The Student Quiz Menu Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class StudentQuizMenu extends BaseMenu {

	/**
	 * Instantiates a new student quiz menu.
	 *
	 * @param cardPanel the card panel
	 */
	public StudentQuizMenu(final CardPanel cardPanel) {
		fileMenu.addSeparator();
		JMenuItem mntmBackToMenu = new JMenuItem("Back to Menu");
		mntmBackToMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				cardPanel.c1.show(cardPanel, "welcome");
			}
		});
		mntmBackToMenu.setIcon(new ImageIcon(StudentQuizMenu.class.getResource("/resources/icons/Very_Basic/undo.png")));
		fileMenu.add(mntmBackToMenu);
		
		JMenuItem mntmGestureHelp = new JMenuItem("Gesture Help");
		mntmGestureHelp.setIcon(new ImageIcon(StudentQuizMenu.class.getResource("/resources/icons/Touch/one_finger.png")));
		mntmGestureHelp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				JOptionPane.showMessageDialog(null,"To use Gestures: \n\nRight click and drag! Simple. \nTypes of gestures: \n\n  Drag Down + Drag Right = Next Question\n  Drag Down + Drag Left = Back to Menu\n\nGesture Questions: \nnHold right click and drag the outline of where you think the path \nof eddy eletron goes");
			}
		});
		helpMenu.addSeparator();
		helpMenu.add(mntmGestureHelp);

	}

}
