//got help from http://stackoverflow.com/questions/1738966/java-jtextfield-with-input-hint
package JPanels.Misc;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JTextField;

/**
 * The Hint Text Field Class
 * Does a 21st century style text box where if empty will show you a hint.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class HintTextArea extends JTextField implements FocusListener {
	
	/** The hint. */
	private final String hint;
	
	/**
	 * Instantiates a new hint text area.
	 *
	 * @param hint the hint
	 */
	public HintTextArea(final String hint) {
		super(hint);
		this.hint = hint;
		super.addFocusListener(this);
	}
	
	/**
	 * Force hint to come up on box.
	 */
	public void forceHint() {
		super.setText(hint);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.FocusListener#focusGained(java.awt.event.FocusEvent)
	 */
	@Override
	public void focusGained(FocusEvent e) {
		if(this.getText().isEmpty()) {
			super.setText("");
		}
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.FocusListener#focusLost(java.awt.event.FocusEvent)
	 */
	@Override
	public void focusLost(FocusEvent e) {
		if(this.getText().isEmpty()) {
			super.setText(hint);
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.text.JTextComponent#getText()
	 */
	@Override
	public String getText() {
		String typed = super.getText();
		return typed.equals(hint) ? "" : typed;
	}
}
