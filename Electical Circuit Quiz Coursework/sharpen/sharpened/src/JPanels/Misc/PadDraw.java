//Read more: http://forum.codecall.net/topic/58137-java-mini-paint-program/#ixzz2CUftAE6J
package JPanels.Misc;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JComponent;

/**
 * The Pad Draw Class
 *
 * @author DolanMiu
 * @version 1.0
 */
class PadDraw extends JComponent{
	
	/** The image. */
	Image image;
	//this is gonna be your image that you draw on
	/** The graphics 2d. */
	Graphics2D graphics2D;
	//this is what we'll be using to draw on
	/** The old y. */
	int currentX, currentY, oldX, oldY;
	//these are gonna hold our mouse coordinates
	//Now for the constructors

	/**
	 * Instantiates a new pad draw.
	 */
	public PadDraw(){
		setDoubleBuffered(false);
		addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e){
				oldX = e.getX();
				oldY = e.getY();
			}
		});
		//if the mouse is pressed it sets the oldX & oldY
		//coordinates as the mouses x & y coordinates
		addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseDragged(MouseEvent e){
				currentX = e.getX();
				currentY = e.getY();
				if(graphics2D != null)
					graphics2D.drawLine(oldX, oldY, currentX, currentY);
				repaint();
				oldX = currentX;
				oldY = currentY;
			}
		});
		//while the mouse is dragged it sets currentX & currentY as the mouses x and y
		//then it draws a line at the coordinates
		//it repaints it and sets oldX and oldY as currentX and currentY
	}


	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g){
		if(image == null){
			image = createImage(getSize().width, getSize().height);
			graphics2D = (Graphics2D)image.getGraphics();
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			clear();
		}
		g.drawImage(image, 0, 0, null);
	}

	//this is the painting bit
	//if it has nothing on it then
	//it creates an image the size of the window
	//sets the value of Graphics as the image
	//sets the rendering
	//runs the clear() method
	//then it draws the image



	/**
	 * Clear the canvas.
	 */
	public void clear(){
		graphics2D.setPaint(Color.white);
		graphics2D.fillRect(0, 0, getSize().width, getSize().height);
		graphics2D.setPaint(Color.black);
		repaint();
	}

	//this is the clear
	//it sets the colors as white
	//then it fills the window with white
	//thin it sets the color back to black

	/**
	 * Red.
	 */
	public void red(){
		graphics2D.setPaint(Color.red);
		repaint();
	}

	//this is the red paint

	/**
	 * Black.
	 */
	public void black(){
		graphics2D.setPaint(Color.black);
		repaint();
	}

	//black paint

	/**
	 * Magenta.
	 */
	public void magenta(){
		graphics2D.setPaint(Color.magenta);
		repaint();
	}

	//magenta paint

	/**
	 * Blue.
	 */
	public void blue(){
		graphics2D.setPaint(Color.blue);
		repaint();
	}

	//blue paint

	/**
	 * Green.
	 */
	public void green(){
		graphics2D.setPaint(Color.green);
		repaint();
	}
	//green paint

	/**
	 * Save image to disk.
	 */
	public void saveImage() {
		BufferedImage bi = null;
		bi = ScreenImage.createImage(this);
		try {
			ScreenImage.writeImage(bi, "Student Notes.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}