//Read more: http://forum.codecall.net/topic/58137-java-mini-paint-program/#ixzz2CUftAE6J
package JPanels.Misc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * The Paint Panel Class
 *
 * @author DolanMiu
 * @version 1.0
 */
public class PaintPanel extends JPanel {
	
	/**
	 * Instantiates a new paint panel.
	 */
	public PaintPanel() {
		paint();
	}
	/**
	 * Create the panel.
	 */
	public void paint() {
		Icon iconB = new ImageIcon("blue.gif");
		//the blue image icon
		Icon iconM = new ImageIcon("magenta.gif");
		//magenta image icon
		Icon iconR = new ImageIcon("red.gif");
		//red image icon
		Icon iconBl = new ImageIcon("black.gif");
		//black image icon
		Icon iconG = new ImageIcon("green.gif");
		//finally the green image icon
		//These will be the images for our colors.
		//JFrame frame = new JFrame("Paint It");
		//Creates a frame with a title of "Paint it"
		//Container content = frame.getContentPane();
		//Creates a new container
		setLayout(new BorderLayout());
		final PadDraw drawPad = new PadDraw();
		final JPanel paintPalet = new JPanel();

		add(drawPad, BorderLayout.CENTER);
		
		//creates a JPanel
		paintPalet.setPreferredSize(new Dimension(32, 68));
		paintPalet.setMinimumSize(new Dimension(32, 68));
		paintPalet.setMaximumSize(new Dimension(32, 68));
		//This sets the size of the panel

		JButton clearButton = new JButton("Clear");
		//creates the clear button and sets the text as "Clear"
		clearButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.clear();
			}
		});

		//this is the clear button, which clears the screen.  This pretty
		//much attaches an action listener to the button and when the
		//button is pressed it calls the clear() method


		JButton redButton = new JButton(iconR);
		//creates the red button and sets the icon we created for red
		redButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.red();
			}
		});
		//when pressed it will call the red() method.  So on and so on =]
		JButton blackButton = new JButton(iconBl);
		//same thing except this is the black button
		blackButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.black();
			}
		});
		JButton magentaButton = new JButton(iconM);
		//magenta button
		magentaButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.magenta();
			}
		});
		JButton blueButton = new JButton(iconB);
		//blue button
		blueButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.blue();
			}
		});
		JButton greenButton = new JButton(iconG);
		//green button
		greenButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				drawPad.green();
			}
		});
		blackButton.setPreferredSize(new Dimension(16, 16));
		magentaButton.setPreferredSize(new Dimension(16, 16));
		redButton.setPreferredSize(new Dimension(16, 16));
		blueButton.setPreferredSize(new Dimension(16, 16));
		greenButton.setPreferredSize(new Dimension(16,16));
		//sets the sizes of the buttons
		paintPalet.add(greenButton);
		paintPalet.add(blueButton);
		paintPalet.add(magentaButton);
		paintPalet.add(blackButton);
		paintPalet.add(redButton);
		paintPalet.add(clearButton);
		//adds the buttons to the panel
		add(paintPalet, BorderLayout.WEST);
		
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				//http://www.java2s.com/Code/Java/2D-Graphics-GUI/DrawanImageandsavetopng.htm
				drawPad.saveImage();
			}
		});
		paintPalet.add(btnSave);
		//sets the panel to the left
		//makes it so you can close
	}
}