package JPanels.Misc;

import java.awt.CardLayout;

import javax.swing.JPanel;

/**
 * The CardPanel Class
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CardPanel extends JPanel {
	
	/** The card layout. */
	public CardLayout c1;
	
	/**
	 * Instantiates a new card panel.
	 */
	public CardPanel() {
		c1 = new CardLayout();
		setLayout(c1);
	}
}
