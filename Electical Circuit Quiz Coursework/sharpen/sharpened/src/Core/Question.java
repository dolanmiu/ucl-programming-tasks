package Core;

import java.util.ArrayList;

import ElectricalComponents.ElectricalComponent;


/**
 * The question class. Every question is created from this class. It contains the circuit, its question and its respective answer.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Question {
	
	/** The answers. */
	public ArrayList<ElectricalComponent> answers = new ArrayList<ElectricalComponent>();
	
	/** The circuit. */
	public Circuit circuit;
	
	/** The difficulty. */
	public String diff;
	
	/** The numb. */
	public int numb;
	
	/** The text. */
	public String text;
	
	/** The type. */
	public String type;
	
	/**
	 * Instantiates a new question.
	 *
	 * @param type the type
	 * @param circuit the circuit
	 * @param diff the difficulty
	 * @param numb the numb
	 * @param text the question text
	 * @param answers the answers
	 */
	public Question (String type, Circuit circuit, String diff, int numb, String text, ArrayList<ElectricalComponent> answers) {
		this.circuit = circuit;
		this.diff = diff;
		this.numb = numb;
		this.text = text;
		this.answers = answers;
		this.type = type;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Sort out the question text.
	 */
	public void sortOutText() {
		String newText = text.replace("\\r\\n|\\r|\\n", "");
		newText = text.replace("\n", "");
		this.text = newText;
	}
	
	/**
	 * Gets the circuit.
	 *
	 * @return the circuit
	 */
	public Circuit getCircuit() {
		return circuit;
	}
	
	/**
	 * Gets the question text.
	 *
	 * @return the question text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Sets the question text.
	 *
	 * @param text the new question text
	 */
	public void setText(String text) {
		this.text = text;
	}
	
	/**
	 * Adds the answer.
	 *
	 * @param answer the answer
	 */
	public void addAnswer(ElectricalComponent answer) {
		answers.add(answer);
	}
	
	/**
	 * Gets the answers.
	 *
	 * @return the answers
	 */
	public ArrayList<ElectricalComponent> getAnswers() {
		return answers;
	}
	
	/**
	 * Removes the answer.
	 *
	 * @param question ID
	 */
	public void removeAnswer(int i) {
		answers.remove(i);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Question: " + numb + " Difficulty: " + diff;
	}
}
