package Core;

import java.util.ArrayList;

import ElectricalComponents.CircuitCalc;
import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Module;
import ElectricalComponents.Wire;

/**
 * The Class Circuit.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Circuit {
	
	/** The modules. */
	ArrayList<Module> modules = new ArrayList<Module>();						//this arraylist is storing the modules
	
	/** The wires. */
	ArrayList<Wire> wires = new ArrayList<Wire>();								//This arraylist stores the wires
	
	/**
	 * Instantiates a new circuit.
	 */
	public Circuit() {
		
	}
	
	/**
	 * Sets the module voltages.
	 */
	public void setModuleVoltages() {
		ArrayList<ElectricalComponent> cells = new ArrayList<ElectricalComponent>();
		float totalVoltage = 0;
		cells = CircuitCalc.getCells(modules);
		
		for (int i = 0; i < cells.size(); i++) {
			totalVoltage += cells.get(i).getVoltage();
		}
		
		for (int i = 0; i < modules.size(); i++) {
			modules.get(i).setVoltsAcross(totalVoltage);
		}
	}
	
	/**
	 * Removes the all wires.
	 */
	public void removeAllWires() {
		wires.clear();
	}
	
	/**
	 * Checks for wire existence.
	 *
	 * @param module1 First module
	 * @param module2 second module
	 * @return true, if successful
	 */
	public boolean hasWire(Module module1, Module module2) {
		for (int i = 0; i < wires.size(); i++) {
			if (wires.get(i).getTo() == module1 && wires.get(i).getFrom() == module2) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the modules.
	 *
	 * @return the modules
	 */
	public ArrayList<Module> getModules() {
		return modules;
	}
	
	/**
	 * Adds the module.
	 *
	 * @param module the module
	 */
	public void addModule(Module module) {
		modules.add(module);
	}
	
	/**
	 * Removes the module.
	 *
	 * @param i the i
	 */
	public void removeModule(int i) {
		modules.remove(i);
	}
	
	/**
	 * Adds the wires.
	 *
	 * @param wire the wire
	 */
	public void addWires(Wire wire) {
		wires.add(wire);
	}
	
	/**
	 * Gets the wires.
	 *
	 * @return the wires
	 */
	public ArrayList<Wire> getWires() {
		return wires;
	}
	
	/**
	 * Prints the module ammount.
	 */
	public void printModuleAmmount() {
		System.out.println(modules.size() + " Modules in this circuit." );
	}
}
