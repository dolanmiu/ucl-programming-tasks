package Core;

import ElectricalComponents.Bulb;
import ElectricalComponents.Cell;
import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Resistor;
import ElectricalComponents.Void;
//This class abstracts the creation of the electrical components to make life easier.
//It would be used in for loops mainly and especailly when reading the XML file to prevent null pointer exceptions etc
/**
 * The class which initialise components.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class InitComponent {
	
	/**
	 * Creates the electrical component.
	 *
	 * @param name the name
	 * @param resistance the resistance
	 * @param voltage the voltage
	 * @param current the current
	 * @param x the x
	 * @param y the y
	 * @param textAnswer the text answer
	 * @return the electrical component
	 */
	public static ElectricalComponent create(String name, float resistance, float voltage, float current, int x, int y, String textAnswer) {
		name = name.toLowerCase();
		if (name.equals("bulb")) {									//series of if  statements which can take as many of as little
			Bulb bulb = new Bulb(resistance, x, y);					//properties of a component as possible.
			return bulb;											//for example, a resistor needs 3, whereas a cell needs 4.
		}
		if (name.equals("resistor")) {
			Resistor resistor = new Resistor(resistance, x, y);
			return resistor;
		}
		if (name.equals("cell")) {
			Cell cell = new Cell(voltage, current, x, y);
			return cell;
		}
		if (name.equals("void")) {
			Void _void = new Void(resistance, voltage, current, x,y, textAnswer);
			return _void;
		}
		System.err.println("Creating Null Object in InitComponent");
		return null;
	}
}
