//golfb.org/stopwatchjava.html
package Core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.Timer;

/**
 * The Stop Watch Class. It is the timer commonly found in questions.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class StopWatch extends JComponent implements ActionListener {
	
	/** The max time. */
	private final long maxTime;
	
	/** The time left. */
	public long timeLeft;
	
	/** The checkpoint. */
	private long checkpoint;
	
	/** The timer. */
	private Timer timer;
	
	/**
	 * Instantiates a new stop watch.
	 *
	 * @param time the time for the watch to tick for
	 */
	public StopWatch(int time) {
		this.timeLeft = time;
		this.maxTime = time;
		setCheckpoint();
		timer = new Timer(1000, this); //every 1000 millisecs activate the timer
		timer.start(); //starts the timer
		setBounds(0,0,80,80);
		this.setPreferredSize(new Dimension(80,80));
	}
	
	/**
	 * Reset.
	 */
	public void reset() {
		timeLeft = maxTime;
		timer.start();
	}
	
	/**
	 * Start.
	 */
	public void start() {
		timer.start();
	}
	
	/**
	 * Stop.
	 */
	public void stop() {
		timer.stop();
	}
	
	/**
	 * Gets the max time.
	 *
	 * @return the max time
	 */
	public long getMaxTime () {
		return maxTime;
	}
	
	/**
	 * Sets the time checkpoint.
	 */
	public void setCheckpoint() {
		this.checkpoint = timeLeft;
	}
	
	/**
	 * Gets the time checkpoint.
	 *
	 * @return the checkpoint
	 */
	public long getCheckpoint() {
		return checkpoint;
	}
	
	/**
	 * Gets the time left.
	 *
	 * @return the time left
	 */
	public long getTimeLeft() {
		return timeLeft;
	}
	
	/**
	 * Gets the elapsed time.
	 *
	 * @return the elapsed time
	 */
	public long getElapsedTime() {
		long elapsed = (maxTime - timeLeft);
		return elapsed;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) { 													//http://harryjoy.com/2012/05/20/circular-progress-bar-in-java-swing/
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (timeLeft <= maxTime) {
			g.setColor(new Color(0x636363));
			int angle = -(int) (((float) timeLeft / maxTime) * 360);
			g.fillArc(0, 0, getWidth(), getHeight(), 90, angle);	 
		}
	}


	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) { //this
    	timeLeft -= 1; //which decrements the time.
    	repaint();
    	if (timeLeft == 0) {
    		timer.stop();
    	}
	}
}