package Core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Module;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * The Writer Class. This Writes from Object to XML
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Writer {
	
	/** The question list. */
	ArrayList<Question> questionList = new ArrayList<Question>();
	
	/**
	 * Instantiates a new writer.
	 */
	public Writer() {
	}
	
	/**
	 * Write file.
	 *
	 * @param path the path to file
	 */
	public void writeFile(String path) {
		questionList = Reader.questionList;
		try {
			writeQuestions(path);
		} catch(ParserConfigurationException parser) {
			System.err.println("The Writer has a Parser Config Exception");
		} catch(FileNotFoundException file) {
			System.err.println("The Writer cannot find the file");
		} catch(IOException ioexception) {
			System.err.println("The Writer has an IO Exception");
		}
	}
	
	/**
	 * Writes the questions into file.
	 *
	 * @param path the path to file
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws FileNotFoundException the file not found exception
	 */
	public void writeQuestions(String path) throws ParserConfigurationException, IOException, FileNotFoundException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document xmlDoc = docBuilder.newDocument();
		
		Element rootElement = xmlDoc.createElement("quiz");
		
		for (int i = 0; i < questionList.size(); i++) {
			Element questionElement = xmlDoc.createElement("question");
			rootElement.appendChild(questionElement);
			questionElement.setAttribute("difficulty", questionList.get(i).diff);
			questionElement.setAttribute("number", questionList.get(i).numb + "");
			questionElement.setAttribute("type", questionList.get(i).getType());
			
			Element answers = xmlDoc.createElement("answers");
			questionElement.appendChild(answers);
			Element circuit = xmlDoc.createElement("circuit");
			questionElement.appendChild(circuit);
			handleCircuit(circuit, questionList.get(i), xmlDoc);

			questionElement.appendChild(setTagValue("text", questionList.get(i).getText(), xmlDoc));
			ArrayList<ElectricalComponent> comps = questionList.get(i).getAnswers();
			handleAnswers(answers, comps, xmlDoc);
		}
		
		xmlDoc.appendChild(rootElement);
		
		OutputFormat outFormat = new OutputFormat(xmlDoc);
		outFormat.setIndenting(true);
		
		File xmlFile = new File(path);
		FileOutputStream outStream = new FileOutputStream(xmlFile);
		
		XMLSerializer serializer = new XMLSerializer(outStream, outFormat);
		
		serializer.serialize(xmlDoc);
	}
	
	/**
	 * Handle circuit.
	 *
	 * @param circuit the circuit
	 * @param currentQ the current question
	 * @param xmlDoc the XML document
	 */
	private void handleCircuit(Element circuit, Question currentQ, Document xmlDoc) {
		ArrayList<Module> modules = currentQ.circuit.getModules();
		for (int i = 0; i < modules.size(); i++) {
			Element module = xmlDoc.createElement("module");
			circuit.appendChild(module);
			handleModules(module, modules.get(i), xmlDoc);
		}
	}
	
	/**
	 * Handle modules.
	 *
	 * @param eModule the element of the module
	 * @param module the module
	 * @param xmlDoc the XML doc
	 */
	private void handleModules(Element eModule, Module module, Document xmlDoc) {
		ArrayList<ElectricalComponent> comps = module.returnComponents();
		for (int i = 0; i < comps.size(); i++) {
			Element comp = xmlDoc.createElement("componant");
			eModule.appendChild(comp);
			handleComponent(comp, comps.get(i), xmlDoc);
		}
	}
	
	/**
	 * Handle answers.
	 *
	 * @param eAnswers the element of the answers
	 * @param answers the answers
	 * @param xmlDoc the XML doc
	 */
	private void handleAnswers(Element eAnswers, ArrayList<ElectricalComponent> answers, Document xmlDoc) {
		for (int i = 0; i < answers.size(); i++) {
			Element eComp = xmlDoc.createElement("componant");
			eAnswers.appendChild(eComp);
			eComp.appendChild(setTagValue("name", answers.get(i).toString(), xmlDoc));
			if (answers.get(i).getResistance() != 0) {
				eComp.appendChild(setTagValue("resistance", answers.get(i).getResistance() + "", xmlDoc));
			}
			if (answers.get(i).getVoltage() != -1f) {
				eComp.appendChild(setTagValue("voltage", answers.get(i).getVoltage() + "", xmlDoc));
			}
			if (answers.get(i).getCurrent() != -1f) {
				eComp.appendChild(setTagValue("current", answers.get(i).getCurrent() + "", xmlDoc));
			}
			if (!answers.get(i).getTextAnswer().equals("")) {
				eComp.appendChild(setTagValue("answer", answers.get(i).getTextAnswer(), xmlDoc));
			}
			eComp.appendChild(setTagValue("x", answers.get(i).getX() + "", xmlDoc));
			eComp.appendChild(setTagValue("y", answers.get(i).getY() + "", xmlDoc));
		}
	}
	
	/**
	 * Handle component.
	 *
	 * @param eComp the element of the component
	 * @param comp the component
	 * @param xmlDoc the XML doc
	 */
	private void handleComponent(Element eComp, ElectricalComponent comp, Document xmlDoc) {
		eComp.appendChild(setTagValue("name", comp.toString(), xmlDoc));
		if (comp.getResistance() != 0) {
			eComp.appendChild(setTagValue("resistance", comp.getResistance() + "", xmlDoc));
		}
		if (comp.getVoltage() != -1f) {
			eComp.appendChild(setTagValue("voltage", comp.getVoltage() + "", xmlDoc));
		}
		if (comp.getCurrent() != -1f) {
			eComp.appendChild(setTagValue("current", comp.getCurrent() + "", xmlDoc));
		}
		eComp.appendChild(setTagValue("x", comp.getX() + "", xmlDoc));
		eComp.appendChild(setTagValue("y", comp.getY() + "", xmlDoc));
	}
	
	/**
	 * Sets the tag value.
	 *
	 * @param tag the tag to set
	 * @param value the value to set
	 * @param xmlDoc the XML doc
	 * @return the element set.
	 */
	private Element setTagValue(String tag, String value, Document xmlDoc) {
		Element attribute = xmlDoc.createElement(tag);
		if (!value.equals("")) {
			Text attributetext = xmlDoc.createTextNode(value);
			attribute.appendChild(attributetext);
		}
		return attribute;
	}
} 
