package Core;

import java.util.ArrayList;


/**
 * The class which marks the students
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Marker {
	
	/** The name. */
	static String name = "";
	
	/** The total score. */
	static int totalScore = 0;
	
	/** The max score. */
	static int maxScore = 100;
	
	/** The motivational message. */
	static String motivationalMessage = "Nothing to report";
	
	/** The question statistics. */
	static ArrayList<String> questionStats = new ArrayList<String>();
	
	/**
	 * Adds the score.
	 *
	 * @param scoreToAdd the score to add
	 */
	public static void addScore(int scoreToAdd) {
		totalScore += scoreToAdd;
	}
	
	/**
	 * Subtract score.
	 *
	 * @param scoreToSubtract the score to subtract
	 */
	public static void subtractScore(int scoreToSubtract) {
		totalScore += scoreToSubtract;
	}
	
	/**
	 * Reset score.
	 */
	public static void resetScore() {
		totalScore = 0;
	}
	
	/**
	 * Prints the score.
	 */
	public static void printScore() {
		System.out.println("The total score is: " + totalScore + " point(s)!");
	}
	
	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public static int getScore() {
		return totalScore;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param nameInput the new name
	 */
	public static void setName(String nameInput) {
		name = nameInput;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public static String getName() {
		return name;
	}
	
	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public static String getMessage() {
		return motivationalMessage;
	}
	
	/**
	 * Gets the grade of the student.
	 *
	 * @return the grade
	 */
	public static char getGrade() {
		if ((float)totalScore/(float)maxScore > 0.80f) {
			motivationalMessage = "You are amazing! Keep it up";
			return 'A';
		} else if((float)totalScore/(float)maxScore > 0.60f) {
			motivationalMessage = "B is for Burger King. Where you are going to work for the rest of your life!";
			return 'B';
		} else if((float)totalScore/(float)maxScore > 0.50f) {
			motivationalMessage = "Seaworld? Why not A-World?";
			return 'C';
		} else if((float)totalScore/(float)maxScore > 0.40f) {
			motivationalMessage = "You have C-Cup? Why not A-Cup?";
			return 'D';
		} else {
			motivationalMessage = "If at first you don't succeed, don't come back home.";
			return 'U';
		}
	}

	/**
	 * Adds the question statistics.
	 *
	 * @param questionNumber the question number
	 * @param isCorrect the is correct
	 * @param timeTaken the time taken
	 * @param scoretoAdd the score to add
	 */
	public static void addQuestionStat(int questionNumber, boolean isCorrect, long timeTaken, float scoretoAdd) {
		questionStats.add("Question " + questionNumber + "\t Correct: " + isCorrect + "  Time Taken: " + timeTaken + " secs  Score Added: " + scoretoAdd);
	}
	
	/**
	 * Gets the question statistics.
	 *
	 * @return the question statistics
	 */
	public static ArrayList<String> getQuestionStat() {
		return questionStats;
	}
}

