import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Core.Reader;
import JPanels.CircuitMaker;
import JPanels.DifficultySelect;
import JPanels.WelcomeWindow;
import JPanels.Misc.CardPanel;


// TODO: Auto-generated Javadoc
/**
 * The Main Window. Where everything is ran from
 *
 * @author DolanMiu
 * @version 0.1Alpha
 */
public class MainWindow extends JFrame {
	
	/** The content pane. */
	private JPanel contentPane;
	//private JPanel cardPanel;

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();		//Creates a new frame to display with JPanels to contain the Quiz
					frame.pack();								//and sets the frame properties
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		File folder = new File("C:\\electric\\");
		File[] listOfFiles = folder.listFiles();
		Reader reader = new Reader();										//The Reader class is the XML parser
		for (File listOfFile : listOfFiles) {
		    if (listOfFile.isFile()) {
		    	//UNCOMMENT THIS AT FINAL
		        /*try {														//When the object is constructed, it sets the directory of the file
		        	reader.scanFile(listOfFile.getAbsolutePath());			//Calls the scanFile method to process the file
		        } catch(Exception e) {}		*/								//If it fails, it will catch to prevent error
		    }
		}
		
        try {										
    		//reader.scanFile("electricalcircuit.xml");		
        	reader.scanFile("electricaloutput.xml");
        } catch(Exception e) {}	

        setTitle("Who wants to be Electricuted");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setMinimumSize(new Dimension(800,600));
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		CardPanel cardPanel = new CardPanel();						//I am using various cards to display the various JPanels
		WelcomeWindow welcome = new WelcomeWindow(cardPanel);		//Each of these creates a new JPanel which is then
		DifficultySelect diffSelect = new DifficultySelect(cardPanel);
        cardPanel.add(welcome, "welcome");
        
        CircuitMaker circuitMaker = new CircuitMaker();				//added to CardPanel

       
        
        cardPanel.add(circuitMaker, "circuitMaker");				//This is where it's added. Only the relevant ones are added for now
        cardPanel.add(diffSelect, "diffSelect");					//circuitMaker (Teacher), diffSelect (Student)

		
		contentPane.add(cardPanel);
	}
}
