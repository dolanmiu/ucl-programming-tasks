package ElectricalComponents;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

/**
 * The Electrical Component Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public abstract class ElectricalComponent extends DraggableComponent {
	
	/** The resistance. */
	protected float resistance = 0;
	
	/** The volts across. */
	protected float voltsAcross = 0;
	
	/** The x. */
	protected int x = 0;
	
	/** The y. */
	protected int y = 0;
	
	/** The voltage. */
	protected float voltage = 0;
	
	/** The current. */
	protected float current = 0;
	
	/** The text answer. */
	protected String textAnswer = ""; //for word type questions
	
	/**
	 * Instantiates a new electrical component.
	 */
	public ElectricalComponent() {
		paintName();
		//paintVoltsAcross();
	}
	
	/**
	 * Sets the text answer.
	 *
	 * @param text The new text answer
	 */
	public void setTextAnswer(String text) {
		textAnswer = text;
	}
	
	/**
	 * Gets the text answer.
	 *
	 * @return The text answer
	 */
	public String getTextAnswer() {
		return textAnswer;
	}
	
	/**
	 * Sets the resistance.
	 *
	 * @param res the new resistance
	 */
	public void setResistance(float res) {
		this.resistance = res;
	}
	
	/**
	 * Gets the resistance.
	 *
	 * @return the resistance
	 */
	public float getResistance() {
		return resistance;
	}
	
	/**
	 * Gets the voltage.
	 *
	 * @return the voltage. By default, unless it is overwritten, it will return -1 (e.g. resistors do not emit voltage).
	 */
	public float getVoltage() {
		return -1f;
	}
	
	/**
	 * Checks for voltage.
	 *
	 * @return true, if successful
	 */
	public boolean hasVoltage() {
		return false;
	}
	
	/**
	 * Sets the voltage.
	 *
	 * @param voltage the new voltage
	 */
	public void setVoltage(float voltage) {}
	
	/**
	 * Gets the current.
	 *
	 * @return the current. By default, unless it is overwritten, it will return -1 (e.g. resistors do not emit current).
	 */
	public float getCurrent() {
		return -1f;
	}
	
	/**
	 * Sets the current.
	 *
	 * @param voltage the new current
	 */
	public void setCurrent(float voltage) {}
	
	/**
	 * Paint name label.
	 */
	protected void paintName() {
		JLabel name = new JLabel("");
		name.setText(toString());
		name.setBackground(Color.WHITE);
		name.setOpaque(true);
		add(name);
		name.setSize(name.getPreferredSize());
		name.setLocation(0,50);
	}
	
	/**
	 * Paint volts across label.
	 */
	protected void paintVoltsAcross() {
		JLabel voltageLabel = new JLabel("");
		voltageLabel.setText(voltsAcross + " V");
		voltageLabel.setBackground(Color.WHITE);
		voltageLabel.setOpaque(true);
		add(voltageLabel);
		voltageLabel.setSize(voltageLabel.getPreferredSize());
		voltageLabel.setLocation(getWidth()-voltageLabel.getWidth(),10);
	}
	
	/**
	 * Paint resistance label.
	 */
	protected void paintResistance() { 						//The children can call this function
		JLabel resistanceLabel = new JLabel("");
		resistanceLabel.setText(resistance + " Ohms");
		resistanceLabel.setBackground(Color.WHITE);
		resistanceLabel.setOpaque(true);
		add(resistanceLabel);
		resistanceLabel.setSize(resistanceLabel.getPreferredSize());
		resistanceLabel.setLocation(0,65);
	}
	
	/**
	 * Check answer collision.
	 *
	 * @param answers The answers which the electrical component collides with
	 * @return the electrical component
	 */
	public ElectricalComponent checkAnsCollision(ArrayList<ElectricalComponent> answers) {
		return null;
	}
	
	/**
	 * Sets the border active.
	 */
	public void setBorderActive() {
		setBorder(new LineBorder(Color.WHITE, 3));
	}
	
	/**
	 * Sets the border not active.
	 */
	public void setBorderDeactive() {
		setBorder(new LineBorder(Color.BLACK, 3));
	}

	/**
	 * Sets the volts across.
	 *
	 * @param voltage the new volts across
	 */
	public void setVoltsAcross(float voltage) {
		this.voltsAcross = voltage;
		paintVoltsAcross();
	}
	
	/**
	 * Gets the volts across.
	 *
	 * @return the volts across
	 */
	public float getVoltsAcross() {
		return voltsAcross;
	}
}
