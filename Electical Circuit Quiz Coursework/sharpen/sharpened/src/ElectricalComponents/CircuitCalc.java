package ElectricalComponents;

import java.util.ArrayList;


/**
 * The Circuit Calculator Class.
 * This Class does all the resistance, voltage and current calculations of the circuit.
 * 
 * @author DolanMiu
 * @version 1.0
 */
public class CircuitCalc {
	
	/** The correct voltage. */
	private static float correctVoltage;
	
	/** The correct current. */
	private static float correctCurrent;
	
	/**
	 * Calculate resistance.
	 *
	 * @param modules the modules
	 * @return the resistance
	 */
	public static float calcResistance(ArrayList<Module> modules) {                                         
		int ammount = modules.size();
		float totalResistance = 0;
		for (int i = 0; i < ammount; i++) {
			totalResistance += calcParallelResistance(modules.get(i));
		}
		return totalResistance;
	}
	
	/**
	 * Sets the valid circuit.
	 *
	 * @param modules modules in the circuit.
	 */
	public static void setValidCircuit(ArrayList<Module> modules) {
		correctVoltage = calcTotalCellVoltage(modules);
		correctCurrent = calcTotalCellCurrent(modules);
	}
	
	/**
	 * Check valid circuit.
	 *
	 * @param modules the modules of the new circuit.
	 * @return true, if successful
	 */
	public static boolean checkValidCircuit(ArrayList<Module> modules) {
		float currentVoltage = CircuitCalc.calcTotalCellVoltage(modules);
		float currentCurrent = CircuitCalc.calcTotalCellCurrent(modules);
		System.out.println("current Voltage" + currentVoltage + " correct Voltage" + correctVoltage);
		System.out.println("current Current" + currentCurrent + " correct Current" + correctCurrent);
		if (currentVoltage == correctVoltage && currentCurrent == correctCurrent) {
			return true;
		}
		return false;
	}
	
	/**
	 * Calculate current.
	 *
	 * @param cell the cell
	 * @return the current
	 */
	public static float calcCurrent(Cell cell) {
		return cell.getCurrent();
	}
	
	/**
	 * Calculate total cell voltage.
	 *
	 * @param modules the modules of circuit
	 * @return the voltage
	 */
	public static float calcTotalCellVoltage(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = getCells(modules);
		float R = calcResistance(modules);
		float I = getCellsCurrent(cells);
		float V = I * R;
		return V;
	}
	
	/**
	 * Calculate total cell current.
	 *
	 * @param modules the modules of circuit
	 * @return the current
	 */
	public static float calcTotalCellCurrent(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = getCells(modules);
		float R = calcResistance(modules);
		float V = getCellsVoltage(cells);
		float I = (V / R);
		return I;
	}
	
	/**
	 * Gets the cells voltage.
	 *
	 * @param cells the cells
	 * @return the cells voltage
	 */
	public static float getCellsVoltage(ArrayList<ElectricalComponent> cells) {
		float totalCurrent = 0;
		for (int i = 0; i < cells.size(); i++) {
			totalCurrent += cells.get(i).getVoltage();
		}
		return totalCurrent;
	}
	
	/**
	 * Gets the cells current.
	 *
	 * @param cells the cells
	 * @return the cells current
	 */
	public static float getCellsCurrent(ArrayList<ElectricalComponent> cells) {
		float totalCurrent = 0;
		for (int i = 0; i < cells.size(); i++) {
			totalCurrent += cells.get(i).getCurrent();
		}
		return totalCurrent;
	}
	
	/**
	 * Gets the cells.
	 *
	 * @param modules the modules
	 * @return the cells
	 */
	public static ArrayList<ElectricalComponent> getCells(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = new ArrayList<ElectricalComponent>();
		for (int i = 0; i < modules.size(); i++) {
			Module module = modules.get(i);
			ArrayList<ElectricalComponent> comps = module.returnComponents();
			for (int j = 0; j < comps.size(); j++) {
				ElectricalComponent comp = comps.get(j);
				if (comp.toString().equals("Cell") || comp.hasVoltage()) {
					cells.add(comp);
				}
			}
		}
		return cells;
	}
	
	/**
	 * Calculate parallel resistance.
	 *
	 * @param module the module
	 * @return The Total Resistance
	 */
	public static float calcParallelResistance(Module module) {
		int ammount = module.getComponentAmmount();
		float totalConductance = 0;
		for (int i = 0; i < ammount; i++) {
			ElectricalComponent comp = module.getComponant(i);
			totalConductance += 1f/comp.getResistance();
		}
		if (Float.isInfinite(totalConductance)) {
			//System.err.println("Conductance is infinate");
		}
		float totalResistance = 1f/totalConductance;
		return totalResistance;
	}
}
