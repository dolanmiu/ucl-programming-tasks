package ElectricalComponents;

// TODO: Auto-generated Javadoc
/**
 * The Resistor Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Resistor extends ElectricalComponent {
	
	/**
	 * Instantiates a new resistor.
	 *
	 * @param resistance the resistance
	 * @param x the x
	 * @param y the y
	 */
	public Resistor(float resistance, int x, int y) {
		this.resistance = resistance;
		this.x = x;
		this.y = y;
		this.setLocation(x, y);
		
		paintResistance();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return "Resistor";
	}
}
