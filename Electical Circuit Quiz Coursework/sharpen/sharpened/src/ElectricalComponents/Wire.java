package ElectricalComponents;

import java.awt.Graphics;

import javax.swing.JComponent;

/**
 * The Wire Class
 * Used to connected two modules together.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Wire extends JComponent {

	/** The connected to. */
	private Module connectedTo;
	
	/** The connected from. */
	private Module connectedFrom;
	
	/**
	 * Instantiates a new wire.
	 *
	 * @param connectedFrom the module connected from
	 * @param connectedTo the module connected to
	 */
	public Wire(Module connectedFrom, Module connectedTo) {
		this.connectedTo = connectedTo;
		this.connectedFrom = connectedFrom;
		setBounds(0, 0, 1000, 1000);
		if (this.connectedTo.getX() == this.connectedFrom.getX()) { 						//i.e normal
			this.connectedFrom.setReversed(false);
			this.connectedTo.setReversed(true);
		}
	}
	
	/**
	 * Gets the module to.
	 *
	 * @return the module to
	 */
	public Module getTo() {
		return connectedTo;
	}
	
	/**
	 * Gets the module from.
	 *
	 * @return the module from
	 */
	public Module getFrom() {
		return connectedFrom;
	}
	
	/**
	 * Prints the connection.
	 */
	public void printConnection() {
		System.out.println("Connected From " + connectedFrom.toString());
		System.out.println("Connected To " + connectedTo.toString());
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int xcoordTo = connectedTo.getX();										//The left of the module
		int ycoordTo = connectedTo.getY() + connectedTo.getHeight()/2;			//The left of the module
		int xcoordFrom = connectedFrom.getX() + connectedFrom.getWidth();		//The right of the module
		int ycoordFrom = connectedFrom.getY() + connectedFrom.getHeight()/2;	//The right of the module
		//System.out.println(connectedTo.getX() + " " + connectedFrom.getX());
		if (connectedTo.getX() > connectedFrom.getX()) { 						//i.e normal
			connectedFrom.setReversed(false);
		}
		
		if (connectedTo.getX() < connectedFrom.getX()) {						//i.e backwards
			connectedFrom.setReversed(true);
		}
		
		if (!connectedFrom.isReversed() && !connectedTo.isReversed()) {
			//g.drawLine(xcoordFrom,ycoordFrom,xcoordTo,ycoordTo); //normal
			g.drawLine(xcoordFrom,ycoordFrom,xcoordTo,ycoordFrom);
			g.drawLine(xcoordTo,ycoordFrom,xcoordTo,ycoordTo);
		}
		
		if (connectedFrom.isReversed() && connectedTo.isReversed()) {
			//g.drawLine(connectedFrom.getX(),ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordTo); // backwards chain
			g.drawLine(connectedFrom.getX(),ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordFrom);
			g.drawLine(xcoordTo+connectedTo.getWidth(),ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordTo);
		}
		
		if (!connectedFrom.isReversed() && connectedTo.isReversed()) {
			//g.drawLine(xcoordFrom,ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordTo); //corner
			g.drawLine(xcoordFrom-1,ycoordFrom,xcoordTo+connectedTo.getWidth()-1,ycoordFrom);
			g.drawLine(xcoordTo+connectedTo.getWidth()-1,ycoordFrom,xcoordTo+connectedTo.getWidth()-1,ycoordTo);
			repaint();
		}
		
		if(connectedFrom.isReversed() && !connectedTo.isReversed()) {
			//g.drawLine(connectedFrom.getX(),ycoordFrom,xcoordTo,ycoordTo); // back to start
			g.drawLine(connectedFrom.getX(),ycoordFrom, xcoordTo,ycoordFrom);
			g.drawLine(xcoordTo,ycoordFrom, xcoordTo,ycoordTo);
		}	
	}
}
