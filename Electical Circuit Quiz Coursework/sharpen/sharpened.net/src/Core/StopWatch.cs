using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace Core
{
	/// <summary>The Stop Watch Class.</summary>
	/// <remarks>The Stop Watch Class. It is the timer commonly found in questions.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class StopWatch : JComponent, ActionListener
	{
		/// <summary>The max time.</summary>
		/// <remarks>The max time.</remarks>
		private readonly long maxTime;

		/// <summary>The time left.</summary>
		/// <remarks>The time left.</remarks>
		public long timeLeft;

		/// <summary>The checkpoint.</summary>
		/// <remarks>The checkpoint.</remarks>
		private long checkpoint;

		/// <summary>The timer.</summary>
		/// <remarks>The timer.</remarks>
		private Timer timer;

		/// <summary>Instantiates a new stop watch.</summary>
		/// <remarks>Instantiates a new stop watch.</remarks>
		/// <param name="time">the time for the watch to tick for</param>
		public StopWatch(int time)
		{
			//golfb.org/stopwatchjava.html
			this.timeLeft = time;
			this.maxTime = time;
			SetCheckpoint();
			timer = new Timer(1000, this);
			//every 1000 millisecs activate the timer
			timer.Start();
			//starts the timer
			SetBounds(0, 0, 80, 80);
			this.SetPreferredSize(new Dimension(80, 80));
		}

		/// <summary>Reset.</summary>
		/// <remarks>Reset.</remarks>
		public virtual void Reset()
		{
			timeLeft = maxTime;
			timer.Start();
		}

		/// <summary>Start.</summary>
		/// <remarks>Start.</remarks>
		public virtual void Start()
		{
			timer.Start();
		}

		/// <summary>Stop.</summary>
		/// <remarks>Stop.</remarks>
		public virtual void Stop()
		{
			timer.Stop();
		}

		/// <summary>Gets the max time.</summary>
		/// <remarks>Gets the max time.</remarks>
		/// <returns>the max time</returns>
		public virtual long GetMaxTime()
		{
			return maxTime;
		}

		/// <summary>Sets the time checkpoint.</summary>
		/// <remarks>Sets the time checkpoint.</remarks>
		public virtual void SetCheckpoint()
		{
			this.checkpoint = timeLeft;
		}

		/// <summary>Gets the time checkpoint.</summary>
		/// <remarks>Gets the time checkpoint.</remarks>
		/// <returns>the checkpoint</returns>
		public virtual long GetCheckpoint()
		{
			return checkpoint;
		}

		/// <summary>Gets the time left.</summary>
		/// <remarks>Gets the time left.</remarks>
		/// <returns>the time left</returns>
		public virtual long GetTimeLeft()
		{
			return timeLeft;
		}

		/// <summary>Gets the elapsed time.</summary>
		/// <remarks>Gets the elapsed time.</remarks>
		/// <returns>the elapsed time</returns>
		public virtual long GetElapsedTime()
		{
			long elapsed = (maxTime - timeLeft);
			return elapsed;
		}

		protected override void PaintComponent(Graphics g)
		{
			//http://harryjoy.com/2012/05/20/circular-progress-bar-in-java-swing/
			Graphics2D g2 = (Graphics2D)g;
			g2.SetRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON
				);
			if (timeLeft <= maxTime)
			{
				g.SetColor(new Color(unchecked((int)(0x636363))));
				int angle = -(int)(((float)timeLeft / maxTime) * 360);
				g.FillArc(0, 0, GetWidth(), GetHeight(), 90, angle);
			}
		}

		public virtual void ActionPerformed(ActionEvent e)
		{
			//this
			timeLeft -= 1;
			//which decrements the time.
			Repaint();
			if (timeLeft == 0)
			{
				timer.Stop();
			}
		}
	}
}
