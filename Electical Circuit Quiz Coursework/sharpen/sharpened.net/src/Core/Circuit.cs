using ElectricalComponents;
using Sharpen;

namespace Core
{
	/// <summary>The Class Circuit.</summary>
	/// <remarks>The Class Circuit.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class Circuit
	{
		/// <summary>The modules.</summary>
		/// <remarks>The modules.</remarks>
		internal AList<Module> modules = new AList<Module>();

		/// <summary>The wires.</summary>
		/// <remarks>The wires.</remarks>
		internal AList<Wire> wires = new AList<Wire>();

		/// <summary>Instantiates a new circuit.</summary>
		/// <remarks>Instantiates a new circuit.</remarks>
		public Circuit()
		{
		}

		//this arraylist is storing the modules
		//This arraylist stores the wires
		/// <summary>Sets the module voltages.</summary>
		/// <remarks>Sets the module voltages.</remarks>
		public virtual void SetModuleVoltages()
		{
			AList<ElectricalComponent> cells = new AList<ElectricalComponent>();
			float totalVoltage = 0;
			cells = CircuitCalc.GetCells(modules);
			for (int i = 0; i < cells.Count; i++)
			{
				totalVoltage += cells[i].GetVoltage();
			}
			for (int i_1 = 0; i_1 < modules.Count; i_1++)
			{
				modules[i_1].SetVoltsAcross(totalVoltage);
			}
		}

		/// <summary>Removes the all wires.</summary>
		/// <remarks>Removes the all wires.</remarks>
		public virtual void RemoveAllWires()
		{
			wires.Clear();
		}

		/// <summary>Checks for wire existence.</summary>
		/// <remarks>Checks for wire existence.</remarks>
		/// <param name="module1">First module</param>
		/// <param name="module2">second module</param>
		/// <returns>true, if successful</returns>
		public virtual bool HasWire(Module module1, Module module2)
		{
			for (int i = 0; i < wires.Count; i++)
			{
				if (wires[i].GetTo() == module1 && wires[i].GetFrom() == module2)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Gets the modules.</summary>
		/// <remarks>Gets the modules.</remarks>
		/// <returns>the modules</returns>
		public virtual AList<Module> GetModules()
		{
			return modules;
		}

		/// <summary>Adds the module.</summary>
		/// <remarks>Adds the module.</remarks>
		/// <param name="module">the module</param>
		public virtual void AddModule(Module module)
		{
			modules.AddItem(module);
		}

		/// <summary>Removes the module.</summary>
		/// <remarks>Removes the module.</remarks>
		/// <param name="i">the i</param>
		public virtual void RemoveModule(int i)
		{
			modules.Remove(i);
		}

		/// <summary>Adds the wires.</summary>
		/// <remarks>Adds the wires.</remarks>
		/// <param name="wire">the wire</param>
		public virtual void AddWires(Wire wire)
		{
			wires.AddItem(wire);
		}

		/// <summary>Gets the wires.</summary>
		/// <remarks>Gets the wires.</remarks>
		/// <returns>the wires</returns>
		public virtual AList<Wire> GetWires()
		{
			return wires;
		}

		/// <summary>Prints the module ammount.</summary>
		/// <remarks>Prints the module ammount.</remarks>
		public virtual void PrintModuleAmmount()
		{
			System.Console.Out.WriteLine(modules.Count + " Modules in this circuit.");
		}
	}
}
