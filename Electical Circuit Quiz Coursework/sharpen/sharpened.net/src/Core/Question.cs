using Core;
using ElectricalComponents;
using Sharpen;

namespace Core
{
	/// <summary>The question class.</summary>
	/// <remarks>The question class. Every question is created from this class. It contains the circuit, its question and its respective answer.
	/// 	</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class Question
	{
		/// <summary>The answers.</summary>
		/// <remarks>The answers.</remarks>
		public AList<ElectricalComponent> answers = new AList<ElectricalComponent>();

		/// <summary>The circuit.</summary>
		/// <remarks>The circuit.</remarks>
		public Circuit circuit;

		/// <summary>The difficulty.</summary>
		/// <remarks>The difficulty.</remarks>
		public string diff;

		/// <summary>The numb.</summary>
		/// <remarks>The numb.</remarks>
		public int numb;

		/// <summary>The text.</summary>
		/// <remarks>The text.</remarks>
		public string text;

		/// <summary>The type.</summary>
		/// <remarks>The type.</remarks>
		public string type;

		/// <summary>Instantiates a new question.</summary>
		/// <remarks>Instantiates a new question.</remarks>
		/// <param name="type">the type</param>
		/// <param name="circuit">the circuit</param>
		/// <param name="diff">the difficulty</param>
		/// <param name="numb">the numb</param>
		/// <param name="text">the question text</param>
		/// <param name="answers">the answers</param>
		public Question(string type, Circuit circuit, string diff, int numb, string text, 
			AList<ElectricalComponent> answers)
		{
			this.circuit = circuit;
			this.diff = diff;
			this.numb = numb;
			this.text = text;
			this.answers = answers;
			this.type = type;
		}

		/// <summary>Gets the type.</summary>
		/// <remarks>Gets the type.</remarks>
		/// <returns>the type</returns>
		public virtual string GetType()
		{
			return type;
		}

		/// <summary>Sort out the question text.</summary>
		/// <remarks>Sort out the question text.</remarks>
		public virtual void SortOutText()
		{
			string newText = text.Replace("\\r\\n|\\r|\\n", string.Empty);
			newText = text.Replace("\n", string.Empty);
			this.text = newText;
		}

		/// <summary>Gets the circuit.</summary>
		/// <remarks>Gets the circuit.</remarks>
		/// <returns>the circuit</returns>
		public virtual Circuit GetCircuit()
		{
			return circuit;
		}

		/// <summary>Gets the question text.</summary>
		/// <remarks>Gets the question text.</remarks>
		/// <returns>the question text</returns>
		public virtual string GetText()
		{
			return text;
		}

		/// <summary>Sets the question text.</summary>
		/// <remarks>Sets the question text.</remarks>
		/// <param name="text">the new question text</param>
		public virtual void SetText(string text)
		{
			this.text = text;
		}

		/// <summary>Adds the answer.</summary>
		/// <remarks>Adds the answer.</remarks>
		/// <param name="answer">the answer</param>
		public virtual void AddAnswer(ElectricalComponent answer)
		{
			answers.AddItem(answer);
		}

		/// <summary>Gets the answers.</summary>
		/// <remarks>Gets the answers.</remarks>
		/// <returns>the answers</returns>
		public virtual AList<ElectricalComponent> GetAnswers()
		{
			return answers;
		}

		/// <summary>Removes the answer.</summary>
		/// <remarks>Removes the answer.</remarks>
		/// <param name="question">ID</param>
		public virtual void RemoveAnswer(int i)
		{
			answers.Remove(i);
		}

		public override string ToString()
		{
			return "Question: " + numb + " Difficulty: " + diff;
		}
	}
}
