using System.IO;
using Com.Sun.Org.Apache.Xml.Internal.Serialize;
using Core;
using ElectricalComponents;
using Javax.Xml.Parsers;
using Org.W3c.Dom;
using Sharpen;

namespace Core
{
	/// <summary>The Writer Class.</summary>
	/// <remarks>The Writer Class. This Writes from Object to XML</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class Writer
	{
		/// <summary>The question list.</summary>
		/// <remarks>The question list.</remarks>
		internal AList<Question> questionList = new AList<Question>();

		/// <summary>Instantiates a new writer.</summary>
		/// <remarks>Instantiates a new writer.</remarks>
		public Writer()
		{
		}

		/// <summary>Write file.</summary>
		/// <remarks>Write file.</remarks>
		/// <param name="path">the path to file</param>
		public virtual void WriteFile(string path)
		{
			questionList = Reader.questionList;
			try
			{
				WriteQuestions(path);
			}
			catch (ParserConfigurationException)
			{
				System.Console.Error.WriteLine("The Writer has a Parser Config Exception");
			}
			catch (FileNotFoundException)
			{
				System.Console.Error.WriteLine("The Writer cannot find the file");
			}
			catch (IOException)
			{
				System.Console.Error.WriteLine("The Writer has an IO Exception");
			}
		}

		/// <summary>Writes the questions into file.</summary>
		/// <remarks>Writes the questions into file.</remarks>
		/// <param name="path">the path to file</param>
		/// <exception cref="Javax.Xml.Parsers.ParserConfigurationException">the parser configuration exception
		/// 	</exception>
		/// <exception cref="System.IO.IOException">Signals that an I/O exception has occurred.
		/// 	</exception>
		/// <exception cref="System.IO.FileNotFoundException">the file not found exception</exception>
		public virtual void WriteQuestions(string path)
		{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.NewInstance();
			DocumentBuilder docBuilder = docFactory.NewDocumentBuilder();
			Document xmlDoc = docBuilder.NewDocument();
			Element rootElement = xmlDoc.CreateElement("quiz");
			for (int i = 0; i < questionList.Count; i++)
			{
				Element questionElement = xmlDoc.CreateElement("question");
				rootElement.AppendChild(questionElement);
				questionElement.SetAttribute("difficulty", questionList[i].diff);
				questionElement.SetAttribute("number", questionList[i].numb + string.Empty);
				questionElement.SetAttribute("type", questionList[i].GetType());
				Element answers = xmlDoc.CreateElement("answers");
				questionElement.AppendChild(answers);
				Element circuit = xmlDoc.CreateElement("circuit");
				questionElement.AppendChild(circuit);
				HandleCircuit(circuit, questionList[i], xmlDoc);
				questionElement.AppendChild(SetTagValue("text", questionList[i].GetText(), xmlDoc
					));
				AList<ElectricalComponent> comps = questionList[i].GetAnswers();
				HandleAnswers(answers, comps, xmlDoc);
			}
			xmlDoc.AppendChild(rootElement);
			OutputFormat outFormat = new OutputFormat(xmlDoc);
			outFormat.SetIndenting(true);
			FilePath xmlFile = new FilePath(path);
			FileOutputStream outStream = new FileOutputStream(xmlFile);
			XMLSerializer serializer = new XMLSerializer(outStream, outFormat);
			serializer.Serialize(xmlDoc);
		}

		/// <summary>Handle circuit.</summary>
		/// <remarks>Handle circuit.</remarks>
		/// <param name="circuit">the circuit</param>
		/// <param name="currentQ">the current question</param>
		/// <param name="xmlDoc">the XML document</param>
		private void HandleCircuit(Element circuit, Question currentQ, Document xmlDoc)
		{
			AList<Module> modules = currentQ.circuit.GetModules();
			for (int i = 0; i < modules.Count; i++)
			{
				Element module = xmlDoc.CreateElement("module");
				circuit.AppendChild(module);
				HandleModules(module, modules[i], xmlDoc);
			}
		}

		/// <summary>Handle modules.</summary>
		/// <remarks>Handle modules.</remarks>
		/// <param name="eModule">the element of the module</param>
		/// <param name="module">the module</param>
		/// <param name="xmlDoc">the XML doc</param>
		private void HandleModules(Element eModule, Module module, Document xmlDoc)
		{
			AList<ElectricalComponent> comps = module.ReturnComponents();
			for (int i = 0; i < comps.Count; i++)
			{
				Element comp = xmlDoc.CreateElement("componant");
				eModule.AppendChild(comp);
				HandleComponent(comp, comps[i], xmlDoc);
			}
		}

		/// <summary>Handle answers.</summary>
		/// <remarks>Handle answers.</remarks>
		/// <param name="eAnswers">the element of the answers</param>
		/// <param name="answers">the answers</param>
		/// <param name="xmlDoc">the XML doc</param>
		private void HandleAnswers(Element eAnswers, AList<ElectricalComponent> answers, 
			Document xmlDoc)
		{
			for (int i = 0; i < answers.Count; i++)
			{
				Element eComp = xmlDoc.CreateElement("componant");
				eAnswers.AppendChild(eComp);
				eComp.AppendChild(SetTagValue("name", answers[i].ToString(), xmlDoc));
				if (answers[i].GetResistance() != 0)
				{
					eComp.AppendChild(SetTagValue("resistance", answers[i].GetResistance() + string.Empty
						, xmlDoc));
				}
				if (answers[i].GetVoltage() != -1f)
				{
					eComp.AppendChild(SetTagValue("voltage", answers[i].GetVoltage() + string.Empty, 
						xmlDoc));
				}
				if (answers[i].GetCurrent() != -1f)
				{
					eComp.AppendChild(SetTagValue("current", answers[i].GetCurrent() + string.Empty, 
						xmlDoc));
				}
				if (!answers[i].GetTextAnswer().Equals(string.Empty))
				{
					eComp.AppendChild(SetTagValue("answer", answers[i].GetTextAnswer(), xmlDoc));
				}
				eComp.AppendChild(SetTagValue("x", answers[i].GetX() + string.Empty, xmlDoc));
				eComp.AppendChild(SetTagValue("y", answers[i].GetY() + string.Empty, xmlDoc));
			}
		}

		/// <summary>Handle component.</summary>
		/// <remarks>Handle component.</remarks>
		/// <param name="eComp">the element of the component</param>
		/// <param name="comp">the component</param>
		/// <param name="xmlDoc">the XML doc</param>
		private void HandleComponent(Element eComp, ElectricalComponent comp, Document xmlDoc
			)
		{
			eComp.AppendChild(SetTagValue("name", comp.ToString(), xmlDoc));
			if (comp.GetResistance() != 0)
			{
				eComp.AppendChild(SetTagValue("resistance", comp.GetResistance() + string.Empty, 
					xmlDoc));
			}
			if (comp.GetVoltage() != -1f)
			{
				eComp.AppendChild(SetTagValue("voltage", comp.GetVoltage() + string.Empty, xmlDoc
					));
			}
			if (comp.GetCurrent() != -1f)
			{
				eComp.AppendChild(SetTagValue("current", comp.GetCurrent() + string.Empty, xmlDoc
					));
			}
			eComp.AppendChild(SetTagValue("x", comp.GetX() + string.Empty, xmlDoc));
			eComp.AppendChild(SetTagValue("y", comp.GetY() + string.Empty, xmlDoc));
		}

		/// <summary>Sets the tag value.</summary>
		/// <remarks>Sets the tag value.</remarks>
		/// <param name="tag">the tag to set</param>
		/// <param name="value">the value to set</param>
		/// <param name="xmlDoc">the XML doc</param>
		/// <returns>the element set.</returns>
		private Element SetTagValue(string tag, string value, Document xmlDoc)
		{
			Element attribute = xmlDoc.CreateElement(tag);
			if (!value.Equals(string.Empty))
			{
				Text attributetext = xmlDoc.CreateTextNode(value);
				attribute.AppendChild(attributetext);
			}
			return attribute;
		}
	}
}
