using Core;
using ElectricalComponents;
using Sharpen;

namespace Core
{
	/// <summary>The class which initialise components.</summary>
	/// <remarks>The class which initialise components.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class InitComponent
	{
		//This class abstracts the creation of the electrical components to make life easier.
		//It would be used in for loops mainly and especailly when reading the XML file to prevent null pointer exceptions etc
		/// <summary>Creates the electrical component.</summary>
		/// <remarks>Creates the electrical component.</remarks>
		/// <param name="name">the name</param>
		/// <param name="resistance">the resistance</param>
		/// <param name="voltage">the voltage</param>
		/// <param name="current">the current</param>
		/// <param name="x">the x</param>
		/// <param name="y">the y</param>
		/// <param name="textAnswer">the text answer</param>
		/// <returns>the electrical component</returns>
		public static ElectricalComponent Create(string name, float resistance, float voltage
			, float current, int x, int y, string textAnswer)
		{
			name = name.ToLower();
			if (name.Equals("bulb"))
			{
				//series of if  statements which can take as many of as little
				Bulb bulb = new Bulb(resistance, x, y);
				//properties of a component as possible.
				return bulb;
			}
			//for example, a resistor needs 3, whereas a cell needs 4.
			if (name.Equals("resistor"))
			{
				Resistor resistor = new Resistor(resistance, x, y);
				return resistor;
			}
			if (name.Equals("cell"))
			{
				Cell cell = new Cell(voltage, current, x, y);
				return cell;
			}
			if (name.Equals("void"))
			{
				Void _void = new Void(resistance, voltage, current, x, y, textAnswer);
				return _void;
			}
			System.Console.Error.WriteLine("Creating Null Object in InitComponent");
			return null;
		}
	}
}
