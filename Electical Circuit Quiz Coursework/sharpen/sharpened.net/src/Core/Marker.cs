using Core;
using Sharpen;

namespace Core
{
	/// <summary>The class which marks the students</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class Marker
	{
		/// <summary>The name.</summary>
		/// <remarks>The name.</remarks>
		internal static string name = string.Empty;

		/// <summary>The total score.</summary>
		/// <remarks>The total score.</remarks>
		internal static int totalScore = 0;

		/// <summary>The max score.</summary>
		/// <remarks>The max score.</remarks>
		internal static int maxScore = 100;

		/// <summary>The motivational message.</summary>
		/// <remarks>The motivational message.</remarks>
		internal static string motivationalMessage = "Nothing to report";

		/// <summary>The question statistics.</summary>
		/// <remarks>The question statistics.</remarks>
		internal static AList<string> questionStats = new AList<string>();

		/// <summary>Adds the score.</summary>
		/// <remarks>Adds the score.</remarks>
		/// <param name="scoreToAdd">the score to add</param>
		public static void AddScore(int scoreToAdd)
		{
			totalScore += scoreToAdd;
		}

		/// <summary>Subtract score.</summary>
		/// <remarks>Subtract score.</remarks>
		/// <param name="scoreToSubtract">the score to subtract</param>
		public static void SubtractScore(int scoreToSubtract)
		{
			totalScore += scoreToSubtract;
		}

		/// <summary>Reset score.</summary>
		/// <remarks>Reset score.</remarks>
		public static void ResetScore()
		{
			totalScore = 0;
		}

		/// <summary>Prints the score.</summary>
		/// <remarks>Prints the score.</remarks>
		public static void PrintScore()
		{
			System.Console.Out.WriteLine("The total score is: " + totalScore + " point(s)!");
		}

		/// <summary>Gets the score.</summary>
		/// <remarks>Gets the score.</remarks>
		/// <returns>the score</returns>
		public static int GetScore()
		{
			return totalScore;
		}

		/// <summary>Sets the name.</summary>
		/// <remarks>Sets the name.</remarks>
		/// <param name="nameInput">the new name</param>
		public static void SetName(string nameInput)
		{
			name = nameInput;
		}

		/// <summary>Gets the name.</summary>
		/// <remarks>Gets the name.</remarks>
		/// <returns>the name</returns>
		public static string GetName()
		{
			return name;
		}

		/// <summary>Gets the message.</summary>
		/// <remarks>Gets the message.</remarks>
		/// <returns>the message</returns>
		public static string GetMessage()
		{
			return motivationalMessage;
		}

		/// <summary>Gets the grade of the student.</summary>
		/// <remarks>Gets the grade of the student.</remarks>
		/// <returns>the grade</returns>
		public static char GetGrade()
		{
			if ((float)totalScore / (float)maxScore > 0.80f)
			{
				motivationalMessage = "You are amazing! Keep it up";
				return 'A';
			}
			else
			{
				if ((float)totalScore / (float)maxScore > 0.60f)
				{
					motivationalMessage = "B is for Burger King. Where you are going to work for the rest of your life!";
					return 'B';
				}
				else
				{
					if ((float)totalScore / (float)maxScore > 0.50f)
					{
						motivationalMessage = "Seaworld? Why not A-World?";
						return 'C';
					}
					else
					{
						if ((float)totalScore / (float)maxScore > 0.40f)
						{
							motivationalMessage = "You have C-Cup? Why not A-Cup?";
							return 'D';
						}
						else
						{
							motivationalMessage = "If at first you don't succeed, don't come back home.";
							return 'U';
						}
					}
				}
			}
		}

		/// <summary>Adds the question statistics.</summary>
		/// <remarks>Adds the question statistics.</remarks>
		/// <param name="questionNumber">the question number</param>
		/// <param name="isCorrect">the is correct</param>
		/// <param name="timeTaken">the time taken</param>
		/// <param name="scoretoAdd">the score to add</param>
		public static void AddQuestionStat(int questionNumber, bool isCorrect, long timeTaken
			, float scoretoAdd)
		{
			questionStats.AddItem("Question " + questionNumber + "\t Correct: " + isCorrect +
				 "  Time Taken: " + timeTaken + " secs  Score Added: " + scoretoAdd);
		}

		/// <summary>Gets the question statistics.</summary>
		/// <remarks>Gets the question statistics.</remarks>
		/// <returns>the question statistics</returns>
		public static AList<string> GetQuestionStat()
		{
			return questionStats;
		}
	}
}
