using Core;
using ElectricalComponents;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JDialogs
{
	/// <summary>
	/// The Create Question Dialog Class
	/// A dialog for the creation of a question.
	/// </summary>
	/// <remarks>
	/// The Create Question Dialog Class
	/// A dialog for the creation of a question.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CreateQuestion : JDialog
	{
		/// <summary>The question type combo box items.</summary>
		/// <remarks>The question type combo box items.</remarks>
		private string[] questionTypeComboBoxItems = new string[] { "Drag and Drop", "Text"
			, "Gesture" };

		/// <summary>The difficulty combo box items.</summary>
		/// <remarks>The difficulty combo box items.</remarks>
		private string[] difficultyComboBoxItems = new string[] { "Easy", "Medium", "Hard"
			 };

		/// <summary>The question type.</summary>
		/// <remarks>The question type.</remarks>
		private JComboBox questionType;

		/// <summary>The difficulty.</summary>
		/// <remarks>The difficulty.</remarks>
		private JComboBox difficulty;

		/// <summary>The number.</summary>
		/// <remarks>The number.</remarks>
		private HintTextArea number;

		/// <summary>The created result.</summary>
		/// <remarks>The created result.</remarks>
		internal Question baby = null;

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		public CreateQuestion()
		{
			questionType = new JComboBox(questionTypeComboBoxItems);
			difficulty = new JComboBox(difficultyComboBoxItems);
			SetMinimumSize(new Dimension(200, 300));
			this.SetLocation(400, 100);
			GetContentPane().SetLayout(new BoxLayout(GetContentPane(), BoxLayout.Y_AXIS));
			GetContentPane().Add(questionType);
			questionType.SetMaximumSize(new Dimension(int.MaxValue, 30));
			Component rigidArea = Box.CreateRigidArea(new Dimension(0, 30));
			GetContentPane().Add(rigidArea);
			GetContentPane().Add(difficulty);
			difficulty.SetMaximumSize(new Dimension(int.MaxValue, 30));
			Component rigidArea2 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea2);
			number = new HintTextArea("Question Number...");
			Javax.Swing.Border.Border origBorder = number.GetBorder();
			number.AddKeyListener(new _KeyAdapter_73(this, origBorder));
			GetContentPane().Add(number);
			number.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Component rigidArea3 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea3);
			JPanel panel = new JPanel();
			GetContentPane().Add(panel);
			panel.SetLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			JButton btnCreateComponent = new JButton("Create Question");
			panel.Add(btnCreateComponent);
			btnCreateComponent.SetIcon(new ImageIcon(typeof(JPanels.JDialogs.CreateQuestion).
				GetResource("/resources/icons/Very_Basic/checkmark.png")));
			btnCreateComponent.AddMouseListener(new _MouseAdapter_91(this));
			SetEnabled(true);
			SetModal(true);
			SetVisible(true);
		}

		private sealed class _KeyAdapter_73 : KeyAdapter
		{
			public _KeyAdapter_73(CreateQuestion _enclosing, Javax.Swing.Border.Border origBorder
				)
			{
				this._enclosing = _enclosing;
				this.origBorder = origBorder;
			}

			public override void KeyPressed(KeyEvent arg0)
			{
				this._enclosing.number.SetBorder(origBorder);
			}

			private readonly CreateQuestion _enclosing;

			private readonly Javax.Swing.Border.Border origBorder;
		}

		private sealed class _MouseAdapter_91 : MouseAdapter
		{
			public _MouseAdapter_91(CreateQuestion _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				if (this._enclosing.InputValidation())
				{
					Circuit circuit = new Circuit();
					AList<ElectricalComponent> answers = new AList<ElectricalComponent>();
					this._enclosing.baby = new Question(this._enclosing.GetQuestionType(), circuit, this
						._enclosing.GetDifficulty(), this._enclosing.GetNumber(), string.Empty, answers);
					this._enclosing.SetVisible(false);
				}
			}

			private readonly CreateQuestion _enclosing;
		}

		/// <summary>Gets the question.</summary>
		/// <remarks>Gets the question.</remarks>
		/// <returns>the question</returns>
		public virtual Question GetQuestion()
		{
			return baby;
		}

		/// <summary>Input validation.</summary>
		/// <remarks>Input validation.</remarks>
		/// <returns>true, if successful</returns>
		private bool InputValidation()
		{
			if (!number.GetText().Equals(string.Empty))
			{
				return true;
			}
			number.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
				)))));
			return false;
		}

		/// <summary>Gets the question type.</summary>
		/// <remarks>Gets the question type.</remarks>
		/// <returns>the question type</returns>
		public virtual string GetQuestionType()
		{
			return questionType.GetSelectedItem().ToString();
		}

		/// <summary>Gets the difficulty.</summary>
		/// <remarks>Gets the difficulty.</remarks>
		/// <returns>the difficulty</returns>
		public virtual string GetDifficulty()
		{
			return difficulty.GetSelectedItem().ToString();
		}

		/// <summary>Gets the number.</summary>
		/// <remarks>Gets the number.</remarks>
		/// <returns>the number</returns>
		public virtual int GetNumber()
		{
			return System.Convert.ToInt32(number.GetText());
		}
	}
}
