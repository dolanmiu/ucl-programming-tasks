using System;
using Core;
using ElectricalComponents;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JDialogs
{
	/// <summary>
	/// The Create Component Dialog Class
	/// Opens up a dialog to create components (answers are also components)
	/// </summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CreateComponent : JDialog
	{
		/// <summary>The component type combo box items.</summary>
		/// <remarks>The component type combo box items.</remarks>
		private string[] componentTypeComboBoxItems = new string[] { "Bulb", "Resistor", 
			"Cell", "Void" };

		/// <summary>The component type.</summary>
		/// <remarks>The component type.</remarks>
		private JComboBox componentType;

		/// <summary>The resistance.</summary>
		/// <remarks>The resistance.</remarks>
		private HintTextArea resistance;

		/// <summary>The voltage.</summary>
		/// <remarks>The voltage.</remarks>
		private HintTextArea voltage;

		/// <summary>The current.</summary>
		/// <remarks>The current.</remarks>
		private HintTextArea current;

		/// <summary>The created result.</summary>
		/// <remarks>The created result.</remarks>
		internal ElectricalComponent baby = null;

		/// <summary>The text answer field.</summary>
		/// <remarks>The text answer field.</remarks>
		private JTextField textAnswerField;

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		public CreateComponent()
		{
			componentType = new JComboBox(componentTypeComboBoxItems);
			SetMinimumSize(new Dimension(200, 300));
			this.SetLocation(400, 100);
			GetContentPane().SetLayout(new BoxLayout(GetContentPane(), BoxLayout.Y_AXIS));
			GetContentPane().Add(componentType);
			componentType.SetMaximumSize(new Dimension(int.MaxValue, 30));
			Component rigidArea = Box.CreateRigidArea(new Dimension(0, 30));
			GetContentPane().Add(rigidArea);
			resistance = new HintTextArea("Resistance...");
			Javax.Swing.Border.Border origBorder = resistance.GetBorder();
			resistance.AddKeyListener(new _KeyAdapter_69(this, origBorder));
			GetContentPane().Add(resistance);
			resistance.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Component rigidArea2 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea2);
			voltage = new HintTextArea("Voltage (Only for Cells)...");
			voltage.AddKeyListener(new _KeyAdapter_81(this, origBorder));
			GetContentPane().Add(voltage);
			voltage.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Component rigidArea3 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea3);
			current = new HintTextArea("Current (Only for Cells)...");
			current.AddKeyListener(new _KeyAdapter_93(this, origBorder));
			GetContentPane().Add(current);
			current.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Component rigidArea4 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea4);
			textAnswerField = new HintTextArea("Answer (For Void + Word type Q)");
			GetContentPane().Add(textAnswerField);
			textAnswerField.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Component rigidArea5 = Box.CreateRigidArea(new Dimension(0, 10));
			GetContentPane().Add(rigidArea5);
			JPanel panel = new JPanel();
			GetContentPane().Add(panel);
			panel.SetLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			JButton btnCreateComponent = new JButton("Create Component");
			panel.Add(btnCreateComponent);
			btnCreateComponent.SetIcon(new ImageIcon(typeof(JPanels.JDialogs.CreateComponent)
				.GetResource("/resources/icons/Very_Basic/checkmark.png")));
			btnCreateComponent.AddMouseListener(new _MouseAdapter_117(this));
			SetEnabled(true);
			SetModal(true);
			SetVisible(true);
		}

		private sealed class _KeyAdapter_69 : KeyAdapter
		{
			public _KeyAdapter_69(CreateComponent _enclosing, Javax.Swing.Border.Border origBorder
				)
			{
				this._enclosing = _enclosing;
				this.origBorder = origBorder;
			}

			public override void KeyReleased(KeyEvent arg0)
			{
				this._enclosing.resistance.SetBorder(origBorder);
			}

			private readonly CreateComponent _enclosing;

			private readonly Javax.Swing.Border.Border origBorder;
		}

		private sealed class _KeyAdapter_81 : KeyAdapter
		{
			public _KeyAdapter_81(CreateComponent _enclosing, Javax.Swing.Border.Border origBorder
				)
			{
				this._enclosing = _enclosing;
				this.origBorder = origBorder;
			}

			public override void KeyReleased(KeyEvent e)
			{
				this._enclosing.resistance.SetBorder(origBorder);
			}

			private readonly CreateComponent _enclosing;

			private readonly Javax.Swing.Border.Border origBorder;
		}

		private sealed class _KeyAdapter_93 : KeyAdapter
		{
			public _KeyAdapter_93(CreateComponent _enclosing, Javax.Swing.Border.Border origBorder
				)
			{
				this._enclosing = _enclosing;
				this.origBorder = origBorder;
			}

			public override void KeyReleased(KeyEvent e)
			{
				this._enclosing.resistance.SetBorder(origBorder);
			}

			private readonly CreateComponent _enclosing;

			private readonly Javax.Swing.Border.Border origBorder;
		}

		private sealed class _MouseAdapter_117 : MouseAdapter
		{
			public _MouseAdapter_117(CreateComponent _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				if (this._enclosing.InputValidation())
				{
					this._enclosing.baby = InitComponent.Create(this._enclosing.GetComponentType(), this
						._enclosing.GetResistance(), this._enclosing.GetVoltage(), this._enclosing.GetCurrent
						(), 100, 200, string.Empty);
					this._enclosing.SetVisible(false);
				}
			}

			private readonly CreateComponent _enclosing;
		}

		/// <summary>Gets the component.</summary>
		/// <remarks>Gets the component.</remarks>
		/// <returns>the component</returns>
		public virtual ElectricalComponent GetComp()
		{
			return baby;
		}

		/// <summary>Input validation.</summary>
		/// <remarks>Input validation.</remarks>
		/// <returns>true, if successful</returns>
		private bool InputValidation()
		{
			string item = componentType.GetSelectedItem().ToString();
			if (item.Equals("Bulb") || item.Equals("Resistor"))
			{
				if (resistance.GetText().Equals(string.Empty))
				{
					resistance.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					return false;
				}
			}
			if (item.Equals("Cell"))
			{
				if (voltage.GetText().Equals(string.Empty) || current.GetText().Equals(string.Empty
					))
				{
					voltage.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					current.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					return false;
				}
			}
			if (item.Equals("Void"))
			{
				if (resistance.GetText().Equals(string.Empty) && voltage.GetText().Equals(string.Empty
					) && current.GetText().Equals(string.Empty))
				{
					resistance.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					voltage.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					current.SetBorder(BorderFactory.CreateLineBorder(new Color(unchecked((int)(0xCF5151
						)))));
					return false;
				}
			}
			return true;
		}

		/// <summary>Gets the component type.</summary>
		/// <remarks>Gets the component type.</remarks>
		/// <returns>the component type</returns>
		public virtual string GetComponentType()
		{
			string s = componentType.GetSelectedItem().ToString();
			return s;
		}

		/// <summary>Gets the resistance.</summary>
		/// <remarks>Gets the resistance.</remarks>
		/// <returns>the resistance</returns>
		public virtual float GetResistance()
		{
			if (!resistance.GetText().Equals(string.Empty))
			{
				float f = System.Convert.ToSingle(resistance.GetText());
				return f;
			}
			return 0;
		}

		/// <summary>Gets the voltage.</summary>
		/// <remarks>Gets the voltage.</remarks>
		/// <returns>the voltage</returns>
		public virtual float GetVoltage()
		{
			try
			{
				voltage.GetText();
			}
			catch (FormatException)
			{
				System.Console.Out.WriteLine("caught number exception");
				return 0;
			}
			if (!voltage.GetText().Equals(string.Empty))
			{
				float f = System.Convert.ToSingle(voltage.GetText());
				return f;
			}
			return 0;
		}

		/// <summary>Gets the current.</summary>
		/// <remarks>Gets the current.</remarks>
		/// <returns>the current</returns>
		public virtual float GetCurrent()
		{
			if (!current.GetText().Equals(string.Empty))
			{
				float f = System.Convert.ToSingle(current.GetText());
				return f;
			}
			return 0;
		}
	}
}
