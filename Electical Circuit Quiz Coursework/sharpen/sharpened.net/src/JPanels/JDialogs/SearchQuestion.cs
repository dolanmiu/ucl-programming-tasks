using Core;
using JPanels;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JDialogs
{
	/// <summary>The Search Question Class.</summary>
	/// <remarks>The Search Question Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class SearchQuestion : JDialog
	{
		/// <summary>The find panel.</summary>
		/// <remarks>The find panel.</remarks>
		private FindPanel find = new FindPanel();

		/// <summary>Instantiates a new search question.</summary>
		/// <remarks>Instantiates a new search question.</remarks>
		public SearchQuestion()
		{
			SetMinimumSize(new Dimension(400, 300));
			this.SetLocation(400, 100);
			JPanel panel = new JPanel();
			panel.Add(find);
			GetContentPane().Add(panel, BorderLayout.NORTH);
			panel.SetLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			JPanel panel_1 = new JPanel();
			GetContentPane().Add(panel_1, BorderLayout.SOUTH);
			JButton btnOk = new JButton("Ok!");
			btnOk.SetIcon(new ImageIcon(typeof(JPanels.JDialogs.SearchQuestion).GetResource("/resources/icons/Very_Basic/checkmark.png"
				)));
			btnOk.AddMouseListener(new _MouseAdapter_43(this));
			panel_1.Add(btnOk);
			SetEnabled(true);
			SetModal(true);
			SetVisible(true);
		}

		private sealed class _MouseAdapter_43 : MouseAdapter
		{
			public _MouseAdapter_43(SearchQuestion _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.SetVisible(false);
			}

			private readonly SearchQuestion _enclosing;
		}

		/// <summary>Gets the selected question.</summary>
		/// <remarks>Gets the selected question.</remarks>
		/// <returns>the selected question</returns>
		public virtual Question GetSelected()
		{
			return find.GetQuestionList().GetSelectedValue();
		}
	}
}
