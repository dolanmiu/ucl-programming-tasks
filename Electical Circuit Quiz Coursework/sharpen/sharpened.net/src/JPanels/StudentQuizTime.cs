using Core;
using JPanels;
using JPanels.Misc;
using Sharpen;

namespace JPanels
{
	/// <summary>
	/// The Timed Student Quiz Class
	/// Quizzes the student with a timer.
	/// </summary>
	/// <remarks>
	/// The Timed Student Quiz Class
	/// Quizzes the student with a timer.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class StudentQuizTime : StudentQuiz
	{
		/// <summary>The watch.</summary>
		/// <remarks>The watch.</remarks>
		protected internal StopWatch watch;

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		/// <param name="cardPanel">the card panel</param>
		/// <param name="difficulty">the difficulty</param>
		/// <param name="shuffle">the shuffle</param>
		public StudentQuizTime(CardPanel cardPanel, string difficulty, bool shuffle) : base
			(cardPanel, difficulty, shuffle)
		{
			watch = new StopWatch(300);
			timerPanel.Add(watch);
		}

		public override float CalculateScore()
		{
			long maxTime = watch.GetMaxTime();
			//Adding a score by calculating the time taken
			long timeLeft = watch.GetTimeLeft();
			float percentageTimeLeft = (float)timeLeft / (float)maxTime;
			//decreases after every question
			float scoretoAdd = (100 / questions.Count) * percentageTimeLeft;
			return scoretoAdd;
		}

		public override long GetTimeTaken()
		{
			long timeTaken = watch.GetCheckpoint() - watch.GetTimeLeft();
			return timeTaken;
		}

		public override void SetTimeCheckPoint()
		{
			watch.SetCheckpoint();
		}
	}
}
