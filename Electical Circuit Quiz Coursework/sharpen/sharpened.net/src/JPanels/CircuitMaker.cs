using System;
using System.IO;
using Core;
using ElectricalComponents;
using JPanels;
using JPanels.JDialogs;
using JPanels.JMenus;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels
{
	/// <summary>
	/// The Circuit Maker Class
	/// The control panel to create circuits.
	/// </summary>
	/// <remarks>
	/// The Circuit Maker Class
	/// The control panel to create circuits.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CircuitMaker : JPanel
	{
		/// <summary>The modules.</summary>
		/// <remarks>The modules.</remarks>
		private AList<Module> modules;

		/// <summary>The question combo box.</summary>
		/// <remarks>The question combo box.</remarks>
		private JComboBox<Question> questionComboBox = new JComboBox<Question>();

		/// <summary>The component info.</summary>
		/// <remarks>The component info.</remarks>
		private DefaultListModel<ElectricalComponent> compInfo = new DefaultListModel<ElectricalComponent
			>();

		/// <summary>The answer info.</summary>
		/// <remarks>The answer info.</remarks>
		private DefaultListModel<ElectricalComponent> answerInfo = new DefaultListModel<ElectricalComponent
			>();

		/// <summary>The circuit draw panel (sand box).</summary>
		/// <remarks>The circuit draw panel (sand box).</remarks>
		private CircuitDraw circuitSandbox = new CircuitDraw(Reader.questionList);

		/// <summary>The module combo box.</summary>
		/// <remarks>The module combo box.</remarks>
		private JComboBox<Module> moduleComboBox = new JComboBox<Module>();

		/// <summary>The question text text-box.</summary>
		/// <remarks>The question text text-box.</remarks>
		private JTextArea textText = new JTextArea();

		/// <summary>The current question id.</summary>
		/// <remarks>The current question id.</remarks>
		private int currentQuestionID;

		/// <summary>The component list.</summary>
		/// <remarks>The component list.</remarks>
		private JList<ElectricalComponent> componentList;

		/// <summary>The answer list.</summary>
		/// <remarks>The answer list.</remarks>
		private JList<ElectricalComponent> answerList;

		/// <summary>The file chooser.</summary>
		/// <remarks>The file chooser.</remarks>
		private JFileChooser fc = new JFileChooser(".");

		/// <summary>The current directory.</summary>
		/// <remarks>The current directory.</remarks>
		private string currentDir = "electricaloutput.xml";

		/// <summary>The text answer.</summary>
		/// <remarks>The text answer.</remarks>
		private HintTextArea txtTextAnswer = new HintTextArea("Text Answer Student Types in..."
			);

		/// <summary>Instantiates a new circuit maker.</summary>
		/// <remarks>Instantiates a new circuit maker.</remarks>
		public CircuitMaker()
		{
			modules = GetModulesFromQuestion(1);
			componentList = new JList<ElectricalComponent>(compInfo);
			answerList = new JList<ElectricalComponent>(answerInfo);
			fc.SetFileFilter(new XMLFileFilter());
			textText.AddKeyListener(new _KeyAdapter_97(this));
			questionComboBox.AddComponentListener(new _ComponentAdapter_104(this));
			SetLayout(new BorderLayout(0, 0));
			JPanel toolbars = new JPanel();
			Add(toolbars, BorderLayout.NORTH);
			Add(circuitSandbox, BorderLayout.CENTER);
			circuitSandbox.SetLayout(null);
			JPanel side = new JPanel();
			side.SetPreferredSize(new Dimension(300, int.MaxValue));
			JPanel topSide = new JPanel();
			JPanel sideQuestionText = new JPanel();
			JPanel sideQuestion = new JPanel();
			JPanel sideContainer = new JPanel();
			JPanel sideComponents = new JPanel();
			Add(side, BorderLayout.WEST);
			side.SetLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
			side.SetMinimumSize(new Dimension(200, 600));
			side.Add(topSide);
			topSide.SetLayout(new BoxLayout(topSide, BoxLayout.Y_AXIS));
			topSide.Add(sideQuestionText);
			sideQuestionText.SetLayout(new BoxLayout(sideQuestionText, BoxLayout.X_AXIS));
			JPanel panel_4 = new JPanel();
			sideQuestion.Add(panel_4);
			JLabel lblQuestionText = new JLabel("Question Text");
			panel_4.Add(lblQuestionText);
			textText.SetLineWrap(true);
			sideQuestion.Add(textText);
			textText.SetColumns(1);
			topSide.Add(sideQuestion);
			sideQuestion.SetLayout(new BoxLayout(sideQuestion, BoxLayout.Y_AXIS));
			JLabel lblQuestion = new JLabel("Question");
			sideQuestionText.Add(lblQuestion);
			PopulateComboBox(questionComboBox, moduleComboBox);
			JScrollPane componentScollPane = new JScrollPane(componentList);
			componentList.SetVisibleRowCount(5);
			questionComboBox.AddItemListener(new _ItemListener_153(this));
			sideQuestionText.Add(questionComboBox);
			moduleComboBox.AddItemListener(new _ItemListener_160(this));
			moduleComboBox.SetModel(new DefaultComboBoxModel<Module>());
			SetFieldData(questionComboBox, moduleComboBox, textText, circuitSandbox);
			sideQuestion.Add(txtTextAnswer);
			side.Add(sideContainer);
			sideContainer.SetLayout(new BoxLayout(sideContainer, BoxLayout.Y_AXIS));
			JPanel panel_2 = new JPanel();
			sideContainer.Add(panel_2);
			JLabel label = new JLabel("Module");
			panel_2.Add(label);
			sideContainer.Add(sideComponents);
			sideComponents.SetLayout(new BoxLayout(sideComponents, BoxLayout.Y_AXIS));
			sideComponents.Add(moduleComboBox);
			sideComponents.Add(Box.CreateVerticalStrut(5));
			sideComponents.Add(componentScollPane);
			JPanel panel_3 = new JPanel();
			sideContainer.Add(panel_3);
			JLabel label_1 = new JLabel("Answers");
			panel_3.Add(label_1);
			JPanel sideText = new JPanel();
			sideContainer.Add(sideText);
			JScrollPane answerScrollPane = new JScrollPane(answerList);
			answerList.SetVisibleRowCount(5);
			sideText.Add(answerScrollPane);
			sideText.SetLayout(new BoxLayout(sideText, BoxLayout.Y_AXIS));
			toolbars.SetLayout(new BoxLayout(toolbars, BoxLayout.Y_AXIS));
			JPanel panel = new JPanel();
			toolbars.Add(panel);
			panel.SetLayout(new BorderLayout(0, 0));
			CircuitMakerMenu menuBar = new CircuitMakerMenu(this);
			panel.Add(menuBar);
			JPanel panel_1 = new JPanel();
			toolbars.Add(panel_1);
			panel_1.SetLayout(new BorderLayout(0, 0));
			JToolBar toolBar = new JToolBar();
			panel_1.Add(toolBar);
			JButton btnAddModule = new JButton("Module");
			btnAddModule.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/plus.png"
				)));
			btnAddModule.AddMouseListener(new _MouseAdapter_211(this));
			JButton btnAddQuestion = new JButton("Question");
			btnAddQuestion.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/plus.png"
				)));
			btnAddQuestion.AddMouseListener(new _MouseAdapter_223(this));
			toolBar.Add(btnAddQuestion);
			JButton btnRemoveQuestion = new JButton("Question");
			btnRemoveQuestion.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource(
				"/resources/icons/Very_Basic/minus.png")));
			toolBar.Add(btnRemoveQuestion);
			toolBar.Add(btnAddModule);
			JButton btnAddComponent = new JButton("Component");
			btnAddComponent.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/plus.png"
				)));
			btnAddComponent.AddMouseListener(new _MouseAdapter_243(this));
			JButton btnRemoveModule = new JButton("Module");
			btnRemoveModule.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/minus.png"
				)));
			btnRemoveModule.AddMouseListener(new _MouseAdapter_257(this));
			toolBar.Add(btnRemoveModule);
			toolBar.Add(btnAddComponent);
			JButton btnRemoveComponent = new JButton("Component");
			btnRemoveComponent.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource
				("/resources/icons/Very_Basic/minus.png")));
			btnRemoveComponent.AddMouseListener(new _MouseAdapter_272(this));
			toolBar.Add(btnRemoveComponent);
			JButton btnAddAnswer = new JButton("Answer");
			btnAddAnswer.AddMouseListener(new _MouseAdapter_281(this));
			btnAddAnswer.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/plus.png"
				)));
			toolBar.Add(btnAddAnswer);
			JButton btnRemoveAnswer = new JButton("Answer");
			btnRemoveAnswer.AddMouseListener(new _MouseAdapter_296(this));
			btnRemoveAnswer.SetIcon(new ImageIcon(typeof(JPanels.CircuitMaker).GetResource("/resources/icons/Very_Basic/minus.png"
				)));
			toolBar.Add(btnRemoveAnswer);
		}

		private sealed class _KeyAdapter_97 : KeyAdapter
		{
			public _KeyAdapter_97(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void KeyReleased(KeyEvent e)
			{
				Reader.questionList[this._enclosing.currentQuestionID].SetText(this._enclosing.textText
					.GetText());
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _ComponentAdapter_104 : ComponentAdapter
		{
			public _ComponentAdapter_104(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void ComponentShown(ComponentEvent e)
			{
				this._enclosing.circuitSandbox.DrawCircuit(0);
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _ItemListener_153 : ItemListener
		{
			public _ItemListener_153(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void ItemStateChanged(ItemEvent arg0)
			{
				this._enclosing.SetFieldData(this._enclosing.questionComboBox, this._enclosing.moduleComboBox
					, this._enclosing.textText, this._enclosing.circuitSandbox);
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _ItemListener_160 : ItemListener
		{
			public _ItemListener_160(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void ItemStateChanged(ItemEvent arg0)
			{
				this._enclosing.UpdateComponentList();
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_211 : MouseAdapter
		{
			public _MouseAdapter_211(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MousePressed(MouseEvent arg0)
			{
				Module module = new Module(this._enclosing.moduleComboBox.GetItemCount());
				Reader.questionList[this._enclosing.currentQuestionID].circuit.AddModule(module);
				this._enclosing.moduleComboBox.AddItem(module);
				this._enclosing.moduleComboBox.SetSelectedItem(module);
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_223 : MouseAdapter
		{
			public _MouseAdapter_223(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				CreateQuestion createModuleDialog = new CreateQuestion();
				Question question = createModuleDialog.GetQuestion();
				createModuleDialog.Dispose();
				if (question != null)
				{
					this._enclosing.AddQuestion(question, this._enclosing.moduleComboBox, this._enclosing
						.circuitSandbox);
				}
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_243 : MouseAdapter
		{
			public _MouseAdapter_243(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				CreateComponent createCompDialog = new CreateComponent();
				ElectricalComponent comp = createCompDialog.GetComp();
				createCompDialog.Dispose();
				if (comp != null)
				{
					this._enclosing.AddComponent(comp, this._enclosing.moduleComboBox, this._enclosing
						.compInfo, this._enclosing.circuitSandbox);
				}
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_257 : MouseAdapter
		{
			public _MouseAdapter_257(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				if (this._enclosing.modules.Count > 1)
				{
					this._enclosing.modules.Remove(this._enclosing.moduleComboBox.GetSelectedItem());
					this._enclosing.UpdateAll();
					this._enclosing.moduleComboBox.SetSelectedItem(this._enclosing.modules[0]);
				}
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_272 : MouseAdapter
		{
			public _MouseAdapter_272(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.RemoveComponent(this._enclosing.componentList, this._enclosing.moduleComboBox
					, this._enclosing.compInfo, this._enclosing.circuitSandbox);
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_281 : MouseAdapter
		{
			public _MouseAdapter_281(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				CreateComponent createCompDialog = new CreateComponent();
				ElectricalComponent comp = createCompDialog.GetComp();
				createCompDialog.Dispose();
				if (comp != null)
				{
					this._enclosing.AddAnswer(comp);
				}
			}

			private readonly CircuitMaker _enclosing;
		}

		private sealed class _MouseAdapter_296 : MouseAdapter
		{
			public _MouseAdapter_296(CircuitMaker _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.RemoveAnswer();
			}

			private readonly CircuitMaker _enclosing;
		}

		/// <summary>Show file open dialog.</summary>
		/// <remarks>Show file open dialog.</remarks>
		public virtual void ShowFileOpen()
		{
			int returnVal = fc.ShowOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				FilePath file = fc.GetSelectedFile();
				Reader.questionList.Clear();
				currentDir = file.GetAbsolutePath();
				try
				{
					Reader.ScanFile(file);
					UpdateAll();
				}
				catch (FileNotFoundException)
				{
				}
			}
		}

		/// <summary>Show file save dialog.</summary>
		/// <remarks>Show file save dialog.</remarks>
		public virtual void ShowFileSave()
		{
			Writer write = new Writer();
			write.WriteFile(currentDir);
		}

		/// <summary>Show file save as dialog.</summary>
		/// <remarks>Show file save as dialog.</remarks>
		public virtual void ShowFileSaveAs()
		{
			int returnVal = fc.ShowOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				FilePath file = fc.GetSelectedFile();
				Writer write = new Writer();
				string path = file.GetAbsolutePath();
				if (!path.EndsWith(".xml"))
				{
					path = file.GetAbsolutePath() + ".xml";
				}
				write.WriteFile(path);
			}
		}

		/// <summary>Sets the question combo box.</summary>
		/// <remarks>Sets the question combo box.</remarks>
		/// <param name="question">the new question combo box</param>
		public virtual void SetQuestionComboBox(Question question)
		{
			int i = GetQuestionID(question);
			questionComboBox.SetSelectedIndex(i);
		}

		/// <summary>Gets the question id.</summary>
		/// <remarks>Gets the question id.</remarks>
		/// <param name="question">the question</param>
		/// <returns>the question id</returns>
		private int GetQuestionID(Question question)
		{
			for (int i = 0; i < questionComboBox.GetItemCount(); i++)
			{
				if (questionComboBox.GetItemAt(i).Equals(question))
				{
					return i;
				}
			}
			return 0;
		}

		/// <summary>Adds the question.</summary>
		/// <remarks>Adds the question.</remarks>
		/// <param name="question">the question</param>
		/// <param name="moduleComboBox">the module combo box</param>
		/// <param name="circuitSandbox">the circuit sand box</param>
		private void AddQuestion(Question question, JComboBox<Module> moduleComboBox, CircuitDraw
			 circuitSandbox)
		{
			Reader.questionList.AddItem(question);
			questionComboBox.AddItem(question);
		}

		/// <summary>Adds the component.</summary>
		/// <remarks>Adds the component.</remarks>
		/// <param name="comp">the component</param>
		/// <param name="moduleComboBox">the module combo box</param>
		/// <param name="info">the info</param>
		/// <param name="circuitSandbox">the circuit sand box</param>
		private void AddComponent(ElectricalComponent comp, JComboBox<Module> moduleComboBox
			, DefaultListModel<ElectricalComponent> info, CircuitDraw circuitSandbox)
		{
			Circuit circuit = Reader.questionList[currentQuestionID].circuit;
			circuit.GetModules()[moduleComboBox.GetSelectedIndex()].AddComponant(comp);
			UpdateAll();
		}

		/// <summary>Removes the component.</summary>
		/// <remarks>Removes the component.</remarks>
		/// <param name="componentList">the component list</param>
		/// <param name="moduleComboBox">the module combo box</param>
		/// <param name="info">the info</param>
		/// <param name="circuitSandbox">the circuit sand box</param>
		private void RemoveComponent(JList<ElectricalComponent> componentList, JComboBox<
			Module> moduleComboBox, DefaultListModel<ElectricalComponent> info, CircuitDraw 
			circuitSandbox)
		{
			Circuit circuit = Reader.questionList[currentQuestionID].circuit;
			Module module = circuit.GetModules()[moduleComboBox.GetSelectedIndex()];
			if (module.GetComponentAmmount() > 1 && componentList.GetSelectedIndex() >= 0)
			{
				//if its selected an item
				module.RemoveComponent(componentList.GetSelectedIndex());
				//also a module cannot contain
				UpdateAll();
			}
		}

		//less than 1 component would make no sense
		/// <summary>Adds the answer.</summary>
		/// <remarks>Adds the answer.</remarks>
		/// <param name="answer">the answer</param>
		private void AddAnswer(ElectricalComponent answer)
		{
			Question question = Reader.questionList[currentQuestionID];
			question.AddAnswer(answer);
			UpdateAll();
		}

		/// <summary>Removes the answer.</summary>
		/// <remarks>Removes the answer.</remarks>
		private void RemoveAnswer()
		{
			if (modules.Count > 1)
			{
				AList<ElectricalComponent> answers = Reader.questionList[currentQuestionID].GetAnswers
					();
				answers.Remove(answerList.GetSelectedIndex());
				UpdateAll();
			}
		}

		/// <summary>Update everything on screen.</summary>
		/// <remarks>Update everything on screen.</remarks>
		public virtual void UpdateAll()
		{
			RefreshWires();
			UpdateModuleComboBox(moduleComboBox);
			if (questionComboBox.GetItemAt(currentQuestionID).GetType().Equals("word"))
			{
				txtTextAnswer.SetVisible(true);
				try
				{
					txtTextAnswer.SetText(questionComboBox.GetItemAt(currentQuestionID).answers[0].GetTextAnswer
						());
				}
				catch (IndexOutOfRangeException)
				{
					txtTextAnswer.ForceHint();
				}
			}
			else
			{
				txtTextAnswer.SetVisible(false);
			}
			UpdateComponentList();
			UpdateAnswerList();
			circuitSandbox.UpdateCircuit(currentQuestionID);
		}

		/// <summary>Sets the field data.</summary>
		/// <remarks>Sets the field data.</remarks>
		/// <param name="questionComboBox">the question combo box</param>
		/// <param name="moduleComboBox">the module combo box</param>
		/// <param name="textText">the question text text-box</param>
		/// <param name="circuitSandbox">the circuit sand box</param>
		private void SetFieldData(JComboBox<Question> questionComboBox, JComboBox<Module>
			 moduleComboBox, JTextArea textText, CircuitDraw circuitSandbox)
		{
			modules = GetModulesFromQuestion(questionComboBox.GetSelectedIndex());
			currentQuestionID = questionComboBox.GetSelectedIndex();
			UpdateAll();
			textText.SetText(GetQuestionText(questionComboBox.GetSelectedIndex()));
			UpdateCircuit(circuitSandbox, questionComboBox.GetSelectedIndex());
		}

		/// <summary>Refresh wires.</summary>
		/// <remarks>Refresh wires.</remarks>
		private void RefreshWires()
		{
			Circuit circuit = Reader.questionList[currentQuestionID].circuit;
			circuit.RemoveAllWires();
			Reader.CreateWires(circuit);
		}

		/// <summary>Update component list.</summary>
		/// <remarks>Update component list.</remarks>
		private void UpdateComponentList()
		{
			compInfo.Clear();
			AList<ElectricalComponent> components = GetComponentsFromModule(moduleComboBox.GetSelectedIndex
				(), currentQuestionID);
			if (components != null)
			{
				for (int i = 0; i < components.Count; i++)
				{
					compInfo.AddElement(components[i]);
				}
			}
		}

		/// <summary>Update answer list.</summary>
		/// <remarks>Update answer list.</remarks>
		private void UpdateAnswerList()
		{
			answerInfo.Clear();
			AList<ElectricalComponent> answers = GetAnswers(currentQuestionID);
			if (answers != null)
			{
				for (int i = 0; i < answers.Count; i++)
				{
					answerInfo.AddElement(answers[i]);
				}
			}
		}

		/// <summary>Update module combo box.</summary>
		/// <remarks>Update module combo box.</remarks>
		/// <param name="moduleComboBox">the module combo box</param>
		private void UpdateModuleComboBox(JComboBox<Module> moduleComboBox)
		{
			moduleComboBox.RemoveAllItems();
			for (int i = 0; i < modules.Count; i++)
			{
				moduleComboBox.AddItem(modules[i]);
			}
		}

		/// <summary>Update circuit.</summary>
		/// <remarks>Update circuit.</remarks>
		/// <param name="circuitSandbox">the circuit sand box</param>
		/// <param name="question">the question</param>
		private void UpdateCircuit(CircuitDraw circuitSandbox, int question)
		{
			circuitSandbox.UpdateCircuit(question);
		}

		/// <summary>Gets the modules from question.</summary>
		/// <remarks>Gets the modules from question.</remarks>
		/// <param name="n">the n</param>
		/// <returns>the modules from question</returns>
		private AList<Module> GetModulesFromQuestion(int n)
		{
			return Reader.questionList[n].circuit.GetModules();
		}

		/// <summary>Gets the question text.</summary>
		/// <remarks>Gets the question text.</remarks>
		/// <param name="n">the n</param>
		/// <returns>the question text</returns>
		private string GetQuestionText(int n)
		{
			return Reader.questionList[n].GetText();
		}

		/// <summary>Populate combo box.</summary>
		/// <remarks>Populate combo box.</remarks>
		/// <param name="questionCombobox">the question combo box</param>
		/// <param name="moduleComboBox">the module combo box</param>
		private void PopulateComboBox(JComboBox<Question> questionCombobox, JComboBox<Module
			> moduleComboBox)
		{
			//Bug Report
			questionCombobox.RemoveAllItems();
			moduleComboBox.RemoveAllItems();
			for (int i = 0; i < Reader.questionList.Count; i++)
			{
				questionCombobox.AddItem(Reader.questionList[i]);
			}
			for (int i_1 = 0; i_1 < Reader.questionList[0].circuit.GetModules().Count; i_1++)
			{
				moduleComboBox.AddItem(Reader.questionList[0].circuit.GetModules()[i_1]);
			}
		}

		/// <summary>Gets the answers.</summary>
		/// <remarks>Gets the answers.</remarks>
		/// <param name="currentQID">the current question ID</param>
		/// <returns>the answers</returns>
		private AList<ElectricalComponent> GetAnswers(int currentQID)
		{
			Question question = Reader.questionList[currentQID];
			AList<ElectricalComponent> components = question.GetAnswers();
			return components;
		}

		/// <summary>Gets the components from module.</summary>
		/// <remarks>Gets the components from module.</remarks>
		/// <param name="n">the n</param>
		/// <param name="currentQID">the current question ID</param>
		/// <returns>the components from module</returns>
		private AList<ElectricalComponent> GetComponentsFromModule(int n, int currentQID)
		{
			if (n >= 0)
			{
				Question question = Reader.questionList[currentQID];
				Module module = question.circuit.GetModules()[n];
				AList<ElectricalComponent> components = module.ReturnComponents();
				return components;
			}
			return null;
		}
	}
}
