using System;
using Com.Smardec.Mousegestures;
using Core;
using JPanels;
using JPanels.JMenus;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace JPanels
{
	/// <summary>
	/// The Student Quiz Class
	/// Quizzes the student
	/// </summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class StudentQuiz : JPanel
	{
		/// <summary>The questions.</summary>
		/// <remarks>The questions.</remarks>
		protected internal AList<Question> questions;

		/// <summary>The circuit panel.</summary>
		/// <remarks>The circuit panel.</remarks>
		protected internal CircuitDraw circuitPanel;

		/// <summary>The question label.</summary>
		/// <remarks>The question label.</remarks>
		protected internal JLabel questionLabel;

		/// <summary>The question text.</summary>
		/// <remarks>The question text.</remarks>
		protected internal JLabel questionText;

		/// <summary>The card panel.</summary>
		/// <remarks>The card panel.</remarks>
		protected internal CardPanel cardPanel;

		/// <summary>The gesture label.</summary>
		/// <remarks>The gesture label.</remarks>
		protected internal JLabel gestureLabel;

		/// <summary>The question number panel.</summary>
		/// <remarks>The question number panel.</remarks>
		protected internal JPanel questionNumberPanel;

		/// <summary>The question text panel.</summary>
		/// <remarks>The question text panel.</remarks>
		protected internal JPanel questionTextPanel;

		/// <summary>The timer panel.</summary>
		/// <remarks>The timer panel.</remarks>
		protected internal JPanel timerPanel;

		/// <summary>The painter.</summary>
		/// <remarks>The painter.</remarks>
		protected internal PaintPanel painter;

		/// <summary>The dock panel.</summary>
		/// <remarks>The dock panel.</remarks>
		protected internal JPanel dockPanel;

		/// <summary>The show hide painter button.</summary>
		/// <remarks>The show hide painter button.</remarks>
		protected internal JButton btnShowHidePainter;

		/// <summary>The show note panel.</summary>
		/// <remarks>The show note panel.</remarks>
		protected internal JPanel showNotePanel;

		/// <summary>The answer field.</summary>
		/// <remarks>The answer field.</remarks>
		protected internal HintTextArea answerField;

		/// <summary>The global gesture.</summary>
		/// <remarks>The global gesture.</remarks>
		protected internal string globalGesture = string.Empty;

		/// <summary>The points label.</summary>
		/// <remarks>The points label.</remarks>
		protected internal JLabel lblPoints;

		/// <summary>The panel.</summary>
		/// <remarks>The panel.</remarks>
		protected internal JPanel panel;

		/// <summary>The panel_1.</summary>
		/// <remarks>The panel_1.</remarks>
		private JPanel panel_1;

		/// <summary>Instantiates a new student quiz.</summary>
		/// <remarks>Instantiates a new student quiz.</remarks>
		/// <param name="cardPanel">the card panel</param>
		/// <param name="difficulty">the difficulty</param>
		/// <param name="shuffle">the shuffle</param>
		public StudentQuiz(CardPanel cardPanel, string difficulty, bool shuffle)
		{
			Marker.ResetScore();
			MouseGestures();
			panel = new JPanel();
			Add(panel);
			panel.SetLayout(new BorderLayout(0, 0));
			panel.SetPreferredSize(new Dimension(1000, 40));
			StudentQuizMenu menu = new StudentQuizMenu(cardPanel);
			panel.Add(menu, BorderLayout.NORTH);
			panel_1 = new JPanel();
			panel.Add(panel_1, BorderLayout.CENTER);
			panel.SetMaximumSize(new Dimension(int.MaxValue, 30));
			this.cardPanel = cardPanel;
			questions = GetQuestions(difficulty);
			if (shuffle)
			{
				Collections.Shuffle(questions);
			}
			SetLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			circuitPanel = new CircuitDraw(questions);
			System.Console.Out.WriteLine("(StudentQuiz) Total Resistance: " + circuitPanel.GetTotalResistance
				());
			JPanel topPanel = new JPanel();
			topPanel.SetMaximumSize(new Dimension(int.MaxValue, 100));
			Add(topPanel);
			topPanel.SetLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
			questionNumberPanel = new JPanel();
			questionNumberPanel.SetBorder(new EmptyBorder(5, 6, 5, 19));
			topPanel.Add(questionNumberPanel);
			questionNumberPanel.SetLayout(new BoxLayout(questionNumberPanel, BoxLayout.Y_AXIS
				));
			gestureLabel = new JLabel("Waiting for gesture");
			questionNumberPanel.Add(gestureLabel);
			questionLabel = new JLabel("Question: " + (circuitPanel.GetQuestionNumber() + 1));
			questionNumberPanel.Add(questionLabel);
			JButton btnNextQuestion = new JButton("Next Question!");
			btnNextQuestion.SetIcon(new ImageIcon(typeof(JPanels.StudentQuiz).GetResource("/resources/icons/Arrows/right.png"
				)));
			topPanel.Add(btnNextQuestion);
			btnNextQuestion.AddMouseListener(new _MouseAdapter_142(this));
			Component horizontalGlue = Box.CreateHorizontalGlue();
			topPanel.Add(horizontalGlue);
			lblPoints = new JLabel("0 Points!");
			topPanel.Add(lblPoints);
			timerPanel = new JPanel();
			timerPanel.SetMaximumSize(new Dimension(100, 100));
			topPanel.Add(timerPanel);
			questionTextPanel = new JPanel();
			questionTextPanel.SetMaximumSize(new Dimension(int.MaxValue, 40));
			Add(questionTextPanel);
			questionText = new JLabel();
			questionTextPanel.Add(questionText);
			questionText.SetText(questions[circuitPanel.GetQuestionNumber()].GetText());
			Add(circuitPanel);
			dockPanel = new JPanel();
			Add(dockPanel);
			dockPanel.SetLayout(new BoxLayout(dockPanel, BoxLayout.Y_AXIS));
			dockPanel.SetMaximumSize(new Dimension(1000, 600));
			showNotePanel = new JPanel();
			dockPanel.Add(showNotePanel);
			btnShowHidePainter = new JButton("Show/Hide Note Taker");
			btnShowHidePainter.AddMouseListener(new _MouseAdapter_176(this));
			showNotePanel.Add(btnShowHidePainter);
			answerField = new HintTextArea("Type Answer Here");
			showNotePanel.Add(answerField);
			SetAnswerText();
			painter = new PaintPanel();
			painter.SetPreferredSize(new Dimension(2000, 600));
			painter.SetVisible(false);
			dockPanel.Add(painter);
		}

		private sealed class _MouseAdapter_142 : MouseAdapter
		{
			public _MouseAdapter_142(StudentQuiz _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseClicked(MouseEvent e)
			{
				this._enclosing.NextQuestion(this._enclosing.questionLabel);
			}

			private readonly StudentQuiz _enclosing;
		}

		private sealed class _MouseAdapter_176 : MouseAdapter
		{
			public _MouseAdapter_176(StudentQuiz _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				if (this._enclosing.painter.IsVisible())
				{
					this._enclosing.painter.SetVisible(false);
				}
				else
				{
					this._enclosing.painter.SetVisible(true);
				}
			}

			private readonly StudentQuiz _enclosing;
		}

		/// <summary>Next question.</summary>
		/// <remarks>Next question.</remarks>
		/// <param name="questionLabel">the question label</param>
		private void NextQuestion(JLabel questionLabel)
		{
			bool questionWrong = false;
			float scoretoAdd = 0;
			Question currentQ = circuitPanel.GetCurrentQuestion();
			if (currentQ.GetType().Equals("word"))
			{
				if (!answerField.GetText().Equals(currentQ.GetAnswers()[0].GetTextAnswer()))
				{
					questionWrong = true;
				}
			}
			else
			{
				if (currentQ.GetType().Equals("gesture"))
				{
					if (currentQ.GetType().Equals("gesture"))
					{
						//Pressing 'next question' is like a skip for this question
						if (!globalGesture.Equals(currentQ.GetAnswers()[0].GetTextAnswer()))
						{
							questionWrong = true;
						}
					}
				}
				else
				{
					if (!circuitPanel.IsCorrect() || circuitPanel.isCollided == false)
					{
						questionWrong = true;
					}
				}
			}
			if (questionWrong)
			{
				JOptionPane.ShowMessageDialog(null, "Wrong Answer!");
			}
			else
			{
				scoretoAdd = CalculateScore();
				Marker.AddScore((int)(scoretoAdd));
				lblPoints.SetText(Marker.GetScore() + " Points!");
			}
			long timeTaken = GetTimeTaken();
			Marker.AddQuestionStat(circuitPanel.GetQuestionNumber() + 1, questionWrong, timeTaken
				, scoretoAdd);
			if (circuitPanel.IsDrawable())
			{
				questionText.SetText(questions[circuitPanel.GetQuestionNumber()].GetText());
				circuitPanel.DrawNextCircuit();
				SetAnswerText();
				questionLabel.SetText("Question: " + (circuitPanel.GetQuestionNumber() + 1));
				SetTimeCheckPoint();
			}
			else
			{
				JOptionPane.ShowMessageDialog(null, "Finish! Here is your result.");
				QuizResult finish = new QuizResult();
				cardPanel.Add(finish, "quizResult");
				cardPanel.c1.Show(cardPanel, "quizResult");
			}
		}

		/// <summary>Sets the answer text.</summary>
		/// <remarks>Sets the answer text.</remarks>
		private void SetAnswerText()
		{
			if (circuitPanel.GetCurrentQuestion().GetType().Equals("word"))
			{
				answerField.SetVisible(true);
			}
			else
			{
				answerField.SetVisible(false);
			}
		}

		/// <summary>Calculate score.</summary>
		/// <remarks>Calculate score.</remarks>
		/// <returns>the score to be added</returns>
		public virtual float CalculateScore()
		{
			float scoretoAdd = (100 / questions.Count);
			return scoretoAdd;
		}

		/// <summary>Gets the time taken.</summary>
		/// <remarks>Gets the time taken.</remarks>
		/// <returns>the time taken to finish the question</returns>
		public virtual long GetTimeTaken()
		{
			long timeTaken = 0;
			return timeTaken;
		}

		/// <summary>Sets the time check point.</summary>
		/// <remarks>Sets the time check point.</remarks>
		public virtual void SetTimeCheckPoint()
		{
		}

		/// <summary>Gets the questions.</summary>
		/// <remarks>Gets the questions.</remarks>
		/// <param name="difficulty">the difficulty</param>
		/// <returns>the questions</returns>
		private AList<Question> GetQuestions(string difficulty)
		{
			return Reader.GetQuestions(difficulty);
		}

		/// <summary>Mouse gestures.</summary>
		/// <remarks>Mouse gestures.</remarks>
		private void MouseGestures()
		{
			Com.Smardec.Mousegestures.MouseGestures mg = new Com.Smardec.Mousegestures.MouseGestures
				();
			mg.SetMouseButton(MouseEvent.BUTTON3_MASK);
			mg.AddMouseGesturesListener(new _MouseGesturesListener_299(this));
			//This method is called when the user releases the mouse button finally
			//Just display the current message for a few milliseconds then
			//redisplay the original text
			mg.Start();
		}

		private sealed class _MouseGesturesListener_299 : MouseGesturesListener
		{
			public _MouseGesturesListener_299(StudentQuiz _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void GestureMovementRecognized(string currentGesture)
			{
				this._enclosing.globalGesture = currentGesture;
				if ("DR".Equals(currentGesture))
				{
					this._enclosing.NextQuestion(this._enclosing.questionLabel);
				}
				this._enclosing.gestureLabel.SetText(currentGesture);
				this._enclosing.GestureQuestion(currentGesture);
			}

			public void ProcessGesture(string gesture)
			{
				try
				{
					Sharpen.Thread.Sleep(10000);
				}
				catch (Exception)
				{
				}
				this._enclosing.gestureLabel.SetText("Waiting for gesture");
			}

			private readonly StudentQuiz _enclosing;
		}

		/// <summary>For the Gesture type question.</summary>
		/// <remarks>For the Gesture type question.</remarks>
		/// <param name="currentGesture">the current gesture</param>
		private void GestureQuestion(string currentGesture)
		{
			Question currentQ = circuitPanel.GetCurrentQuestion();
			if (currentQ.GetType().Equals("gesture"))
			{
				if (currentGesture.Equals(currentQ.GetAnswers()[0].GetTextAnswer()))
				{
					JOptionPane.ShowMessageDialog(null, "You did it!");
					NextQuestion(questionLabel);
				}
			}
		}
	}
}
