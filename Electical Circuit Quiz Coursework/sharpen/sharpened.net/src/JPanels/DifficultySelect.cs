using Core;
using JPanels;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace JPanels
{
	/// <summary>
	/// The Difficulty Selection Class
	/// Allows the student to select the difficulty of the questions.
	/// </summary>
	/// <remarks>
	/// The Difficulty Selection Class
	/// Allows the student to select the difficulty of the questions.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class DifficultySelect : JPanel
	{
		/// <summary>The name field.</summary>
		/// <remarks>The name field.</remarks>
		private HintTextArea nameField;

		/// <summary>The welcome label.</summary>
		/// <remarks>The welcome label.</remarks>
		private JLabel lblWelcome;

		/// <summary>The shuffle toggle button.</summary>
		/// <remarks>The shuffle toggle button.</remarks>
		internal JToggleButton tglbtnShuffle = new JToggleButton("Shuffle");

		/// <summary>The timed toggle button.</summary>
		/// <remarks>The timed toggle button.</remarks>
		internal JToggleButton tglbtnTimed = new JToggleButton("Timed?");

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		/// <param name="cardPanel">the card panel</param>
		public DifficultySelect(CardPanel cardPanel)
		{
			SetLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			JPanel containerPanel = new JPanel();
			Add(containerPanel);
			containerPanel.SetLayout(new BoxLayout(containerPanel, BoxLayout.Y_AXIS));
			lblWelcome = new JLabel("Welcome!");
			lblWelcome.SetForeground(Color.DARK_GRAY);
			lblWelcome.SetAlignmentX(Component.CENTER_ALIGNMENT);
			containerPanel.Add(lblWelcome);
			lblWelcome.SetFont(new Font("Dotum", Font.PLAIN, 50));
			nameField = new HintTextArea("What is your name?");
			nameField.AddKeyListener(new _KeyAdapter_65(this));
			Component rigidArea_1 = Box.CreateRigidArea(new Dimension(60, 30));
			containerPanel.Add(rigidArea_1);
			containerPanel.Add(nameField);
			nameField.SetMaximumSize(new Dimension(200, 20));
			nameField.SetColumns(10);
			JPanel buttonPanel = new JPanel();
			containerPanel.Add(buttonPanel);
			//buttonPanel.setMaximumSize(new Dimension(800,100));
			buttonPanel.SetLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
			Component horizontalGlue = Box.CreateHorizontalGlue();
			buttonPanel.Add(horizontalGlue);
			JPanel diffPanel = new JPanel();
			buttonPanel.Add(diffPanel);
			diffPanel.SetBorder(new EmptyBorder(0, 70, 0, 50));
			diffPanel.SetMaximumSize(new Dimension(0, 100));
			diffPanel.SetPreferredSize(new Dimension(500, 100));
			diffPanel.SetLayout(new BoxLayout(diffPanel, BoxLayout.X_AXIS));
			JButton btnEasy = new JButton("Easy");
			btnEasy.SetIcon(new ImageIcon(typeof(JPanels.DifficultySelect).GetResource("/resources/icons/Emoticons/lol.png"
				)));
			btnEasy.SetAlignmentX(Component.CENTER_ALIGNMENT);
			btnEasy.SetAlignmentY(Component.CENTER_ALIGNMENT);
			diffPanel.Add(btnEasy);
			diffPanel.Add(Box.CreateRigidArea(new Dimension(60, 0)));
			JButton btnMedium = new JButton("Medium");
			btnMedium.SetIcon(new ImageIcon(typeof(JPanels.DifficultySelect).GetResource("/resources/icons/Emoticons/question.png"
				)));
			diffPanel.Add(btnMedium);
			diffPanel.Add(Box.CreateRigidArea(new Dimension(60, 0)));
			JButton btnHard = new JButton("Hard");
			btnHard.SetIcon(new ImageIcon(typeof(JPanels.DifficultySelect).GetResource("/resources/icons/Emoticons/crying.png"
				)));
			btnHard.AddMouseListener(new _MouseAdapter_108(this, cardPanel));
			diffPanel.Add(btnHard);
			Component horizontalGlue2 = Box.CreateHorizontalGlue();
			horizontalGlue2.SetMaximumSize(new Dimension(400, 10));
			buttonPanel.Add(horizontalGlue2);
			JPanel toggleButtonPanel = new JPanel();
			buttonPanel.Add(toggleButtonPanel);
			toggleButtonPanel.SetAlignmentX(Component.RIGHT_ALIGNMENT);
			toggleButtonPanel.SetLayout(new BoxLayout(toggleButtonPanel, BoxLayout.Y_AXIS));
			toggleButtonPanel.SetPreferredSize(new Dimension(200, 50));
			tglbtnTimed.SetIcon(new ImageIcon(typeof(JPanels.DifficultySelect).GetResource("/resources/icons/System/dashboard.png"
				)));
			tglbtnTimed.SetAlignmentX(Component.CENTER_ALIGNMENT);
			toggleButtonPanel.Add(tglbtnTimed);
			Component rigidArea = Box.CreateRigidArea(new Dimension(60, 10));
			toggleButtonPanel.Add(rigidArea);
			tglbtnShuffle.SetIcon(new ImageIcon(typeof(JPanels.DifficultySelect).GetResource(
				"/resources/icons/Very_Basic/sinchronize.png")));
			tglbtnShuffle.SetAlignmentX(Component.CENTER_ALIGNMENT);
			toggleButtonPanel.Add(tglbtnShuffle);
			Component horizontalGlue3 = Box.CreateHorizontalGlue();
			buttonPanel.Add(horizontalGlue3);
			btnMedium.AddMouseListener(new _MouseAdapter_135(this, cardPanel));
			btnEasy.AddMouseListener(new _MouseAdapter_141(this, cardPanel));
		}

		private sealed class _KeyAdapter_65 : KeyAdapter
		{
			public _KeyAdapter_65(DifficultySelect _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void KeyReleased(KeyEvent arg0)
			{
				this._enclosing.lblWelcome.SetText("Welcome " + this._enclosing.nameField.GetText
					() + "!");
				Marker.SetName(this._enclosing.nameField.GetText());
			}

			private readonly DifficultySelect _enclosing;
		}

		private sealed class _MouseAdapter_108 : MouseAdapter
		{
			public _MouseAdapter_108(DifficultySelect _enclosing, CardPanel cardPanel)
			{
				this._enclosing = _enclosing;
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				this._enclosing.CreateQuizPanel("hard", cardPanel);
			}

			private readonly DifficultySelect _enclosing;

			private readonly CardPanel cardPanel;
		}

		private sealed class _MouseAdapter_135 : MouseAdapter
		{
			public _MouseAdapter_135(DifficultySelect _enclosing, CardPanel cardPanel)
			{
				this._enclosing = _enclosing;
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.CreateQuizPanel("medium", cardPanel);
			}

			private readonly DifficultySelect _enclosing;

			private readonly CardPanel cardPanel;
		}

		private sealed class _MouseAdapter_141 : MouseAdapter
		{
			public _MouseAdapter_141(DifficultySelect _enclosing, CardPanel cardPanel)
			{
				this._enclosing = _enclosing;
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				this._enclosing.CreateQuizPanel("easy", cardPanel);
			}

			private readonly DifficultySelect _enclosing;

			private readonly CardPanel cardPanel;
		}

		/// <summary>Creates the quiz panel.</summary>
		/// <remarks>Creates the quiz panel.</remarks>
		/// <param name="difficulty">the difficulty</param>
		/// <param name="cardPanel">the card panel</param>
		private void CreateQuizPanel(string difficulty, CardPanel cardPanel)
		{
			StudentQuiz quiz = null;
			if (difficulty.Equals("easy"))
			{
				if (tglbtnTimed.IsSelected())
				{
					quiz = new StudentQuizTime(cardPanel, "easy", tglbtnShuffle.IsSelected());
				}
				else
				{
					quiz = new StudentQuiz(cardPanel, "easy", tglbtnShuffle.IsSelected());
				}
			}
			if (difficulty.Equals("medium"))
			{
				if (tglbtnTimed.IsSelected())
				{
					quiz = new StudentQuizTime(cardPanel, "medium", tglbtnShuffle.IsSelected());
				}
				else
				{
					quiz = new StudentQuiz(cardPanel, "medium", tglbtnShuffle.IsSelected());
				}
			}
			if (difficulty.Equals("hard"))
			{
				if (tglbtnTimed.IsSelected())
				{
					quiz = new StudentQuizTime(cardPanel, "hard", tglbtnShuffle.IsSelected());
				}
				else
				{
					quiz = new StudentQuiz(cardPanel, "hard", tglbtnShuffle.IsSelected());
				}
			}
			cardPanel.Add(quiz, "studentQuiz");
			cardPanel.c1.Show(cardPanel, "studentQuiz");
		}
	}
}
