using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JMenus
{
	/// <summary>The Base Menu Bar Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class BaseMenu : JMenuBar
	{
		/// <summary>The file menu.</summary>
		/// <remarks>The file menu.</remarks>
		protected internal JMenu fileMenu = new JMenu("File");

		/// <summary>The help menu.</summary>
		/// <remarks>The help menu.</remarks>
		protected internal JMenu helpMenu = new JMenu("Help");

		/// <summary>The edit menu.</summary>
		/// <remarks>The edit menu.</remarks>
		protected internal JMenu editMenu = new JMenu("Edit");

		/// <summary>Instantiates a new base menu.</summary>
		/// <remarks>Instantiates a new base menu.</remarks>
		public BaseMenu()
		{
			Add(fileMenu);
			Add(editMenu);
			Add(helpMenu);
			editMenu.SetVisible(false);
			JMenuItem mntmAboutUs = new JMenuItem("About Us");
			mntmAboutUs.SetIcon(new ImageIcon(typeof(JPanels.JMenus.BaseMenu).GetResource("/resources/icons/System/help.png"
				)));
			mntmAboutUs.AddMouseListener(new _MouseAdapter_40());
			helpMenu.Add(mntmAboutUs);
			JMenuItem exitAction = new JMenuItem("Exit");
			exitAction.SetIcon(new ImageIcon(typeof(JPanels.JMenus.BaseMenu).GetResource("/resources/icons/System/logout.png"
				)));
			exitAction.AddMouseListener(new _MouseAdapter_50());
			//2 is cancel
			fileMenu.Add(exitAction);
		}

		private sealed class _MouseAdapter_40 : MouseAdapter
		{
			public _MouseAdapter_40()
			{
			}

			public override void MouseReleased(MouseEvent e)
			{
				JOptionPane.ShowMessageDialog(null, "Who wants to be Electricuted? Made by Dolan Miu. (c) 2012"
					);
			}
		}

		private sealed class _MouseAdapter_50 : MouseAdapter
		{
			public _MouseAdapter_50()
			{
			}

			public override void MouseReleased(MouseEvent e)
			{
				int result = JOptionPane.ShowConfirmDialog(null, "Are you sure?", "alert", JOptionPane
					.OK_CANCEL_OPTION);
				if (result == 0)
				{
					System.Environment.Exit(0);
				}
			}
		}
	}
}
