using Core;
using JPanels;
using JPanels.JDialogs;
using JPanels.JMenus;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JMenus
{
	/// <summary>The Circuit Maker Menu Bar Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CircuitMakerMenu : BaseMenu
	{
		/// <summary>The circuit.</summary>
		/// <remarks>The circuit.</remarks>
		internal CircuitMaker circuit;

		/// <summary>Instantiates a new circuit maker menu.</summary>
		/// <remarks>Instantiates a new circuit maker menu.</remarks>
		/// <param name="cm">the CircuitMaker object</param>
		public CircuitMakerMenu(CircuitMaker cm)
		{
			this.circuit = cm;
			editMenu.SetVisible(true);
			JMenuItem newAction = new JMenuItem("New Quiz");
			newAction.SetIcon(new ImageIcon(typeof(JPanels.JMenus.CircuitMakerMenu).GetResource
				("/resources/icons/Adds/add_file.png")));
			newAction.AddMouseListener(new _MouseAdapter_37(this));
			//2 is cancel
			JMenuItem openAction = new JMenuItem("Open Quiz");
			openAction.AddMouseListener(new _MouseAdapter_49(this));
			JMenuItem searchQuestion = new JMenuItem("Search Question");
			searchQuestion.AddMouseListener(new _MouseAdapter_56(this));
			editMenu.Add(searchQuestion);
			fileMenu.AddSeparator();
			fileMenu.Add(newAction);
			fileMenu.Add(openAction);
			JMenuItem mntmSaveQuiz = new JMenuItem("Save Quiz");
			mntmSaveQuiz.SetIcon(new ImageIcon(typeof(JPanels.JMenus.CircuitMakerMenu).GetResource
				("/resources/icons/System/save.png")));
			mntmSaveQuiz.AddMouseListener(new _MouseAdapter_71(this));
			fileMenu.Add(mntmSaveQuiz);
			JMenuItem mntmSaveQuizAs = new JMenuItem("Save Quiz As..");
			mntmSaveQuizAs.SetIcon(new ImageIcon(typeof(JPanels.JMenus.CircuitMakerMenu).GetResource
				("/resources/icons/System/save_as.png")));
			mntmSaveQuizAs.AddMouseListener(new _MouseAdapter_81(this));
			fileMenu.Add(mntmSaveQuizAs);
			editMenu.AddSeparator();
			helpMenu.AddSeparator();
			JMenuItem mntmTutorial = new JMenuItem("Tutorial On Circuit Creation");
			mntmTutorial.AddMouseListener(new _MouseAdapter_93());
			JMenuItem mntmTutorialOnQuestion = new JMenuItem("Tutorial on Question Creation");
			mntmTutorialOnQuestion.AddMouseListener(new _MouseAdapter_101());
			helpMenu.Add(mntmTutorialOnQuestion);
			JMenuItem mntmTutorialOnAnswer = new JMenuItem("Tutorial on Answer Creation");
			mntmTutorialOnAnswer.AddMouseListener(new _MouseAdapter_110());
			helpMenu.Add(mntmTutorialOnAnswer);
			helpMenu.Add(mntmTutorial);
		}

		private sealed class _MouseAdapter_37 : MouseAdapter
		{
			public _MouseAdapter_37(CircuitMakerMenu _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				int result = JOptionPane.ShowConfirmDialog(null, "Are you sure? Be sure you saved!"
					, "alert", JOptionPane.OK_CANCEL_OPTION);
				if (result == 0)
				{
					Reader.questionList.Clear();
					this._enclosing.circuit.UpdateAll();
				}
			}

			private readonly CircuitMakerMenu _enclosing;
		}

		private sealed class _MouseAdapter_49 : MouseAdapter
		{
			public _MouseAdapter_49(CircuitMakerMenu _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.circuit.ShowFileOpen();
			}

			private readonly CircuitMakerMenu _enclosing;
		}

		private sealed class _MouseAdapter_56 : MouseAdapter
		{
			public _MouseAdapter_56(CircuitMakerMenu _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				SearchQuestion search = new SearchQuestion();
				Question question = search.GetSelected();
				this._enclosing.circuit.SetQuestionComboBox(question);
			}

			private readonly CircuitMakerMenu _enclosing;
		}

		private sealed class _MouseAdapter_71 : MouseAdapter
		{
			public _MouseAdapter_71(CircuitMakerMenu _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.circuit.ShowFileSave();
			}

			private readonly CircuitMakerMenu _enclosing;
		}

		private sealed class _MouseAdapter_81 : MouseAdapter
		{
			public _MouseAdapter_81(CircuitMakerMenu _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseReleased(MouseEvent e)
			{
				this._enclosing.circuit.ShowFileSaveAs();
			}

			private readonly CircuitMakerMenu _enclosing;
		}

		private sealed class _MouseAdapter_93 : MouseAdapter
		{
			public _MouseAdapter_93()
			{
			}

			public override void MouseReleased(MouseEvent e)
			{
				JOptionPane.ShowMessageDialog(null, "Basically, you use the buttons on the top to create modules \n(parrallel parts of a circuit), then you put components inside them. Simple! \nYou could alternatively just put 1 component per module... \nto create a 100% series circuit!"
					);
			}
		}

		private sealed class _MouseAdapter_101 : MouseAdapter
		{
			public _MouseAdapter_101()
			{
			}

			public override void MouseReleased(MouseEvent e)
			{
				JOptionPane.ShowMessageDialog(null, "Click on the \"+Question\" button, select what type of question. \nDrag is simple drag and drop, Text allows the student \nto type an answer in, Gesture makes the student follow a path you \nset with his/her finger! Fill the question \nwith text and its own circuit.\n\n Remember, there are 4 Types of Questions: \n\t1) Drag and Drop \n\t2) Text THEN Drag and Drop (Similar)\n\t3) Text \n\t4) Gestures"
					);
			}
		}

		private sealed class _MouseAdapter_110 : MouseAdapter
		{
			public _MouseAdapter_110()
			{
			}

			public override void MouseReleased(MouseEvent e)
			{
				JOptionPane.ShowMessageDialog(null, "The answer creation system uses the DolanSMART checker (c)\n\n By creating your circuit with the Voids having the appropriate resistances, voltages etc..\n you in effect create the WORKING circuit. By dragging answers onto the Voids, the answers replace the Voids. The system checks to see if the initial Circuit state matches with the \nstate afterwards. If they match, the student got it correct! This way, it will \naccomodate ALL variations (if any) of answers if the circuit becomes complex."
					);
			}
		}
	}
}
