using JPanels.JMenus;
using JPanels.Misc;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.JMenus
{
	/// <summary>The Student Quiz Menu Class.</summary>
	/// <remarks>The Student Quiz Menu Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class StudentQuizMenu : BaseMenu
	{
		/// <summary>Instantiates a new student quiz menu.</summary>
		/// <remarks>Instantiates a new student quiz menu.</remarks>
		/// <param name="cardPanel">the card panel</param>
		public StudentQuizMenu(CardPanel cardPanel)
		{
			fileMenu.AddSeparator();
			JMenuItem mntmBackToMenu = new JMenuItem("Back to Menu");
			mntmBackToMenu.AddMouseListener(new _MouseAdapter_28(cardPanel));
			mntmBackToMenu.SetIcon(new ImageIcon(typeof(JPanels.JMenus.StudentQuizMenu).GetResource
				("/resources/icons/Very_Basic/undo.png")));
			fileMenu.Add(mntmBackToMenu);
			JMenuItem mntmGestureHelp = new JMenuItem("Gesture Help");
			mntmGestureHelp.SetIcon(new ImageIcon(typeof(JPanels.JMenus.StudentQuizMenu).GetResource
				("/resources/icons/Touch/one_finger.png")));
			mntmGestureHelp.AddMouseListener(new _MouseAdapter_39());
			helpMenu.AddSeparator();
			helpMenu.Add(mntmGestureHelp);
		}

		private sealed class _MouseAdapter_28 : MouseAdapter
		{
			public _MouseAdapter_28(CardPanel cardPanel)
			{
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent e)
			{
				cardPanel.c1.Show(cardPanel, "welcome");
			}

			private readonly CardPanel cardPanel;
		}

		private sealed class _MouseAdapter_39 : MouseAdapter
		{
			public _MouseAdapter_39()
			{
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				JOptionPane.ShowMessageDialog(null, "To use Gestures: \n\nRight click and drag! Simple. \nTypes of gestures: \n\n  Drag Down + Drag Right = Next Question\n  Drag Down + Drag Left = Back to Menu\n\nGesture Questions: \nnHold right click and drag the outline of where you think the path \nof eddy eletron goes"
					);
			}
		}
	}
}
