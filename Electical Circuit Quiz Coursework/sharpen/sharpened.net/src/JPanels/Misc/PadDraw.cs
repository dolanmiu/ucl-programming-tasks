using System.IO;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Java.Awt.Image;
using Javax.Swing;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>The Pad Draw Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	internal class PadDraw : JComponent
	{
		/// <summary>The image.</summary>
		/// <remarks>The image.</remarks>
		internal Image image;

		/// <summary>The graphics 2d.</summary>
		/// <remarks>The graphics 2d.</remarks>
		internal Graphics2D graphics2D;

		/// <summary>The old y.</summary>
		/// <remarks>The old y.</remarks>
		internal int currentX;

		/// <summary>The old y.</summary>
		/// <remarks>The old y.</remarks>
		internal int currentY;

		/// <summary>The old y.</summary>
		/// <remarks>The old y.</remarks>
		internal int oldX;

		/// <summary>The old y.</summary>
		/// <remarks>The old y.</remarks>
		internal int oldY;

		/// <summary>Instantiates a new pad draw.</summary>
		/// <remarks>Instantiates a new pad draw.</remarks>
		public PadDraw()
		{
			//Read more: http://forum.codecall.net/topic/58137-java-mini-paint-program/#ixzz2CUftAE6J
			//this is gonna be your image that you draw on
			//this is what we'll be using to draw on
			//these are gonna hold our mouse coordinates
			//Now for the constructors
			SetDoubleBuffered(false);
			AddMouseListener(new _MouseAdapter_40(this));
			//if the mouse is pressed it sets the oldX & oldY
			//coordinates as the mouses x & y coordinates
			AddMouseMotionListener(new _MouseMotionAdapter_48(this));
		}

		private sealed class _MouseAdapter_40 : MouseAdapter
		{
			public _MouseAdapter_40(PadDraw _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MousePressed(MouseEvent e)
			{
				this._enclosing.oldX = e.GetX();
				this._enclosing.oldY = e.GetY();
			}

			private readonly PadDraw _enclosing;
		}

		private sealed class _MouseMotionAdapter_48 : MouseMotionAdapter
		{
			public _MouseMotionAdapter_48(PadDraw _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MouseDragged(MouseEvent e)
			{
				this._enclosing.currentX = e.GetX();
				this._enclosing.currentY = e.GetY();
				if (this._enclosing.graphics2D != null)
				{
					this._enclosing.graphics2D.DrawLine(this._enclosing.oldX, this._enclosing.oldY, this
						._enclosing.currentX, this._enclosing.currentY);
				}
				this._enclosing.Repaint();
				this._enclosing.oldX = this._enclosing.currentX;
				this._enclosing.oldY = this._enclosing.currentY;
			}

			private readonly PadDraw _enclosing;
		}

		//while the mouse is dragged it sets currentX & currentY as the mouses x and y
		//then it draws a line at the coordinates
		//it repaints it and sets oldX and oldY as currentX and currentY
		protected override void PaintComponent(Graphics g)
		{
			if (image == null)
			{
				image = CreateImage(GetSize().width, GetSize().height);
				graphics2D = (Graphics2D)image.GetGraphics();
				graphics2D.SetRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON
					);
				Clear();
			}
			g.DrawImage(image, 0, 0, null);
		}

		//this is the painting bit
		//if it has nothing on it then
		//it creates an image the size of the window
		//sets the value of Graphics as the image
		//sets the rendering
		//runs the clear() method
		//then it draws the image
		/// <summary>Clear the canvas.</summary>
		/// <remarks>Clear the canvas.</remarks>
		public virtual void Clear()
		{
			graphics2D.SetPaint(Color.white);
			graphics2D.FillRect(0, 0, GetSize().width, GetSize().height);
			graphics2D.SetPaint(Color.black);
			Repaint();
		}

		//this is the clear
		//it sets the colors as white
		//then it fills the window with white
		//thin it sets the color back to black
		/// <summary>Red.</summary>
		/// <remarks>Red.</remarks>
		public virtual void Red()
		{
			graphics2D.SetPaint(Color.red);
			Repaint();
		}

		//this is the red paint
		/// <summary>Black.</summary>
		/// <remarks>Black.</remarks>
		public virtual void Black()
		{
			graphics2D.SetPaint(Color.black);
			Repaint();
		}

		//black paint
		/// <summary>Magenta.</summary>
		/// <remarks>Magenta.</remarks>
		public virtual void Magenta()
		{
			graphics2D.SetPaint(Color.magenta);
			Repaint();
		}

		//magenta paint
		/// <summary>Blue.</summary>
		/// <remarks>Blue.</remarks>
		public virtual void Blue()
		{
			graphics2D.SetPaint(Color.blue);
			Repaint();
		}

		//blue paint
		/// <summary>Green.</summary>
		/// <remarks>Green.</remarks>
		public virtual void Green()
		{
			graphics2D.SetPaint(Color.green);
			Repaint();
		}

		//green paint
		/// <summary>Save image to disk.</summary>
		/// <remarks>Save image to disk.</remarks>
		public virtual void SaveImage()
		{
			BufferedImage bi = null;
			bi = ScreenImage.CreateImage(this);
			try
			{
				ScreenImage.WriteImage(bi, "Student Notes.png");
			}
			catch (IOException e)
			{
				Sharpen.Runtime.PrintStackTrace(e);
			}
		}
	}
}
