using System.Collections.Generic;
using System.IO;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Image;
using Javax.Imageio;
using Javax.Swing;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>The Screen Image Class</summary>
	/// <author>Rob Camick</author>
	/// <version>1.0</version>
	public class ScreenImage
	{
		/// <summary>The types.</summary>
		/// <remarks>The types.</remarks>
		private static IList<string> types = Arrays.AsList(ImageIO.GetWriterFileSuffixes(
			));

		//http://tips4java.wordpress.com/2008/10/13/screen-image/
		// TODO: Auto-generated Javadoc
		/// <summary>Create a BufferedImage for Swing components.</summary>
		/// <remarks>
		/// Create a BufferedImage for Swing components.
		/// The entire component will be captured to an image.
		/// </remarks>
		/// <param name="component">the component</param>
		/// <returns>image the image for the given region</returns>
		public static BufferedImage CreateImage(JComponent component)
		{
			Dimension d = component.GetSize();
			if (d.width == 0 || d.height == 0)
			{
				d = component.GetPreferredSize();
				component.SetSize(d);
			}
			Rectangle region = new Rectangle(0, 0, d.width, d.height);
			return ScreenImage.CreateImage(component, region);
		}

		/// <summary>Create a BufferedImage for Swing components.</summary>
		/// <remarks>
		/// Create a BufferedImage for Swing components.
		/// All or part of the component can be captured to an image.
		/// </remarks>
		/// <param name="component">the component</param>
		/// <param name="region">the region</param>
		/// <returns>image the image for the given region</returns>
		public static BufferedImage CreateImage(JComponent component, Rectangle region)
		{
			//  Make sure the component has a size and has been layed out.
			//  (necessary check for components not added to a realized frame)
			if (!component.IsDisplayable())
			{
				Dimension d = component.GetSize();
				if (d.width == 0 || d.height == 0)
				{
					d = component.GetPreferredSize();
					component.SetSize(d);
				}
				LayoutComponent(component);
			}
			BufferedImage image = new BufferedImage(region.width, region.height, BufferedImage
				.TYPE_INT_RGB);
			Graphics2D g2d = image.CreateGraphics();
			//  Paint a background for non-opaque components,
			//  otherwise the background will be black
			if (!component.IsOpaque())
			{
				g2d.SetColor(component.GetBackground());
				g2d.FillRect(region.x, region.y, region.width, region.height);
			}
			g2d.Translate(-region.x, -region.y);
			component.Paint(g2d);
			g2d.Dispose();
			return image;
		}

		/// <summary>Convenience method to create a BufferedImage of the desktop.</summary>
		/// <remarks>Convenience method to create a BufferedImage of the desktop.</remarks>
		/// <returns>image the image for the given region</returns>
		/// <exception cref="Java.Awt.AWTException">see Robot class constructors</exception>
		/// <exception cref="System.IO.IOException">if an error occurs during writing</exception>
		public static BufferedImage CreateDesktopImage()
		{
			Dimension d = Toolkit.GetDefaultToolkit().GetScreenSize();
			Rectangle region = new Rectangle(0, 0, d.width, d.height);
			return ScreenImage.CreateImage(region);
		}

		/// <summary>Create a BufferedImage for AWT components.</summary>
		/// <remarks>Create a BufferedImage for AWT components.</remarks>
		/// <param name="component">the component</param>
		/// <returns>image the image for the given region</returns>
		/// <exception cref="Java.Awt.AWTException">see Robot class constructors</exception>
		public static BufferedImage CreateImage(Component component)
		{
			Point p = new Point(0, 0);
			SwingUtilities.ConvertPointToScreen(p, component);
			Rectangle region = component.GetBounds();
			region.x = p.x;
			region.y = p.y;
			return ScreenImage.CreateImage(region);
		}

		/// <summary>Create a BufferedImage from a rectangular region on the screen.</summary>
		/// <remarks>
		/// Create a BufferedImage from a rectangular region on the screen.
		/// This will include Swing components JFrame, JDialog and JWindow
		/// which all extend from Component, not JComponent.
		/// </remarks>
		/// <param name="region">the region</param>
		/// <returns>image the image for the given region</returns>
		/// <exception cref="Java.Awt.AWTException">see Robot class constructors</exception>
		public static BufferedImage CreateImage(Rectangle region)
		{
			BufferedImage image = new Robot().CreateScreenCapture(region);
			return image;
		}

		/// <summary>Write a BufferedImage to a File.</summary>
		/// <remarks>Write a BufferedImage to a File.</remarks>
		/// <param name="image">the image</param>
		/// <param name="fileName">the file name</param>
		/// <exception cref="System.IO.IOException">if an error occurs during writing</exception>
		public static void WriteImage(BufferedImage image, string fileName)
		{
			if (fileName == null)
			{
				return;
			}
			int offset = fileName.LastIndexOf(".");
			if (offset == -1)
			{
				string message = "file suffix was not specified";
				throw new IOException(message);
			}
			string type = Sharpen.Runtime.Substring(fileName, offset + 1);
			if (types.Contains(type))
			{
				ImageIO.Write(image, type, new FilePath(fileName));
			}
			else
			{
				string message = "unknown writer file suffix (" + type + ")";
				throw new IOException(message);
			}
		}

		/// <summary>Layout component.</summary>
		/// <remarks>Layout component.</remarks>
		/// <param name="component">the component</param>
		internal static void LayoutComponent(Component component)
		{
			lock (component.GetTreeLock())
			{
				component.DoLayout();
				if (component is Container)
				{
					foreach (Component child in ((Container)component).GetComponents())
					{
						LayoutComponent(child);
					}
				}
			}
		}
	}
}
