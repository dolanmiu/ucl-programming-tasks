using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>
	/// The Hint Text Field Class
	/// Does a 21st century style text box where if empty will show you a hint.
	/// </summary>
	/// <remarks>
	/// The Hint Text Field Class
	/// Does a 21st century style text box where if empty will show you a hint.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class HintTextArea : JTextField, FocusListener
	{
		/// <summary>The hint.</summary>
		/// <remarks>The hint.</remarks>
		private readonly string hint;

		/// <summary>Instantiates a new hint text area.</summary>
		/// <remarks>Instantiates a new hint text area.</remarks>
		/// <param name="hint">the hint</param>
		public HintTextArea(string hint) : base(hint)
		{
			//got help from http://stackoverflow.com/questions/1738966/java-jtextfield-with-input-hint
			this.hint = hint;
			base.AddFocusListener(this);
		}

		/// <summary>Force hint to come up on box.</summary>
		/// <remarks>Force hint to come up on box.</remarks>
		public virtual void ForceHint()
		{
			base.SetText(hint);
		}

		public virtual void FocusGained(FocusEvent e)
		{
			if (this.GetText().IsEmpty())
			{
				base.SetText(string.Empty);
			}
		}

		public virtual void FocusLost(FocusEvent e)
		{
			if (this.GetText().IsEmpty())
			{
				base.SetText(hint);
			}
		}

		public override string GetText()
		{
			string typed = base.GetText();
			return typed.Equals(hint) ? string.Empty : typed;
		}
	}
}
