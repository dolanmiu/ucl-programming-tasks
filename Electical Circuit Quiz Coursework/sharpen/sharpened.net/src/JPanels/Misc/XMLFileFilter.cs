using JPanels.Misc;
using Javax.Swing.Filechooser;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>The XML File Filter Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class XMLFileFilter : FileFilter
	{
		//http://www.javaprogrammingforums.com/java-programming-tutorials/4954-java-tip-aug-4-2010-how-use-file-filters.html
		public override string GetDescription()
		{
			return "XML File (.xml)";
		}

		public override bool Accept(FilePath f)
		{
			if (f.IsDirectory())
			{
				return true;
			}
			return f.GetName().EndsWith(".xml");
		}
	}
}
