using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>The Paint Panel Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class PaintPanel : JPanel
	{
		/// <summary>Instantiates a new paint panel.</summary>
		/// <remarks>Instantiates a new paint panel.</remarks>
		public PaintPanel()
		{
			//Read more: http://forum.codecall.net/topic/58137-java-mini-paint-program/#ixzz2CUftAE6J
			Paint();
		}

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		public virtual void Paint()
		{
			Icon iconB = new ImageIcon("blue.gif");
			//the blue image icon
			Icon iconM = new ImageIcon("magenta.gif");
			//magenta image icon
			Icon iconR = new ImageIcon("red.gif");
			//red image icon
			Icon iconBl = new ImageIcon("black.gif");
			//black image icon
			Icon iconG = new ImageIcon("green.gif");
			//finally the green image icon
			//These will be the images for our colors.
			//JFrame frame = new JFrame("Paint It");
			//Creates a frame with a title of "Paint it"
			//Container content = frame.getContentPane();
			//Creates a new container
			SetLayout(new BorderLayout());
			PadDraw drawPad = new PadDraw();
			JPanel paintPalet = new JPanel();
			Add(drawPad, BorderLayout.CENTER);
			//creates a JPanel
			paintPalet.SetPreferredSize(new Dimension(32, 68));
			paintPalet.SetMinimumSize(new Dimension(32, 68));
			paintPalet.SetMaximumSize(new Dimension(32, 68));
			//This sets the size of the panel
			JButton clearButton = new JButton("Clear");
			//creates the clear button and sets the text as "Clear"
			clearButton.AddActionListener(new _ActionListener_63(drawPad));
			//this is the clear button, which clears the screen.  This pretty
			//much attaches an action listener to the button and when the
			//button is pressed it calls the clear() method
			JButton redButton = new JButton(iconR);
			//creates the red button and sets the icon we created for red
			redButton.AddActionListener(new _ActionListener_76(drawPad));
			//when pressed it will call the red() method.  So on and so on =]
			JButton blackButton = new JButton(iconBl);
			//same thing except this is the black button
			blackButton.AddActionListener(new _ActionListener_84(drawPad));
			JButton magentaButton = new JButton(iconM);
			//magenta button
			magentaButton.AddActionListener(new _ActionListener_91(drawPad));
			JButton blueButton = new JButton(iconB);
			//blue button
			blueButton.AddActionListener(new _ActionListener_98(drawPad));
			JButton greenButton = new JButton(iconG);
			//green button
			greenButton.AddActionListener(new _ActionListener_105(drawPad));
			blackButton.SetPreferredSize(new Dimension(16, 16));
			magentaButton.SetPreferredSize(new Dimension(16, 16));
			redButton.SetPreferredSize(new Dimension(16, 16));
			blueButton.SetPreferredSize(new Dimension(16, 16));
			greenButton.SetPreferredSize(new Dimension(16, 16));
			//sets the sizes of the buttons
			paintPalet.Add(greenButton);
			paintPalet.Add(blueButton);
			paintPalet.Add(magentaButton);
			paintPalet.Add(blackButton);
			paintPalet.Add(redButton);
			paintPalet.Add(clearButton);
			//adds the buttons to the panel
			Add(paintPalet, BorderLayout.WEST);
			JButton btnSave = new JButton("Save");
			btnSave.AddMouseListener(new _MouseAdapter_126(drawPad));
			//http://www.java2s.com/Code/Java/2D-Graphics-GUI/DrawanImageandsavetopng.htm
			paintPalet.Add(btnSave);
		}

		private sealed class _ActionListener_63 : ActionListener
		{
			public _ActionListener_63(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Clear();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _ActionListener_76 : ActionListener
		{
			public _ActionListener_76(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Red();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _ActionListener_84 : ActionListener
		{
			public _ActionListener_84(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Black();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _ActionListener_91 : ActionListener
		{
			public _ActionListener_91(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Magenta();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _ActionListener_98 : ActionListener
		{
			public _ActionListener_98(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Blue();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _ActionListener_105 : ActionListener
		{
			public _ActionListener_105(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public void ActionPerformed(ActionEvent e)
			{
				drawPad.Green();
			}

			private readonly PadDraw drawPad;
		}

		private sealed class _MouseAdapter_126 : MouseAdapter
		{
			public _MouseAdapter_126(PadDraw drawPad)
			{
				this.drawPad = drawPad;
			}

			public override void MouseReleased(MouseEvent e)
			{
				drawPad.SaveImage();
			}

			private readonly PadDraw drawPad;
		}
		//sets the panel to the left
		//makes it so you can close
	}
}
