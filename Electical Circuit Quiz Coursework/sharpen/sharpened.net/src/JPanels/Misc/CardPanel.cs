using Java.Awt;
using Javax.Swing;
using Sharpen;

namespace JPanels.Misc
{
	/// <summary>The CardPanel Class</summary>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CardPanel : JPanel
	{
		/// <summary>The card layout.</summary>
		/// <remarks>The card layout.</remarks>
		public CardLayout c1;

		/// <summary>Instantiates a new card panel.</summary>
		/// <remarks>Instantiates a new card panel.</remarks>
		public CardPanel()
		{
			c1 = new CardLayout();
			SetLayout(c1);
		}
	}
}
