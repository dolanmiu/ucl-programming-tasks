using JPanels.JMenus;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels
{
	/// <summary>The Welcome Window Class.</summary>
	/// <remarks>
	/// The Welcome Window Class.
	/// Shows at the beginning of the application.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class WelcomeWindow : JPanel
	{
		/// <summary>Instantiates a new welcome window.</summary>
		/// <remarks>Instantiates a new welcome window.</remarks>
		/// <param name="cardPanel">the card panel</param>
		public WelcomeWindow(CardPanel cardPanel)
		{
			SetBounds(100, 100, 898, 477);
			SetLayout(new BorderLayout(0, 0));
			JPanel panel = new JPanel();
			Add(panel, BorderLayout.CENTER);
			panel.SetLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			Component horizontalGlue = Box.CreateHorizontalGlue();
			panel.Add(horizontalGlue);
			BaseMenu menu = new BaseMenu();
			Add(menu, BorderLayout.NORTH);
			JPanel panel_2 = new JPanel();
			panel.Add(panel_2);
			panel_2.SetLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
			JPanel panel_4 = new JPanel();
			panel_2.Add(panel_4);
			panel_4.SetLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));
			JLabel lblWhoWantsTo = new JLabel("Who wants to be Electricuted?");
			panel_4.Add(lblWhoWantsTo);
			lblWhoWantsTo.SetForeground(Color.DARK_GRAY);
			lblWhoWantsTo.SetFont(new Font("Dotum", Font.PLAIN, 50));
			Component rigidArea_1 = Box.CreateRigidArea(new Dimension(0, 30));
			panel_2.Add(rigidArea_1);
			JPanel panel_1 = new JPanel();
			panel_2.Add(panel_1);
			panel_1.SetLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
			Component horizontalGlue3 = Box.CreateHorizontalGlue();
			panel_1.Add(horizontalGlue3);
			JPanel panel_3 = new JPanel();
			panel_1.Add(panel_3);
			panel_3.SetLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
			JButton btnTeacher = new JButton("Teacher");
			btnTeacher.SetIcon(new ImageIcon(typeof(JPanels.WelcomeWindow).GetResource("/resources/icons/Security/key.png"
				)));
			panel_3.Add(btnTeacher);
			Component rigidArea_2 = Box.CreateRigidArea(new Dimension(0, 30));
			panel_3.Add(rigidArea_2);
			JButton btnStudent = new JButton("Student");
			btnStudent.SetIcon(new ImageIcon(typeof(JPanels.WelcomeWindow).GetResource("/resources/icons/System/login.png"
				)));
			panel_3.Add(btnStudent);
			Component horizontalGlue4 = Box.CreateHorizontalGlue();
			panel_1.Add(horizontalGlue4);
			btnStudent.AddMouseListener(new _MouseAdapter_82(cardPanel));
			//Writer write = new Writer();
			btnTeacher.AddMouseListener(new _MouseAdapter_90(cardPanel));
			Component horizontalGlue2 = Box.CreateHorizontalGlue();
			panel.Add(horizontalGlue2);
		}

		private sealed class _MouseAdapter_82 : MouseAdapter
		{
			public _MouseAdapter_82(CardPanel cardPanel)
			{
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent arg0)
			{
				cardPanel.c1.Show(cardPanel, "diffSelect");
			}

			private readonly CardPanel cardPanel;
		}

		private sealed class _MouseAdapter_90 : MouseAdapter
		{
			public _MouseAdapter_90(CardPanel cardPanel)
			{
				this.cardPanel = cardPanel;
			}

			public override void MouseReleased(MouseEvent e)
			{
				JPasswordField pwd = new JPasswordField(10);
				int action = JOptionPane.ShowConfirmDialog(null, pwd, "Enter Password", JOptionPane
					.OK_CANCEL_OPTION);
				if (action < 0)
				{
					JOptionPane.ShowMessageDialog(null, "Cancel, X or escape key selected");
				}
				string password = new string(pwd.GetPassword());
				if (password.Equals(string.Empty))
				{
					cardPanel.c1.Show(cardPanel, "circuitMaker");
				}
				else
				{
					JOptionPane.ShowMessageDialog(null, "Incorrect Password.");
				}
			}

			private readonly CardPanel cardPanel;
		}
	}
}
