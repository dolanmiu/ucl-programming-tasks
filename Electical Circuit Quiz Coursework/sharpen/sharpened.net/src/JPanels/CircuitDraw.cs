using System;
using Core;
using ElectricalComponents;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels
{
	/// <summary>The Circuit Draw Class.</summary>
	/// <remarks>
	/// The Circuit Draw Class.
	/// Panel to draw a circuit.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class CircuitDraw : JPanel, ActionListener
	{
		/// <summary>The questions.</summary>
		/// <remarks>The questions.</remarks>
		public AList<Question> questions;

		/// <summary>The last drawn.</summary>
		/// <remarks>The last drawn.</remarks>
		public int lastDrawn = 0;

		/// <summary>The is collided.</summary>
		/// <remarks>The is collided.</remarks>
		public bool isCollided;

		/// <summary>Instantiates a new circuit draw.</summary>
		/// <remarks>Instantiates a new circuit draw.</remarks>
		/// <param name="questions">the questions</param>
		public CircuitDraw(AList<Question> questions)
		{
			this.questions = questions;
			SetLayout(null);
			DrawCircuit(0);
			CircuitCalc.SetValidCircuit(questions[lastDrawn].circuit.GetModules());
		}

		/// <summary>Update circuit.</summary>
		/// <remarks>Update circuit.</remarks>
		/// <param name="question">the question</param>
		public virtual void UpdateCircuit(int question)
		{
			RemoveAll();
			UpdateUI();
			lastDrawn = question;
			DrawCircuit(lastDrawn);
			CircuitCalc.SetValidCircuit(questions[lastDrawn].circuit.GetModules());
		}

		/// <summary>Draw circuit.</summary>
		/// <remarks>Draw circuit.</remarks>
		/// <param name="question">the question</param>
		public virtual void DrawCircuit(int question)
		{
			Timer timer = new Timer(0, this);
			timer.Start();
			Question currentQuestion = questions[question];
			Circuit circuit = currentQuestion.circuit;
			AList<Module> modules = circuit.GetModules();
			for (int i = 0; i < modules.Count; i++)
			{
				for (int j = 0; j < modules[i].GetComponentAmmount(); j++)
				{
					Add(modules[i].GetComponant(j));
					Add(modules[i]);
				}
			}
			for (int i_1 = 0; i_1 < currentQuestion.answers.Count; i_1++)
			{
				Add(currentQuestion.answers[i_1]);
			}
			for (int i_2 = 0; i_2 < currentQuestion.circuit.GetWires().Count; i_2++)
			{
				Add(currentQuestion.circuit.GetWires()[i_2]);
			}
		}

		/// <summary>Checks if is drawable.</summary>
		/// <remarks>Checks if is drawable.</remarks>
		/// <returns>true, if is drawable</returns>
		public virtual bool IsDrawable()
		{
			if (lastDrawn + 1 < questions.Count)
			{
				return true;
			}
			return false;
		}

		/// <summary>Draw next circuit.</summary>
		/// <remarks>Draw next circuit.</remarks>
		public virtual void DrawNextCircuit()
		{
			if (IsDrawable())
			{
				UpdateCircuit(lastDrawn + 1);
			}
		}

		/// <summary>Gets the question number.</summary>
		/// <remarks>Gets the question number.</remarks>
		/// <returns>the question number</returns>
		public virtual int GetQuestionNumber()
		{
			return lastDrawn;
		}

		/// <summary>Gets the current question.</summary>
		/// <remarks>Gets the current question.</remarks>
		/// <returns>the current question</returns>
		public virtual Question GetCurrentQuestion()
		{
			return questions[lastDrawn];
		}

		/// <summary>Gets the total resistance.</summary>
		/// <remarks>Gets the total resistance.</remarks>
		/// <returns>the total resistance</returns>
		public virtual float GetTotalResistance()
		{
			AList<Module> modules = questions[lastDrawn].circuit.GetModules();
			float resistance = CircuitCalc.CalcResistance(modules);
			return (float)Math.Round(resistance * 100) / 100;
		}

		/// <summary>Checks if is correct.</summary>
		/// <remarks>Checks if is correct.</remarks>
		/// <returns>true, if is correct</returns>
		public virtual bool IsCorrect()
		{
			return CircuitCalc.CheckValidCircuit(questions[lastDrawn].circuit.GetModules());
		}

		/// <summary>Check collisions.</summary>
		/// <remarks>Check collisions.</remarks>
		public virtual void CheckCollisions()
		{
			isCollided = false;
			Question question = questions[lastDrawn];
			AList<ElectricalComponent> answers = question.GetAnswers();
			//get array of component answers
			AList<Module> modules = question.circuit.GetModules();
			for (int i = 0; i < modules.Count; i++)
			{
				AList<ElectricalComponent> comps = question.circuit.GetModules()[i].ReturnComponents
					();
				//Get components of circuit
				for (int j = 0; j < comps.Count; j++)
				{
					ElectricalComponent collided = comps[j].CheckAnsCollision(answers);
					//Check collision
					if (collided != null)
					{
						comps[j].SetResistance(collided.GetResistance());
						comps[j].SetVoltage(collided.GetVoltage());
						//Only for Cells in theory
						comps[j].SetCurrent(collided.GetCurrent());
						//Only for Cells in theory
						isCollided = true;
					}
				}
			}
		}

		/// <summary>Check wires are OK.</summary>
		/// <remarks>Check wires are OK.</remarks>
		public virtual void CheckWires()
		{
			AList<Module> modules = questions[lastDrawn].circuit.GetModules();
			for (int i = 0; i < modules.Count; i++)
			{
				modules[i].RefreshBounds();
			}
		}

		public virtual void ActionPerformed(ActionEvent e)
		{
			CheckCollisions();
			CheckWires();
		}
	}
}
