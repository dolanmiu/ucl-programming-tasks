using System.IO;
using Core;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace JPanels
{
	/// <summary>The Quiz Result Class.</summary>
	/// <remarks>
	/// The Quiz Result Class.
	/// Displays the grade/score of the student, and creates a report if need be.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class QuizResult : JPanel
	{
		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		public QuizResult()
		{
			int score = Marker.GetScore();
			char grade = Marker.GetGrade();
			SetLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			JPanel panel_3 = new JPanel();
			Add(panel_3);
			panel_3.SetLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));
			Component horizontalGlue = Box.CreateHorizontalGlue();
			panel_3.Add(horizontalGlue);
			JPanel panel = new JPanel();
			panel_3.Add(panel);
			panel.SetLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			JPanel panel_2 = new JPanel();
			panel_2.SetBorder(new EmptyBorder(0, 0, 40, 0));
			panel.Add(panel_2);
			panel_2.SetLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
			JLabel lblCongrats = new JLabel("Congratulations " + Marker.GetName() + "!");
			lblCongrats.SetForeground(Color.DARK_GRAY);
			panel_2.Add(lblCongrats);
			lblCongrats.SetFont(new Font("Dotum", Font.PLAIN, 50));
			JPanel panel_4 = new JPanel();
			panel.Add(panel_4);
			panel_4.SetLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
			JPanel panel_5 = new JPanel();
			panel_5.SetBorder(new EmptyBorder(0, 0, 5, 0));
			panel_4.Add(panel_5);
			panel_5.SetLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));
			JLabel lblYouHaveScored = new JLabel("You have scored:");
			lblYouHaveScored.SetFont(new Font("Dotum", Font.PLAIN, 21));
			panel_5.Add(lblYouHaveScored);
			lblYouHaveScored.SetText("You have scored: " + score + " points!");
			JPanel panel_6 = new JPanel();
			panel_6.SetBorder(new EmptyBorder(0, 0, 5, 0));
			panel_4.Add(panel_6);
			panel_6.SetLayout(new BoxLayout(panel_6, BoxLayout.X_AXIS));
			JLabel lblGrade = new JLabel("Grade:");
			lblGrade.SetFont(new Font("Dotum", Font.PLAIN, 32));
			panel_6.Add(lblGrade);
			lblGrade.SetText("Grade: " + grade);
			JPanel panel_7 = new JPanel();
			panel_7.SetBorder(new EmptyBorder(0, 0, 5, 0));
			panel_4.Add(panel_7);
			panel_7.SetLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));
			JLabel lblMotivationalMessage = new JLabel("Motivational Message");
			lblMotivationalMessage.SetFont(new Font("Dotum", Font.PLAIN, 12));
			panel_7.Add(lblMotivationalMessage);
			lblMotivationalMessage.SetText(Marker.GetMessage());
			JPanel panel_1 = new JPanel();
			panel.Add(panel_1);
			panel_1.SetLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
			JButton btnSaveReport = new JButton("Save Report");
			panel_1.Add(btnSaveReport);
			Component horizontalGlue2 = Box.CreateHorizontalGlue();
			panel_3.Add(horizontalGlue2);
			btnSaveReport.AddMouseListener(new _MouseAdapter_99(this));
		}

		private sealed class _MouseAdapter_99 : MouseAdapter
		{
			public _MouseAdapter_99(QuizResult _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void MousePressed(MouseEvent e)
			{
				try
				{
					this._enclosing.CreateReport();
				}
				catch (IOException e1)
				{
					Sharpen.Runtime.PrintStackTrace(e1);
				}
			}

			private readonly QuizResult _enclosing;
		}

		/// <summary>Creates the report.</summary>
		/// <remarks>Creates the report.</remarks>
		/// <exception cref="System.IO.IOException">Signals that an I/O exception has occurred.
		/// 	</exception>
		private void CreateReport()
		{
			FilePath file = new FilePath("c:\\write.txt");
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.Write("Student Name: " + Marker.GetName());
			output.NewLine();
			output.Write("Score: " + Marker.GetScore());
			output.NewLine();
			output.Write("Grade: " + Marker.GetGrade());
			output.NewLine();
			output.NewLine();
			output.Write("Questions Attempted:");
			output.NewLine();
			for (int i = 0; i < Marker.GetQuestionStat().Count; i++)
			{
				output.Write("\t" + Marker.GetQuestionStat()[i]);
				output.NewLine();
			}
			output.Close();
			System.Console.Out.WriteLine("File written");
		}
	}
}
