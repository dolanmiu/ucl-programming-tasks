using System;
using Core;
using JPanels;
using JPanels.Misc;
using Java.Awt;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

/// <summary>The Main Window.</summary>
/// <remarks>The Main Window. Where everything is ran from</remarks>
/// <author>DolanMiu</author>
/// <version>0.1Alpha</version>
[System.Serializable]
public class MainWindow : JFrame
{
	/// <summary>The content pane.</summary>
	/// <remarks>The content pane.</remarks>
	private JPanel contentPane;

	// TODO: Auto-generated Javadoc
	//private JPanel cardPanel;
	/// <summary>Launch the application.</summary>
	/// <remarks>Launch the application.</remarks>
	/// <param name="args">the arguments</param>
	public static void Main(string[] args)
	{
		EventQueue.InvokeLater(new _Runnable_36());
	}

	private sealed class _Runnable_36 : Runnable
	{
		public _Runnable_36()
		{
		}

		public void Run()
		{
			try
			{
				MainWindow frame = new MainWindow();
				//Creates a new frame to display with JPanels to contain the Quiz
				frame.Pack();
				//and sets the frame properties
				frame.SetVisible(true);
			}
			catch (Exception e)
			{
				Sharpen.Runtime.PrintStackTrace(e);
			}
		}
	}

	/// <summary>Create the frame.</summary>
	/// <remarks>Create the frame.</remarks>
	public MainWindow()
	{
		FilePath folder = new FilePath("C:\\electric\\");
		FilePath[] listOfFiles = folder.ListFiles();
		Reader reader = new Reader();
		//The Reader class is the XML parser
		foreach (FilePath listOfFile in listOfFiles)
		{
			if (listOfFile.IsFile())
			{
			}
		}
		//UNCOMMENT THIS AT FINAL
		//If it fails, it will catch to prevent error
		try
		{
			//reader.scanFile("electricalcircuit.xml");		
			reader.ScanFile("electricaloutput.xml");
		}
		catch (Exception)
		{
		}
		SetTitle("Who wants to be Electricuted");
		SetDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SetBounds(100, 100, 450, 300);
		SetMinimumSize(new Dimension(800, 600));
		contentPane = new JPanel();
		contentPane.SetBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.SetLayout(new BorderLayout(0, 0));
		SetContentPane(contentPane);
		CardPanel cardPanel = new CardPanel();
		//I am using various cards to display the various JPanels
		WelcomeWindow welcome = new WelcomeWindow(cardPanel);
		//Each of these creates a new JPanel which is then
		DifficultySelect diffSelect = new DifficultySelect(cardPanel);
		cardPanel.Add(welcome, "welcome");
		CircuitMaker circuitMaker = new CircuitMaker();
		//added to CardPanel
		cardPanel.Add(circuitMaker, "circuitMaker");
		//This is where it's added. Only the relevant ones are added for now
		cardPanel.Add(diffSelect, "diffSelect");
		//circuitMaker (Teacher), diffSelect (Student)
		contentPane.Add(cardPanel);
	}
}
