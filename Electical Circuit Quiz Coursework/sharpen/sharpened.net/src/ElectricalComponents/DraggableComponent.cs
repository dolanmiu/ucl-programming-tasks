using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Draggable Component Class.</summary>
	/// <remarks>
	/// The Draggable Component Class.
	/// Every Electrical component which is on stage should be draggable.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class DraggableComponent : JComponent
	{
		/// <summary>The screen x.</summary>
		/// <remarks>The screen x.</remarks>
		private volatile int screenX = 0;

		/// <summary>The screen y.</summary>
		/// <remarks>The screen y.</remarks>
		private volatile int screenY = 0;

		/// <summary>The x coordinate.</summary>
		/// <remarks>The x coordinate.</remarks>
		private volatile int myX = 0;

		/// <summary>The y coordinate.</summary>
		/// <remarks>The y coordinate.</remarks>
		private volatile int myY = 0;

		/// <summary>Instantiates a new draggable component.</summary>
		/// <remarks>Instantiates a new draggable component.</remarks>
		public DraggableComponent()
		{
			//http://stackoverflow.com/questions/874360/swing-creating-a-draggable-component
			SetBorder(new LineBorder(Color.GRAY, 3));
			SetBackground(Color.WHITE);
			SetBounds(0, 0, 100, 100);
			SetOpaque(false);
			AddMouseListener(new _MouseListener_41(this));
			AddMouseMotionListener(new _MouseMotionListener_59(this));
		}

		private sealed class _MouseListener_41 : MouseListener
		{
			public _MouseListener_41(DraggableComponent _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void MouseClicked(MouseEvent e)
			{
			}

			public void MousePressed(MouseEvent e)
			{
				this._enclosing.screenX = e.GetXOnScreen();
				this._enclosing.screenY = e.GetYOnScreen();
				this._enclosing.myX = this._enclosing.GetX();
				this._enclosing.myY = this._enclosing.GetY();
			}

			public void MouseReleased(MouseEvent e)
			{
			}

			public void MouseEntered(MouseEvent e)
			{
			}

			public void MouseExited(MouseEvent e)
			{
			}

			private readonly DraggableComponent _enclosing;
		}

		private sealed class _MouseMotionListener_59 : MouseMotionListener
		{
			public _MouseMotionListener_59(DraggableComponent _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void MouseDragged(MouseEvent e)
			{
				int deltaX = e.GetXOnScreen() - this._enclosing.screenX;
				int deltaY = e.GetYOnScreen() - this._enclosing.screenY;
				this._enclosing.SetLocation(this._enclosing.myX + deltaX, this._enclosing.myY + deltaY
					);
			}

			public void MouseMoved(MouseEvent e)
			{
			}

			private readonly DraggableComponent _enclosing;
		}
	}
}
