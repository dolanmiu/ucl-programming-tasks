using ElectricalComponents;
using Java.Awt;
using Javax.Swing;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Module Class.</summary>
	/// <remarks>
	/// The Module Class.
	/// Every Electrical component is housed in a module.
	/// Sometimes more than one component is in one for parallel circuits!
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Module : JComponent
	{
		/// <summary>The components.</summary>
		/// <remarks>The components.</remarks>
		private AList<ElectricalComponent> components = new AList<ElectricalComponent>();

		/// <summary>The module number.</summary>
		/// <remarks>The module number.</remarks>
		private int moduleNumber;

		/// <summary>The reversed.</summary>
		/// <remarks>The reversed.</remarks>
		public bool reversed = true;

		/// <summary>The voltage.</summary>
		/// <remarks>The voltage.</remarks>
		public float voltage = 0;

		/// <summary>Instantiates a new module.</summary>
		/// <remarks>Instantiates a new module.</remarks>
		/// <param name="moduleNumber">the module number</param>
		public Module(int moduleNumber)
		{
			SetBounds(0, 0, 1000, 1000);
			//the size of the module
			this.moduleNumber = moduleNumber;
		}

		/// <summary>Removes the component.</summary>
		/// <remarks>Removes the component.</remarks>
		/// <param name="i">componentID;</param>
		public virtual void RemoveComponent(int i)
		{
			components.Remove(i);
		}

		/// <summary>Sets the component voltage.</summary>
		/// <remarks>Sets the component voltage.</remarks>
		public virtual void SetComponentVoltage()
		{
			for (int i = 0; i < components.Count; i++)
			{
				components[i].SetVoltsAcross(voltage);
			}
		}

		/// <summary>Gets the voltage.</summary>
		/// <remarks>Gets the voltage.</remarks>
		/// <returns>the voltage</returns>
		public virtual float GetVoltage()
		{
			return voltage;
		}

		/// <summary>Sets the volts across.</summary>
		/// <remarks>Sets the volts across.</remarks>
		/// <param name="voltage">the new volts across</param>
		public virtual void SetVoltsAcross(float voltage)
		{
			this.voltage = voltage;
			for (int i = 0; i < components.Count; i++)
			{
				components[i].SetVoltsAcross(voltage);
			}
		}

		/// <summary>Adds the component.</summary>
		/// <remarks>Adds the component.</remarks>
		/// <param name="comp">the component</param>
		public virtual void AddComponant(ElectricalComponent comp)
		{
			components.AddItem(comp);
			RefreshBounds();
		}

		/// <summary>Checks if is reversed.</summary>
		/// <remarks>Checks if is reversed.</remarks>
		/// <returns>true, if is reversed</returns>
		public virtual bool IsReversed()
		{
			return reversed;
		}

		/// <summary>Sets the reversed.</summary>
		/// <remarks>Sets the reversed.</remarks>
		/// <param name="bool">the new reversed</param>
		public virtual void SetReversed(bool @bool)
		{
			if (@bool == true)
			{
				reversed = true;
			}
			else
			{
				reversed = false;
			}
		}

		/// <summary>Refresh bounds.</summary>
		/// <remarks>Refresh bounds.</remarks>
		public virtual void RefreshBounds()
		{
			int highestX = 0;
			int highestY = 0;
			int lowestX = int.MaxValue;
			int lowestY = int.MaxValue;
			int width = 0;
			int height = 0;
			for (int i = 0; i < components.Count; i++)
			{
				if (lowestX > components[i].GetX())
				{
					lowestX = components[i].GetX();
				}
				if (lowestY > components[i].GetY())
				{
					lowestY = components[i].GetY();
				}
				if (highestY < components[i].GetY())
				{
					highestY = components[i].GetY();
				}
				if (highestX < components[i].GetX())
				{
					highestX = components[i].GetX();
				}
				width = highestX - lowestX;
				height = highestY - lowestY;
			}
			SetBounds(lowestX - 30, lowestY - 30, width + 160, height + 160);
			Repaint();
		}

		protected override void PaintComponent(Graphics g)
		{
			base.PaintComponent(g);
			Rectangle bound = GetBounds();
			g.DrawLine(0, 80, 0, bound.height - 80);
			g.DrawLine(bound.width - 1, 80, bound.width - 1, bound.height - 80);
			for (int i = 0; i < components.Count; i++)
			{
				int xcoord = components[i].GetX() - this.GetX();
				int ycoord = components[i].GetY() - this.GetY() + 50;
				//shift it to centre
				g.DrawLine(xcoord, ycoord, 0, ycoord);
				g.DrawLine(xcoord + 100, ycoord, GetWidth(), ycoord);
			}
		}

		/// <summary>Prints the components.</summary>
		/// <remarks>Prints the components.</remarks>
		public virtual void PrintComponents()
		{
			System.Console.Out.WriteLine("Size of componants: " + components.Count);
			for (int i = 0; i < components.Count; i++)
			{
				System.Console.Out.WriteLine(components[i].ToString());
			}
		}

		/// <summary>Gets the component.</summary>
		/// <remarks>Gets the component.</remarks>
		/// <param name="i">component ID</param>
		/// <returns>the component</returns>
		public virtual ElectricalComponent GetComponant(int i)
		{
			return components[i];
		}

		/// <summary>Gets the component amount.</summary>
		/// <remarks>Gets the component amount.</remarks>
		/// <returns>the component amount</returns>
		public virtual int GetComponentAmmount()
		{
			return components.Count;
		}

		/// <summary>Return components.</summary>
		/// <remarks>Return components.</remarks>
		/// <returns>the array list</returns>
		public virtual AList<ElectricalComponent> ReturnComponents()
		{
			return components;
		}

		public override string ToString()
		{
			return ("Module " + moduleNumber);
		}
	}
}
