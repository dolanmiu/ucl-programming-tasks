using ElectricalComponents;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Cell Class.</summary>
	/// <remarks>The Cell Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Cell : ElectricalComponent
	{
		/// <summary>The img.</summary>
		/// <remarks>The img.</remarks>
		private Image img;

		/// <summary>Instantiates a new cell.</summary>
		/// <remarks>Instantiates a new cell.</remarks>
		/// <param name="voltage">the voltage</param>
		/// <param name="current">the current</param>
		/// <param name="x">the x</param>
		/// <param name="y">the y</param>
		public Cell(float voltage, float current, int x, int y)
		{
			this.voltage = voltage;
			this.current = current;
			this.x = x;
			this.y = y;
			this.SetLocation(x, y);
			SetBorder(new LineBorder(Color.GREEN, 3));
			//ImageIcon icon = new ImageIcon("bulb.jpg");
			PaintVoltage();
			PaintCurrent();
		}

		/// <summary>Paint voltage label.</summary>
		/// <remarks>Paint voltage label.</remarks>
		private void PaintVoltage()
		{
			JTextArea voltageText = new JTextArea(voltage + string.Empty);
			voltageText.AddKeyListener(new _KeyAdapter_51(this, voltageText));
			voltageText.SetBackground(Color.WHITE);
			voltageText.SetOpaque(true);
			Add(voltageText);
			voltageText.SetSize(voltageText.GetPreferredSize());
			voltageText.SetLocation(0, 65);
			JLabel voltageUnit = new JLabel("V");
			voltageUnit.SetBackground(Color.WHITE);
			voltageUnit.SetOpaque(true);
			Add(voltageUnit);
			voltageUnit.SetSize(voltageUnit.GetPreferredSize());
			voltageUnit.SetLocation(18, 65);
		}

		private sealed class _KeyAdapter_51 : KeyAdapter
		{
			public _KeyAdapter_51(Cell _enclosing, JTextArea voltageText)
			{
				this._enclosing = _enclosing;
				this.voltageText = voltageText;
			}

			public override void KeyReleased(KeyEvent arg0)
			{
				this._enclosing.voltage = float.ParseFloat(voltageText.GetText());
			}

			private readonly Cell _enclosing;

			private readonly JTextArea voltageText;
		}

		/// <summary>Paint current label.</summary>
		/// <remarks>Paint current label.</remarks>
		private void PaintCurrent()
		{
			JTextArea currentText = new JTextArea(current + string.Empty);
			currentText.AddMouseListener(new _MouseAdapter_77(this, currentText));
			currentText.SetBackground(Color.WHITE);
			currentText.SetOpaque(true);
			Add(currentText);
			currentText.SetSize(currentText.GetPreferredSize());
			currentText.SetLocation(0, 80);
			JLabel currentUnit = new JLabel("A");
			currentUnit.SetBackground(Color.WHITE);
			currentUnit.SetOpaque(true);
			Add(currentUnit);
			currentUnit.SetSize(currentUnit.GetPreferredSize());
			currentUnit.SetLocation(18, 80);
		}

		private sealed class _MouseAdapter_77 : MouseAdapter
		{
			public _MouseAdapter_77(Cell _enclosing, JTextArea currentText)
			{
				this._enclosing = _enclosing;
				this.currentText = currentText;
			}

			public override void MouseExited(MouseEvent arg0)
			{
				this._enclosing.current = float.ParseFloat(currentText.GetText());
			}

			private readonly Cell _enclosing;

			private readonly JTextArea currentText;
		}

		public override float GetVoltage()
		{
			return voltage;
		}

		public override void SetVoltage(float voltage)
		{
			this.voltage = voltage;
		}

		public override float GetCurrent()
		{
			return current;
		}

		public override void SetCurrent(float current)
		{
			this.current = current;
		}

		public override string ToString()
		{
			return "Cell";
		}
	}
}
