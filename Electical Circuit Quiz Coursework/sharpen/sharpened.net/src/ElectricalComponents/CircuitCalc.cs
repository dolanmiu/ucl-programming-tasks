using ElectricalComponents;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Circuit Calculator Class.</summary>
	/// <remarks>
	/// The Circuit Calculator Class.
	/// This Class does all the resistance, voltage and current calculations of the circuit.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class CircuitCalc
	{
		/// <summary>The correct voltage.</summary>
		/// <remarks>The correct voltage.</remarks>
		private static float correctVoltage;

		/// <summary>The correct current.</summary>
		/// <remarks>The correct current.</remarks>
		private static float correctCurrent;

		/// <summary>Calculate resistance.</summary>
		/// <remarks>Calculate resistance.</remarks>
		/// <param name="modules">the modules</param>
		/// <returns>the resistance</returns>
		public static float CalcResistance(AList<Module> modules)
		{
			int ammount = modules.Count;
			float totalResistance = 0;
			for (int i = 0; i < ammount; i++)
			{
				totalResistance += CalcParallelResistance(modules[i]);
			}
			return totalResistance;
		}

		/// <summary>Sets the valid circuit.</summary>
		/// <remarks>Sets the valid circuit.</remarks>
		/// <param name="modules">modules in the circuit.</param>
		public static void SetValidCircuit(AList<Module> modules)
		{
			correctVoltage = CalcTotalCellVoltage(modules);
			correctCurrent = CalcTotalCellCurrent(modules);
		}

		/// <summary>Check valid circuit.</summary>
		/// <remarks>Check valid circuit.</remarks>
		/// <param name="modules">the modules of the new circuit.</param>
		/// <returns>true, if successful</returns>
		public static bool CheckValidCircuit(AList<Module> modules)
		{
			float currentVoltage = CircuitCalc.CalcTotalCellVoltage(modules);
			float currentCurrent = CircuitCalc.CalcTotalCellCurrent(modules);
			System.Console.Out.WriteLine("current Voltage" + currentVoltage + " correct Voltage"
				 + correctVoltage);
			System.Console.Out.WriteLine("current Current" + currentCurrent + " correct Current"
				 + correctCurrent);
			if (currentVoltage == correctVoltage && currentCurrent == correctCurrent)
			{
				return true;
			}
			return false;
		}

		/// <summary>Calculate current.</summary>
		/// <remarks>Calculate current.</remarks>
		/// <param name="cell">the cell</param>
		/// <returns>the current</returns>
		public static float CalcCurrent(Cell cell)
		{
			return cell.GetCurrent();
		}

		/// <summary>Calculate total cell voltage.</summary>
		/// <remarks>Calculate total cell voltage.</remarks>
		/// <param name="modules">the modules of circuit</param>
		/// <returns>the voltage</returns>
		public static float CalcTotalCellVoltage(AList<Module> modules)
		{
			AList<ElectricalComponent> cells = GetCells(modules);
			float R = CalcResistance(modules);
			float I = GetCellsCurrent(cells);
			float V = I * R;
			return V;
		}

		/// <summary>Calculate total cell current.</summary>
		/// <remarks>Calculate total cell current.</remarks>
		/// <param name="modules">the modules of circuit</param>
		/// <returns>the current</returns>
		public static float CalcTotalCellCurrent(AList<Module> modules)
		{
			AList<ElectricalComponent> cells = GetCells(modules);
			float R = CalcResistance(modules);
			float V = GetCellsVoltage(cells);
			float I = (V / R);
			return I;
		}

		/// <summary>Gets the cells voltage.</summary>
		/// <remarks>Gets the cells voltage.</remarks>
		/// <param name="cells">the cells</param>
		/// <returns>the cells voltage</returns>
		public static float GetCellsVoltage(AList<ElectricalComponent> cells)
		{
			float totalCurrent = 0;
			for (int i = 0; i < cells.Count; i++)
			{
				totalCurrent += cells[i].GetVoltage();
			}
			return totalCurrent;
		}

		/// <summary>Gets the cells current.</summary>
		/// <remarks>Gets the cells current.</remarks>
		/// <param name="cells">the cells</param>
		/// <returns>the cells current</returns>
		public static float GetCellsCurrent(AList<ElectricalComponent> cells)
		{
			float totalCurrent = 0;
			for (int i = 0; i < cells.Count; i++)
			{
				totalCurrent += cells[i].GetCurrent();
			}
			return totalCurrent;
		}

		/// <summary>Gets the cells.</summary>
		/// <remarks>Gets the cells.</remarks>
		/// <param name="modules">the modules</param>
		/// <returns>the cells</returns>
		public static AList<ElectricalComponent> GetCells(AList<Module> modules)
		{
			AList<ElectricalComponent> cells = new AList<ElectricalComponent>();
			for (int i = 0; i < modules.Count; i++)
			{
				Module module = modules[i];
				AList<ElectricalComponent> comps = module.ReturnComponents();
				for (int j = 0; j < comps.Count; j++)
				{
					ElectricalComponent comp = comps[j];
					if (comp.ToString().Equals("Cell") || comp.HasVoltage())
					{
						cells.AddItem(comp);
					}
				}
			}
			return cells;
		}

		/// <summary>Calculate parallel resistance.</summary>
		/// <remarks>Calculate parallel resistance.</remarks>
		/// <param name="module">the module</param>
		/// <returns>The Total Resistance</returns>
		public static float CalcParallelResistance(Module module)
		{
			int ammount = module.GetComponentAmmount();
			float totalConductance = 0;
			for (int i = 0; i < ammount; i++)
			{
				ElectricalComponent comp = module.GetComponant(i);
				totalConductance += 1f / comp.GetResistance();
			}
			if (float.IsInfinite(totalConductance))
			{
			}
			//System.err.println("Conductance is infinate");
			float totalResistance = 1f / totalConductance;
			return totalResistance;
		}
	}
}
