using ElectricalComponents;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Resistor Class.</summary>
	/// <remarks>The Resistor Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Resistor : ElectricalComponent
	{
		/// <summary>Instantiates a new resistor.</summary>
		/// <remarks>Instantiates a new resistor.</remarks>
		/// <param name="resistance">the resistance</param>
		/// <param name="x">the x</param>
		/// <param name="y">the y</param>
		public Resistor(float resistance, int x, int y)
		{
			// TODO: Auto-generated Javadoc
			this.resistance = resistance;
			this.x = x;
			this.y = y;
			this.SetLocation(x, y);
			PaintResistance();
		}

		public override string ToString()
		{
			return "Resistor";
		}
	}
}
