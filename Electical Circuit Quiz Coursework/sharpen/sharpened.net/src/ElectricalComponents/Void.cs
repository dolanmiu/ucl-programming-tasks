using ElectricalComponents;
using Java.Awt;
using Javax.Swing.Border;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Void Class.</summary>
	/// <remarks>
	/// The Void Class.
	/// Is an empty component to be filled with a component during the answering process of the quiz.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Void : ElectricalComponent
	{
		/// <summary>The image.</summary>
		/// <remarks>The image.</remarks>
		private Image img;

		/// <summary>Instantiates a new void.</summary>
		/// <remarks>Instantiates a new void.</remarks>
		/// <param name="resistance">the resistance</param>
		/// <param name="voltage">the voltage</param>
		/// <param name="current">the current</param>
		/// <param name="x">the x</param>
		/// <param name="y">the y</param>
		/// <param name="textAnswer">the text answer</param>
		public Void(float resistance, float voltage, float current, int x, int y, string 
			textAnswer)
		{
			this.resistance = resistance;
			this.voltage = voltage;
			this.current = current;
			SetBorder(new LineBorder(Color.BLACK, 3));
			this.x = x;
			this.y = y;
			this.SetLocation(x, y);
			this.textAnswer = textAnswer;
		}

		//img = Toolkit.getDefaultToolkit().createImage("src/resources/bulb.jpg");
		protected override void PaintComponent(Graphics g)
		{
			g.DrawImage(img, 0, 0, this);
		}

		public override ElectricalComponent CheckAnsCollision(AList<ElectricalComponent> 
			answers)
		{
			for (int i = 0; i < answers.Count; i++)
			{
				Rectangle answerBounds = answers[i].GetBounds();
				if (answerBounds.Intersects(GetBounds()))
				{
					SetBorderActive();
					return answers[i];
				}
				else
				{
					SetBorderDeactive();
				}
			}
			return null;
		}

		public override bool HasVoltage()
		{
			if (this.voltage > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public override float GetVoltage()
		{
			if (this.voltage > 0)
			{
				return voltage;
			}
			else
			{
				return 0;
			}
		}

		public override void SetVoltage(float voltage)
		{
			this.voltage = voltage;
		}

		public override void SetCurrent(float current)
		{
			this.current = current;
		}

		public override float GetCurrent()
		{
			if (this.current > 0)
			{
				return current;
			}
			else
			{
				return 0;
			}
		}

		public override string ToString()
		{
			return "Void";
		}
	}
}
