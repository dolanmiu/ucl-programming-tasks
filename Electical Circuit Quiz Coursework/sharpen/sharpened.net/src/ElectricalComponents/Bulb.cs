using ElectricalComponents;
using Java.Awt;
using Javax.Swing.Border;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Bulb Class.</summary>
	/// <remarks>The Bulb Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Bulb : ElectricalComponent
	{
		/// <summary>The img.</summary>
		/// <remarks>The img.</remarks>
		private Image img;

		/// <summary>The border colour.</summary>
		/// <remarks>The border colour.</remarks>
		public Color borderColour;

		/// <summary>Instantiates a new bulb.</summary>
		/// <remarks>Instantiates a new bulb.</remarks>
		/// <param name="resistance">the resistance</param>
		/// <param name="x">the x</param>
		/// <param name="y">the y</param>
		public Bulb(float resistance, int x, int y)
		{
			SetBorder(new LineBorder(Color.YELLOW, 3));
			this.resistance = resistance;
			this.x = x;
			this.y = y;
			this.SetLocation(x, y);
			img = Toolkit.GetDefaultToolkit().CreateImage("src/resources/bulb.jpg");
			PaintResistance();
		}

		protected override void PaintComponent(Graphics g)
		{
			g.DrawImage(img, 0, 0, this);
		}

		public override string ToString()
		{
			return "Bulb";
		}
	}
}
