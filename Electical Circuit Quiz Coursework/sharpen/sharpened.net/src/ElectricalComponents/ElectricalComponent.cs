using ElectricalComponents;
using Java.Awt;
using Javax.Swing;
using Javax.Swing.Border;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>The Electrical Component Class.</summary>
	/// <remarks>The Electrical Component Class.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public abstract class ElectricalComponent : DraggableComponent
	{
		/// <summary>The resistance.</summary>
		/// <remarks>The resistance.</remarks>
		protected internal float resistance = 0;

		/// <summary>The volts across.</summary>
		/// <remarks>The volts across.</remarks>
		protected internal float voltsAcross = 0;

		/// <summary>The x.</summary>
		/// <remarks>The x.</remarks>
		protected internal int x = 0;

		/// <summary>The y.</summary>
		/// <remarks>The y.</remarks>
		protected internal int y = 0;

		/// <summary>The voltage.</summary>
		/// <remarks>The voltage.</remarks>
		protected internal float voltage = 0;

		/// <summary>The current.</summary>
		/// <remarks>The current.</remarks>
		protected internal float current = 0;

		/// <summary>The text answer.</summary>
		/// <remarks>The text answer.</remarks>
		protected internal string textAnswer = string.Empty;

		/// <summary>Instantiates a new electrical component.</summary>
		/// <remarks>Instantiates a new electrical component.</remarks>
		public ElectricalComponent()
		{
			//for word type questions
			PaintName();
		}

		//paintVoltsAcross();
		/// <summary>Sets the text answer.</summary>
		/// <remarks>Sets the text answer.</remarks>
		/// <param name="text">The new text answer</param>
		public virtual void SetTextAnswer(string text)
		{
			textAnswer = text;
		}

		/// <summary>Gets the text answer.</summary>
		/// <remarks>Gets the text answer.</remarks>
		/// <returns>The text answer</returns>
		public virtual string GetTextAnswer()
		{
			return textAnswer;
		}

		/// <summary>Sets the resistance.</summary>
		/// <remarks>Sets the resistance.</remarks>
		/// <param name="res">the new resistance</param>
		public virtual void SetResistance(float res)
		{
			this.resistance = res;
		}

		/// <summary>Gets the resistance.</summary>
		/// <remarks>Gets the resistance.</remarks>
		/// <returns>the resistance</returns>
		public virtual float GetResistance()
		{
			return resistance;
		}

		/// <summary>Gets the voltage.</summary>
		/// <remarks>Gets the voltage.</remarks>
		/// <returns>the voltage. By default, unless it is overwritten, it will return -1 (e.g. resistors do not emit voltage).
		/// 	</returns>
		public virtual float GetVoltage()
		{
			return -1f;
		}

		/// <summary>Checks for voltage.</summary>
		/// <remarks>Checks for voltage.</remarks>
		/// <returns>true, if successful</returns>
		public virtual bool HasVoltage()
		{
			return false;
		}

		/// <summary>Sets the voltage.</summary>
		/// <remarks>Sets the voltage.</remarks>
		/// <param name="voltage">the new voltage</param>
		public virtual void SetVoltage(float voltage)
		{
		}

		/// <summary>Gets the current.</summary>
		/// <remarks>Gets the current.</remarks>
		/// <returns>the current. By default, unless it is overwritten, it will return -1 (e.g. resistors do not emit current).
		/// 	</returns>
		public virtual float GetCurrent()
		{
			return -1f;
		}

		/// <summary>Sets the current.</summary>
		/// <remarks>Sets the current.</remarks>
		/// <param name="voltage">the new current</param>
		public virtual void SetCurrent(float voltage)
		{
		}

		/// <summary>Paint name label.</summary>
		/// <remarks>Paint name label.</remarks>
		protected internal virtual void PaintName()
		{
			JLabel name = new JLabel(string.Empty);
			name.SetText(ToString());
			name.SetBackground(Color.WHITE);
			name.SetOpaque(true);
			Add(name);
			name.SetSize(name.GetPreferredSize());
			name.SetLocation(0, 50);
		}

		/// <summary>Paint volts across label.</summary>
		/// <remarks>Paint volts across label.</remarks>
		protected internal virtual void PaintVoltsAcross()
		{
			JLabel voltageLabel = new JLabel(string.Empty);
			voltageLabel.SetText(voltsAcross + " V");
			voltageLabel.SetBackground(Color.WHITE);
			voltageLabel.SetOpaque(true);
			Add(voltageLabel);
			voltageLabel.SetSize(voltageLabel.GetPreferredSize());
			voltageLabel.SetLocation(GetWidth() - voltageLabel.GetWidth(), 10);
		}

		/// <summary>Paint resistance label.</summary>
		/// <remarks>Paint resistance label.</remarks>
		protected internal virtual void PaintResistance()
		{
			//The children can call this function
			JLabel resistanceLabel = new JLabel(string.Empty);
			resistanceLabel.SetText(resistance + " Ohms");
			resistanceLabel.SetBackground(Color.WHITE);
			resistanceLabel.SetOpaque(true);
			Add(resistanceLabel);
			resistanceLabel.SetSize(resistanceLabel.GetPreferredSize());
			resistanceLabel.SetLocation(0, 65);
		}

		/// <summary>Check answer collision.</summary>
		/// <remarks>Check answer collision.</remarks>
		/// <param name="answers">The answers which the electrical component collides with</param>
		/// <returns>the electrical component</returns>
		public virtual ElectricalComponents.ElectricalComponent CheckAnsCollision(AList<ElectricalComponents.ElectricalComponent
			> answers)
		{
			return null;
		}

		/// <summary>Sets the border active.</summary>
		/// <remarks>Sets the border active.</remarks>
		public virtual void SetBorderActive()
		{
			SetBorder(new LineBorder(Color.WHITE, 3));
		}

		/// <summary>Sets the border not active.</summary>
		/// <remarks>Sets the border not active.</remarks>
		public virtual void SetBorderDeactive()
		{
			SetBorder(new LineBorder(Color.BLACK, 3));
		}

		/// <summary>Sets the volts across.</summary>
		/// <remarks>Sets the volts across.</remarks>
		/// <param name="voltage">the new volts across</param>
		public virtual void SetVoltsAcross(float voltage)
		{
			this.voltsAcross = voltage;
			PaintVoltsAcross();
		}

		/// <summary>Gets the volts across.</summary>
		/// <remarks>Gets the volts across.</remarks>
		/// <returns>the volts across</returns>
		public virtual float GetVoltsAcross()
		{
			return voltsAcross;
		}
	}
}
