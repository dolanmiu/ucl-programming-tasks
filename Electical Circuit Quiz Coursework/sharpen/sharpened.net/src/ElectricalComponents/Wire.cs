using ElectricalComponents;
using Java.Awt;
using Javax.Swing;
using Sharpen;

namespace ElectricalComponents
{
	/// <summary>
	/// The Wire Class
	/// Used to connected two modules together.
	/// </summary>
	/// <remarks>
	/// The Wire Class
	/// Used to connected two modules together.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class Wire : JComponent
	{
		/// <summary>The connected to.</summary>
		/// <remarks>The connected to.</remarks>
		private Module connectedTo;

		/// <summary>The connected from.</summary>
		/// <remarks>The connected from.</remarks>
		private Module connectedFrom;

		/// <summary>Instantiates a new wire.</summary>
		/// <remarks>Instantiates a new wire.</remarks>
		/// <param name="connectedFrom">the module connected from</param>
		/// <param name="connectedTo">the module connected to</param>
		public Wire(Module connectedFrom, Module connectedTo)
		{
			this.connectedTo = connectedTo;
			this.connectedFrom = connectedFrom;
			SetBounds(0, 0, 1000, 1000);
			if (this.connectedTo.GetX() == this.connectedFrom.GetX())
			{
				//i.e normal
				this.connectedFrom.SetReversed(false);
				this.connectedTo.SetReversed(true);
			}
		}

		/// <summary>Gets the module to.</summary>
		/// <remarks>Gets the module to.</remarks>
		/// <returns>the module to</returns>
		public virtual Module GetTo()
		{
			return connectedTo;
		}

		/// <summary>Gets the module from.</summary>
		/// <remarks>Gets the module from.</remarks>
		/// <returns>the module from</returns>
		public virtual Module GetFrom()
		{
			return connectedFrom;
		}

		/// <summary>Prints the connection.</summary>
		/// <remarks>Prints the connection.</remarks>
		public virtual void PrintConnection()
		{
			System.Console.Out.WriteLine("Connected From " + connectedFrom.ToString());
			System.Console.Out.WriteLine("Connected To " + connectedTo.ToString());
		}

		protected override void PaintComponent(Graphics g)
		{
			base.PaintComponent(g);
			int xcoordTo = connectedTo.GetX();
			//The left of the module
			int ycoordTo = connectedTo.GetY() + connectedTo.GetHeight() / 2;
			//The left of the module
			int xcoordFrom = connectedFrom.GetX() + connectedFrom.GetWidth();
			//The right of the module
			int ycoordFrom = connectedFrom.GetY() + connectedFrom.GetHeight() / 2;
			//The right of the module
			//System.out.println(connectedTo.getX() + " " + connectedFrom.getX());
			if (connectedTo.GetX() > connectedFrom.GetX())
			{
				//i.e normal
				connectedFrom.SetReversed(false);
			}
			if (connectedTo.GetX() < connectedFrom.GetX())
			{
				//i.e backwards
				connectedFrom.SetReversed(true);
			}
			if (!connectedFrom.IsReversed() && !connectedTo.IsReversed())
			{
				//g.drawLine(xcoordFrom,ycoordFrom,xcoordTo,ycoordTo); //normal
				g.DrawLine(xcoordFrom, ycoordFrom, xcoordTo, ycoordFrom);
				g.DrawLine(xcoordTo, ycoordFrom, xcoordTo, ycoordTo);
			}
			if (connectedFrom.IsReversed() && connectedTo.IsReversed())
			{
				//g.drawLine(connectedFrom.getX(),ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordTo); // backwards chain
				g.DrawLine(connectedFrom.GetX(), ycoordFrom, xcoordTo + connectedTo.GetWidth(), ycoordFrom
					);
				g.DrawLine(xcoordTo + connectedTo.GetWidth(), ycoordFrom, xcoordTo + connectedTo.
					GetWidth(), ycoordTo);
			}
			if (!connectedFrom.IsReversed() && connectedTo.IsReversed())
			{
				//g.drawLine(xcoordFrom,ycoordFrom,xcoordTo+connectedTo.getWidth(),ycoordTo); //corner
				g.DrawLine(xcoordFrom - 1, ycoordFrom, xcoordTo + connectedTo.GetWidth() - 1, ycoordFrom
					);
				g.DrawLine(xcoordTo + connectedTo.GetWidth() - 1, ycoordFrom, xcoordTo + connectedTo
					.GetWidth() - 1, ycoordTo);
				Repaint();
			}
			if (connectedFrom.IsReversed() && !connectedTo.IsReversed())
			{
				//g.drawLine(connectedFrom.getX(),ycoordFrom,xcoordTo,ycoordTo); // back to start
				g.DrawLine(connectedFrom.GetX(), ycoordFrom, xcoordTo, ycoordFrom);
				g.DrawLine(xcoordTo, ycoordFrom, xcoordTo, ycoordTo);
			}
		}
	}
}
