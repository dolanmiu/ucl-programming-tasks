using Com.Smardec.Mousegestures;
using Sharpen;

namespace Com.Smardec.Mousegestures
{
	/// <summary>Interface for mouse gestures listener.</summary>
	/// <remarks>Interface for mouse gestures listener.</remarks>
	/// <author>Smardec</author>
	/// <version>1.2</version>
	public interface MouseGesturesListener
	{
		/// <summary>Called when full mouse gesture is recogized (mouse button is released).</summary>
		/// <remarks>Called when full mouse gesture is recogized (mouse button is released).</remarks>
		/// <param name="gesture">
		/// String representation of mouse gesture. "L" for left, "R" for right,
		/// "U" for up, "D" for down movements. For example: "ULD".
		/// </param>
		void ProcessGesture(string gesture);

		/// <summary>Called when new mouse movement is recognized but mouse gesture is not yet completed.
		/// 	</summary>
		/// <remarks>Called when new mouse movement is recognized but mouse gesture is not yet completed.
		/// 	</remarks>
		/// <param name="currentGesture">
		/// String representation of recognized movements. "L" for left, "R" for right,
		/// "U" for up, "D" for down movements. For example: "ULD".
		/// </param>
		void GestureMovementRecognized(string currentGesture);
	}
}
