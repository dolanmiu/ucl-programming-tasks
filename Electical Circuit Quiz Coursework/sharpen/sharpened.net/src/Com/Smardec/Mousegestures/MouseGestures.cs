using System.Collections;
using Com.Smardec.Mousegestures;
using Java.Awt;
using Java.Awt.Event;
using Sharpen;

namespace Com.Smardec.Mousegestures
{
	/// <summary>Main class for mouse gestures.</summary>
	/// <remarks>
	/// Main class for mouse gestures.
	/// <br />Sample usage:
	/// <code><pre>
	/// MouseGestures mouseGestures = new MouseGestures();
	/// mouseGestures.addMouseGesturesListener(myMouseGesturesListener);
	/// mouseGestures.start();</pre>
	/// </code>
	/// </remarks>
	/// <author>Smardec</author>
	/// <version>1.2</version>
	public class MouseGestures
	{
		/// <summary>Responsible for monitoring mouse gestures.</summary>
		/// <remarks>Responsible for monitoring mouse gestures.</remarks>
		private AWTEventListener mouseGesturesEventListener = null;

		/// <summary>Responsible for processing mouse events.</summary>
		/// <remarks>Responsible for processing mouse events.</remarks>
		private MouseGesturesRecognizer mouseGesturesRecognizer;

		/// <summary>Vector of listeners.</summary>
		/// <remarks>Vector of listeners.</remarks>
		private ArrayList listeners = new ArrayList();

		/// <summary>Specifies mouse button for gestures handling.</summary>
		/// <remarks>
		/// Specifies mouse button for gestures handling.
		/// Can be <code>MouseEvent.BUTTON1_MASK</code>, <code>MouseEvent.BUTTON2_MASK</code>
		/// or <code>MouseEvent.BUTTON3_MASK</code>.
		/// The default is <code>MouseEvent.BUTTON3_MASK</code> (right mouse button).
		/// </remarks>
		private int mouseButton = MouseEvent.BUTTON3_MASK;

		/// <summary>Starts monitoring mouse gestures.</summary>
		/// <remarks>Starts monitoring mouse gestures.</remarks>
		public virtual void Start()
		{
			if (mouseGesturesEventListener == null)
			{
				mouseGesturesEventListener = new _AWTEventListener_67(this);
			}
			// prevents displaying popup menu and so on
			// clear temporary information
			// execute action                                
			// clear temporary information
			Toolkit.GetDefaultToolkit().AddAWTEventListener(mouseGesturesEventListener, AWTEvent
				.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK);
		}

		private sealed class _AWTEventListener_67 : AWTEventListener
		{
			public _AWTEventListener_67(MouseGestures _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void EventDispatched(AWTEvent @event)
			{
				if (@event is MouseEvent)
				{
					MouseEvent mouseEvent = (MouseEvent)@event;
					if ((mouseEvent.GetModifiers() & this._enclosing.mouseButton) == this._enclosing.
						mouseButton)
					{
						this._enclosing.mouseGesturesRecognizer.ProcessMouseEvent(mouseEvent);
					}
					if (((mouseEvent.GetID() == MouseEvent.MOUSE_RELEASED) || (mouseEvent.GetID() == 
						MouseEvent.MOUSE_CLICKED)) && (mouseEvent.GetModifiers() & this._enclosing.mouseButton
						) == this._enclosing.mouseButton)
					{
						if (this._enclosing.mouseGesturesRecognizer.IsGestureRecognized())
						{
							mouseEvent.Consume();
							string gesture = this._enclosing.mouseGesturesRecognizer.GetGesture();
							this._enclosing.mouseGesturesRecognizer.ClearTemporaryInfo();
							this._enclosing.FireProcessMouseGesture(gesture);
						}
						else
						{
							this._enclosing.mouseGesturesRecognizer.ClearTemporaryInfo();
						}
					}
				}
			}

			private readonly MouseGestures _enclosing;
		}

		/// <summary>Stops monitoring mouse gestures.</summary>
		/// <remarks>Stops monitoring mouse gestures.</remarks>
		public virtual void Stop()
		{
			if (mouseGesturesEventListener != null)
			{
				Toolkit.GetDefaultToolkit().RemoveAWTEventListener(mouseGesturesEventListener);
			}
		}

		/// <summary>Returns current grid size (minimum mouse movement length to be recognized).
		/// 	</summary>
		/// <remarks>Returns current grid size (minimum mouse movement length to be recognized).
		/// 	</remarks>
		/// <returns>Grid size in pixels. Default is 30.</returns>
		public virtual int GetGridSize()
		{
			return mouseGesturesRecognizer.GetGridSize();
		}

		/// <summary>Sets grid size (minimum mouse movement length to be recognized).</summary>
		/// <remarks>Sets grid size (minimum mouse movement length to be recognized).</remarks>
		/// <param name="gridSize">New grid size in pixels</param>
		public virtual void SetGridSize(int gridSize)
		{
			mouseGesturesRecognizer.SetGridSize(gridSize);
		}

		/// <summary>Returns mouse button used for gestures handling.</summary>
		/// <remarks>Returns mouse button used for gestures handling.</remarks>
		/// <returns>
		/// <code>MouseEvent.BUTTON1_MASK</code>, <code>MouseEvent.BUTTON2_MASK</code>
		/// or <code>MouseEvent.BUTTON3_MASK</code>
		/// </returns>
		public virtual int GetMouseButton()
		{
			return mouseButton;
		}

		/// <summary>Sets mouse button used for gestures handling.</summary>
		/// <remarks>Sets mouse button used for gestures handling.</remarks>
		/// <param name="mouseButton">
		/// <code>MouseEvent.BUTTON1_MASK</code>, <code>MouseEvent.BUTTON2_MASK</code>
		/// or <code>MouseEvent.BUTTON3_MASK</code>
		/// </param>
		public virtual void SetMouseButton(int mouseButton)
		{
			this.mouseButton = mouseButton;
		}

		/// <summary>Adds mouse gestures listener.</summary>
		/// <remarks>Adds mouse gestures listener.</remarks>
		/// <param name="listener">
		/// Instance of
		/// <see cref="MouseGesturesListener">MouseGesturesListener</see>
		/// </param>
		public virtual void AddMouseGesturesListener(MouseGesturesListener listener)
		{
			if (listener == null)
			{
				return;
			}
			listeners.AddItem(listener);
		}

		/// <summary>Removes mouse gestures listener.</summary>
		/// <remarks>Removes mouse gestures listener.</remarks>
		/// <param name="listener">
		/// Instance of
		/// <see cref="MouseGesturesListener">MouseGesturesListener</see>
		/// </param>
		public virtual void RemoveMouseGesturesListener(MouseGesturesListener listener)
		{
			if (listener == null)
			{
				return;
			}
			listeners.Remove(listener);
		}

		/// <summary>Fires event when full mouse gesture is recogized (mouse button is released).
		/// 	</summary>
		/// <remarks>Fires event when full mouse gesture is recogized (mouse button is released).
		/// 	</remarks>
		/// <param name="gesture">
		/// String representation of mouse gesture. "L" for left, "R" for right,
		/// "U" for up, "D" for down movements. For example: "ULD".
		/// </param>
		private void FireProcessMouseGesture(string gesture)
		{
			for (int i = 0; i < listeners.Count; i++)
			{
				((MouseGesturesListener)listeners[i]).ProcessGesture(gesture);
			}
		}

		/// <summary>Fires event when new mouse movement is recognized but mouse gesture is not yet completed.
		/// 	</summary>
		/// <remarks>Fires event when new mouse movement is recognized but mouse gesture is not yet completed.
		/// 	</remarks>
		/// <param name="gesture">
		/// String representation of recognized movements. "L" for left, "R" for right,
		/// "U" for up, "D" for down movements. For example: "ULD".
		/// </param>
		internal virtual void FireGestureMovementRecognized(string gesture)
		{
			for (int i = 0; i < listeners.Count; i++)
			{
				((MouseGesturesListener)listeners[i]).GestureMovementRecognized(gesture);
			}
		}

		public MouseGestures()
		{
			mouseGesturesRecognizer = new MouseGesturesRecognizer(this);
		}
	}
}
