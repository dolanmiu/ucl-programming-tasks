using System;
using System.Text;
using Com.Smardec.Mousegestures;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace Com.Smardec.Mousegestures
{
	/// <summary>Responsible for processing mouse events and recognition of mouse movements.
	/// 	</summary>
	/// <remarks>Responsible for processing mouse events and recognition of mouse movements.
	/// 	</remarks>
	/// <author>Smardec</author>
	/// <version>1.2</version>
	internal class MouseGesturesRecognizer
	{
		/// <summary>String representation of left movement.</summary>
		/// <remarks>String representation of left movement.</remarks>
		private static readonly string LEFT_MOVE = "L";

		/// <summary>String representation of right movement.</summary>
		/// <remarks>String representation of right movement.</remarks>
		private static readonly string RIGHT_MOVE = "R";

		/// <summary>String representation of up movement.</summary>
		/// <remarks>String representation of up movement.</remarks>
		private static readonly string UP_MOVE = "U";

		/// <summary>String representation of down movement.</summary>
		/// <remarks>String representation of down movement.</remarks>
		private static readonly string DOWN_MOVE = "D";

		/// <summary>Grid size.</summary>
		/// <remarks>Grid size. Default is 30.</remarks>
		private int gridSize = 30;

		/// <summary>
		/// Reference to
		/// <see cref="MouseGestures">MouseGestures</see>
		/// .
		/// </summary>
		private MouseGestures mouseGestures;

		/// <summary>Start point for current movement.</summary>
		/// <remarks>Start point for current movement.</remarks>
		private Point startPoint = null;

		/// <summary>String representation of gesture.</summary>
		/// <remarks>String representation of gesture.</remarks>
		private StringBuilder gesture = new StringBuilder();

		/// <summary>Creates MouseGesturesRecognizer.</summary>
		/// <remarks>Creates MouseGesturesRecognizer.</remarks>
		/// <param name="mouseGestures">
		/// Reference to
		/// <see cref="MouseGestures">MouseGestures</see>
		/// </param>
		internal MouseGesturesRecognizer(MouseGestures mouseGestures)
		{
			this.mouseGestures = mouseGestures;
		}

		/// <summary>Processes mouse event.</summary>
		/// <remarks>Processes mouse event.</remarks>
		/// <param name="mouseEvent">MouseEvent</param>
		internal virtual void ProcessMouseEvent(MouseEvent mouseEvent)
		{
			if (!(mouseEvent.GetSource() is Component))
			{
				return;
			}
			Point mouseEventPoint = mouseEvent.GetPoint();
			SwingUtilities.ConvertPointToScreen(mouseEventPoint, (Component)mouseEvent.GetSource
				());
			if (startPoint == null)
			{
				startPoint = mouseEventPoint;
				return;
			}
			int deltaX = GetDeltaX(startPoint, mouseEventPoint);
			int deltaY = GetDeltaY(startPoint, mouseEventPoint);
			int absDeltaX = Math.Abs(deltaX);
			int absDeltaY = Math.Abs(deltaY);
			if ((absDeltaX < gridSize) && (absDeltaY < gridSize))
			{
				return;
			}
			float absTangent = ((float)absDeltaX) / absDeltaY;
			if (absTangent < 1)
			{
				if (deltaY < 0)
				{
					SaveMove(UP_MOVE);
				}
				else
				{
					SaveMove(DOWN_MOVE);
				}
			}
			else
			{
				if (deltaX < 0)
				{
					SaveMove(LEFT_MOVE);
				}
				else
				{
					SaveMove(RIGHT_MOVE);
				}
			}
			startPoint = mouseEventPoint;
		}

		/// <summary>Returns delta x.</summary>
		/// <remarks>Returns delta x.</remarks>
		/// <param name="a">First point</param>
		/// <param name="b">Second point</param>
		/// <returns>Delta x</returns>
		private int GetDeltaX(Point a, Point b)
		{
			return b.x - a.x;
		}

		/// <summary>Returns delta y.</summary>
		/// <remarks>Returns delta y.</remarks>
		/// <param name="a">First point</param>
		/// <param name="b">Second point</param>
		/// <returns>Delta y</returns>
		private int GetDeltaY(Point a, Point b)
		{
			return b.y - a.y;
		}

		/// <summary>Adds movement to buffer.</summary>
		/// <remarks>Adds movement to buffer.</remarks>
		/// <param name="move">String representation of recognized movement</param>
		private void SaveMove(string move)
		{
			// should not store two equal moves in succession
			if ((gesture.Length > 0) && (gesture[gesture.Length - 1] == move[0]))
			{
				return;
			}
			gesture.Append(move);
			mouseGestures.FireGestureMovementRecognized(GetGesture());
		}

		/// <summary>Returns current grid size (minimum mouse movement length to be recognized).
		/// 	</summary>
		/// <remarks>Returns current grid size (minimum mouse movement length to be recognized).
		/// 	</remarks>
		/// <returns>Grid size in pixels. Default is 30.</returns>
		internal virtual int GetGridSize()
		{
			return gridSize;
		}

		/// <summary>Sets grid size (minimum mouse movement length to be recognized).</summary>
		/// <remarks>Sets grid size (minimum mouse movement length to be recognized).</remarks>
		/// <param name="gridSize">New grid size in pixels</param>
		internal virtual void SetGridSize(int gridSize)
		{
			this.gridSize = gridSize;
		}

		/// <summary>Returns string representation of mouse gesture.</summary>
		/// <remarks>Returns string representation of mouse gesture.</remarks>
		/// <returns>
		/// String representation of mouse gesture. "L" for left, "R" for right,
		/// "U" for up, "D" for down movements. For example: "ULD".
		/// </returns>
		internal virtual string GetGesture()
		{
			return gesture.ToString();
		}

		/// <summary>Indicates whether any movements were recognized.</summary>
		/// <remarks>Indicates whether any movements were recognized.</remarks>
		/// <returns><code>true</code> if there are recognized movements; <code>false</code> otherwise
		/// 	</returns>
		internal virtual bool IsGestureRecognized()
		{
			return gesture.Length > 0;
		}

		/// <summary>Clears temporary info about previous gesture.</summary>
		/// <remarks>Clears temporary info about previous gesture.</remarks>
		internal virtual void ClearTemporaryInfo()
		{
			startPoint = null;
			gesture.Delete(0, gesture.Length);
		}
	}
}
