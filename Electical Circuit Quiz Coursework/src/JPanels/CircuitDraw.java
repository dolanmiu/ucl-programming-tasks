package JPanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

import Core.Circuit;
import Core.Question;
import ElectricalComponents.CircuitCalc;
import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Module;


/**
 * The Circuit Draw Class.
 * Panel to draw a circuit.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CircuitDraw extends JPanel implements ActionListener {
	
	/** The questions. */
	public ArrayList<Question> questions;
	
	/** The last drawn. */
	public int lastDrawn = 0;
	
	/** The is collided. */
	public boolean isCollided;
	
	/**
	 * Instantiates a new circuit draw.
	 *
	 * @param questions the questions
	 */
	public CircuitDraw(ArrayList<Question> questions) {
		this.questions = questions;
		setLayout(null);
		drawCircuit(0);
		CircuitCalc.setValidCircuit(questions.get(lastDrawn).circuit.getModules());
	}
	
	/**
	 * Update circuit.
	 *
	 * @param question the question
	 */
	public void updateCircuit(int question) {
		removeAll();
		updateUI();
		lastDrawn = question;
		drawCircuit(lastDrawn);
		CircuitCalc.setValidCircuit(questions.get(lastDrawn).circuit.getModules());
	}
	
	/**
	 * Draw circuit.
	 *
	 * @param question the question
	 */
	public void drawCircuit(int question) {
		Timer timer = new Timer(0, this);
		timer.start();
		Question currentQuestion = questions.get(question);
		Circuit circuit = currentQuestion.circuit;
		ArrayList<Module> modules = circuit.getModules();
		for (int i = 0; i < modules.size(); i++) {
			for (int j = 0; j < modules.get(i).getComponentAmmount(); j++) {
				add(modules.get(i).getComponant(j));
				add(modules.get(i));
			}
		}
		for (int i = 0; i < currentQuestion.answers.size(); i++) {
			add(currentQuestion.answers.get(i));
		}
		for (int i = 0; i < currentQuestion.circuit.getWires().size(); i++) {
			add(currentQuestion.circuit.getWires().get(i));
		}
	}
	
	/**
	 * Checks if is drawable.
	 *
	 * @return true, if is drawable
	 */
	public boolean isDrawable() {
		if (lastDrawn+1 < questions.size()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Draw next circuit.
	 */
	public void drawNextCircuit() {
		if (isDrawable()) {
			updateCircuit(lastDrawn+1);
		}
	}
	
	/**
	 * Gets the question number.
	 *
	 * @return the question number
	 */
	public int getQuestionNumber() {
		return lastDrawn;
	}
	
	/**
	 * Gets the current question.
	 *
	 * @return the current question
	 */
	public Question getCurrentQuestion() {
		return questions.get(lastDrawn);
	}
	
	/**
	 * Gets the total resistance.
	 *
	 * @return the total resistance
	 */
	public float getTotalResistance() {
		ArrayList<Module> modules = questions.get(lastDrawn).circuit.getModules();
		float resistance = CircuitCalc.calcResistance(modules);
		return (float) Math.round(resistance * 100) / 100;
	}
	
	/**
	 * Checks if is correct.
	 *
	 * @return true, if is correct
	 */
	public boolean isCorrect() {
		return CircuitCalc.checkValidCircuit(questions.get(lastDrawn).circuit.getModules());
	}
	
	/**
	 * Check collisions.
	 */
	public void checkCollisions() {
		isCollided = false;
		Question question = questions.get(lastDrawn);
		ArrayList<ElectricalComponent> answers = question.getAnswers(); //get array of component answers
		ArrayList<Module> modules = question.circuit.getModules();
		for (int i = 0; i < modules.size(); i++) {
			ArrayList<ElectricalComponent> comps = question.circuit.getModules().get(i).returnComponents(); //Get components of circuit
			for (int j = 0; j < comps.size(); j++) {
				ElectricalComponent collided = comps.get(j).checkAnsCollision(answers); //Check collision
				if (collided != null) {
					comps.get(j).setResistance(collided.getResistance());
					comps.get(j).setVoltage(collided.getVoltage()); //Only for Cells in theory
					comps.get(j).setCurrent(collided.getCurrent()); //Only for Cells in theory
					isCollided = true;
				}
			}
		}
	}
	
	/**
	 * Check wires are OK.
	 */
	public void checkWires() {
		ArrayList<Module> modules = questions.get(lastDrawn).circuit.getModules();
		for (int i = 0; i < modules.size(); i++) {
			modules.get(i).refreshBounds();
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		checkCollisions();
		checkWires();
	}
}
