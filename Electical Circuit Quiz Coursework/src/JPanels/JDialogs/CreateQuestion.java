package JPanels.JDialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Core.Circuit;
import Core.Question;
import ElectricalComponents.ElectricalComponent;
import JPanels.Misc.HintTextArea;


/**
 * The Create Question Dialog Class
 *  A dialog for the creation of a question.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class CreateQuestion extends JDialog {
	
	/** The question type combo box items. */
	private String[] questionTypeComboBoxItems = { "Drag and Drop", "Text", "Gesture"};
	
	/** The difficulty combo box items. */
	private String[] difficultyComboBoxItems = { "Easy", "Medium", "Hard"};
	
	/** The question type. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox questionType = new JComboBox(questionTypeComboBoxItems);
	
	/** The difficulty. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox difficulty = new JComboBox(difficultyComboBoxItems);
	
	/** The number. */
	private HintTextArea number;
	
	/** The created result. */
	Question baby = null;
	/**
	 * Create the panel.
	 */
	public CreateQuestion() {
		setMinimumSize(new Dimension(200,300));
		this.setLocation(400, 100);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().add(questionType);
		questionType.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
		Component rigidArea = Box.createRigidArea(new Dimension(0,30));
		getContentPane().add(rigidArea);
		getContentPane().add(difficulty);
		difficulty.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
		Component rigidArea2 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea2);
		number = new HintTextArea("Question Number...");
		final Border origBorder = number.getBorder();
		number.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				number.setBorder(origBorder);
			}
		});
		getContentPane().add(number);
		number.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));
		Component rigidArea3 = Box.createRigidArea(new Dimension(0,10));
		getContentPane().add(rigidArea3);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JButton btnCreateComponent = new JButton("Create Question");
		panel.add(btnCreateComponent);
		btnCreateComponent.setIcon(new ImageIcon(CreateQuestion.class.getResource("/resources/icons/Very_Basic/checkmark.png")));
		btnCreateComponent.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				if (inputValidation()) {
					Circuit circuit = new Circuit();
					ArrayList<ElectricalComponent> answers = new ArrayList<ElectricalComponent>();
					baby = new Question(getQuestionType(),circuit,getDifficulty(),getNumber(),"",answers);
					setVisible(false);
				}
			}
		});
		setEnabled(true);
		setModal(true);
		setVisible(true);

	}
	
	/**
	 * Gets the question.
	 *
	 * @return the question
	 */
	public Question getQuestion() {
		return baby;
	}
	
	/**
	 * Input validation.
	 *
	 * @return true, if successful
	 */
	private boolean inputValidation() {
		if (!number.getText().equals("")) {
			/*try {
				getNumber();
			} catch(NumberException) {
				
			}*/
			return true;
		}
		number.setBorder(BorderFactory.createLineBorder(new Color(0xCF5151)));
		return false;
	}
	
	/**
	 * Gets the question type.
	 *
	 * @return the question type
	 */
	public String getQuestionType() {
		return questionType.getSelectedItem().toString();
	}
	
	/**
	 * Gets the difficulty.
	 *
	 * @return the difficulty
	 */
	public String getDifficulty() {
		return difficulty.getSelectedItem().toString();
	}
	
	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public int getNumber() {
		return Integer.parseInt(number.getText());
	}
}
