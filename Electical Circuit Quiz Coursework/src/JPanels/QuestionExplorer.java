package JPanels;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import Core.Question;
import Core.Reader;

public class QuestionExplorer extends JPanel {

	/**
	 * Create the panel.
	 */
	public QuestionExplorer() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(null);
		
		populateQuestions();
		
	}
	
	public void populateQuestions() {
		for (int i = 0; i < Reader.questionList.size(); i++) {
			Question q = Reader.questionList.get(i);
			//QuestionDrag qd = new QuestionDrag(q.getType(), q.circuit, q.diff, q.numb,q.getText(), q.getAnswers());
			add(q);
			q.setLocation(100, 200);
		}
	}

}
