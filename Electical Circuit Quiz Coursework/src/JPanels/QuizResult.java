package JPanels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Core.Marker;

/**
 * The Quiz Result Class.
 * Displays the grade/score of the student, and creates a report if need be.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class QuizResult extends JPanel {

	/**
	 * Create the panel.
	 */
	public QuizResult() {
		int score = Marker.getScore();
		char grade = Marker.getGrade();
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));
        Component horizontalGlue = Box.createHorizontalGlue();
        panel_3.add(horizontalGlue);
		JPanel panel = new JPanel();
		panel_3.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EmptyBorder(0, 0, 40, 0));
		panel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		JLabel lblCongrats = new JLabel("Congratulations " + Marker.getName() + "!");
		lblCongrats.setForeground(Color.DARK_GRAY);
		panel_2.add(lblCongrats);
		lblCongrats.setFont(new Font("Dotum", Font.PLAIN, 50));
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new EmptyBorder(0, 0, 5, 0));
		panel_4.add(panel_5);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.X_AXIS));
		
		JLabel lblYouHaveScored = new JLabel("You have scored:");
		lblYouHaveScored.setFont(new Font("Dotum", Font.PLAIN, 21));
		panel_5.add(lblYouHaveScored);
		lblYouHaveScored.setText("You have scored: " + score + " points!");
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new EmptyBorder(0, 0, 5, 0));
		panel_4.add(panel_6);
		panel_6.setLayout(new BoxLayout(panel_6, BoxLayout.X_AXIS));
		
		JLabel lblGrade = new JLabel("Grade:");
		lblGrade.setFont(new Font("Dotum", Font.PLAIN, 32));
		panel_6.add(lblGrade);
		lblGrade.setText("Grade: " + grade);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBorder(new EmptyBorder(0, 0, 5, 0));
		panel_4.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));
		
		JLabel lblMotivationalMessage = new JLabel("Motivational Message");
		lblMotivationalMessage.setFont(new Font("Dotum", Font.PLAIN, 12));
		panel_7.add(lblMotivationalMessage);
		lblMotivationalMessage.setText(Marker.getMessage());
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JButton btnSaveReport = new JButton("Save Report");
		panel_1.add(btnSaveReport);
		Component horizontalGlue2 = Box.createHorizontalGlue();
		panel_3.add(horizontalGlue2);
		btnSaveReport.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				try {
					createReport();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

	}
	
	/**
	 * Creates the report.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void createReport() throws IOException {
		File file = new File("Student Result.txt");
		BufferedWriter output = new BufferedWriter(new FileWriter(file));

		output.write("Student Name: " + Marker.getName());
		output.newLine();
		output.write("Score: " + Marker.getScore());
		output.newLine();
		output.write("Grade: " + Marker.getGrade());
		output.newLine();
		output.newLine();
		output.write("Questions Attempted:");
		output.newLine();
		for (int i = 0; i < Marker.getQuestionStat().size(); i++) {
			output.write("\t"+Marker.getQuestionStat().get(i));
			output.newLine();
		}
		output.close();
		System.out.println("File written");
	}
	
}
