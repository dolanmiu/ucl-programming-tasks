//http://www.javaprogrammingforums.com/java-programming-tutorials/4954-java-tip-aug-4-2010-how-use-file-filters.html
package JPanels.Misc;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * The XML File Filter Class
 *
 * @author DolanMiu
 * @version 1.0
 */
public class XMLFileFilter extends FileFilter {

	/* (non-Javadoc)
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	public String getDescription() {
		return "XML File (.xml)";
	}

	/* (non-Javadoc)
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 */
	public boolean accept(File f) {
		if(f.isDirectory())
		{
			return true;
		}
		return f.getName().endsWith(".xml");
	}
}