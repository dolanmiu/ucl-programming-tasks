package Core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Writter {
	
	public Writter() {
		try {
			writeQuestions();
		} catch(ParserConfigurationException parser) {
			
		} catch(FileNotFoundException file) {
			
		} catch(IOException ioexception) {
			
		}
	}
	
	public void writeQuestions() throws ParserConfigurationException, IOException, FileNotFoundException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document xmlDoc = docBuilder.newDocument();
		
		Element rootElement = xmlDoc.createElement("quiz");
		Element questionElement = xmlDoc.createElement("question");
		questionElement.setAttribute("diff", "easy");
		questionElement.setAttribute("number", "1");
		Element circuit = xmlDoc.createElement("circuit");
		
		Text questionText = xmlDoc.createTextNode("To be or not to be?");
		
		questionElement.appendChild(questionText);
		questionElement.appendChild(circuit);
		rootElement.appendChild(questionElement);
		xmlDoc.appendChild(rootElement);
		
		OutputFormat outFormat = new OutputFormat(xmlDoc);
		outFormat.setIndenting(true);
		
		File xmlFile = new File("electricaloutput.xml");
		FileOutputStream outStream = new FileOutputStream(xmlFile);
		
		XMLSerializer serializer = new XMLSerializer(outStream, outFormat);
		
		serializer.serialize(xmlDoc);
	}

} 
