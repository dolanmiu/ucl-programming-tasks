package ElectricalComponents;

import java.util.ArrayList;


/**
 * The Circuit Calculator Class.
 * This Class does all the resistance, voltage and current calculations of the circuit.
 * 
 * @author DolanMiu
 * @version 1.0
 */
public class CircuitCalc {
	
	/** The correct voltage. */
	private static float correctVoltage;
	
	/** The correct current. */
	private static float correctCurrent;
	
	/**
	 * Calculate resistance.
	 *
	 * @param modules the modules
	 * @return the resistance
	 */
	public static float calcResistance(ArrayList<Module> modules) {                                         
		int ammount = modules.size();										//Calculates the resistance of a set of modules.
		float totalResistance = 0;											//Useful when designing circuits.
		for (int i = 0; i < ammount; i++) {
			totalResistance += calcParallelResistance(modules.get(i));
		}
		return totalResistance;
	}
	
	/**
	 * Sets the valid circuit.
	 *
	 * @param modules modules in the circuit.
	 */
	public static void setValidCircuit(ArrayList<Module> modules) {			//This calculates the correct answer of the circuit. It is ran before the student
		correctVoltage = calcTotalCellVoltage(modules);						//answers a question, so the system will know if the student got the correct answer
		correctCurrent = calcTotalCellCurrent(modules);						//or not.
	}
	
	/**
	 * Check valid circuit.
	 *
	 * @param modules the modules of the new circuit.
	 * @return true, if successful
	 */
	public static boolean checkValidCircuit(ArrayList<Module> modules) {
		float currentVoltage = CircuitCalc.calcTotalCellVoltage(modules);								//This is where it checks with the pupil
		float currentCurrent = CircuitCalc.calcTotalCellCurrent(modules);
		System.out.println("current Voltage" + currentVoltage + " correct Voltage" + correctVoltage);
		System.out.println("current Current" + currentCurrent + " correct Current" + correctCurrent);
		if (currentVoltage == correctVoltage && currentCurrent == correctCurrent) {
			return true;
		}
		return false;
	}
	
	/**
	 * Calculate current.
	 *
	 * @param cell the cell
	 * @return the current
	 */
	public static float calcCurrent(Cell cell) {
		return cell.getCurrent();
	}
	
	/**
	 * Calculate total cell voltage.
	 *
	 * @param modules the modules of circuit
	 * @return the voltage
	 */
	public static float calcTotalCellVoltage(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = getCells(modules);				//Ohms law in action. Used to calculate the voltage of a cell given the total resistance
		float R = calcResistance(modules);										//and current.
		float I = getCellsCurrent(cells);
		float V = I * R;
		return V;
	}
	
	/**
	 * Calculate total cell current.
	 *
	 * @param modules the modules of circuit
	 * @return the current
	 */
	public static float calcTotalCellCurrent(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = getCells(modules);				//Another variation of Ohm's law. This time outputting the current.
		float R = calcResistance(modules);
		float V = getCellsVoltage(cells);
		float I = (V / R);
		return I;
	}
	
	/**
	 * Gets the cells voltage.
	 *
	 * @param cells the cells
	 * @return the cells total voltage
	 */
	public static float getCellsVoltage(ArrayList<ElectricalComponent> cells) {
		float totalVoltage = 0;													//If there are more than 1 cell in the circuit. It finds the total voltage by
		for (int i = 0; i < cells.size(); i++) {								//summing them up.
			totalVoltage += cells.get(i).getVoltage();
		}
		return totalVoltage;
	}
	
	/**
	 * Gets the cells current.
	 *
	 * @param cells the cells
	 * @return the cells total current
	 */
	public static float getCellsCurrent(ArrayList<ElectricalComponent> cells) {
		float totalCurrent = 0;													//Same for the current
		for (int i = 0; i < cells.size(); i++) {
			totalCurrent += cells.get(i).getCurrent();
		}
		return totalCurrent;
	}
	
	/**
	 * Gets the cells.
	 *
	 * @param modules the modules
	 * @return the cells
	 */
	public static ArrayList<ElectricalComponent> getCells(ArrayList<Module> modules) {
		ArrayList<ElectricalComponent> cells = new ArrayList<ElectricalComponent>();			//Returns all the cells in a circuit, used normally incase
		for (int i = 0; i < modules.size(); i++) {												//the circuit has more than one cell.
			Module module = modules.get(i);
			ArrayList<ElectricalComponent> comps = module.returnComponents();
			for (int j = 0; j < comps.size(); j++) {
				ElectricalComponent comp = comps.get(j);
				if (comp.toString().equals("Cell") || comp.hasVoltage()) {
					cells.add(comp);
				}
			}
		}
		return cells;
	}
	
	/**
	 * Calculate parallel resistance.
	 *
	 * @param module the module
	 * @return The Total Resistance
	 */
	public static float calcParallelResistance(Module module) {
		int ammount = module.getComponentAmmount();
		float totalConductance = 0;																//Used in conjunction with the calcResistance() method to calculate
		for (int i = 0; i < ammount; i++) {														//the resistance.
			ElectricalComponent comp = module.getComponant(i);
			totalConductance += 1f/comp.getResistance();
		}
		if (Float.isInfinite(totalConductance)) {
			//System.err.println("Conductance is infinate");
		}
		float totalResistance = 1f/totalConductance;
		return totalResistance;
	}
}
