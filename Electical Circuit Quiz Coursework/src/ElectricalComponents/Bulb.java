package ElectricalComponents;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.border.LineBorder;

/**
 * The Bulb Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Bulb extends ElectricalComponent {
	
	/** The img. */
	private Image img;
	
	/** The border colour. */
	public Color borderColour;
	
	/**
	 * Instantiates a new bulb.
	 *
	 * @param resistance the resistance
	 * @param x the x
	 * @param y the y
	 */
	public Bulb(float resistance, int x, int y) {
		setBorder(new LineBorder(Color.YELLOW, 3));
		this.resistance = resistance;
		this.x = x;
		this.y = y;
		this.setLocation(x, y);
		img = Toolkit.getDefaultToolkit().createImage("src/resources/bulb.png");		//Each electrical component will have its own respective picture.
		
		paintResistance();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
         g.drawImage(img, 0, 0, this);
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return "Bulb";
	}
}
