package ElectricalComponents;
//http://stackoverflow.com/questions/874360/swing-creating-a-draggable-component
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.border.LineBorder;

/**
 * The Draggable Component Class.
 * Every Electrical component which is on stage should be draggable.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class DraggableComponent extends JComponent {
	
	/** The screen x. */
	private volatile int screenX = 0;
	
	/** The screen y. */
	private volatile int screenY = 0;
	
	/** The x coordinate. */
	private volatile int myX = 0;
	
	/** The y coordinate. */
	private volatile int myY = 0;
	
	/**
	 * Instantiates a new draggable component.
	 */
	public DraggableComponent() {
		setBorder(new LineBorder(Color.GRAY, 3));
		setBackground(Color.WHITE);
		setBounds(0, 0, 100, 100);
		setOpaque(false);

		addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) { }
			@Override
			public void mousePressed(MouseEvent e) {
				screenX = e.getXOnScreen();
				screenY = e.getYOnScreen();
				myX = getX();
				myY = getY();
			}
			@Override
			public void mouseReleased(MouseEvent e) { }
			@Override
			public void mouseEntered(MouseEvent e) { }
			@Override
			public void mouseExited(MouseEvent e) { }
		});
		
		addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int deltaX = e.getXOnScreen() - screenX;
				int deltaY = e.getYOnScreen() - screenY;
				setLocation(myX + deltaX, myY + deltaY);
			}
			@Override
			public void mouseMoved(MouseEvent e) { }
		});
	}
}
