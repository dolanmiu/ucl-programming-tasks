package ElectricalComponents;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

/**
 * The Cell Class.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Cell extends ElectricalComponent {
	
	/** The img. */
	private Image img;
	
	/**
	 * Instantiates a new cell.
	 *
	 * @param voltage the voltage
	 * @param current the current
	 * @param x the x
	 * @param y the y
	 */
	public Cell(float voltage, float current, int x, int y) {
		this.voltage = voltage;
		this.current = current;
		this.x = x;
		this.y = y;
		this.setLocation(x, y);
		setBorder(new LineBorder(Color.GREEN, 3));
		img = Toolkit.getDefaultToolkit().createImage("src/resources/cell.png");		//Each electrical component will have its own respective picture.
		paintVoltage();
		paintCurrent();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
         g.drawImage(img, 0, 0, this);
	}
	
	/**
	 * Paint voltage label.
	 */
	private void paintVoltage() {
		final JTextArea voltageText = new JTextArea(voltage + "");			//The Cell will have a voltage and a current accociated with it
		voltageText.addKeyListener(new KeyAdapter() {						//adding these will display it in the questions, and will also allow the user to edit
			@Override														//the values for question answering purposes
			public void keyReleased(KeyEvent arg0) {
				voltage = Float.parseFloat(voltageText.getText());
			}
		});
		
		voltageText.setBackground(Color.WHITE);
		voltageText.setOpaque(true);
		add(voltageText);
		voltageText.setSize(voltageText.getPreferredSize());
		voltageText.setLocation(getWidth()-voltageText.getWidth()-12,65);
		
		JLabel voltageUnit = new JLabel("V");
		voltageUnit.setBackground(Color.WHITE);
		voltageUnit.setOpaque(true);
		add(voltageUnit);
		voltageUnit.setSize(voltageUnit.getPreferredSize());
		voltageUnit.setLocation(getWidth()-voltageText.getWidth(),65);
	}
	
	/**
	 * Paint current label.
	 */
	private void paintCurrent() {
		final JTextArea currentText = new JTextArea(current + "");
		currentText.addMouseListener(new MouseAdapter() {
			@Override													//same for current
			public void mouseExited(MouseEvent arg0) {
				current = Float.parseFloat(currentText.getText());
			}
		});
		currentText.setBackground(Color.WHITE);
		currentText.setOpaque(true);
		add(currentText);
		currentText.setSize(currentText.getPreferredSize());
		currentText.setLocation(getWidth()-currentText.getWidth()-12,80);
		
		JLabel currentUnit = new JLabel("A");
		currentUnit.setBackground(Color.WHITE);
		currentUnit.setOpaque(true);
		add(currentUnit);
		currentUnit.setSize(currentUnit.getPreferredSize());
		currentUnit.setLocation(getWidth()-currentUnit.getWidth(),80);
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#getVoltage()
	 */
	public float getVoltage() {
		return voltage;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#setVoltage(float)
	 */
	public void setVoltage(float voltage) {
		this.voltage = voltage;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#getCurrent()
	 */
	public float getCurrent() {
		return current;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#setCurrent(float)
	 */
	public void setCurrent(float current) {
		this.current = current;
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return "Cell";
	}
}
