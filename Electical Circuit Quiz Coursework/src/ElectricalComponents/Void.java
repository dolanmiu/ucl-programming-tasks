package ElectricalComponents;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.border.LineBorder;

/**
 * The Void Class.
 * Is an empty component to be filled with a component during the answering process of the quiz.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Void extends ElectricalComponent {
	
	/** The image. */
	private Image img;
	
	/**
	 * Instantiates a new void.
	 *
	 * @param resistance the resistance
	 * @param voltage the voltage
	 * @param current the current
	 * @param x the x
	 * @param y the y
	 * @param textAnswer the text answer
	 */
	public Void(float resistance, float voltage, float current, int x, int y, String textAnswer) {
		this.resistance = resistance;
		this.voltage = voltage;
		this.current = current;
		setBorder(new LineBorder(Color.BLACK, 3));
		this.x = x;
		this.y = y;
		this.setLocation(x, y);
		this.textAnswer = textAnswer;
		//img = Toolkit.getDefaultToolkit().createImage("src/resources/bulb.jpg");
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
         g.drawImage(img, 0, 0, this);
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#checkAnsCollision(java.util.ArrayList)
	 */
	public ElectricalComponent checkAnsCollision(ArrayList<ElectricalComponent> answers) {
		for (int i = 0; i < answers.size(); i++) {
			Rectangle answerBounds = answers.get(i).getBounds();				//Checks for collisions with other components. Used for answering questions.
			if (answerBounds.intersects(getBounds())) {							//If so, then it changes the border colour of itself for an indication to
				setBorderActive();												//the user.
				return answers.get(i);
			} else {
				setBorderDeactive();
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#hasVoltage()
	 */
	public boolean hasVoltage() {
		if (this.voltage > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#getVoltage()
	 */
	public float getVoltage() {
		if (this.voltage > 0) {
			return voltage;
		} else {
			return 0;
		}
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#setVoltage(float)
	 */
	public void setVoltage(float voltage) {
		this.voltage = voltage;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#setCurrent(float)
	 */
	public void setCurrent(float current) {
		this.current = current;
	}
	
	/* (non-Javadoc)
	 * @see ElectricalComponents.ElectricalComponent#getCurrent()
	 */
	public float getCurrent() {
		if (this.current > 0) {
			return current;
		} else {
			return 0;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return "Void";
	}
}
