package ElectricalComponents;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JComponent;

/**
 * The Module Class.
 * Every Electrical component is housed in a module.
 * Sometimes more than one component is in one for parallel circuits! 
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Module extends JComponent {
	
	/** The components. */
	private ArrayList<ElectricalComponent> components = new ArrayList<ElectricalComponent>();
	
	/** The module number. */
	private int moduleNumber;
	
	/** The reversed. */
	public boolean reversed = true;
	
	/** The voltage. */
	public float voltage = 0;
	
	/**
	 * Instantiates a new module.
	 *
	 * @param moduleNumber the module number
	 */
	public Module(int moduleNumber) {
		setBounds(0, 0, 1000, 1000); //the size of the module
		this.moduleNumber = moduleNumber;
	}
	
	/**
	 * Removes the component.
	 *
	 * @param i componentID;
	 */
	public void removeComponent(int i) {
		components.remove(i);
	}
	
	/**
	 * Sets the component voltage.
	 */
	public void setComponentVoltage() {
		for (int i = 0; i < components.size(); i++) {
			components.get(i).setVoltsAcross(voltage);
		}
	}
	
	/**
	 * Gets the voltage.
	 *
	 * @return the voltage
	 */
	public float getVoltage() {
		return voltage;
	}
	
	/**
	 * Sets the volts across.
	 *
	 * @param voltage the new volts across
	 */
	public void setVoltsAcross(float voltage) {
		this.voltage = voltage;
		for (int i = 0; i < components.size(); i++) {
			components.get(i).setVoltsAcross(voltage);
		}
	}
	
	/**
	 * Adds the component.
	 *
	 * @param comp the component
	 */
	public void addComponant(ElectricalComponent comp) {
		components.add(comp);
		refreshBounds();
	}
	
	/**
	 * Checks if is reversed.
	 *
	 * @return true, if is reversed
	 */
	public boolean isReversed() {
		return reversed;
	}
	
	/**
	 * Sets the reversed.
	 *
	 * @param bool the new reversed
	 */
	public void setReversed(boolean bool) {
		if (bool == true) {
			reversed = true;
		} else {
			reversed = false;
		}
	}
	
	/**
	 * Refresh bounds.
	 */
	public void refreshBounds() {
		int highestX = 0, highestY = 0;
		int lowestX = Integer.MAX_VALUE, lowestY = Integer.MAX_VALUE;
		int width = 0, height = 0;
		for (int i = 0; i < components.size(); i++) {					//This is used for setting the wires for the parallel circuit.
			if (lowestX > components.get(i).getX()) {					//Remember: modules are parallel sections of the series circuit.
				lowestX = components.get(i).getX();
			}
			if (lowestY > components.get(i).getY()) {
				lowestY = components.get(i).getY();
			}
			if (highestY < components.get(i).getY()) {
				highestY = components.get(i).getY();
			}
			if (highestX < components.get(i).getX()) {
				highestX = components.get(i).getX();
			}
			width = highestX - lowestX;
			height = highestY - lowestY;
			
		}
		setBounds(lowestX-30, lowestY-30, width+160, height+160);
		repaint();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle bound = getBounds();											//This is where we create the wires for the parallel part of the circuit!
		g.drawLine(0, 80, 0, bound.height-80);									//Various draw commands, and centring creates a very robust method to accommodate
		g.drawLine(bound.width-1, 80, bound.width-1, bound.height-80);			//as many parallel components as there need be!
		for (int i = 0; i < components.size(); i++) {
			int xcoord = components.get(i).getX() - this.getX();
			int ycoord = components.get(i).getY() - this.getY() + 50; //shift it to centre
			g.drawLine(xcoord, ycoord, 0, ycoord);
			g.drawLine(xcoord+100, ycoord, getWidth() ,ycoord);
		}
	}
	
	/**
	 * Prints the components.
	 */
	public void printComponents() {
		System.out.println("Size of componants: " + components.size());
		for (int i = 0; i < components.size(); i++) {
			System.out.println(components.get(i).toString());
		}
	}
	
	/**
	 * Gets the component.
	 *
	 * @param i component ID
	 * @return the component
	 */
	public ElectricalComponent getComponant(int i) {
		return components.get(i);
	}
	
	/**
	 * Gets the component amount.
	 *
	 * @return the component amount
	 */
	public int getComponentAmmount() {
		return components.size();
	}
	
	/**
	 * Return components.
	 *
	 * @return the array list
	 */
	public ArrayList<ElectricalComponent> returnComponents() {
		return components;
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	public String toString() {
		return ("Module " + moduleNumber);
	}
}
