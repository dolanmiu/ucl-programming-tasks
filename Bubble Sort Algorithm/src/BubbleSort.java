import java.util.Scanner;
import java.util.Arrays;


public class BubbleSort {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a list of numbers to sort seperated by a space");
		String[] input = scanner.nextLine().split("\\s+");
		int[] numbers = new int[input.length];
		for (int i = 0; i < input.length; i++) {
			numbers[i] = Integer.parseInt(input[i]);
		}
		System.out.println(Arrays.toString(input));
		System.out.println(Arrays.toString(bubbleSort(numbers)));
	}
	
	private static int[] bubbleSort(int[] numbers) {
		for (int j = 0; j < numbers.length; j++) {
			for (int i = 0; i < numbers.length -1; i++) {
				if(numbers[i] > numbers[i+1]) {
					int temp = numbers[i];
					numbers[i] = numbers[i+1];
					numbers[i+1] = temp;
				}
			}
		}
		return numbers;
	}
}
