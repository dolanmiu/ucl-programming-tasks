using Core;
using JPanels.Misc;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace JPanels
{
	/// <summary>
	/// The Find Panel Class
	/// Allows for the search of questions.
	/// </summary>
	/// <remarks>
	/// The Find Panel Class
	/// Allows for the search of questions. Used in Circuit Maker Class.
	/// </remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	[System.Serializable]
	public class FindPanel : JPanel
	{
		/// <summary>The questions.</summary>
		/// <remarks>The questions.</remarks>
		private AList<Question> questions = Reader.questionList;

		/// <summary>The text field.</summary>
		/// <remarks>The text field.</remarks>
		private HintTextArea textField;

		/// <summary>The question list model.</summary>
		/// <remarks>The question list model.</remarks>
		private DefaultListModel<Question> questionInfo = new DefaultListModel<Question>(
			);

		/// <summary>The question list.</summary>
		/// <remarks>The question list.</remarks>
		private JList<Question> questionList;

		/// <summary>The selected.</summary>
		/// <remarks>The selected.</remarks>
		private Question selected;

		/// <summary>Create the panel.</summary>
		/// <remarks>Create the panel.</remarks>
		public FindPanel()
		{
			questionList = new JList<Question>(questionInfo);
			SetLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			JPanel panel = new JPanel();
			panel.SetMaximumSize(new Dimension(int.MaxValue, 50));
			Add(panel);
			textField = new HintTextArea("Search...");
			textField.AddKeyListener(new _KeyAdapter_53(this));
			panel.Add(textField);
			textField.SetColumns(10);
			JPanel panel_1 = new JPanel();
			Add(panel_1);
			panel_1.SetLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
			JScrollPane scrollPane = new JScrollPane(questionList);
			questionList.SetVisibleRowCount(12);
			panel_1.Add(scrollPane);
		}

		private sealed class _KeyAdapter_53 : KeyAdapter
		{
			public _KeyAdapter_53(FindPanel _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public override void KeyReleased(KeyEvent arg0)
			{
				this._enclosing.Search(this._enclosing.textField.GetText());
			}

			private readonly FindPanel _enclosing;
		}

		/// <summary>Gets the question list.</summary>
		/// <remarks>Gets the question list.</remarks>
		/// <returns>the question list</returns>
		public virtual JList<Question> GetQuestionList()
		{
			return questionList;
		}

		/// <summary>Gets the selected.</summary>
		/// <remarks>Gets the selected.</remarks>
		/// <returns>the selected</returns>
		public virtual Question GetSelected()
		{
			return selected;
		}

		/// <summary>Search.</summary>
		/// <remarks>Search.</remarks>
		/// <param name="rawQuery">the raw search query</param>
		public virtual void Search(string rawQuery)
		{
			questionInfo.Clear();
			string query = rawQuery.ToLower();
			//String[] multiQ = query.split("\\s+");
			for (int i = 0; i < questions.Count; i++)
			{
				//if (multiQuery(multiQ, questions.get(i))) {
				string text = questions[i].GetText().ToLower();
				string diff = questions[i].diff.ToLower();
				if (text.Contains(query) || diff.Contains(query))
				{
					questionInfo.AddElement(questions[i]);
				}
			}
			if (query.IsEmpty())
			{
				questionInfo.Clear();
			}
		}

		/// <summary>Multi-query.</summary>
		/// <remarks>Multi-query.</remarks>
		/// <param name="multiQ">the multi-query</param>
		/// <param name="question">the question</param>
		/// <returns>true, if successful</returns>
		public virtual bool MultiQuery(string[] multiQ, Question question)
		{
			for (int i = 0; i < multiQ.Length; i++)
			{
				string diff = question.diff;
				string text = question.GetText();
				if (!text.Contains(multiQ[i]) || !diff.Contains(multiQ[i]))
				{
					return false;
				}
			}
			return true;
		}
	}
}
