using System;
using Core;
using ElectricalComponents;
using Javax.Xml.Parsers;
using Org.W3c.Dom;
using Sharpen;

namespace Core
{
	/// <summary>The Reader Class.</summary>
	/// <remarks>The Reader Class. It reads in the XML and converts it into objects.</remarks>
	/// <author>DolanMiu</author>
	/// <version>1.0</version>
	public class Reader
	{
		/// <summary>The file.</summary>
		/// <remarks>The file.</remarks>
		private static FilePath fFile;

		/// <summary>The question list.</summary>
		/// <remarks>The question list.</remarks>
		public static AList<Question> questionList = new AList<Question>();

		//My very own elegant hierarchical XML parser
		/// <summary>Scans the file.</summary>
		/// <remarks>Scans the file.</remarks>
		/// <param name="file">the file</param>
		/// <exception cref="System.IO.FileNotFoundException">the file not found exception</exception>
		public static void ScanFile(FilePath file)
		{
			fFile = file;
			try
			{
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.NewInstance();
				DocumentBuilder dBuilder = dbFactory.NewDocumentBuilder();
				Document doc = dBuilder.Parse(fFile);
				doc.GetDocumentElement().Normalize();
				NodeList questions = doc.GetElementsByTagName("question");
				HandleQuestions(questions);
			}
			catch (Exception e)
			{
				Sharpen.Runtime.PrintStackTrace(e);
			}
		}

		/// <summary>Scans the file.</summary>
		/// <remarks>Scans the file.</remarks>
		/// <param name="filename">the file name</param>
		/// <exception cref="System.IO.FileNotFoundException">the file not found exception</exception>
		public void ScanFile(string filename)
		{
			fFile = new FilePath(filename);
			try
			{
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.NewInstance();
				DocumentBuilder dBuilder = dbFactory.NewDocumentBuilder();
				Document doc = dBuilder.Parse(fFile);
				doc.GetDocumentElement().Normalize();
				NodeList questions = doc.GetElementsByTagName("question");
				HandleQuestions(questions);
			}
			catch (Exception e)
			{
				Sharpen.Runtime.PrintStackTrace(e);
			}
		}

		/// <summary>Handle questions.</summary>
		/// <remarks>Handle questions.</remarks>
		/// <param name="questions">the questions</param>
		private static void HandleQuestions(NodeList questions)
		{
			System.Console.Out.WriteLine("Ammount of questions: " + questions.GetLength());
			for (int i = 0; i < questions.GetLength(); i++)
			{
				Element element = (Element)questions.Item(i);
				//one question
				NodeList circuitNodes = element.GetElementsByTagName("circuit");
				NodeList answerNodes = element.GetElementsByTagName("answers");
				Node answers = answerNodes.Item(0);
				Element circNode = (Element)circuitNodes.Item(0);
				NodeList modules = circNode.GetElementsByTagName("module");
				string diff = element.GetAttribute("difficulty");
				int numb = System.Convert.ToInt32(element.GetAttribute("number"));
				string text = GetTagValue("text", element);
				string type = element.GetAttribute("type");
				Circuit circuit = new Circuit();
				AList<ElectricalComponent> answerList = new AList<ElectricalComponent>();
				answerList = GetAnswers(answers);
				Question question = new Question(type, circuit, diff, numb, text, answerList);
				questionList.AddItem(question);
				HandleModules(circuit, modules);
				circuit.SetModuleVoltages();
				question.SortOutText();
			}
		}

		/// <summary>Creates the wires.</summary>
		/// <remarks>Creates the wires.</remarks>
		/// <param name="circuit">the circuit</param>
		public static void CreateWires(Circuit circuit)
		{
			for (int i = 0; i < circuit.GetModules().Count; i++)
			{
				//for each module
				if (i != circuit.modules.Count - 1)
				{
					Wire wire = new Wire(circuit.modules[i], circuit.modules[i + 1]);
					circuit.AddWires(wire);
				}
				else
				{
					Wire wire = new Wire(circuit.modules[i], circuit.modules[0]);
					circuit.AddWires(wire);
				}
			}
		}

		//.printConnection();
		/// <summary>Handle modules.</summary>
		/// <remarks>Handle modules.</remarks>
		/// <param name="circuit">the circuit</param>
		/// <param name="moduleNodes">the module nodes</param>
		private static void HandleModules(Circuit circuit, NodeList moduleNodes)
		{
			for (int i = 0; i < moduleNodes.GetLength(); i++)
			{
				Node moduleNode = moduleNodes.Item(i);
				System.Console.Out.WriteLine("\tModule: " + i);
				Module module = new Module(i);
				circuit.AddModule(module);
				HandleComponents(module, moduleNode, i);
			}
			CreateWires(circuit);
		}

		/// <summary>Handle components.</summary>
		/// <remarks>Handle components.</remarks>
		/// <param name="module">the module</param>
		/// <param name="moduleNode">the module node</param>
		/// <param name="moduleNumber">the module number</param>
		private static void HandleComponents(Module module, Node moduleNode, int moduleNumber
			)
		{
			NodeList components = moduleNode.GetChildNodes();
			for (int i = 0; i < components.GetLength(); i++)
			{
				Node componant = components.Item(i);
				if (componant.GetNodeType() == Node.ELEMENT_NODE)
				{
					Element eElement = (Element)componant;
					string name = GetTagValue("name", eElement);
					float resistance = float.ParseFloat((GetTagValue("resistance", eElement)));
					float voltage = float.ParseFloat((GetTagValue("voltage", eElement)));
					float current = float.ParseFloat((GetTagValue("current", eElement)));
					int x = System.Convert.ToInt32((GetTagValue("x", eElement)));
					int y = System.Convert.ToInt32((GetTagValue("y", eElement)));
					ElectricalComponent comp = InitComponent.Create(name, resistance, voltage, current
						, x, y, string.Empty);
					module.AddComponant(comp);
				}
			}
		}

		/// <summary>Gets the tag value.</summary>
		/// <remarks>Gets the tag value.</remarks>
		/// <param name="sTag">the s tag</param>
		/// <param name="eElement">the e element</param>
		/// <returns>the tag value</returns>
		private static string GetTagValue(string sTag, Element eElement)
		{
			try
			{
				NodeList nlList = eElement.GetElementsByTagName(sTag).Item(0).GetChildNodes();
				Node nValue = (Node)nlList.Item(0);
				return nValue.GetNodeValue();
			}
			catch (ArgumentNullException)
			{
				return "0";
			}
		}

		//This is normally forgotten
		/// <summary>Gets the questions.</summary>
		/// <remarks>Gets the questions.</remarks>
		/// <param name="difficulty">the difficulty</param>
		/// <returns>the questions</returns>
		public static AList<Question> GetQuestions(string difficulty)
		{
			AList<Question> selectedQuestions = new AList<Question>();
			for (int i = 0; i < questionList.Count; i++)
			{
				Question question = questionList[i];
				string diff = question.diff;
				if (diff.Equals(difficulty))
				{
					selectedQuestions.AddItem(question);
				}
			}
			return selectedQuestions;
		}

		/// <summary>Gets the answers.</summary>
		/// <remarks>Gets the answers.</remarks>
		/// <param name="answers">the answers</param>
		/// <returns>the answers</returns>
		private static AList<ElectricalComponent> GetAnswers(Node answers)
		{
			AList<ElectricalComponent> answerList = new AList<ElectricalComponent>();
			NodeList components = answers.GetChildNodes();
			for (int i = 0; i < components.GetLength(); i++)
			{
				Node componant = components.Item(i);
				if (componant.GetNodeType() == Node.ELEMENT_NODE)
				{
					Element eElement = (Element)componant;
					string name = GetTagValue("name", eElement);
					float resistance = float.ParseFloat((GetTagValue("resistance", eElement)));
					float voltage = float.ParseFloat((GetTagValue("voltage", eElement)));
					float current = float.ParseFloat((GetTagValue("current", eElement)));
					int x = System.Convert.ToInt32((GetTagValue("x", eElement)));
					int y = System.Convert.ToInt32((GetTagValue("y", eElement)));
					string textAnswer = GetTagValue("answer", eElement);
					ElectricalComponent comp = InitComponent.Create(name, resistance, voltage, current
						, x, y, textAnswer);
					answerList.AddItem(comp);
				}
			}
			return answerList;
		}
	}
}
