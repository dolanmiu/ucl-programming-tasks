package JPanels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import Core.Marker;
import JPanels.Misc.CardPanel;
import JPanels.Misc.HintTextArea;

/**
 * The Difficulty Selection Class
 * Allows the student to select the difficulty of the questions.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class DifficultySelect extends JPanel {
	
	/** The name field. */
	private HintTextArea nameField;
	
	/** The welcome label. */
	private JLabel lblWelcome;
	
	/** The shuffle toggle button. */
	JToggleButton tglbtnShuffle = new JToggleButton("Shuffle");
	
	/** The timed toggle button. */
	JToggleButton tglbtnTimed = new JToggleButton("Timed?");

	/**
	 * Create the panel.
	 *
	 * @param cardPanel the card panel
	 */
	public DifficultySelect(final CardPanel cardPanel) {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel containerPanel = new JPanel();
		add(containerPanel);
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.Y_AXIS));
		
		lblWelcome = new JLabel("Welcome!");
		lblWelcome.setForeground(Color.DARK_GRAY);
		lblWelcome.setAlignmentX(Component.CENTER_ALIGNMENT);
		containerPanel.add(lblWelcome);
		lblWelcome.setFont(new Font("Dotum", Font.PLAIN, 50));
		
		nameField = new HintTextArea("What is your name?");
		nameField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				lblWelcome.setText("Welcome " + nameField.getText() + "!");
				Marker.setName(nameField.getText());
			}
		});
		Component rigidArea_1 = Box.createRigidArea(new Dimension(60,30));
		containerPanel.add(rigidArea_1);
		containerPanel.add(nameField);
		nameField.setMaximumSize(new Dimension(200,20));
		nameField.setColumns(10);
		
		JPanel buttonPanel = new JPanel();
		containerPanel.add(buttonPanel);
		//buttonPanel.setMaximumSize(new Dimension(800,100));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		Component horizontalGlue = Box.createHorizontalGlue();
		buttonPanel.add(horizontalGlue);
		
		JPanel diffPanel = new JPanel();
		buttonPanel.add(diffPanel);
		diffPanel.setBorder(new EmptyBorder(0, 70, 0, 50));
		diffPanel.setMaximumSize(new Dimension(0,100));
		diffPanel.setPreferredSize(new Dimension(500,100));
		diffPanel.setLayout(new BoxLayout(diffPanel, BoxLayout.X_AXIS));
		
		JButton btnEasy = new JButton("Easy");
		btnEasy.setIcon(new ImageIcon(DifficultySelect.class.getResource("/resources/icons/Emoticons/lol.png")));
		btnEasy.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnEasy.setAlignmentY(Component.CENTER_ALIGNMENT);
		diffPanel.add(btnEasy);
		
		diffPanel.add(Box.createRigidArea(new Dimension(60,0)));
		
		JButton btnMedium = new JButton("Medium");
		btnMedium.setIcon(new ImageIcon(DifficultySelect.class.getResource("/resources/icons/Emoticons/question.png")));
		diffPanel.add(btnMedium);
		
		diffPanel.add(Box.createRigidArea(new Dimension(60,0)));
		
		JButton btnHard = new JButton("Hard");
		btnHard.setIcon(new ImageIcon(DifficultySelect.class.getResource("/resources/icons/Emoticons/crying.png")));
		btnHard.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				createQuizPanel("hard", cardPanel);
			}
		});
		diffPanel.add(btnHard);
		Component horizontalGlue2 = Box.createHorizontalGlue();
		horizontalGlue2.setMaximumSize(new Dimension(400,10));
		buttonPanel.add(horizontalGlue2);
		JPanel toggleButtonPanel = new JPanel();
		buttonPanel.add(toggleButtonPanel);
		toggleButtonPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		toggleButtonPanel.setLayout(new BoxLayout(toggleButtonPanel, BoxLayout.Y_AXIS));
		toggleButtonPanel.setPreferredSize(new Dimension(200,50));
		tglbtnTimed.setIcon(new ImageIcon(DifficultySelect.class.getResource("/resources/icons/System/dashboard.png")));
		
		tglbtnTimed.setAlignmentX(Component.CENTER_ALIGNMENT);
		toggleButtonPanel.add(tglbtnTimed);
		Component rigidArea = Box.createRigidArea(new Dimension(60,10));
		toggleButtonPanel.add(rigidArea);
		tglbtnShuffle.setIcon(new ImageIcon(DifficultySelect.class.getResource("/resources/icons/Very_Basic/sinchronize.png")));
		
		tglbtnShuffle.setAlignmentX(Component.CENTER_ALIGNMENT);
		toggleButtonPanel.add(tglbtnShuffle);
		Component horizontalGlue3 = Box.createHorizontalGlue();
		buttonPanel.add(horizontalGlue3);
		btnMedium.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				createQuizPanel("medium", cardPanel);
			}
		});
		btnEasy.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				createQuizPanel("easy", cardPanel);
			}
		});

	}
	
	/**
	 * Creates the quiz panel.
	 *
	 * @param difficulty the difficulty
	 * @param cardPanel the card panel
	 */
	private void createQuizPanel(String difficulty, CardPanel cardPanel) {
		StudentQuiz quiz = null;
		if (difficulty.equals("easy")) {
			if (tglbtnTimed.isSelected()) {
				quiz = new StudentQuizTime(cardPanel, "easy", tglbtnShuffle.isSelected());
			} else {
				quiz = new StudentQuiz(cardPanel, "easy", tglbtnShuffle.isSelected());
			}
		}
		if (difficulty.equals("medium")) {
			if (tglbtnTimed.isSelected()) {
				quiz = new StudentQuizTime(cardPanel, "medium", tglbtnShuffle.isSelected());
			} else {
				quiz = new StudentQuiz(cardPanel, "medium", tglbtnShuffle.isSelected());
			}
		}
		if (difficulty.equals("hard")) {
			if (tglbtnTimed.isSelected()) {
				quiz = new StudentQuizTime(cardPanel, "hard", tglbtnShuffle.isSelected());
			} else {
				quiz = new StudentQuiz(cardPanel, "hard", tglbtnShuffle.isSelected());
			}
		}
		cardPanel.add(quiz, "studentQuiz");
		cardPanel.c1.show(cardPanel, "studentQuiz");
	}

}
