using System;
using System.Text;
using Com.Smardec.Mousegestures;
using Java.Awt;
using Java.Awt.Event;
using Javax.Swing;
using Sharpen;

namespace Com.Smardec.Mousegestures.Test
{
	/// <summary>Simple test frame.</summary>
	/// <remarks>Simple test frame.</remarks>
	[System.Serializable]
	public class TestFrame : JFrame
	{
		private MouseGestures mouseGestures = new MouseGestures();

		private JLabel statusLabel = new JLabel("Mouse gesture: ");

		public static void Main(string[] args)
		{
			Com.Smardec.Mousegestures.Test.TestFrame frame = new Com.Smardec.Mousegestures.Test.TestFrame
				();
			frame.Show();
		}

		public TestFrame()
		{
			SetDefaultCloseOperation(EXIT_ON_CLOSE);
			SetTitle("Mouse Gestures Test Frame");
			SetIconImage(new ImageIcon(GetType().GetClassLoader().GetResource("com/smardec/mousegestures/test/img/logo.gif"
				)).GetImage());
			InitSize();
			GetContentPane().SetLayout(new BorderLayout());
			InitControls();
			InitStatusBar();
			InitMouseGestures();
		}

		private void InitSize()
		{
			Dimension screenSize = Toolkit.GetDefaultToolkit().GetScreenSize();
			Dimension size = new Dimension(640, 480);
			if (size.height > screenSize.height)
			{
				size.height = screenSize.height;
			}
			if (size.width > screenSize.width)
			{
				size.width = screenSize.width;
			}
			SetSize(size);
			SetLocation((screenSize.width - size.width) / 2, (screenSize.height - size.height
				) / 2);
		}

		private void InitControls()
		{
			JCheckBox jCheckBoxButton1 = new JCheckBox("Right button");
			jCheckBoxButton1.AddActionListener(new _ActionListener_77(this));
			JCheckBox jCheckBoxButton2 = new JCheckBox("Middle button");
			jCheckBoxButton2.AddActionListener(new _ActionListener_83(this));
			JCheckBox jCheckBoxButton3 = new JCheckBox("Left button");
			jCheckBoxButton3.AddActionListener(new _ActionListener_89(this));
			ButtonGroup buttonGroup = new ButtonGroup();
			buttonGroup.Add(jCheckBoxButton1);
			buttonGroup.Add(jCheckBoxButton2);
			buttonGroup.Add(jCheckBoxButton3);
			jCheckBoxButton1.SetSelected(true);
			JPanel jPanel = new JPanel(new GridLayout(4, 1));
			jPanel.SetBorder(BorderFactory.CreateEmptyBorder(5, 10, 5, 10));
			jPanel.Add(new JLabel("Select mouse button used for gestures handling."));
			jPanel.Add(jCheckBoxButton1);
			jPanel.Add(jCheckBoxButton2);
			jPanel.Add(jCheckBoxButton3);
			GetContentPane().Add(jPanel, BorderLayout.NORTH);
		}

		private sealed class _ActionListener_77 : ActionListener
		{
			public _ActionListener_77(TestFrame _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void ActionPerformed(ActionEvent e)
			{
				this._enclosing.mouseGestures.SetMouseButton(MouseEvent.BUTTON3_MASK);
			}

			private readonly TestFrame _enclosing;
		}

		private sealed class _ActionListener_83 : ActionListener
		{
			public _ActionListener_83(TestFrame _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void ActionPerformed(ActionEvent e)
			{
				this._enclosing.mouseGestures.SetMouseButton(MouseEvent.BUTTON2_MASK);
			}

			private readonly TestFrame _enclosing;
		}

		private sealed class _ActionListener_89 : ActionListener
		{
			public _ActionListener_89(TestFrame _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void ActionPerformed(ActionEvent e)
			{
				this._enclosing.mouseGestures.SetMouseButton(MouseEvent.BUTTON1_MASK);
			}

			private readonly TestFrame _enclosing;
		}

		private void InitStatusBar()
		{
			JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			jPanel.SetBorder(BorderFactory.CreateLoweredBevelBorder());
			jPanel.Add(statusLabel);
			GetContentPane().Add(jPanel, BorderLayout.SOUTH);
		}

		private void InitMouseGestures()
		{
			mouseGestures = new MouseGestures();
			mouseGestures.AddMouseGesturesListener(new _MouseGesturesListener_119(this));
			mouseGestures.Start();
		}

		private sealed class _MouseGesturesListener_119 : MouseGesturesListener
		{
			public _MouseGesturesListener_119(TestFrame _enclosing)
			{
				this._enclosing = _enclosing;
			}

			public void GestureMovementRecognized(string currentGesture)
			{
				this._enclosing.SetGestureString(this.AddCommas(currentGesture));
			}

			public void ProcessGesture(string gesture)
			{
				try
				{
					Sharpen.Thread.Sleep(200);
				}
				catch (Exception)
				{
				}
				this._enclosing.SetGestureString(string.Empty);
			}

			private string AddCommas(string gesture)
			{
				StringBuilder stringBuffer = new StringBuilder();
				for (int i = 0; i < gesture.Length; i++)
				{
					stringBuffer.Append(gesture[i]);
					if (i != gesture.Length - 1)
					{
						stringBuffer.Append(",");
					}
				}
				return stringBuffer.ToString();
			}

			private readonly TestFrame _enclosing;
		}

		private void SetGestureString(string gesture)
		{
			statusLabel.SetText("Mouse gesture: " + gesture);
		}
	}
}
