package JPanels;

import Core.StopWatch;
import JPanels.Misc.CardPanel;

/**
 * The Timed Student Quiz Class
 * Quizzes the student with a timer.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class StudentQuizTime extends StudentQuiz {
	
	/** The watch. */
	protected StopWatch watch;
	
	/**
	 * Create the panel.
	 *
	 * @param cardPanel the card panel
	 * @param difficulty the difficulty
	 * @param shuffle the shuffle
	 */
	public StudentQuizTime(CardPanel cardPanel, String difficulty, boolean shuffle) {
		super(cardPanel, difficulty, shuffle);
		watch = new StopWatch(300);
		timerPanel.add(watch);
	}
	
	/* (non-Javadoc)
	 * @see JPanels.StudentQuiz#calculateScore()
	 */
	public float calculateScore() {
		long maxTime = watch.getMaxTime();							//Adding a score by calculating the time taken
		long timeLeft = watch.getTimeLeft();
		float percentageTimeLeft = (float)timeLeft/(float)maxTime; //decreases after every question
		float scoretoAdd = (100/questions.size())*percentageTimeLeft;
		return scoretoAdd;
	}
	
	/* (non-Javadoc)
	 * @see JPanels.StudentQuiz#getTimeTaken()
	 */
	public long getTimeTaken() {
		long timeTaken = watch.getCheckpoint() - watch.getTimeLeft();
		return timeTaken;
	}
	
	/* (non-Javadoc)
	 * @see JPanels.StudentQuiz#setTimeCheckPoint()
	 */
	public void setTimeCheckPoint() {
		watch.setCheckpoint();
	}

}
