package Core;
//My very own elegant hierarchical XML parser
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ElectricalComponents.ElectricalComponent;
import ElectricalComponents.Module;
import ElectricalComponents.Wire;

/**
 * The Reader Class. It reads in the XML and converts it into objects.
 *
 * @author DolanMiu
 * @version 1.0
 */
public class Reader {
	
	/** The file. */
	private static File fFile;
	
	/** The question list. */
	public static ArrayList<Question> questionList = new ArrayList<Question>();
    
    /**
     * Scans the file.
     *
     * @param file the file
     * @throws FileNotFoundException the file not found exception
     */
    public static void scanFile(File file) throws FileNotFoundException {
    	fFile = file;
    	try {
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fFile);
    		doc.getDocumentElement().normalize();
    		NodeList questions = doc.getElementsByTagName("question");
    		handleQuestions(questions);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /**
     * Scans the file.
     *
     * @param filename the file name
     * @throws FileNotFoundException the file not found exception
     */
    public final void scanFile(String filename) throws FileNotFoundException {
    	fFile = new File(filename);
    	try {
    		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		Document doc = dBuilder.parse(fFile);
    		doc.getDocumentElement().normalize();
    		NodeList questions = doc.getElementsByTagName("question");
    		handleQuestions(questions);
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /**
     * Handle questions.
     *
     * @param questions the questions
     */
    private static void handleQuestions(NodeList questions) {
    	System.out.println("Ammount of questions: " + questions.getLength());
		for (int i = 0; i < questions.getLength(); i++) {
			Element element = (Element) questions.item(i); //one question
			NodeList circuitNodes = element.getElementsByTagName("circuit");
			NodeList answerNodes = element.getElementsByTagName("answers");
			Node answers = answerNodes.item(0);
			Element circNode = (Element) circuitNodes.item(0);
			NodeList modules = circNode.getElementsByTagName("module");
			String diff = element.getAttribute("difficulty");
			int numb = Integer.parseInt(element.getAttribute("number"));
			String text = getTagValue("text", element);
			String type = element.getAttribute("type");
			
    		Circuit circuit = new Circuit();
    		ArrayList<ElectricalComponent> answerList = new ArrayList<ElectricalComponent>();
    		answerList = getAnswers(answers);
    		Question question = new Question(type, circuit, diff, numb, text, answerList);
    		questionList.add(question);
			handleModules(circuit, modules);
			circuit.setModuleVoltages();
			question.sortOutText();
		}
    }
    
    /**
     * Creates the wires.
     *
     * @param circuit the circuit
     */
    public static void createWires(Circuit circuit) {
    	for (int i = 0; i < circuit.getModules().size(); i++) { //for each module
    		if (i != circuit.modules.size()-1) {
    			Wire wire = new Wire(circuit.modules.get(i), circuit.modules.get(i+1));
    			circuit.addWires(wire);
    		} else {
    			Wire wire = new Wire(circuit.modules.get(i), circuit.modules.get(0));
    			circuit.addWires(wire);
    		}
    		//.printConnection();
    	}
    }
    
    /**
     * Handle modules.
     *
     * @param circuit the circuit
     * @param moduleNodes the module nodes
     */
    private static void handleModules(Circuit circuit, NodeList moduleNodes) {
		for (int i = 0; i < moduleNodes.getLength(); i++) {
			Node moduleNode = moduleNodes.item(i);
			System.out.println("\tModule: " + i);
			Module module = new Module(i);
    		circuit.addModule(module);
    		handleComponents(module, moduleNode, i);
		}
		createWires(circuit);
    }
    
    /**
     * Handle components.
     *
     * @param module the module
     * @param moduleNode the module node
     * @param moduleNumber the module number
     */
    private static void handleComponents(Module module, Node moduleNode, int moduleNumber) {
    	NodeList components = moduleNode.getChildNodes();
    	for (int i = 0; i < components.getLength(); i++) {
    		Node componant = components.item(i);
    		if (componant.getNodeType() == Node.ELEMENT_NODE) {
    			Element eElement = (Element) componant;
    			String name = getTagValue("name", eElement);
    			float resistance = Float.parseFloat((getTagValue("resistance", eElement)));
    			float voltage = Float.parseFloat((getTagValue("voltage", eElement)));
    			float current = Float.parseFloat((getTagValue("current", eElement)));
    			int x = Integer.parseInt((getTagValue("x", eElement)));
    			int y = Integer.parseInt((getTagValue("y", eElement)));
    			ElectricalComponent comp = InitComponent.create(name, resistance, voltage, current, x, y, "");
    			module.addComponant(comp);
    		}
    	}
    }
    
    /**
     * Gets the tag value.
     *
     * @param sTag the s tag
     * @param eElement the e element
     * @return the tag value
     */
    private static String getTagValue(String sTag, Element eElement) {
    	try {
    		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
    		Node nValue = (Node) nlList.item(0);
    		return nValue.getNodeValue();
    	} catch (NullPointerException e) {
    		return "0"; //This is normally forgotten
    	}
    }
    
	/**
	 * Gets the questions.
	 *
	 * @param difficulty the difficulty
	 * @return the questions
	 */
	public static ArrayList<Question> getQuestions(String difficulty) {
    	ArrayList<Question> selectedQuestions = new ArrayList<Question>();
    	for (int i = 0; i < questionList.size(); i++) {
    		Question question = questionList.get(i);
    		String diff = question.diff;
			if (diff.equals(difficulty)) {
				selectedQuestions.add(question);
			}
		}
		return selectedQuestions;
    }
	
	/**
	 * Gets the answers.
	 *
	 * @param answers the answers
	 * @return the answers
	 */
	private static ArrayList<ElectricalComponent> getAnswers(Node answers) {
		ArrayList<ElectricalComponent> answerList = new ArrayList<ElectricalComponent>();
    	NodeList components = answers.getChildNodes();
    	for (int i = 0; i < components.getLength(); i++) {
    		
    		Node componant = components.item(i);
    		if (componant.getNodeType() == Node.ELEMENT_NODE) {
    			Element eElement = (Element) componant;
    			String name = getTagValue("name", eElement);
    			float resistance = Float.parseFloat((getTagValue("resistance", eElement)));
    			float voltage = Float.parseFloat((getTagValue("voltage", eElement)));
    			float current = Float.parseFloat((getTagValue("current", eElement)));
    			int x = Integer.parseInt((getTagValue("x", eElement)));
    			int y = Integer.parseInt((getTagValue("y", eElement)));
    			String textAnswer = getTagValue("answer", eElement);
    			ElectricalComponent comp = InitComponent.create(name, resistance, voltage, current, x, y, textAnswer);
    			answerList.add(comp);
    		}
    	}
    	return answerList;
	}
}