import java.util.ArrayList;;

public class Converter {
	private static ArrayList<String> lengths = new ArrayList<String>(); 
	private static ArrayList<String> weights = new ArrayList<String>(); 
	
	public static float convert(float a, String from, String to) {
		lengths.add("m");
		lengths.add("cm");
		lengths.add("inches");
		lengths.add("inch");
		lengths.add("yards");
		lengths.add("as");
		if (lengths.contains(from)) {
			 return length(a,from,to);
		}
		return 0f;
		
	}
	
	public static float length (float a, String from, String to) {
		float meterConversionFactor = 0;
		switch (from) {
		case "m":
			meterConversionFactor = 1f;
			break;
		case "cm":
			meterConversionFactor = 0.01f;
			break;
		case "inches":
			meterConversionFactor = 0.0254f;
			break;
		case "inch":
			meterConversionFactor = 0.0254f;
			break;
		case "yards":
			meterConversionFactor = 0.9144f;
			break;
		case "as":
			meterConversionFactor = 149597870700f;
			break;
		}
		
		float valueInMeters = a*meterConversionFactor;
		System.out.println("Value In Meters: " + valueInMeters);

		switch (to) {
		case "m":
			return valueInMeters;
		case "cm":
			return valueInMeters*100f;
		case "inches":
			return valueInMeters*39.3700787f;
		case "inch":
			return valueInMeters*39.3700787f;
		case "yards":
			return valueInMeters*1.0936133f;
		case "as":
			return valueInMeters*0.000000000000668458712f;
		}
		return 0f;
	}
	
	public static float weight (float a, String from, String to) {
		float meterConversionFactor = 0;
		switch (from) {
		case "m":
			meterConversionFactor = 1f;
			break;
		case "cm":
			meterConversionFactor = 0.01f;
			break;
		case "inches":
			meterConversionFactor = 0.0254f;
			break;
		case "inch":
			meterConversionFactor = 0.0254f;
			break;
		case "yards":
			meterConversionFactor = 0.9144f;
			break;
		case "as":
			meterConversionFactor = 149597870700f;
			break;
		}
		
		float valueInKg = a*meterConversionFactor;
		System.out.println("Value In Meters: " + valueInKg);

		switch (to) {
		case "m":
			return valueInKg;
		case "cm":
			return valueInKg*100f;
		case "inches":
			return valueInKg*39.3700787f;
		case "inch":
			return valueInKg*39.3700787f;
		case "yards":
			return valueInKg*1.0936133f;
		case "as":
			return valueInKg*0.000000000000668458712f;
		}
		return 0f;
	}
}
