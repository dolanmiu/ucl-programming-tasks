
import java.util.Scanner;
public class main {	
	
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println ("Enter first number with unit after a space");
		String input1 = scanner.nextLine();
		System.out.println ("Enter the unit to change to");
		String ConversionUnit = scanner.nextLine();
		
		String[] firstUnit = new String[2];
		firstUnit = input1.split("\\s+");
		
		float result = Converter.convert(Integer.parseInt(firstUnit[0]), firstUnit[1], ConversionUnit);
		System.out.println(result);
	}
}
