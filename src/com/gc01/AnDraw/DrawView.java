package com.gc01.AnDraw;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;



/**
 * DrawView.java 
 * @author Dean Mohamedally 12/10/2011
 * A first drawing view example
 */
public class DrawView extends View implements OnTouchListener
{

	private Paint backgroundPaint = new Paint();
	private Paint drawPaint = new Paint();
	private Paint circlePaint = new Paint();
	private Paint textPaint = new Paint();


	private float sx, sy;

	public DrawView(Context context)
	{
		super(context);


		setFocusable(true);
		setFocusableInTouchMode(true);

		backgroundPaint.setColor(Color.WHITE);
		backgroundPaint.setAntiAlias(true);
		backgroundPaint.setStyle(Style.FILL);

		drawPaint.setColor(Color.BLUE);
		drawPaint.setStyle(Style.FILL);

		circlePaint.setColor(Color.RED);
		circlePaint.setStyle(Style.FILL);

		textPaint.setColor(Color.GREEN);
		textPaint.setStyle(Style.FILL);

		this.setOnTouchListener(this);
	}

	@Override
	public void onDraw(Canvas canvas)
	{
		// Draw white background
		canvas.drawRect(this.getLeft(), this.getTop(), this.getRight(), this.getBottom(), backgroundPaint);



		//draw a rectangle with blue paint
		canvas.drawRect(100,140, 200,180, drawPaint); 

		//draw text with green paint
		canvas.drawText("Hello World!", 110, 160, textPaint);

		//draw a circle with red paint with the touch coordinates
		canvas.drawCircle(sx-30,sy-30, 30, circlePaint);

	}

	public boolean onTouch(View v, MotionEvent event)
	{   
		//update the coordinates for the OnDraw method above, with wherever we touch
		sx = event.getX();
		sy = event.getY();

		invalidate();
		return true;
	}

}


