/**
 * <dl>
 * <dt> Purpose: 
 * <dd> Pretty Bar Chart
 * 
 * <dt> Description:
 * <dd> I made a bar chart which accepts a number of values whilst maintaining a pretty UI by using soft pastel colours and not harsh lines
 * </dl>
 * @author dolanmiu
 * @version $Date: 10/18/2012 11:31AM $
 *
 */
package BarChart;

import javax.swing.JFrame;

public class BarChart {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(400,300);
		double[] values = new double[6]; //need to change this when adding values
		String[] names = new String[6];
		
		values[0] = 1;
		names[0] = "Item 1";
		values[1] = 2;
		names[1] = "Item 2";
		values[2] = 7;
		names[2] = "Item 3";
		values[3] = 2;
		names[3] = "Item 4";
		values[4] = 1;
		names[4] = "Item 5";
		values[5] = 10;
		names[5] = "Item 6";
		
		ChartData data = new ChartData(values, names);
		frame.getContentPane().add(new ChartPanel(data));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
