package BarChart;

import java.awt.Color;
import java.util.Random;

public class ChartData {
	
	double[] values;
	String[] names;
	Color[] colour;
	public ChartData(double[] values, String[] names) {
		this.values = values;
		this.names = names;
		this.colour = new Color[values.length];
		for (int i = 0; i < values.length; i++) {
			Random random = new Random();
			float hue = random.nextFloat();
			float saturation = (random.nextInt(2000) + 1000) / 10000f; // Saturation between 0.1 and 0.3
			float luminance = 0.9f;
			Color colour = Color.getHSBColor(hue, saturation, luminance);
			this.colour[i] = colour;
		}
	}
	
	public double getMaxValue() {
		double largest = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] > largest) {
				largest = values[i];
			}
		}
		return largest;
	}
	
	public double[] getValues() {
		return values;
	}
	
	public double getValuei(int i) {
		return values[i];
	}
	
	public Color getColouri(int i) {
		return colour[i];
	}
}
