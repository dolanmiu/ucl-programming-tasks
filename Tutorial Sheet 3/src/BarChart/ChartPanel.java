package BarChart;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

import javax.swing.JPanel;

public class ChartPanel extends JPanel {
	ChartData data;
	public ChartPanel(ChartData data) {
		setBackground(Color.white);
		this.data = data;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		drawChart(g2d);
	}
	
	private void drawChart(Graphics2D g2d) {
		Dimension d = getSize();
		int width = d.width;
		int height = d.height;
		
		float unitSeparation = width/data.getValues().length;
		for (int i = 0; i < data.getValues().length; i++) {
			Polygon p = new Polygon();
			double highestVal = data.getMaxValue();
			double percentage = data.getValuei(i)/highestVal;
			int heightValue = (int)((1-percentage)*height);
			
			p = drawPolygon(p, unitSeparation, heightValue, height, i); //removing p does nothing?
			
			g2d.setColor(Color.gray);
			g2d.draw(p);
	        g2d.setColor(data.getColouri(i));
	        g2d.fillPolygon(p);
		}
	}
	
	private Polygon drawPolygon(Polygon p, float unitSeparation, int heightValue, int height, int i) {
		p.addPoint((int)(i*unitSeparation+unitSeparation), (int)(heightValue));
		p.addPoint((int)(i*unitSeparation),(int)(heightValue));
		
		p.addPoint((int)(i*unitSeparation),height);
		p.addPoint((int)(i*unitSeparation+unitSeparation),height);
		return p;
	}
}
