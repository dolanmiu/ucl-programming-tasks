/**
 * <dl>
 * <dt> Purpose: 
 * <dd> Removing an element from an array
 * 
 * <dt> Description:
 * <dd> This removes an element from an array using a for loop and if statement. It recreates the array and returns it.
 * </dl>
 * @author dolanmiu
 * @version $Date: 9/27/2012 11:31AM $
 *
 */
import java.util.Arrays;
public class ArrayElementRemoval {
	public static void main(String[] args) {
		String[] array = {"The" , "quick" , "brown" , "fox" , "jumps" , "over" , "the" , "lazy" , "dog."};
		System.out.println(array.length);
		String output = Arrays.toString(remElement(array, 5));
		System.out.println(output);
	}
	
	private static String[] remElement(String[] array, int location) {
		String[] newArray = new String[array.length-1];
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (i != location) {
				System.out.println(i);
				newArray[count] = array[i];
				count++;
			}
		}
		return newArray;
	}
}
