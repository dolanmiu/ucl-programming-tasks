/**
 * <dl>
 * <dt> Purpose: 
 * <dd> Devise a minimalist way to check if a number is even or not. 
 * 
 * <dt> Description:
 * <dd> At first it was complex, with an un-needed local isEven variable with multiple returns. Now its a single return with no local variables.
 * </dl>
 * @author dolanmiu
 * @version $Date: 10/08/2012 11:31AM $
 *
 */
public class ProgramFlow {
	public static void main(String[] args) {
		System.out.print(isEven(3));
		System.out.print(isEven(4));
		
	}
	
	private static boolean isEven(int number) {
		/*boolean isEven;
		if (number % 2 == 0) {
			isEven = true;
		}
		isEven = false;*/
		return (number % 2 == 0) ? true : false;
	}
}
