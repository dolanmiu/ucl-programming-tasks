package ChessExtended;
/**
 * <dl>
 * <dt> Purpose: 
 * <dd> Create working chess and extending it
 * 
 * <dt> Description:
 * <dd> My chess masterpiece. Uses various methods and techniques from my arsenal of programming knowledge. Now with each unit as an object
 * </dl>
 * @author dolanmiu
 * @version $Date: 10/18/2012 11:31AM $
 *
 */
import java.util.Scanner;

import pieces.AbstractPiece;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;

public class VirtualChess {
	private final static int CHESSBOARD_SIZE = 8;
	private static AbstractPiece[][] chessboard = new AbstractPiece[CHESSBOARD_SIZE][CHESSBOARD_SIZE];
	static int playerTurn = 0; //0 is player1, 1 is player2
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		initUnits();
		while (true) {
			printBoard(chessboard);
			System.out.println("Your move: " );
			String[] command = new String[3];
			command = scanner.nextLine().split("\\s+");
			if (command[0].equals("exit")) {
				scanner.close();
				System.exit(0);
			}
			if (verifySyntax(command)) {
				moveUnit(returnCoords(command, 0), returnCoords(command, 2)); //X0 to X0
			} else {
				System.out.println("Invalid Syntax");
				System.out.println("");
			}
		}
	}
	
	private static void changePlayer() {
		if (playerTurn == 0 || playerTurn == 1) { 
			if (playerTurn == 0) {
				playerTurn = 1; 
				System.out.println("Player 2's turn!");
				return;
			}
			if (playerTurn == 1) { 
				playerTurn = 0; 
				System.out.println("Player 1's turn!");
				return;
			}
		}
	}
	
	private static void moveUnit(int[] coordsFrom, int[] coordsTo) {
		if (!verifyMove(coordsFrom, coordsTo)) {
			System.out.println("Invalid Move, the unit cannot reach to your destination.");
			return;
		}
		//Moves unit
		chessboard[coordsTo[0]][coordsTo[1]] = chessboard[coordsFrom[0]][coordsFrom[1]];
		chessboard[coordsFrom[0]][coordsFrom[1]] = null;
		changePlayer();
	}
	
	private static boolean verifySyntax(String[] command) {
		if (command[0].length() != 2 || command[2].length() != 2) {
			System.out.println("Coordinates too short or too long");
			return false;
		}
		char[] firstCoord = command[0].toCharArray();
		char[] secondCoord = command[2].toCharArray();
		if (!(firstCoord[1] >= '0' && firstCoord[1] <= '9')) {
			System.out.println("No number found in coordinate 1");
			return false;
		}
		if (!(secondCoord[1] >= '0' && secondCoord[1] <= '9')) {
			System.out.println("No number found in coordinate 2");
			return false;
		}
		if (!Character.isLetter(firstCoord[0]) || !Character.isLetter(secondCoord[0])) {
			System.out.println("The first part of the coordinates isn't a character");
			return false;
		}
		return true;
	}
	
	private static boolean isPlayersUnit(int[] coordsFrom, int[] coordsTo) {
		if (playerTurn == 0) {
			if (chessboard[coordsFrom[0]][coordsFrom[1]].isWhite() == false) { //doesn't allow black units
				return false;
			}
			if (chessboard[coordsTo[0]][coordsTo[1]] != null) { //if coordinate contains a unit
				if (chessboard[coordsTo[0]][coordsTo[1]].isWhite()) { //if its a white unit
					return false;
				}
			}
		} else {
			if (chessboard[coordsFrom[0]][coordsFrom[1]].isWhite() == true) { //doesn't allow white units
				return false;
			}
			if (chessboard[coordsTo[0]][coordsTo[1]] != null) { //if coordinate contains a unit
				if (!chessboard[coordsTo[0]][coordsTo[1]].isWhite()) { //if its a black unit
					return false;
				}
			}
		}
		return true;
	}
	
	private static boolean verifyMove(int[] coordsFrom, int[] coordsTo) {
		System.out.println(chessboard[coordsFrom[0]][coordsFrom[1]]);
		if (!isPlayersUnit(coordsFrom, coordsTo)) {
			System.out.println("Invalid Move, Remember your colour Sir.");
			return false;
		}
		if (chessboard[coordsFrom[0]][coordsFrom[1]] == null) {
			System.out.println("Invalid Move, the first coordinate contains no Unit");
			return false;
		}
		if(chessboard[coordsFrom[0]][coordsFrom[1]].isMoveValid(coordsFrom[0], coordsFrom[1], coordsTo[0], coordsTo[1])) {
			return true;
		}
		return false;
	}
	
	private static int[] returnCoords(String[] command, int number) {
		char[] position = command[number].toCharArray();
		int[] coordinates = new int[2];
		
		switch (position[0]) {
			case 'a':
				coordinates[0] = 0;
				break;
			case 'b':
				coordinates[0] = 1;
				break;
			case 'c':
				coordinates[0] = 2;
				break;
			case 'd':
				coordinates[0] = 3;
				break;	
			case 'e':
				coordinates[0] = 4;
				break;
			case 'f':
				coordinates[0] = 5;
				break;
			case 'g':
				coordinates[0] = 6;
				break;
			case 'h':
				coordinates[0] = 7;
				break;
		}
		coordinates[1] = Character.getNumericValue(position[1]);
		coordinates[1] = 8-coordinates[1];
		return coordinates;
		
	}
	
	private static void initUnits() {
		for (int i = 0; i < chessboard.length; i ++) {
			for (int j = 0; j < chessboard[0].length; j++) {
				chessboard[i][j] = null;
			}
		}
		chessboard[0][0] = new Rook(false);
		chessboard[1][0] = new Knight(false);
		chessboard[2][0] = new Bishop(false);
		chessboard[3][0] = new King(false);
		chessboard[4][0] = new Queen(false);
		chessboard[5][0] = new Bishop(false);
		chessboard[6][0] = new Knight(false);
		chessboard[7][0] = new Rook(false);
		
		for (int i = 0; i < 8; i ++) {
			chessboard[i][1] = new Pawn(false);
		}
		
		chessboard[0][7] = new Rook(true);
		chessboard[1][7] = new Knight(true);
		chessboard[2][7] = new Bishop(true);
		chessboard[3][7] = new King(true);
		chessboard[4][7] = new Queen(true);
		chessboard[5][7] = new Bishop(true);
		chessboard[6][7] = new Knight(true);
		chessboard[7][7] = new Rook(true);
		
		for (int i = 0; i < chessboard.length; i ++) {
			chessboard[i][6] = new Pawn(true);
		}
		
	}
	
	public static void printBoard(AbstractPiece[][] chessboard) {
		System.out.println(" \ta\tb\tc\td\te\tf\tg\th\t\n");
		for (int i = 0; i < chessboard.length; i++) {
			System.out.print(8-i + "\t");
			for (int j = 0; j < chessboard[0].length; j++) {
				try {
				chessboard[j][i].draw();
				} catch (NullPointerException e) {
					System.out.print("\t");
				}
				
			}
			System.out.println("");
		}
	}
	
	public static void print(String string) {
		System.out.print(string);
	}
}