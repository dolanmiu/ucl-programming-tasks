package pieces;

public class Bishop extends AbstractPiece {

	public Bishop(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}

	public void draw() {
		if (isWhite) {
			System.out.print("b");
		} else {
			System.out.print("w");
		}
		System.out.print("B\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		if (Math.abs(srcRow - destRow) == Math.abs(srcCol - destCol)) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 3;
	}
}
