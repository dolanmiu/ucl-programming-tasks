package pieces;

public class Pawn extends AbstractPiece {
	
	public Pawn(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}

	public void draw() {
		if (isWhite) {
			System.out.print("w");
		} else {
			System.out.print("b");
		}
		System.out.print("P\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		//System.out.println(srcRow + " " + destRow + " " + srcCol + " " + destCol);
		//System.out.println(Math.abs(srcRow - destRow) + Math.abs(srcCol - destCol));
		if ((srcRow == destRow || srcCol == destCol) && (Math.abs(srcRow - destRow) + Math.abs(srcCol - destCol) == 1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 1;
	}
}
