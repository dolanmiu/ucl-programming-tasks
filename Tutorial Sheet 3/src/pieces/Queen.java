package pieces;

public class Queen extends AbstractPiece {
	
	public Queen(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}

	public void draw() {
		if (isWhite) {
			System.out.print("w");
		} else {
			System.out.print("b");
		}
		System.out.print("Q\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		if ((srcRow == destRow || srcCol == destCol) || (Math.abs(srcRow - destRow) == Math.abs(srcCol - destCol))) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 9;
	}
}
