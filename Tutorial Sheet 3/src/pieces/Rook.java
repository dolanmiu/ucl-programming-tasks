package pieces;

public class Rook extends AbstractPiece{

	public Rook(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}
	
	public void draw() {
		if (isWhite) {
			System.out.print("w");
		} else {
			System.out.print("b");
		}
		System.out.print("R\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		if (srcRow == destRow || srcCol == destCol) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 5;
	}

}
