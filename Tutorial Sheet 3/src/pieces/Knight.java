package pieces;

public class Knight extends AbstractPiece {
	
	public Knight(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}

	public void draw() {
		if (isWhite) {
			System.out.print("w");
		} else {
			System.out.print("b");
		}
		System.out.print("K\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		int totalDistance = Math.abs(srcRow - destRow) + Math.abs(srcCol - destCol);
		
		if (totalDistance == 3 && (Math.abs(srcRow - destRow) <= 2) && (Math.abs(srcCol - destCol) <= 2)) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 3;
	}
}
