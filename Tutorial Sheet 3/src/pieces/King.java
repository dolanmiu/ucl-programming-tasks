package pieces;

public class King extends AbstractPiece {
	
	public King(boolean isWhite) {
		super(isWhite);
		// TODO Auto-generated constructor stub
	}

	public void draw() {
		if (isWhite) {
			System.out.print("W");
		} else {
			System.out.print("B");
		}
		System.out.print("K\t");
	}
	
	public boolean isMoveValid(int srcRow, int srcCol, int destRow, int destCol) {
		if ((srcRow == destRow || srcCol == destCol) && (Math.abs(srcRow - destRow) + Math.abs(srcCol - destCol) == 1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public int relativeValue() {
		return 8;
	}
}
