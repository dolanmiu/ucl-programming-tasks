/**
 * <dl>
 * <dt> Purpose: 
 * <dd> Radar Graph
 * 
 * <dt> Description:
 * <dd> It produces a radar graph with a random set of data values in a pretty format.
 * </dl>
 * @author dolanmiu
 * @version $Date: 10/18/2012 11:31AM $
 *
 */
package radargraph;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Controller {
	public static void main(String[] args) throws Exception{
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
		
	}
	
	private static void createAndShowGUI() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(640,480));
		frame.add(new RadarGraph());
		frame.pack();
		frame.setVisible(true);
	}
}
