package radargraph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;

import javax.swing.JPanel;

public class RadarGraph extends JPanel{
	private Data data = new Data(3,5, Color.pink, Color.magenta, Color.cyan);
	
	public RadarGraph() {
		setBackground(Color.white);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		//CLS
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		drawWeb(g2d);
	}
	
	private void drawWeb(Graphics2D g2d) {
		int width = super.getSize().width/2;
		int height = super.getSize().height/2;
		
		float armLength = height*0.9f;
		double armAngleSeparation = 2*Math.PI/5;
		drawData(g2d, width, height, armLength, armAngleSeparation);
		drawGridLines(g2d, width, height, armLength, armAngleSeparation);
		for (int i = 0; i < 5; i++) {
			int endPointX = (int)(trigArm(armLength,'c',armAngleSeparation,i));
			int endPointY = (int)(trigArm(armLength,'s',armAngleSeparation,i));
			g2d.drawLine(width, height, width+endPointX, height+endPointY);
		}
		
		String[] labels = {"hello", "bye", "world", "computer", "science" };
		drawLabels(g2d, labels, width, height, armAngleSeparation, armLength);
	}
	
	private void drawLabels(Graphics g2d, String[] labels, int width, int height, double armAngleSeparation, float armLength) {
		for (int j = 0; j < labels.length; j++) {
			char[] chars = labels[j].toCharArray();
			int endPointX = (int)(1.05*trigArm(armLength,'c',armAngleSeparation,j));
			int endPointY = (int)(1.05*trigArm(armLength,'s',armAngleSeparation,j));
			g2d.drawChars(chars, 0, chars.length, width+endPointX, height+endPointY);
		}
	}
	
	private void drawGridLines(Graphics2D g2d, int width, int height, float armLength, double armAngleSeparation) {
		for (int i = 0; i < 11; i++) {
			for (int j = 0; j < data.width(); j++) {
				int endPointX, endPointY;
				int startPointX = (int)(0.1f*i*trigArm(armLength,'c',armAngleSeparation,j));
				int startPointY = (int)(0.1f*i*trigArm(armLength,'s',armAngleSeparation,j));
				if (j+1 == 5) {
					endPointX = (int)(0.1f*i*trigArm(armLength,'c',armAngleSeparation,0));
					endPointY = (int)(0.1f*i*trigArm(armLength,'s',armAngleSeparation,0));
				} else {
					endPointX = (int)(0.1f*i*trigArm(armLength,'c',armAngleSeparation,j+1));
					endPointY = (int)(0.1f*i*trigArm(armLength,'s',armAngleSeparation,j+1));
				}
				g2d.setColor(Color.gray);
				g2d.drawLine(width+startPointX, height+startPointY, width+endPointX, height+endPointY);
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void drawData2(Graphics2D g2d, int width, int height, float armLength, double armAngleSeparation) {
		for (int i = 0; i < data.height(); i++) {
			for (int j = 0; j < data.width(); j++) {
				int endPointX, endPointY;
				int startPointX = (int)(data.getValue(i, j)*trigArm(armLength,'c',armAngleSeparation,j));
				int startPointY = (int)(data.getValue(i, j)*trigArm(armLength,'s',armAngleSeparation,j));
				if (j+1 == 5) {
					endPointX = (int)(data.getValue(i, 0)*trigArm(armLength,'c',armAngleSeparation,0));
					endPointY = (int)(data.getValue(i, 0)*trigArm(armLength,'s',armAngleSeparation,0));
				} else {
					endPointX = (int)(data.getValue(i, j+1)*trigArm(armLength,'c',armAngleSeparation,j+1));
					endPointY = (int)(data.getValue(i, j+1)*trigArm(armLength,'s',armAngleSeparation,j+1));
				}
				g2d.drawLine(width+startPointX, height+startPointY, width+endPointX, height+endPointY);
			}
		}
	}
	
	private void drawData(Graphics2D g2d, int width, int height, float armLength, double armAngleSeparation) {
		for (int i = 0; i < data.height(); i++) {
			Polygon p = new Polygon();
			for (int j = 0; j < data.width(); j++) {
				int startPointX = (int)(data.getValue(i, j)*trigArm(armLength,'c',armAngleSeparation,j));
				int startPointY = (int)(data.getValue(i, j)*trigArm(armLength,'s',armAngleSeparation,j));
				p.addPoint(width+startPointX, height+startPointY);
			}
			g2d.setColor(Color.gray);
	        g2d.draw(p);
	        g2d.setColor(data.getColour(i));
	        g2d.fillPolygon(p);
		}
	}
	
	private double trigArm(float armLength, char trig, double angle, int angleMultiplier) {
		if (trig == 'c') {
			return armLength*(Math.cos(angle*angleMultiplier-(2*Math.PI/4)));
		}
		if (trig == 's') {
			return armLength*(Math.sin(angle*angleMultiplier-(2*Math.PI/4)));
		}
		return 0;
	}
}
