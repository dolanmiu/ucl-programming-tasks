package radargraph;

import java.awt.Color;
import java.util.Random;

public class Data {
	double[][] colouredOutlines;
	Color c1;
	Color c2; 
	Color c3;
			
	//public int width = colouredOutlines[0].length;
	//public int length = colouredOutlines.length;
	
	public Data(int x, int y, Color c1, Color c2, Color c3) {
		Random rand = new Random(100);
		colouredOutlines = new double[x][y];
		for (int i = 0; i < colouredOutlines.length; i++) {
			for (int j = 0; j < colouredOutlines[0].length; j++) {
				colouredOutlines[i][j] = rand.nextDouble();
			}
		}
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
	}
	
	public double getValue(int i, int j) {
		return colouredOutlines[i][j];
	}
	
	public int height() {
		return colouredOutlines.length;
	}
	
	public int width() {
		return colouredOutlines[0].length;
	}
	
	public Color getColour(int dataSet) {
		switch (dataSet) {
		case 0:
			return c1;
		case 1:
			return c2;
		case 2:
			return c3;
		} 
		return null;
	}
	

}
