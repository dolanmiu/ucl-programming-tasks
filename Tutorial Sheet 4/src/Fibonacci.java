import java.util.Arrays;


public class Fibonacci {
	public static void main(String[] args) {
		int[][] seq = fibonacci(13);
		System.out.println(Arrays.toString(seq[0]));
		System.out.println(Arrays.toString(seq[1]));
		for (int i = 0; i < seq[0].length; i++) {
			System.out.print("("+seq[0][i]+"):");
			System.out.print(seq[1][i]);
			System.out.print(" ");
		}
		System.out.print("\n");
		System.out.println(RecursiveFibonacci(13)); //needs n+1
		
		
	}
	
	public static int[][] fibonacci(int size) {
		int[][] fibonacciSeq = new int[2][size];
		fibonacciSeq[1][0] = 0;
		fibonacciSeq[1][1] = 1;
		fibonacciSeq[0][1] = 1;
		for (int i = 2; i < size; i++) {
			fibonacciSeq[1][i] = fibonacciSeq[1][i-1] + fibonacciSeq[1][i-2];
			fibonacciSeq[0][i] = i;
		}
		return fibonacciSeq;
	}
	
	public static long RecursiveFibonacci(int n) {
		if(n == 1 || n == 2 || n == 0) {
			return 1;
		} else {
			return (RecursiveFibonacci(n-1)+RecursiveFibonacci(n-2));
		}
	}
}
