
public class Factorial {
	public static void main(String[] args) {
		System.out.println(RecursiveFactorial(5));
		System.out.println(factorial(5));
	}
	
	public static long factorial(int n) {
		long answer = n; 
		for (int i = 1; i < n; i++) {
			answer *= (n-i);
		}
		return answer;
	}
	
	public static long RecursiveFactorial(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n*RecursiveFactorial(n-1);
		}
	}
}
