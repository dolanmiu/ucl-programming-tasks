import java.util.Scanner;


public class Photograph {
	public static void main(String args[]) {
		Scanner scanner = new Scanner (System.in);
		System.out.println("Enter the DPI of the image (density):");
		float dpi = scanner.nextFloat();
		System.out.println("Enter the ammount of pixels for the image:");
		int pixels = scanner.nextInt();
		System.out.println("Enter the focal distance in cm (Eye: 1.7 cm):");
		float focalDistance = scanner.nextFloat();
		System.out.println("Enter the object distance in cm:");
		float objectDistance = scanner.nextFloat();
		scanner.close();
		float ans = calculate(dpi, pixels, focalDistance, objectDistance);
		System.out.println("The object size is " + ans + "cm.");
	}
	
	public static float calculate(float dpi, int pixels, float focalDistance, float objectDistance) {
		float sizeXPrimeInch = (float)pixels/dpi;
		float sizeXPrime = sizeXPrimeInch*2.54f;
		float sizeX = (objectDistance*sizeXPrime)/(focalDistance);
		return sizeX;
	}
}
