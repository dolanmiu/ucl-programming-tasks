
public class Cosine {
	public static void main(String[] args) {
		System.out.println(Cos((float) (0.4*Math.PI)));
		System.out.println(Secant((float) (0.4*Math.PI)));
	}
	
	public static float Cos(float x) {
		boolean isPositive = false;
		float answer = 1;
		for (int i = 0; i < 10; i++) {		
			if (i % 2 == 0 && i != 0) {
				if (isPositive == true) {
					float factorial = (float)factorial(i);
					float power = (float)Math.pow(x, i);
					answer += power/factorial;
					isPositive = false;
				} else {
					float factorial = (float)factorial(i);
					float power = (float)Math.pow(x, i);
					answer -= power/factorial;
					isPositive = true;
				}
			}
		}
		return answer;
	}
	
	public static long factorial(int n) {
		if (n == 0) {
			return 1;
		} else {
			return n*factorial(n-1);
		}
	}
	
	public static float Secant(float x) {
		return ((float)1/Cos(x));
	}
}
