
public class HarmonicSeries {
	public static void main(String[] args) {
		float harmonicltr = leftToRight(60000);
		System.out.println(harmonicltr);
		
		float harmonicrtl = rightToLeft(60000);
		System.out.println(harmonicrtl);
		
		
	}
	
	public static float rightToLeft(int n) {
		float harmonic = 0;
		for (int i = n; i > 0; i--) {
			harmonic += (float)1/(float)i;
		}
		return harmonic;
	}
	
	public static float leftToRight(int n) {
		float harmonic = 1;
		for (int i = 1; i < n; i++) {
			harmonic += (float)1/(float)i;
		}
		return harmonic;
	}
}
