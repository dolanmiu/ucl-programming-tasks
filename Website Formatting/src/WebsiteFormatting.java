import java.util.Scanner;


public class WebsiteFormatting {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter 2 numbers, width and height of table: ");
		int width = scanner.nextInt();
		int height = scanner.nextInt();
		System.out.println(width + " " + height);
		
		generateTable(width, height);
		
		
	}
	
	private static void generateTable(int width, int height) {
		System.out.println("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		for (int i = 0; i < width; i++) {
			System.out.println("\t<tr>");
			for (int j = 0; j < height; j++) {
				System.out.println("\t\t<td>&nbsp;</td>");
			}
			System.out.println("\t</tr>");
		}
		System.out.println("</table>");
		
		
		
	}
}
