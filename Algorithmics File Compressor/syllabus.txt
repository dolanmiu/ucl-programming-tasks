Code: COMPGC05

Year: MSc

Prerequisites: 
This course should be taken in conjunction with the core courses for
this programme (ie GC01, GC02, GC03 and GC04)

Term: 2

Taught By:
Denise Gorse (50%) 
Jens Krinke (50%)

Aims:
To introduce more formal aspects of algorithms and data structures
than in the first term. Properties of data types such as queues and
search trees. Techniques for analysing the complexity and decidability
of algorithms. Formal models of computation.

Learning Outcomes:
Knowledge of a selection of standard data structures and abstract data
types. The ability to choose appropriate data structures for
programming problems. Understanding of a variety of common algorithms
and why some are more efficient than others. Ability to discuss
uncomputable and intractable algorithms.