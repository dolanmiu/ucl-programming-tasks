import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class FileHandler {
	private byte[] data;
	
	public FileHandler() {
		data = null;
	}
	
	public void LoadFile(String location) throws IOException {
		String extension = location.substring(location.lastIndexOf(".") + 1, location.length());
		if (extension.equals("zl")) {
			data = ReadFile(location);
		} else {
			Path path = Paths.get(location);
			data = Files.readAllBytes(path);
		}
	}
	
	public void Compress(String path) throws IOException {
		Tree tree = new Tree();;
		int[] bits = ToBinary(data);
		for (int i = 0; i < bits.length; i++) {
			tree.InsertBitToNode(bits[i]);
		}
		tree.GetSequencePairs();
		System.out.println("Compression: " + tree.toString());
		WriteFile(path, tree);
	}
	
	public void Decompress(String path) throws IOException {
		File file = new File(path);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fo = new FileOutputStream(file);
		fo.write(data);
		fo.flush();
		fo.close();
	}
	
	private byte[] ReconstructFile(ArrayList<Integer> shortFile) {
		Tree newFileTree = new Tree();
		for (int i = 1; i < shortFile.size(); i++) {
			int bitValue = 0;
			if (shortFile.get(i) < 0) {
				bitValue = 1;
			}
			if (shortFile.get(i) != 0) {
				newFileTree.InsertMapEntryToNode(Math.abs(shortFile.get(i)), bitValue);
			}
		}
		System.out.println("Decompression: " + newFileTree.toString());
		return newFileTree.GetBytes();
	}
	
	private int[] ToBinary(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * Byte.SIZE);
		
		for(int i = 0; i < Byte.SIZE * bytes.length; i++) {
			sb.append((bytes[i / Byte.SIZE] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
		}
		String binaryString = sb.toString();
		int[] binary = new int[binaryString.length()];
		char [] asChar = binaryString.toCharArray();
		
		int i=0;
		for (char c : asChar) {
			binary[i++] = Character.digit(c,10);
		}
		return binary;
	}
	
	private void WriteFile(String path, Tree tree) throws IOException {
		DataOutputStream output = new DataOutputStream(new FileOutputStream(path));
		int[][] seqPairs = tree.GetSequencePairs();
		output.writeInt(32767);
		for (int i = 0; i < seqPairs.length; i++) {
			int valueToWrite;
			if (seqPairs[i][1] == 1) {
				valueToWrite = seqPairs[i][0] * -1;
			} else {
				valueToWrite = seqPairs[i][0];
			}
			output.writeShort(valueToWrite);
		}
		//output.writeShort(0);
		output.close();
	}
	
	private byte[] ReadFile(String path) throws IOException {
		DataInputStream input = new DataInputStream(new FileInputStream(path));
		ArrayList<Integer> file = new ArrayList<Integer>();
		int maxIntHeader = input.readInt();
		file.add(maxIntHeader);
		while (input.available() != 0) {
			int value = input.readShort();
			file.add(value);
		}
		input.close();  
		byte[] fileData = ReconstructFile(file);
		return fileData;
	}
}
