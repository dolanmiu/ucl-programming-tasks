import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;


public class Tree {
	
	private TreeNode root;
	private int indexCount;
	private TreeNode currentNode;
	private int[][] nodeSequence;
	private HashMap<Integer, TreeNode> nodeMap;
	
	public Tree() {
		nodeSequence = new int[0][0];
		indexCount = 1;
		NodeEntry entry = new NodeEntry(-1);
		entry.SetIndex(indexCount);
		indexCount++;
		root = new TreeNode(entry);
		currentNode = root;
		nodeMap = new HashMap<Integer, TreeNode>();
		nodeMap.put(1, root);
	}
	
	public void InsertBitToNode(int bit) {
		NodeEntry entry = new NodeEntry(bit);
		TreeNode newNode = new TreeNode(entry);
		newNode.SetParent(currentNode);
		AddToTree(newNode, currentNode);
	}
	
	public void InsertMapEntryToNode(int key, int bitValue) {
		TreeNode node = CreateBiggestNode(bitValue);
		if (bitValue == 0) {
			nodeMap.get(key).SetLeftNode(node);
		} else {
			nodeMap.get(key).SetRightNode(node);
		}
		nodeMap.put(node.entry.GetIndex(), node);
		node.SetParent(nodeMap.get(key));
		if (node != root) {
			nodeSequence = AddRow(nodeSequence, node.GetParentNode().entry.GetIndex(), node.entry.GetValue());
		}
	}
	
	public byte[] GetBytes() {
		ArrayList<Integer> bits = new ArrayList<Integer>();
		for (int i = 1; i < FindMapMaxValue(nodeMap).getKey(); i++) {
			bits.addAll(TraverseBranch(nodeMap.get(i)));
		}
		String stringBits = ListToString(bits);
	    return GetBytesFromBits(stringBits);
	}
	
	private byte[] GetBytesFromBits(String bits) {
		BitSet bites = new BitSet(bits.length());
		for (int i = 0; i < bits.length(); i++) {
			if (Character.getNumericValue(bits.charAt(i)) == 1) {
				bites.set(i, true);
			} else {
				bites.set(i, false);
			}
		}
		byte[] byteArray = ToByteArray(bits);
	    return byteArray;
	}
	
	private byte[] ToByteArray(String bits) {
		String[] bitStrings = bits.split("(?<=\\G........)");
		byte[] byteArray = new byte[bitStrings.length];
		for (int i = 0; i < bitStrings.length; i++) {
			for (int j = 1; j < 8; j++) {
				int bit;
				try {
				bit = Character.getNumericValue(bitStrings[i].charAt(j));
				} catch (Exception e) {
					bit = 0;
				}
				if (bit == 1) {
					int power = 7-j;
					int order = (int) Math.pow(2, power) ;
					byteArray[i] += order;
				}
			}
		}
		//System.out.println(bitStrings);
		return byteArray;
	}
	
	private String ListToString(ArrayList<Integer> intList)
	{
		String listString = "";
		for (int s : intList)
		{
		    listString += s;
		}
		return listString;
	}
	
	private ArrayList<Integer> TraverseBranch(TreeNode destinationNode) {
		ArrayList<Integer> subBits = new ArrayList<Integer>();
		TreeNode node = destinationNode;
		while (node != root) {
			int bit = node.entry.GetValue();
			if (bit != -1) {
				subBits.add(bit);
			}
			node = node.GetParentNode();
		}
		Collections.reverse(subBits);
		return subBits;
	}
	
	private TreeNode CreateBiggestNode(int bit) {
		NodeEntry entry = new NodeEntry(bit);
		entry.SetIndex(FindMapMaxValue(nodeMap).getKey() + 1);
		TreeNode newNode = new TreeNode(entry);
		return newNode;
	}
	
	private Entry<Integer,TreeNode> FindMapMaxValue(HashMap<Integer,TreeNode> map) {
		Entry<Integer,TreeNode> maxEntry = null;
		for(Entry<Integer,TreeNode> entry : map.entrySet()) {
		    if (maxEntry == null || entry.getKey() > maxEntry.getKey()) {
		        maxEntry = entry;
		    }
		}
		return maxEntry;
	}
	
	private void AddToTree(TreeNode newNode, TreeNode currentNode) {
		if (newNode.entry.GetValue() == 0) {
			if (currentNode.GetLeftNode() == null) {
				newNode = AddNode(newNode);
				currentNode.SetLeftNode(newNode);
			} else {
				this.currentNode = currentNode.GetLeftNode();
			}
		}
		
		if (newNode.entry.GetValue() == 1) {
			if (currentNode.GetRightNode() == null) {
				newNode = AddNode(newNode);
				currentNode.SetRightNode(newNode);
			} else {
				this.currentNode = currentNode.GetRightNode();
			}
		}
	}
	
	private TreeNode AddNode(TreeNode newNode) {
		newNode.entry.SetIndex(indexCount);
		indexCount++;
		this.currentNode = root;
		nodeSequence = AddRow(nodeSequence, newNode.GetParentNode().entry.GetIndex(), newNode.entry.GetValue());
		return newNode;
	}
	
    private int[][] AddRow(int[][] previous, int index, int value) {
        int prevRowCount = previous.length;
        int[][] withExtraRow = new int[prevRowCount + 1][];
        System.arraycopy(previous, 0, withExtraRow, 0, previous.length);
        withExtraRow[prevRowCount] = new int[] { index, value };
        return withExtraRow;
    }
	
	public int[][] GetSequencePairs() {
		return nodeSequence;
	}
	
	@Override
	public String toString() {
		String outputString = new String();
		for (int[] is : nodeSequence) {
			outputString = outputString + Arrays.toString(is);
		}
		return outputString;
	}
}
