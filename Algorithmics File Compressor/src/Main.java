import java.io.IOException;


public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to the Dolan's implementation of the Lempel-Ziv compressor!");
		System.out.println("Working...");

		//Scanner scanner = new Scanner(System.in);
		//String[] input = scanner.nextLine().split("(?!^)");
		//scanner.close();
		String preFilePath = null;
		String postFilePath = null;
		boolean isDecompress = false;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-x")) {
				isDecompress = true;
			}
		}
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].contains(".")) {
				if (GetExtension(args[i]).equals("zl")) {
					if (isDecompress) {
						preFilePath = args[i];
					} else {
						postFilePath = args[i];
					}
				}
			}
		}
		try {
			Program(args[0],args[1],isDecompress);
		} catch(Exception e) {
			System.out.println("Something went wrong. Check Syntax matches, or run in command-line: ");
			System.out.println("\t$ java Main original compressed");
			System.out.println("\t$ java Main compressed decompressed -x");
		}
	}
	
	private static void Program(String preFilePath, String postFilePath, boolean isDecompress) {
		if (preFilePath == null || postFilePath == null) {
			System.out.println("File paths are null. Check Syntax matches: ");
			System.out.println("\t$ java Main original compressed");
			System.out.println("\t$ java Main compressed decompressed -x");
			System.exit(0);
		}
		if (isDecompress) {
			FileHandler fhDecompress = new FileHandler();
			if (!GetExtension(preFilePath).equals("zl")) {
				System.out.println("Can only decompress a .zl file format!");
				System.exit(0);
			}
			try {
				fhDecompress.LoadFile(preFilePath);
				fhDecompress.Decompress(postFilePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			FileHandler fhCompress = new FileHandler();
			if (!GetExtension(postFilePath).equals("zl")) {
				System.out.println("Can only compress to a .zl file format!");
				System.exit(0);
			}
			try {
				fhCompress.LoadFile(preFilePath);
				fhCompress.Compress(postFilePath);
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}

		System.out.println("Compression Complete");
	}
	
	private static String GetExtension(String location) {
		String extension = location.substring(location.lastIndexOf(".") + 1, location.length());
		return extension;
	}
}
