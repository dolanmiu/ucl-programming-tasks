
public class TreeNode {
	
	protected NodeEntry entry;
	protected TreeNode leftNode;
	protected TreeNode rightNode;
	protected TreeNode parent;
	
	public TreeNode(NodeEntry o) {
		this.entry = o;
		leftNode = null;
		rightNode = null;
	}
	
	public TreeNode(NodeEntry o, TreeNode leftNode, TreeNode rightNode, TreeNode parent) {
		entry = o;
		this.leftNode =	leftNode;
		this.rightNode = rightNode;
		this.parent = parent;
	}
	
	public void SetLeftNode(TreeNode newNode) {
		leftNode = newNode;
	}
	
	public TreeNode GetParentNode() {
		return parent;
	}
	
	public void SetParent(TreeNode node) {
		parent = node;
	}
	
	public void SetRightNode(TreeNode newNode) {
		rightNode = newNode;
	}
	
	public TreeNode GetLeftNode() {
		return leftNode;
	}
	
	public TreeNode GetRightNode() {
		return rightNode;
	}
}
