
public class NodeEntry {

	private int value;
	private int index;
	
	public NodeEntry(int value) {
		this.value = value;
	}
	
	public NodeEntry(int value, int index) {
		this.value = value;
		this.index = index;
	}
	
	public int GetValue() {
		return value;
	}
	
	public void SetIndex(int i) {
		index = i;
	}
	
	public int GetIndex() {
		return index;
	}
}
