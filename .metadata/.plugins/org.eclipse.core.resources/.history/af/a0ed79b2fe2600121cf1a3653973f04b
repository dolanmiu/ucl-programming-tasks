package JPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Core.Circuit;
import Core.Question;
import Core.Reader;
import ElectricalComponants.Bulb;
import ElectricalComponants.ElectricalComponent;
import ElectricalComponants.Module;

public class CircuitMaker extends JPanel {
	public ArrayList<Module> modules = getModulesFromQuestion(1);
	private int currentQuestionID;
	private int moduleNumber = 0;
	int moduleIndex = 0;
	int spacing = 5;

	public CircuitMaker() {
		final DefaultListModel<ElectricalComponent> info = new DefaultListModel<ElectricalComponent>();
		
		final JTextField resistanceField = new JTextField();
		resistanceField.setColumns(1);
		setLayout(new BorderLayout(0, 0));
		final CircuitDraw circuitSandbox = new CircuitDraw(0);
		add(circuitSandbox, BorderLayout.CENTER);
		circuitSandbox.setLayout(null);
		final JComboBox<Module> moduleComboBox = new JComboBox<Module>();
		
		JPanel side = new JPanel();
		add(side, BorderLayout.WEST);
		side.setLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
		side.setMinimumSize(new Dimension(200,600));
		
		JPanel topSide = new JPanel();
		side.add(topSide);
		topSide.setLayout(new BoxLayout(topSide, BoxLayout.Y_AXIS));
		
		JPanel sideQuestionText = new JPanel();
		topSide.add(sideQuestionText);
		sideQuestionText.setLayout(new BoxLayout(sideQuestionText, BoxLayout.X_AXIS));
		
		JPanel sideQuestion = new JPanel();
		JLabel lblQuestionText = new JLabel("Text");
		sideQuestion.add(lblQuestionText);
		final JTextArea textText= new JTextArea();
		textText.setWrapStyleWord(true);
		sideQuestion.add(textText);
		textText.setColumns(1);
		topSide.add(sideQuestion);
		sideQuestion.setLayout(new BoxLayout(sideQuestion, BoxLayout.X_AXIS));
		
		JLabel lblQuestion = new JLabel("Question");
		sideQuestionText.add(lblQuestion);
		final JComboBox<Question> questionComboBox = new JComboBox<Question>();
		populateComboBox(questionComboBox, moduleComboBox);
		
		final JList<ElectricalComponent> componentList = new JList<ElectricalComponent>(info);
		JScrollPane scrollPane = new JScrollPane(componentList);
		componentList.setVisibleRowCount(4);
		
		questionComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				modules = getModulesFromQuestion(questionComboBox.getSelectedIndex());
				currentQuestionID = questionComboBox.getSelectedIndex();
				moduleComboBox.removeAllItems();
				for (int i = 0; i < modules.size(); i++) {
					moduleComboBox.addItem(modules.get(i));
				}
				textText.setText(getQuestionText(questionComboBox.getSelectedIndex()));
				updateCircuit(circuitSandbox, questionComboBox.getSelectedIndex());
			}
		});
		sideQuestionText.add(questionComboBox);
		
		JPanel sideContainer = new JPanel();
		side.add(sideContainer);
		sideContainer.setLayout(new BoxLayout(sideContainer, BoxLayout.X_AXIS));
		JPanel sideText = new JPanel();
		sideContainer.add(sideText);
		sideText.setLayout(new BoxLayout(sideText, BoxLayout.Y_AXIS));
		sideText.add(new JLabel("Module"));
		sideText.add(Box.createVerticalStrut(5));
		sideText.add(new JLabel("Componant List"));
		sideText.add(Box.createVerticalStrut(5));
		sideText.add(new JLabel("Resistance"));
		moduleComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				info.clear();
				ArrayList<ElectricalComponent> components = getComponentsFromModule(moduleComboBox.getSelectedIndex(),currentQuestionID);
				if (components != null) {
					for (int i = 0; i < components.size(); i++) {
						info.addElement(components.get(i));
					}
				}
			}
		});
		moduleComboBox.setModel(new DefaultComboBoxModel<Module>());
		
		JButton btnAddModule = new JButton("Add Module");
		btnAddModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				moduleNumber++;
				Module module = new Module(moduleNumber);
				modules.add(module);
				moduleComboBox.addItem(module);
				moduleComboBox.setSelectedItem(module);
			}
		});
		String[] componantTypeComboBoxItems = { "Bulb", "Resistor", "Cell" };
		final JComboBox componantType = new JComboBox(componantTypeComboBoxItems);
		JButton btnAddComponant = new JButton("Add Componant");
		btnAddComponant.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (componantType.getSelectedItem() == "Bulb") {
					if (resistanceField.getText().equals("")) {
						JOptionPane.showMessageDialog(null,"You need to input a resistance", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						Bulb bulb = new Bulb(Integer.parseInt(resistanceField.getText()), 100, 200);
						info.addElement(bulb);
					}
				}
			}
		});
		
		JButton btnRemoveModule = new JButton("Remove Module");
		btnRemoveModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				modules.remove(moduleComboBox.getSelectedItem());
			}
		});
		
		
		JPanel sideComponents = new JPanel();
		sideContainer.add(sideComponents);
		sideComponents.setLayout(new BoxLayout(sideComponents, BoxLayout.Y_AXIS));
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(moduleComboBox);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(scrollPane);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(resistanceField);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(componantType);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnAddComponant);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnAddModule);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnRemoveModule);

	}
	
	private void updateCircuit(CircuitDraw circuitSandbox, int question) {
		circuitSandbox.updateCircuit(question);
	}

	private ArrayList<Module> getModulesFromQuestion(int n) {
		return Reader.questionList.get(n).circuit.getModules();
	}
	
	private String getQuestionText(int n) {
		 return Reader.questionList.get(n).getText();
	}
	
	private void populateComboBox(JComboBox<Question> questionCombobox, JComboBox<Module> moduleComboBox) { //Bug Report
		for (int i = 0; i < Reader.questionList.size(); i++) {
			questionCombobox.addItem(Reader.questionList.get(i));
		}
		for (int i = 0; i < Reader.questionList.get(0).circuit.getModules().size(); i++) {
			moduleComboBox.addItem(Reader.questionList.get(0).circuit.getModules().get(i));
		}
	}
	
	private ArrayList<ElectricalComponent> getComponentsFromModule(int n, int currentQID) {
		if (n >= 0) {
			Question question = Reader.questionList.get(currentQID);
			Module module = question.circuit.getModules().get(n);
			ArrayList<ElectricalComponent> components = module.getComponents();
			return components;
		}
		return null;
	}
}
