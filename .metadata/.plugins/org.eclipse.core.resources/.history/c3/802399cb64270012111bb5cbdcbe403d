package JPanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Core.InitComponent;
import Core.Question;
import Core.Reader;
import ElectricalComponants.Bulb;
import ElectricalComponants.ElectricalComponent;
import ElectricalComponants.Module;

@SuppressWarnings("serial")
public class CircuitMaker extends JPanel {
	public ArrayList<Module> modules = getModulesFromQuestion(1);
	private int currentQuestionID;
	int moduleIndex = 0;
	int spacing = 5;
	Question currentQuestion;

	public CircuitMaker() {
		String[] componantTypeComboBoxItems = { "Bulb", "Resistor", "Cell" };
		final DefaultListModel<ElectricalComponent> info = new DefaultListModel<ElectricalComponent>();
		final JComboBox componantType = new JComboBox(componantTypeComboBoxItems);
		final CircuitDraw circuitSandbox = new CircuitDraw(0);
		final JTextField resistanceField = new JTextField();
		final JComboBox<Module> moduleComboBox = new JComboBox<Module>();
		final JTextArea textText= new JTextArea();
		final JComboBox<Question> questionComboBox = new JComboBox<Question>();
		final JList<ElectricalComponent> componentList = new JList<ElectricalComponent>(info);
		
		resistanceField.setColumns(1);
		setLayout(new BorderLayout(0, 0));

		add(circuitSandbox, BorderLayout.CENTER);
		circuitSandbox.setLayout(null);

		JPanel side = new JPanel();
		JPanel topSide = new JPanel();
		JPanel sideQuestionText = new JPanel();
		JPanel sideQuestion = new JPanel();
		JPanel sideContainer = new JPanel();
		JPanel sideText = new JPanel();
		JPanel sideComponents = new JPanel();
		
		add(side, BorderLayout.WEST);
		side.setLayout(new BoxLayout(side, BoxLayout.Y_AXIS));
		side.setMinimumSize(new Dimension(200,600));
		
		side.add(topSide);
		topSide.setLayout(new BoxLayout(topSide, BoxLayout.Y_AXIS));
		
		topSide.add(sideQuestionText);
		sideQuestionText.setLayout(new BoxLayout(sideQuestionText, BoxLayout.X_AXIS));
		
		JLabel lblQuestionText = new JLabel("Text");
		sideQuestion.add(lblQuestionText);
		textText.setWrapStyleWord(true);
		sideQuestion.add(textText);
		textText.setColumns(1);
		topSide.add(sideQuestion);
		sideQuestion.setLayout(new BoxLayout(sideQuestion, BoxLayout.X_AXIS));
		
		JLabel lblQuestion = new JLabel("Question");
		sideQuestionText.add(lblQuestion);
		populateComboBox(questionComboBox, moduleComboBox);
		
		JScrollPane scrollPane = new JScrollPane(componentList);
		componentList.setVisibleRowCount(4);
		
		questionComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				modules = getModulesFromQuestion(questionComboBox.getSelectedIndex());
				currentQuestionID = questionComboBox.getSelectedIndex();
				currentQuestion = (Question) questionComboBox.getSelectedItem();
				System.out.println(currentQuestion);
				moduleComboBox.removeAllItems();
				for (int i = 0; i < modules.size(); i++) {
					moduleComboBox.addItem(modules.get(i));
				}
				textText.setText(getQuestionText(questionComboBox.getSelectedIndex()));
				updateCircuit(circuitSandbox, questionComboBox.getSelectedIndex());
			}
		});
		sideQuestionText.add(questionComboBox);

		moduleComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				info.clear();
				ArrayList<ElectricalComponent> components = getComponentsFromModule(moduleComboBox.getSelectedIndex(),currentQuestionID);
				if (components != null) {
					for (int i = 0; i < components.size(); i++) {
						info.addElement(components.get(i));
					}
				}
			}
		});
		moduleComboBox.setModel(new DefaultComboBoxModel<Module>());
		
		JButton btnAddModule = new JButton("Add Module");
		btnAddModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Module module = new Module(moduleComboBox.getItemCount());
				//modules.add(module);
				currentQuestion.circuit.addModule(module);
				moduleComboBox.addItem(module);
				moduleComboBox.setSelectedItem(module);
			}
		});
		JButton btnAddComponant = new JButton("Add Componant");
		btnAddComponant.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (componantType.getSelectedItem() == "Bulb") {
					if (resistanceField.getText().equals("")) {
						JOptionPane.showMessageDialog(null,"You need to input a resistance", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						String name = componantType.getSelectedItem().toString();
						float resistance = Integer.parseInt(resistanceField.getText());
						Bulb comp = (Bulb) InitComponent.create(name,resistance,0,0,100,200);
						//Bulb bulb = new Bulb(Integer.parseInt(resistanceField.getText()), 100, 200);
						info.addElement(comp);
					}
				}
			}
		});
		
		JButton btnRemoveModule = new JButton("Remove Module");
		btnRemoveModule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				modules.remove(moduleComboBox.getSelectedItem());
			}
		});
		
		side.add(sideContainer);
		sideContainer.setLayout(new BoxLayout(sideContainer, BoxLayout.X_AXIS));
		sideContainer.add(sideText);
		sideText.setLayout(new BoxLayout(sideText, BoxLayout.Y_AXIS));
		sideText.add(new JLabel("Module"));
		sideText.add(Box.createVerticalStrut(5));
		sideText.add(new JLabel("Componant List"));
		sideText.add(Box.createVerticalStrut(5));
		sideText.add(new JLabel("Resistance"));
		sideContainer.add(sideComponents);
		sideComponents.setLayout(new BoxLayout(sideComponents, BoxLayout.Y_AXIS));
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(moduleComboBox);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(scrollPane);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(resistanceField);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(componantType);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnAddComponant);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnAddModule);
		sideComponents.add(Box.createVerticalStrut(spacing));
		sideComponents.add(btnRemoveModule);

	}
	
	private void updateCircuit(CircuitDraw circuitSandbox, int question) {
		circuitSandbox.updateCircuit(question);
	}

	private ArrayList<Module> getModulesFromQuestion(int n) {
		return Reader.questionList.get(n).circuit.getModules();
	}
	
	private String getQuestionText(int n) {
		 return Reader.questionList.get(n).getText();
	}
	
	private void populateComboBox(JComboBox<Question> questionCombobox, JComboBox<Module> moduleComboBox) { //Bug Report
		for (int i = 0; i < Reader.questionList.size(); i++) {
			questionCombobox.addItem(Reader.questionList.get(i));
		}
		for (int i = 0; i < Reader.questionList.get(0).circuit.getModules().size(); i++) {
			moduleComboBox.addItem(Reader.questionList.get(0).circuit.getModules().get(i));
		}
	}
	
	private ArrayList<ElectricalComponent> getComponentsFromModule(int n, int currentQID) {
		if (n >= 0) {
			System.out.println("n: " + n);
			Question question = Reader.questionList.get(currentQID);
			Module module = question.circuit.getModules().get(n);
			ArrayList<ElectricalComponent> components = module.getComponents();
			return components;
		}
		return null;
	}
}
