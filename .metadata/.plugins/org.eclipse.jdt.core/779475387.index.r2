 INDEX VERSION 1.126  >� src/Core/Circuit.java	 InitComponen	 	MainClass rker	 Question	 Reader	 	StopWatch ElectricalComponents/Bulb Cell 
ircuitCalc DraggableComponent 
Electrical Module Resistor Void Wire JPanels/CardPanel ircuitDragTest w Maker DifficultySelect 	QuizResul StudentQuiz WelcomeWindow Main   T getModulesFromQuestion/1���� getQuestionText/1���� getQuestions/1    printModuleAmmount/0     reset/0���� getTimeLeft/0���� 	getText/0���� setBorderDeactive/0���� calcResistance/1���� runApp/0���� itemStateChanged/1���� calcParallelResistance/1���� getVoltage/0���� getTotalResistance/0���� getComponant/1���� getAnswers/0���� subtractScore/1���� removeAnswer/1���� mouseDragged/1���� paintComponent/1    addComponant/1���� refreshBounds/0���� enable/0���� calcVoltage/2���� getQuestionNumber/0���� printConnection/0���� populateComboBox/2���� calcCurrent/1���� populatePanels/0���� handleComponents/3���� stop/0���� getMaxTime/0���� nextQuestion/1���� mouseReleased/1   
 updateCircuit/1���� componentShown/1���� removeModule/1     mouseMoved/1���� calcVoltage/1���� getMessage/0���� getTotalVoltage/0���� drawCircuit/1���� checkCollisions/0���� start/0���� mouseEntered/1���� getBounds/0���� setResistance/1���� 
getGrade/0���� mouseClicked/1   
 updateCircuit/2���� setMagnetDrag/1���� create/6���� handleModules/2���� getResistance/0���� 
getScore/0���� getElapsedTime/0���� 
scanFile/2���� printComponents/0���� checkAnsCollision/1    getTagValue/2���� actionPerformed/1    getComponantAmmount/0���� handleQuestions/3���� mouseExited/1���� setBorderActive/0���� paint/1���� printScore/0���� run/0���� getComponentsFromModule/2���� mousePressed/1   
 getModules/0     returnComponents/0���� drawNextCircuit/0���� updateComponentList/2���� 
toString/0    isCorrect/0���� 
addScore/1���� createWires/1���� main/1���� addModule/1     getAnswers/1���� getCurrent/0���� getCircuit/0���� addAnswer/1����    SuppressWarnings    Override   
  < btnRemoveModule���� String    res���� sideText���� Float   	 
currentQID���� ItemListener���� 
moduleNode���� maxScore���� dom���� JOptionPane    util    	 	ArrayList    	 componantType���� Timer    NORTH���� deltaY���� border   
 net���� out   	  	JComboBox���� CENTER    e   
 g    i    	 btnAddModule���� j���� n���� x    enabled���� y    
JSeparator���� DocumentBuilder���� diff    fFile���� totalResistance   	 watch���� circuitDragTest���� 	separator���� YELLOW���� 
isCollided    circuit    separator_1���� lblMotivationalMessage���� MouseAdapter    ArrayList<ElectricalComponent>    
miginfocom���� int    	
 err    collided���� sideComponents���� sideContainer���� c1    Reader    circuitPanel���� DocumentBuilderFactory���� swing   
 	ImageIcon���� InitComponent    highestY���� filename���� password���� font���� %DefaultListModel<ElectricalComponent>���� SwingConstants���� action���� timeLeft    BorderLayout    topLeft���� myX���� Resistor���� circNode���� topPanel���� 
scoretoAdd���� moduleNumber���� Scanner���� JScrollPane���� 	Dimension    Core    
scrollPane���� 	Component���� score���� char    BLUE���� JPanels���� OK_CANCEL_OPTION���� myY���� 	lastDrawn���� WHITE   
 grade���� JComboBox<Module>���� _void���� boolean   
 
difficulty    btnAddComponant���� questionList    PLAIN���� Toolkit���� 	ItemEvent���� selectedQuestions���� 
btnStudent���� awt   
 Image    SOUTH���� drawable���� ElectricalComponents     questionCombobox���� 	cardPanel    	questions    JToolBar���� bulb���� moduleComboBox���� nlList���� timeChecker���� sideQuestionText���� DefaultListModel���� w3c���� 	btnMedium���� connectedTo���� JPasswordField���� questionText���� 
createBulb���� 	Rectangle    Wire[]     width���� ERROR_MESSAGE���� parsers���� float   	 welcome���� resistanceField���� Integer    GREEN���� ElectricalComponent   	 Question    NodeList���� DraggableComponent���� 
MainWindow���� X_AXIS    DEFAULT_SIZE���� JList���� GroupLayout���� Object   	 	 timer    
diffSelect���� in���� io���� 
answerList���� 	BoxLayout    
CardLayout    ComponentPlacement���� StudentQuiz���� JPanel   	 Font���� CircuitCalc    contentPane���� finish���� DifficultySelect���� answerBounds���� Math    groupLayout���� ammount���� componantTypeComboBoxItems���� componentList���� spacing���� 	MAX_VALUE    btnEasy���� lblYouHaveScored���� System   
  	Exception    nValue���� ArrayList<Module>    	 moduleNodes���� lblCongrats���� lang   	 	 currentQuestion    totalConductance���� mc���� topSide���� long   
 CircuitMaker���� elapsed���� Node���� pwd���� Y_AXIS    answerNodes���� Element���� 
magnetDrag���� lblQuestion���� modules    	 BLACK    Bulb    RELATED���� name    circuitSandbox    dBuilder���� 
resistance   		 JButton    javax   
 info���� resistor���� MouseMotionListener���� ComponentAdapter���� EXIT_ON_CLOSE���� percentageTimeLeft���� questionLabel���� File���� Graphics    connectedFrom���� lblQuestionText���� lblGrade���� 
EventQueue���� magnet���� JLabel    WelcomeWindow���� event   
 height���� currentVoltage���� 
JComponent   
 module    	 numb    	StopWatch���� org���� answer���� Document���� MouseListener���� comp   	 lowestX���� 
totalScore���� void    
 highestX���� ActionListener    
QuizResult    WIDTH���� xml���� String[]���� 	componant���� 	MainClass���� cell   	 questionComboBox���� textText���� LayoutStyle���� ELEMENT_NODE���� current    doc���� motivationalMessage���� Wire     
timerLabel���� ActionEvent    number���� screenX���� quiz���� 
scoreToAdd���� bounds    eElement���� Short���� time���� 
LineBorder   
 
btnTeacher���� 	Alignment���� img    JFrame���� sTag���� btnHard���� question    DefaultComboBoxModel���� ArrayList<Question>    circuitMaker���� TRAILING���� wires���� correctVoltage���� scoreToSubtract���� correctResistance���� currentResistance���� element���� lowestY���� btnNextQuestion���� deltaX���� java    	
 sideQuestion���� text    circuitNodes���� scanner���� screenY���� 	JTextArea���� NullPointerException���� ComponentEvent���� comps���� reader���� CircuitDragTest���� JComboBox<Question>���� Runnable���� Marker    voltage    Box���� 	dbFactory���� Circuit    frame���� FileNotFoundException���� currentQuestionID���� Void���� side    maxTime    toolBar���� Cell   	 LEADING���� 	CardPanel    	MigLayout���� 
components    WEST    Color   
 
JTextField���� Module    	 answers    CircuitDraw    
MouseEvent   
 EmptyBorder����   3 serialVersionUID   
 
resistance���� 
totalScore���� circuitPanel���� maxScore���� questionText���� screenX���� myX���� connectedFrom���� 
magnetDrag���� wires     img    timer���� motivationalMessage���� correctVoltage���� modules     moduleNumber���� moduleIndex���� enabled���� spacing���� questionLabel���� myY���� currentQuestion���� correctResistance���� questionList���� timeLeft���� fFile���� borderColour���� contentPane���� screenY���� answers���� currentQuestionID���� connectedTo���� voltage���� 
components���� 
isCollided���� maxTime���� circuit���� text���� 	questions    	lastDrawn���� numb���� bounds���� 	cardPanel���� x���� y���� diff���� c1���� watch���� current���� 
timerLabel����   ; Wire/2���� ArrayList/0     WelcomeWindow/1���� Void/3���� JTextField/0���� 
JToolBar/0���� File/1���� CircuitMaker/0���� BorderLayout/2    	JButton/1    JComboBox/1���� MouseMotionListener/0���� 
Runnable/0���� JList/1���� JSeparator/0���� 	Scanner/1���� Bulb/3    JPanel/0   	 StudentQuiz/2���� StopWatch/1���� DraggableComponent/0���� CircuitDraw/1    Reader/1���� EmptyBorder/4���� DefaultListModel/0���� JScrollPane/1���� JPasswordField/1���� ElectricalComponent/0    CardPanel/0���� Module/1    String/1���� 	Circuit/0���� DefaultComboBoxModel/0���� JLabel/0���� LineBorder/2   
 GroupLayout/1���� JComboBox/0���� DifficultySelect/1���� 
Resistor/3���� JTextArea/0���� CircuitDragTest/0���� BoxLayout/2    MainWindow/0���� MouseListener/0���� JLabel/1    JComponent/0   
 Timer/2    CardLayout/0���� Dimension/2    MouseAdapter/0    ComponentAdapter/0���� Font/3���� ItemListener/0���� 
Question/5���� JFrame/0���� Rectangle/4    QuizResult/0���� Cell/4���� Object/0   	 	   % +MouseListener///0//ElectricalComponents/IC ���� 1MouseMotionListener///0//ElectricalComponents/IC ���� Object//MainClass///Core/CC���� Object//Circuit///Core/CC     !ComponentAdapter///0//JPanels/IC ���� .Object//CircuitCalc///ElectricalComponents/CC���� 'Object//Wire///ElectricalComponents/CC���� JFrame//MainWindow////CC���� Runnable///0///IC ����  Object//InitComponent///Core/CC���� JPanel//CardPanel///JPanels/CC���� "JPanel//CircuitMaker///JPanels/CC���� %JPanel//CircuitDragTest///JPanels/CC���� ItemListener///0//JPanels/IC ���� Object///0//JPanels/CC      JPanel//QuizResult///JPanels/CC���� BDraggableComponent//ElectricalComponent///ElectricalComponents/CC���� $ActionListener//StopWatch///Core/IC���� !JPanel//CircuitDraw///JPanels/CC���� &JPanel//DifficultySelect///JPanels/CC���� #JPanel//WelcomeWindow///JPanels/CC���� !JPanel//StudentQuiz///JPanels/CC���� Object//Reader///Core/CC���� $Object///0//ElectricalComponents/CC ���� )ActionListener//CircuitDraw///JPanels/IC���� )ActionListener//StudentQuiz///JPanels/IC���� Object//Question///Core/CC���� 4ElectricalComponent//Cell///ElectricalComponents/CC���� 4ElectricalComponent//Bulb///ElectricalComponents/CC���� 8ElectricalComponent//Resistor///ElectricalComponents/CC���� 4ElectricalComponent//Void///ElectricalComponents/CC���� MouseAdapter///0//JPanels/IC     9JComponent//DraggableComponent///ElectricalComponents/CC���� -JComponent//Module///ElectricalComponents/CC���� Object//Marker///Core/CC���� Object//StopWatch///Core/CC���� Object///0///CC ����    CardPanel/JPanels// ���� Circuit/Core//      StopWatch/Core// ���� Wire/ElectricalComponents// ���� Marker/Core// ���� CircuitMaker/JPanels// ���� CircuitDraw/JPanels// ���� QuizResult/JPanels// ���� ,ElectricalComponent/ElectricalComponents// ���� Void/ElectricalComponents// ���� +DraggableComponent/ElectricalComponents// ���� WelcomeWindow/JPanels// ���� Reader/Core// ���� Module/ElectricalComponents// ���� StudentQuiz/JPanels// ���� !Resistor/ElectricalComponents// ���� MainClass/Core// ���� $CircuitCalc/ElectricalComponents// ���� Cell/ElectricalComponents// ���� DifficultySelect/JPanels// ���� InitComponent/Core// ���� /ElectricalComponents/0/  ���� MainWindow/// ���� Question/Core// ���� Bulb/ElectricalComponents// ���� /JPanels/0/      CircuitDragTest/JPanels// ���� //0/  ����   � printModuleAmmount/0���� removeAll/0���� createSequentialGroup/0���� getElementsByTagName/1���� getVoltage/0���� get/1   	 addMouseListener/1   
 calcResistance/1    getDefaultToolkit/0���� addComponentListener/1���� isCorrect/0���� addMouseMotionListener/1���� addComponent/1���� invokeLater/1���� setLayout/1   	 setBorder/1   
 getQuestionText/1���� stop/0���� setColumns/1���� add/2    populatePanels/0���� printScore/0���� normalize/0���� setMaximumSize/1���� checkAnsCollision/1���� handleModules/2���� start/0    value/0    handleComponents/3���� addItemListener/1���� setContentPane/1���� getTagValue/2���� getAttribute/1���� getSelectedItem/0���� setMinimumSize/1    newInstance/0���� setHorizontalGroup/1���� getAnswers/1���� getLength/0���� showConfirmDialog/4���� size/0    	 removeAllItems/0���� isInfinite/1���� newDocumentBuilder/0���� toLowerCase/0���� setOpaque/1���� close/0���� runApp/0���� createParallelGroup/1���� addComponant/1    
setTitle/1���� addElement/1���� createVerticalStrut/1���� getNodeValue/0���� getResistance/0���� getQuestionNumber/0���� getY/0   
 getXOnScreen/0���� 	addItem/1���� item/1���� getMessage/0���� add/1   
  getTotalResistance/0    calcVoltage/1���� setLocation/2   
 getComponant/1   	 setResistance/1���� setVisible/1���� updateComponentList/2���� getYOnScreen/0���� remove/1     getX/0   
 updateCircuit/2���� getSelectedIndex/0���� 
setModel/1���� 
getScore/0���� round/1    populateComboBox/2���� getBounds/0���� create/6    printStackTrace/0    handleQuestions/3���� createWires/1���� getQuestions/1���� 
drawRect/4���� reset/0���� getItemCount/0���� 
parseInt/1    addPreferredGap/1���� drawNextCircuit/0���� drawCircuit/1    
scanFile/2���� 	println/1   
  	setFont/1���� drawImage/4    getComponantAmmount/0   	 intersects/1���� parseFloat/1���� getNodeType/0���� 
toString/0    	getText/0���� setVisibleRowCount/1���� 	setText/1    getModules/0    getTotalVoltage/0���� show/2    addContainerGap/2���� setSelectedItem/1���� getMaxTime/0���� getChildNodes/0���� setBorderDeactive/0���� showMessageDialog/4���� 
updateUI/0    checkCollisions/0���� equals/1    addContainerGap/0���� setBorderActive/0���� getModulesFromQuestion/1���� showMessageDialog/2    getComponentsFromModule/2���� getDocumentElement/0���� clear/0���� getTimeLeft/0���� 
addGroup/1���� setWrapStyleWord/1���� setMagnetDrag/1���� returnComponents/0    addGap/1���� getCurrent/0���� setBackground/1���� 
getGrade/0���� setBounds/4   
 getAnswers/0���� addModule/1    updateCircuit/1    refreshBounds/0���� getPassword/0���� 
addScore/1���� createImage/1���� parse/1���� setDefaultCloseOperation/1���� nextQuestion/1���� setVerticalGroup/1���� calcParallelResistance/1����    /DraggableComponent/0/ /ElectricalComponents/ ���� 4DifficultySelect/1/ /JPanels/CardPanel/cardPanel/ ���� /CircuitDraw/1/ /JPanels/ArrayList/questions/ ���� AStudentQuiz/2/ /JPanels/CardPanel,String/cardPanel,difficulty/ ���� /#/ က/JPanels    /#/ က/ElectricalComponents���� StopWatch/1/ /Core/int/time/ ���� CircuitDragTest/0/ /JPanels/ ���� CircuitMaker/0/ /JPanels/ ���� CardPanel/0/ /JPanels/ ���� #Reader/1/ /Core/String/filename/ ���� >Bulb/3/ /ElectricalComponents/float,int,int/resistance,x,y/ ���� QuizResult/0/ /JPanels/ ���� InitComponent/#/ /Core���� XQuestion/5/ /Core/Circuit,String,int,String,ArrayList/circuit,diff,numb,text,answers/ ���� MainWindow/0/ // ���� 4Module/1/ /ElectricalComponents/int/moduleNumber/ ���� MainClass/#/ /Core���� Circuit/0/ /Core/      ICell/4/ /ElectricalComponents/float,float,int,int/voltage,current,x,y/ ���� BResistor/3/ /ElectricalComponents/float,int,int/resistance,x,y/ ���� %CircuitCalc/#/ /ElectricalComponents���� /#/ က/���� 1WelcomeWindow/1/ /JPanels/CardPanel/cardPanel/ ���� Marker/#/ /Core���� 0ElectricalComponent/0/ /ElectricalComponents/ ���� >Void/3/ /ElectricalComponents/float,int,int/resistance,x,y/ ���� IWire/2/ /ElectricalComponents/Module,Module/connectedFrom,connectedTo/ ����   /     t   	 
methodDecl  s annotationRef  # ref  ] 	fieldDecl  � constructorRef  � superRef  $% typeDecl  *U 	methodRef  -� constructorDecl  9^