import java.util.Random;
import java.util.Arrays;

public class GenerateLottery {
	public static void main(String[] args) {
		System.out.println(Arrays.toString(generate()));
		
	}
	
	public static int[] generate() {
		Random aRandom = new Random();
		int[] lottery = new int[6];
		for (int i = 0; i < 6; i++) {
			lottery[i] = aRandom.nextInt(49)+1;
		}
		return lottery;
	}

}
