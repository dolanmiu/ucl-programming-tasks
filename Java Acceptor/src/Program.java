import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Program {
	static int lineNo = 0;
	static HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
	static int currentRegister = 0;
	static ArrayList<String> lines = new ArrayList<String>();
	static boolean running = true;
	
	public static void main(String args[]) {
		try {
			readFile("acceptorText.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void readFile(String file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            lines.add(line);
	            line = br.readLine();
	        }
	        String everything = sb.toString();
	        
	    } finally {
	        br.close();
	        while(running) {
	        	gateKeeperCheck("(\\w{1,4})\\s*?\\(\\s*?((\\$?(\\d+)\\s*?[,]?\\s*?)+)\\)", lines.get(currentRegister));
	        }
	    }
	}
	
	public static boolean gateKeeperCheck(String regex, String TotalsearchString) {
		boolean work = regexChecker(regex, TotalsearchString);
		if (!work) {
			System.out.println("There is a Syntax Error at line " + lineNo);
		}
		return work;

	}
	
	public static boolean regexChecker(String regex, String searchString) {
		Pattern regexCheck = Pattern.compile(regex);
		Matcher regexMatcher = regexCheck.matcher(searchString);
		while (regexMatcher.find()) {
			lineNo++;
			if (regexMatcher.group().length() != 0) {
				//System.out.println(regexMatcher.group().trim());
				int argAmmount = checkOperator(regexMatcher.group(1), regexMatcher.group(2));
				if (argAmmount == -1) {
					return false;
				}
				boolean isSyntaxCorrect = checkValues(regexMatcher.group(2),argAmmount);
				if (!isSyntaxCorrect) {
					return false;
				}
				processLine(regexMatcher.group(1),regexMatcher.group(2));
			}
		}
		return true;
	}
	
	public static boolean semanticCheck(char type1, char type2, String value1, String value2) {
		Pattern regexCheck = Pattern.compile("\\$\\d+"); //if register, it will produce value
		Matcher valueMatch = regexCheck.matcher(value1);
		//System.out.println("yiis" + valueMatch.group(0));
		/*if (type1 == 'r') {
			valueMatch = regexCheck.matcher(value1);
			if (valueMatch.group().length() != 0) {
				System.out.println("Incorrect Type of arguments. Check order of register and values.");
				return false;
			}
		}*/
		if (type1 == 'n') {
			valueMatch = regexCheck.matcher(value1);
			if (valueMatch.group().length() != 0) {
				System.out.println("Incorrect Type of arguments. Check order of register and values.");
				return false;
			}
		}
		if (type2 == 'r') {
			valueMatch = regexCheck.matcher(value2);
			if (valueMatch.group().length() == 0) {
				System.out.println("Incorrect Type of arguments. Check order of register and values.");
				return false;
			}
		}
		if (type2 == 'n') {
			valueMatch = regexCheck.matcher(value2);
			if (valueMatch.group().length() != 0) {
				System.out.println("Incorrect Type of arguments. Check order of register and values.");
				return false;
			}
		}
		return true;
		
	}
	
	public static int checkOperator(String operator, String values) {
		String[] string = values.split("\\s*,\\s*");
		operator = operator.toUpperCase();
		if (operator.equals("SET")) {
			//semanticCheck('r','n',string[0], string[1]);
			return 2;
		}
		if (operator.equals("CPY")) {
			return 2;
		}
		if (operator.equals("INC")) {
			return 1;
		}
		if (operator.equals("DEC")) {
			return 1;
		}
		if (operator.equals("JZ")) {
			return 2;
		}
		if (operator.equals("J")) {
			return 1;
		}
		if (operator.equals("CLR")) {
			return 1;
		}
		if (operator.equals("STOP")) {
			return 0;
		}
		System.out.println("Sorry, command " + operator + " not recongised.");
		return -1;
	}
	
	public static boolean checkValues(String values, int argAmmount) {
		String[] string = values.split("\\s+,\\s*");
		if (argAmmount != string.length) {
			System.out.println("Incorrect number of arguments");
			running = false;
			return false;
		}
		return true;
	}
	
	public static boolean processLine(String operator, String values) {
		String[] string = values.split("\\s*,\\s*");
		System.out.println("current Register: " + (currentRegister+1) + " " + operator);
		for (int i = 0; i < string.length; i++) {
			string[i] = string[i].trim();
		}
		if (operator.equals("SET")) {
			hashMap.put(registerToNumb(string[0]), Integer.parseInt(string[1]));
		}
		if (operator.equals("CPY")) {
			int fromValue = hashMap.get(registerToNumb(string[0]));
			hashMap.put(registerToNumb(string[1]), fromValue);
			System.out.println(fromValue);
		}
		if (operator.equals("INC")) {
			int tempVal = hashMap.get(registerToNumb(string[0]));
			tempVal++;
			hashMap.put(registerToNumb(string[0]), tempVal);
		}
		if (operator.equals("DEC")) {
			int tempVal = hashMap.get(registerToNumb(string[0]));
			tempVal--;
			hashMap.put(registerToNumb(string[0]), tempVal);
		}
		if (operator.equals("JZ")) {
			if (hashMap.get(registerToNumb(string[0])) == 0) {
				currentRegister = Integer.parseInt(string[1])-2;
				System.out.println(hashMap.get(registerToNumb(string[0])));
			}
		}
		if (operator.equals("J")) {
			currentRegister = Integer.parseInt(string[0])-2;
		}
		if (operator.equals("CLR")) {
			hashMap.put(registerToNumb(string[0]), 0);
		}
		if (operator.equals("STOP")) {
			running = false;
		}
		currentRegister++;
		for (int i = 0; i < string.length; i++) {
			//System.out.print(string[i] + " ");
		}
		System.out.println(hashMap.get(0));
		return true;
	}
	
	public static int registerToNumb(String s) {
		String string = s.substring(0,0)+s.substring(0+1);
		int value = Integer.parseInt(string);
		return value;
	}
}
