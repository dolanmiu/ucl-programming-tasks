import java.util.Scanner;

//This program uses Object called Currency. Be warned.
//It basically creates a currency Object, and manipulates that object around.
public class CurrencyConversion {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the currency and the unit (With no spaces): ");
		String input = scanner.nextLine();
		String[] result = convert(input);
		System.out.println("Here are the converted currencies: ");
		for (int i = 0; i<result.length; i++) {
			System.out.println(result[i]);
		}
		
		
	}
	
	private static String[] convert(String input) {
		String[] data = input.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		Currency currencyFrom = null;
		try {
			Integer.parseInt(data[0]);
		}
		catch(NumberFormatException e) {
			currencyFrom = new Currency(data[0], Integer.parseInt(data[1]));
		}
		try {
			Integer.parseInt(data[1]);
		}
		catch(NumberFormatException e) {
			currencyFrom = new Currency(data[1], Integer.parseInt(data[0]));
		}
		return currencyFrom.getConversions();
		
	}
	
	
}
