//Is this what you wanted? It is a 1D array.
//I have made it easier to read.
//The Array has been split up into many lines.
//The [i,i] value of the printed output is 0.0 because the distance to itself is obviously 0.
import java.lang.Math;
import java.util.Arrays;

public class DistanceMap {
	public static void main(String[] args) {
		final double[] STATIONS = { 1, 2, 3, 4, 5}; //Distances
		//System.out.print(STATIONS.length*STATIONS.length);
		double[] distances = new double[STATIONS.length*STATIONS.length];
		for (int i = 0; i < STATIONS.length; i++) {
			for (int j = 0; j < STATIONS.length; j++) {
				distances[5*i+j] = Math.abs(STATIONS[i] - STATIONS[j]);
			}
		}
		for (int i = 0; i < distances.length; i++) {
			if (i % 5 == 0) {
				System.out.println();
			}
			System.out.print(distances[i] + " ");
		}
		System.out.println("\n" + Arrays.toString(distances));
	}
}
