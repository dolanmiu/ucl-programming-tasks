import java.util.Arrays;

public class NathanSort {
	public static void main(String [] args) {
		int[] studentScores = {11,13,3,1,4,5}; //only even number of students work
		int[] seatingPlan = new int[studentScores.length];
		
		studentScores = checkEvenOdd(studentScores, seatingPlan); //adds -1. it will ALWAYS make the studentScore even.
		//It will adjust the length of studentScores depending on if the student number is odd or even.
		if (studentScores.length % 2 != 0) { //divisible by 2
			seatingPlan = new int[studentScores.length]; //reason it doesn't work fully is because this array length doesn't change.
		}
		
		studentScores = bubbleSort(studentScores); //bubble sort algorithm - standard algorithm used to sort it from lowest to highest
		
		seatingPlan = nathansAlgorithm(studentScores, seatingPlan);	 //The main part
		
		seatingPlan = removeNegOne(studentScores, seatingPlan); 		//Removes the -1 if it exists
		
		System.out.println("Number of students: " + studentScores.length);
		System.out.println("Numerical Order: " + Arrays.toString(studentScores));
		System.out.println("Smart Dumb: " + Arrays.toString(seatingPlan));
	}
	
	public static int[] checkEvenOdd(int[] studentScores, int[] seatingPlan) {
		if (studentScores.length %2 != 0) {
			System.out.println("odd");
			int[] tempScore = new int[studentScores.length+1];
			for (int i = 0; i < studentScores.length; i++) {
				tempScore[i] = studentScores[i];
			}
			tempScore[studentScores.length] = -1;
			return tempScore;
		} else {
			return studentScores; //if its even, do nothing to it
		}
	}
	
	public static int[] bubbleSort(int[] studentScores) {
		for (int i = 0; i < studentScores.length; i++) {
			for (int j = 0; j < studentScores.length-1; j++) {
				if (studentScores[j] > studentScores[j+1]) {
					int temp = studentScores[j+1];
					studentScores[j+1] = studentScores[j];
					studentScores[j] = temp;
				}
			}
		}
		return studentScores;
	}
	
	public static int[] nathansAlgorithm(int[] studentScores, int[] seatingPlan) {
		int[] newSeatingPlan = new int[studentScores.length];
		for (int i = 0; i < studentScores.length; i++) {
			if (newSeatingPlan[i] == 0) {
				try {
					newSeatingPlan[i] = studentScores[studentScores.length-1-i]; //minus 1 cos it starts at 0
					newSeatingPlan[i+1] = studentScores[i];
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("Array out of bounds");
					break;
				}
			}
		}
		return newSeatingPlan;
	}
	
	public static int[] removeNegOne(int[] studentScores, int[] seatingPlan) {
		for(int i = 0; i < studentScores.length; i++) {
			
		}
		if (studentScores.length % 2 == 0) {
			System.out.println("even");
			int count = 0;
			int[] tempSeatingPlan = new int[studentScores.length-1];
			for(int i = 0; i < studentScores.length; i++) {
				if(seatingPlan[i] != -1) {
					tempSeatingPlan[count] = seatingPlan[i];
					count++;
				}
			}
			return tempSeatingPlan;
		}
		return seatingPlan;
	}
}