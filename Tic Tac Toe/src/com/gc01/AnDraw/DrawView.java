//Made by Dolan Miu
package com.gc01.AnDraw;


import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;



/**
 * DrawView.java 
 * @author Dolan Miu 12/10/2011
 * A first drawing view example
 */
public class DrawView extends View implements OnTouchListener
{
	private ArrayList<Cell> drawnCircles = new ArrayList<Cell>();
	private ArrayList<Cell> drawnCrosses = new ArrayList<Cell>();
	private Paint circlePaint = new Paint();
	private Paint gridPaint = new Paint();
	private Paint backgroundPaint = new Paint();
	private float sx = 0, sy = 0;
	private boolean isPlayer1 = true;
	private Grid grid;

	public DrawView(Context context)
	{
		super(context);
		setFocusable(true);
		setFocusableInTouchMode(true);
		backgroundPaint.setColor(Color.WHITE);
		gridPaint.setColor(Color.BLACK);
		circlePaint.setColor(Color.RED);
		circlePaint.setStyle(Style.FILL);
		this.setOnTouchListener(this);
		grid = new Grid(320, 480); //replace with this.getwidth.
		//new AlertDialog.Builder(context).setMessage("hello").show();
	}

	@Override
	public void onDraw(Canvas canvas)
	{
		canvas.drawRect(this.getLeft(), this.getTop(), this.getRight(), this.getBottom(), backgroundPaint);
		canvas.drawLine(this.getWidth()/3, 0, this.getWidth()/3, this.getHeight(), gridPaint);
		canvas.drawLine(this.getWidth()*(2f/3f), 0, this.getWidth()*(2f/3f), this.getHeight(), gridPaint);
		canvas.drawLine(0, this.getHeight()/3, this.getWidth(), this.getHeight()/3, gridPaint);
		canvas.drawLine(0, this.getHeight()*(2f/3f), this.getWidth(), this.getHeight()*(2f/3f), gridPaint);
		
		for (int i = 0; i < grid.getCells().length; i++) {
			for (int j = 0; j < grid.getCells()[0].length; j++) {
				grid.getCells()[i][j].setCanvas(canvas);
			}
		}

		for (int i = 0; i < drawnCrosses.size(); i++) {
			drawnCrosses.get(i).drawCross();
		}
		for (int i = 0; i < drawnCircles.size(); i++) {
			drawnCircles.get(i).drawCircle();
		}
		findWinner(grid.getCells());
	}

	public boolean onTouch(View v, MotionEvent event)
	{   
		//update the coordinates for the OnDraw method above, with wherever we touch
		if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
		} else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
			sx = event.getX();
			sy = event.getY();
			addShit((int)sx, (int)sy);
			//checkWinner();
			invalidate();
		}
		return true;
	}
	
	public void findWinner(Cell[][] board) {
		Cell winner = null;

		// check rows
		boolean foundWinner = false;
		for (int i = 0; i < 3 && !foundWinner; i++) {
			Cell spot1 = board[0][i];
			Cell spot2 = board[1][i];
			Cell spot3 = board[2][i];
			if ( (spot1.getState() == spot2.getState()) && (spot2.getState() == spot3.getState())) {
				winner = spot1;
				foundWinner = true;
			}
		}

		// Check columns
		if (!foundWinner) {
			for (int i = 0; i < 3 && !foundWinner; i++) {
				Cell spot1 = board[i][0];
				Cell spot2 = board[i][1];
				Cell spot3 = board[i][2];
				if ( (spot1.getState() == spot2.getState()) && (spot2.getState() == spot3.getState()) ) {
					winner = spot1;
					foundWinner = true;
				}
			}
		}

		// Check diagonals
		if (!foundWinner) {
			Cell spot1 = board[0][0];
			Cell spot2 = board[1][1];
			Cell spot3 = board[2][2];
			if ( (spot1.getState() == spot2.getState()) && (spot2.getState() == spot3.getState()) ) {
				winner = spot1;
				//foundWinner = true;
			} else {
				spot1 = board[0][2];
				spot2 = board[1][1];
				spot3 = board[2][0];
				if ( (spot1.getState() == spot2.getState()) && (spot2.getState() == spot3.getState()) ) {
					winner = spot1;
					//foundWinner = true;
				}
			}
		}
		if (foundWinner) {
			System.out.println("winner is " + winner.getState());
		}
		return;
	}
	
	private void checkWinner() {
		int sum = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Cell cell = grid.getCells()[i][j];
				//if (cell.getState()) {
				//	sum++;
				//}
				if (sum == 3) {
					System.out.println("winner");
					//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
				}
			}
			sum = 0;
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Cell cell = grid.getCells()[j][i];
				//if (cell.getState()) {
				//	sum++;
				//}
				if (sum == 3) {
					System.out.println("winner");
					//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
				}
			}
			sum = 0;
		}
	}
	
	private void addShit(int x, int y) {
		if (isPlayer1) {
			Cell cell = grid.getCell(x,y);
			drawnCrosses.add(cell);
			isPlayer1 = false;
			return;
		} else {
			Cell cell = grid.getCell(x,y);
			drawnCircles.add(cell);
			isPlayer1 = true;
			return;
		}
	}
}


