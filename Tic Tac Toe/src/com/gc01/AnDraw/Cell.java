package com.gc01.AnDraw;

import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Cell {
	private Canvas canvas;
	private float x, y;
	private Paint cellPaint = new Paint();
	private Paint penPaint = new Paint();
	private String id;
	private char state;
	
	public Cell(float x, float y, String id) {
		Random r = new Random();
		state = (char)(r.nextInt(26) + 'a');
		this.id = id;
		this.x = x;
		this.y = y;
		cellPaint.setColor(Color.BLACK);
		penPaint.setColor(Color.BLACK);
		penPaint.setStrokeWidth(10);
	}
	
	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}
	
	public void draw() {
		canvas.drawText("hello", x, y+10, cellPaint);
	}
	
	public void drawCross() {
		canvas.drawLine(x,y,x+100, y+100, penPaint);
		canvas.drawLine(x+100,y,x, y+100, penPaint);
		state = 'x';
	}
	
	public void drawCircle() {
		canvas.drawCircle(x+50, y+60, 50, penPaint);
		state = 'o';
	}
	
	public char getState() {
		return state;
	}
	
	@Override
	public String toString() {
		return id;
	}
}
