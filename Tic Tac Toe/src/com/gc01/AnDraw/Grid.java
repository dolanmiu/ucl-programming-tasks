package com.gc01.AnDraw;


public class Grid {
	//ArrayList<Cell> cells = new ArrayList<Cell>();
	private Cell[][] cells = new Cell[3][3];
	private int width;
	private int height;
	
	public Grid(int width, int height) {
		this.width = width;
		this.height = height;
		createCells();
		//gridPaint.setColor(Color.rgb(0,0,0));
		/*canvas.drawLine(width/3, 0, width/3, height, gridPaint);
		canvas.drawLine(width*(2f/3f), 0, width*(2f/3f), height, gridPaint);
		canvas.drawLine(0, height/3, width, height/3, gridPaint);
		canvas.drawLine(0, height*(2f/3f), width, height*(2f/3f), gridPaint);*/
	}
	
	private void createCells() {
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				Cell cell = new Cell(width*((float)i/(float)3),height*((float)j/(float)3), i + "" + j);
				//cells.add(cell);
				cells[i][j] = cell;
				//cell.draw();
				//cell.drawCircle();
			}
		}
	}
	
	public Cell[][] getCells() {
		return cells;
	}
	
	public Cell getCell(int x, int y) {
		return checkCell(x, y);
	}
	
	private Cell checkCell(float x, float y) {
		if (x < width/3) {
			if (y < height/3) {
				return cells[0][0];
				//return cells.get(0);
			}
			if (y > height/3 && y < height*(2f/3f)) {
				return cells[0][1];
				//return cells.get(3);
			}
			if (y > height*(2f/3f)) {
				return cells[0][2];
				//return cells.get(6);
			}
		}
		if (x > width/3 && x < width*(2f/3f)) {
			if (y < height/3) {
				return cells[1][0];
				//return cells.get(1);
			}
			if (y > height/3 && y < height*(2f/3f)) {
				return cells[1][1];
				//return cells.get(4);
			}
			if (y > height*(2f/3f)) {
				return cells[1][2];
				//return cells.get(7);
			}
		}
		if (x > width*(2f/3f)) {
			if (y < height/3) {
				return cells[2][0];
				//return cells.get(2);
			}
			if (y > height/3 && y < height*(2f/3f)) {
				return cells[2][1];
				//return cells.get(5);
			}
			if (y > height*(2f/3f)) {
				return cells[2][2];
				//return cells.get(8);
			}
		}
		System.err.println("No cell returned in checkCell");
		return null;
	}
}
